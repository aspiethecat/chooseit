package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.Optional;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

public abstract class NamedEntitySpecification<
		EntityType extends NamedEntity,
		ConfigurationType extends NamedEntityConfiguration<
				EntityType, ? extends NamedEntitySpecification<EntityType, ConfigurationType>>>
		extends NamedEntityConfiguration
{
	@XmlAttribute(name = "required")
	private boolean m_required = false;

	@Nullable
	@XmlTransient
	private ConfigurationType m_configuration = null;

	public final void bind(
			final @Nullable ConfigurationType configuration)
			throws IllegalStateException
	{
		if (Objects.nonNull(m_configuration))
			throw new IllegalStateException("Configuration already bound");

		m_configuration = configuration;
	}

	public abstract boolean isPreConfigured();

	@Contract(pure = true)
	public final boolean isRequired() {
		return m_required;
	}

	@Contract(pure = true)
	public final boolean hasConfiguration() {
		return Objects.nonNull(m_configuration);
	}

	@NotNull
	@Contract(pure = true)
	public final Optional<ConfigurationType> configuration() {
		return Optional.ofNullable(m_configuration);
	}
}
