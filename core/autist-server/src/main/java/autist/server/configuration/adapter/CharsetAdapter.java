package autist.server.configuration.adapter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;

import autist.configuration.ConfigurationAdapter;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class CharsetAdapter extends ConfigurationAdapter<String, Charset> {
	@Override
	@NotNull
	@Contract("_ -> new")
	public Charset unmarshal(final @NotNull String charset)
			throws UnsupportedCharsetException
	{
		return Charset.forName(charset);
	}
}
