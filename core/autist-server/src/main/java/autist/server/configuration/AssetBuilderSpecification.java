package autist.server.configuration;

import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import autist.server.application.AssetBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
public class AssetBuilderSpecification {
	@XmlAttribute(name = "class")
	@XmlJavaTypeAdapter(AssetBuilder.ClassAdapter.class)
	private Class<? extends AssetBuilder> m_builder = null;

	@Nullable
	public Class<? extends AssetBuilder> getBuilderClass() {
		return m_builder;
	}
}
