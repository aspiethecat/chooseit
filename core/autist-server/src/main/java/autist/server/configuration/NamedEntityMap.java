package autist.server.configuration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

public class NamedEntityMap<T extends NamedEntityConfiguration>
		implements Map<String, T>
{
	private final Map<String, T> entityMap;

	public NamedEntityMap() {
		this(Collections.emptyMap());
	}

	public NamedEntityMap(final Map<String, T> entityMap) {
		this.entityMap = Collections.unmodifiableMap(entityMap);
	}

	@Override
	public boolean isEmpty() {
		return entityMap.isEmpty();
	}

	@Override
	public int size() {
		return entityMap.size();
	}

	@Override
	public boolean containsKey(final Object key) {
		return entityMap.containsKey(key);
	}

	@Override
	public boolean containsValue(final Object value) {
		return entityMap.containsValue(value);
	}

	@Override
	public T get(final Object key) {
		return entityMap.get(key);
	}

	@Nullable
	@Override
	public T put(final String key, final T value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void putAll(final @NotNull Map<? extends String, ? extends T> m) {
		throw new UnsupportedOperationException();
	}

	@Override
	public T remove(final Object key) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException();
	}

	@NotNull
	@Override
	public Set<String> keySet() {
		return entityMap.keySet();
	}

	@NotNull
	@Override
	public Set<Entry<String, T>> entrySet() {
		return entityMap.entrySet();
	}

	@NotNull
	@Override
	public Collection<T> values() {
		return entityMap.values();
	}
}
