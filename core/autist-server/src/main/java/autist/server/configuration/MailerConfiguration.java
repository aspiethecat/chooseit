package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

import java.time.Duration;

import java.nio.charset.Charset;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import javax.mail.internet.InternetAddress;

/**
 * Mailer configuration.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@SuppressWarnings({"WeakerAccess", "unused"})
public final class MailerConfiguration {
	/**
	 * Default secure connection flag.
	 */
	public static final boolean DEFAULT_SECURE = false;

	/**
	 * Default SMTP server port number for insecure connections.
	 */
	public static final int DEFAULT_INSECURE_PORT = 587;

	/**
	 * Default SMTP server port number for secure connections.
	 */
	public static final int DEFAULT_SECURE_PORT = 465;

	/**
	 * Default SMTP server address.
	 *
	 * @see #getServerAddress()
	 * @see #DEFAULT_SECURE
	 * @see #DEFAULT_SECURE_PORT
	 * @see #DEFAULT_INSECURE_PORT
	 */
	public static final SocketAddress DEFAULT_SERVER_ADDRESS =
			new InetSocketAddress(
					InetAddress.getLoopbackAddress(),
					DEFAULT_SECURE ? DEFAULT_SECURE_PORT : DEFAULT_INSECURE_PORT);

	/**
	 * Default SMTP server connection timeout.
	 */
	public static final Duration DEFAULT_CONNECTION_TIMEOUT = Duration.ofSeconds(60);

	/**
	 * Default SMTP server read timeout.
	 */
	public static final Duration DEFAULT_READ_TIMEOUT = Duration.ofSeconds(60);

	/**
	 * Default SMTP server write timeout.
	 */
	public static final Duration DEFAULT_WRITE_TIMEOUT = Duration.ofSeconds(60);

	/**
	 * SMTP server address. By default set to #DEFAULT_ADDRESS.
	 *
	 * @see #getServerAddress()
	 * @see #DEFAULT_SERVER_ADDRESS
	 */
	@NotNull
	@XmlAttribute(name = "server")
	private SocketAddress m_serverAddress = DEFAULT_SERVER_ADDRESS;

	/**
	 * SMTP server connection timeout in milliseconds. By default set to #DEFAULT_CONNECTION_TIMEOUT.
	 * Can be set to null.
	 *
	 * @see #getConnectionTimeout()
	 * @see #DEFAULT_CONNECTION_TIMEOUT
	 */
	@Nullable
	@XmlAttribute(name = "connectionTimeout")
	private Duration m_connectionTimeout = DEFAULT_CONNECTION_TIMEOUT;

	/**
	 * SMTP server read timeout in milliseconds. By default set to #DEFAULT_READ_TIMEOUT.
	 * Can be set to null.
	 *
	 * @see #getReadTimeout()
	 * @see #DEFAULT_READ_TIMEOUT
	 */
	@Nullable
	@XmlAttribute(name = "readTimeout")
	private Duration m_readTimeout = DEFAULT_READ_TIMEOUT;

	/**
	 * SMTP server write timeout in milliseconds. By default set to #DEFAULT_WRITE_TIMEOUT.
	 * Can be set to null.
	 *
	 * @see #getWriteTimeout()
	 * @see #DEFAULT_WRITE_TIMEOUT
	 */
	@Nullable
	@XmlAttribute(name = "writeTimeout")
	private Duration m_writeTimeout = DEFAULT_WRITE_TIMEOUT;

	/**
	 * SMTP server user name.
	 *
	 * @see #getUserName()
	 */
	@Nullable
	@XmlAttribute(name = "userName")
	private String m_userName = null;

	/**
	 * SMTP server password.
	 *
	 * @see #getPassword()
	 */
	@Nullable
	@XmlAttribute(name = "password")
	private String m_password = null;

	/**
	 * Secure connection flag. By default set to #DEFAULT_SECURE.
	 *
	 * @see #isSecure()
	 * @see #DEFAULT_SECURE
	 */
	@XmlAttribute(name = "secure")
	private boolean m_secure = DEFAULT_SECURE;

	/**
	 * Default sender address.
	 *
	 * @see #getSenderAddress()
	 */
	@Nullable
	@XmlAttribute(name = "senderAddress")
	private InternetAddress m_senderAddress = null;

	/**
	 * Default sender name.
	 *
	 * @see #getSenderName()
	 */
	@Nullable
	@XmlAttribute(name = "senderName")
	private String m_senderName = null;

	/**
	 * Default charset. Set to JVM default charset if not specified.
	 *
	 * @see #getCharset()
	 * @see Charset#defaultCharset()
	 */
	@NotNull
	@XmlAttribute(name = "charset")
	private Charset m_charset = Charset.defaultCharset();

	@NotNull
	@Contract(pure = true)
	public SocketAddress getServerAddress() {
		return m_serverAddress;
	}

	@NotNull
	@Contract(pure = true)
	public Optional<Duration> getConnectionTimeout() {
		return Optional.ofNullable(m_connectionTimeout);
	}

	@NotNull
	@Contract(pure = true)
	public Optional<Duration> getReadTimeout() {
		return Optional.ofNullable(m_readTimeout);
	}

	@NotNull
	@Contract(pure = true)
	public Optional<Duration> getWriteTimeout() {
		return Optional.ofNullable(m_writeTimeout);
	}

	@NotNull
	@Contract(pure = true)
	public Optional<String> getUserName() {
		return Optional.ofNullable(m_userName);
	}

	@NotNull
	@Contract(pure = true)
	public Optional<String> getPassword() {
		return Optional.ofNullable(m_password);
	}

	@Contract(pure = true)
	public boolean isSecure() {
		return m_secure;
	}

	@NotNull
	@Contract(pure = true)
	public Optional<InternetAddress> getSenderAddress() {
		return Optional.ofNullable(m_senderAddress);
	}

	@NotNull
	@Contract(pure = true)
	public Optional<String> getSenderName() {
		return Optional.ofNullable(m_senderName);
	}

	@NotNull
	@Contract(pure = true)
	public Charset getCharset() {
		return m_charset;
	}
}
