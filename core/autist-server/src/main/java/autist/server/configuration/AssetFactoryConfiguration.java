package autist.server.configuration;

import org.jetbrains.annotations.Contract;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
public final class AssetFactoryConfiguration {
	/**
	 * Resource roots.
	 */
	@XmlElement(name = "roots")
	@XmlJavaTypeAdapter(AssetRootConfiguration.ContainerAdapter.class)
	private NamedEntityMap<AssetRootConfiguration> m_roots = new NamedEntityMap<>();

	/**
	 * Resource types.
	 */
	@XmlElement(name = "types")
	@XmlJavaTypeAdapter(AssetTypeConfiguration.ContainerAdapter.class)
	private NamedEntityMapEx<AssetTypeConfiguration> m_types = new NamedEntityMapEx<>();

	@Contract(pure = true)
	public NamedEntityMap<AssetRootConfiguration> getRoots() {
		return m_roots;
	}

	@Contract(pure = true)
	public NamedEntityMapEx<AssetTypeConfiguration> getTypes() {
		return m_types;
	}
}
