package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;

import autist.configuration.ConfigurationException;

import autist.server.application.ApplicationContext;

public class NamedEntity {
	@FunctionalInterface
	public interface Builder<
			EntityType extends NamedEntity,
			SpecificationType extends NamedEntitySpecification<EntityType, ?>>
	{
		EntityType build(
				final @NotNull ApplicationContext context,
				final @NotNull SpecificationType specification)
				throws ConfigurationException;
	}

	@NotNull
	private final String m_name;

	public NamedEntity(final @NotNull String name) {
		m_name = Objects.requireNonNull(name, "name == null");
	}

	@NotNull
	@Contract(pure = true)
	public final String name() {
		return m_name;
	}

	protected static <
			EntityType extends NamedEntity,
			SpecificationType extends NamedEntitySpecification<EntityType, ConfigurationType>,
			ConfigurationType extends NamedEntityConfiguration<EntityType, SpecificationType>>
	List<EntityType> buildList(
			final @NotNull ApplicationContext context,
			final @NotNull Class<EntityType> clazz,
			final @NotNull Builder<EntityType, SpecificationType> builder,
			final @NotNull Map<String, SpecificationType> specificationMap,
			final @NotNull Map<String, ConfigurationType> configurationMap)
			throws ConfigurationException
	{
		final List<ConfigurationException> warnings = new ArrayList<>(0);
		final List<ConfigurationException> errors = new ArrayList<>(0);

		validateMap(clazz, specificationMap, configurationMap, errors);

		final List<EntityType> entityList = new ArrayList<>(specificationMap.size());
		specificationMap.values().stream()
				.map((item) -> bind(item, configurationMap.get(item.name())))
				.filter((item) -> validate(clazz, item, errors, warnings))
				.filter((item) -> isConfigurable(clazz, item, warnings))
				.map((item) -> configure(context, clazz, builder, item, errors))
				.filter(Objects::nonNull)
				.forEach(entityList::add);

		warnings.forEach((item) -> context.log(Level.CONFIG, item.getMessage()));

		if (!errors.isEmpty()) {
			errors.forEach((item) -> context.log(Level.SEVERE, item.getMessage()));
			throw new ConfigurationException(String.format(
					"Invalid %s configuration",
					NamedEntityType.description(clazz)));
		}

		return entityList;
	}

	private static <
			EntityType extends NamedEntity,
			SpecificationType extends NamedEntitySpecification<EntityType, ConfigurationType>,
			ConfigurationType extends NamedEntityConfiguration<EntityType, SpecificationType>>
	SpecificationType bind(
			final @NotNull SpecificationType specification,
			final @Nullable ConfigurationType configuration)
	{
		specification.bind(configuration);

		return specification;
	}

	private static <
			EntityType extends NamedEntity,
			SpecificationType extends NamedEntitySpecification<EntityType, ?>>
	EntityType configure(
			final @NotNull ApplicationContext context,
			final @NotNull Class<EntityType> clazz,
			final @NotNull Builder<EntityType, SpecificationType> builder,
			final @NotNull SpecificationType specification,
			final @NotNull Collection<ConfigurationException> errors)
	{
		EntityType result = null;

		try {
			result = builder.build(context, specification);
		} catch (final ConfigurationException e) {
			errors.add(new ConfigurationException(
					String.format(
							"Can't configure %s: %s",
							NamedEntityType.description(clazz), specification.name()),
					e));
		}

		return result;
	}

	private static <
			EntityType extends NamedEntity,
			SpecificationType extends NamedEntitySpecification<EntityType, ?>>
	boolean isConfigurable(
			final @NotNull Class<EntityType> clazz,
			final @NotNull SpecificationType specification,
			final @NotNull List<ConfigurationException> warnings)
	{
		if (!specification.hasConfiguration() && !specification.isPreConfigured()) {
			warnings.add(new ConfigurationException(String.format(
					"Skipping optional %s: %s (not configured)",
					NamedEntityType.description(clazz), specification.name())));
			return false;
		}

		return true;
	}

	private static <
			EntityType extends NamedEntity,
			SpecificationType extends NamedEntitySpecification<EntityType, ?>>
	boolean validate(
			final @NotNull Class<EntityType> clazz,
			final @NotNull SpecificationType specification,
			final @NotNull List<ConfigurationException> errors,
			final @NotNull List<ConfigurationException> warnings)
	{
		final var typeName = NamedEntityType.description(clazz);
		final var instanceName = specification.name();

		boolean result = true;

		if (specification.hasConfiguration()) {
			if (specification.isPreConfigured()) {
				errors.add(new ConfigurationException(String.format(
						"Overriding preconfigured %s not allowed: %s",
						typeName, instanceName)));
				result = false;
			}
		} else if (specification.isRequired() && !specification.isPreConfigured()) {
			errors.add(new ConfigurationException(String.format(
					"Required %s not configured: %s",
					typeName, instanceName)));
			result = false;
		} else {
			warnings.add(new ConfigurationException(String.format(
					"Configuring %s: %s",
					typeName, instanceName)));
		}

		return result;
	}

	private static <
			EntityType extends NamedEntity,
			SpecificationType extends NamedEntitySpecification<EntityType, ConfigurationType>,
			ConfigurationType extends NamedEntityConfiguration<EntityType, SpecificationType>>
	void validateMap(
			final @NotNull Class<EntityType> clazz,
			final @NotNull Map<String, SpecificationType> specificationMap,
			final @NotNull Map<String, ConfigurationType> configurationMap,
			final @NotNull List<ConfigurationException> errors)
	{
		configurationMap.values().stream()
				.filter((item) -> !specificationMap.containsKey(item.name()))
				.forEach((item) -> errors.add(new ConfigurationException(String.format(
						"Unknown %s specified in configuration: %s",
						NamedEntityType.description(clazz), item.name()))));
	}
}
