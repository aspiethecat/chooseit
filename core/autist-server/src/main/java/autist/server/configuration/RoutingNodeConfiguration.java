package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlType;

import autist.commons.net.URN;

import autist.configuration.ConfigurationRoot;

import autist.application.routing.RouteParameter;

@SuppressWarnings({"WeakerAccess", "unused"})
@XmlType(name = "route")
public final class RoutingNodeConfiguration implements ConfigurationRoot {
	/**
	 * Name (required).
	 */
	@Nullable
	@XmlAttribute(name = "name")
	private String m_name = null;

	/**
	 * Name pattern to match (optional).
	 */
	@Nullable
	@XmlAttribute(name = "pattern")
	private Pattern m_pattern = null;

	/**
	 * Path parameter names list.
	 */
	@NotNull
	@XmlElement(name = "parameters")
	private List<RouteParameter> m_parameters = Collections.emptyList();

	/**
	 * Route identifier (optional).
	 */
	@Nullable
	@XmlAttribute(name = "key")
	private String m_key = null;

	/**
	 * Route tags list.
	 */
	@NotNull
	@XmlList
	@XmlAttribute(name = "tags")
	private List<String> m_tags = Collections.emptyList();

	/**
	 * Filter class names list.
	 */
	@NotNull
	@XmlList
	@XmlElement(name = "filter")
	private List<String> m_filters = Collections.emptyList();

	/**
	 * Redirect path (optional).
	 */
	@Nullable
	@XmlAttribute(name = "target")
	private URN m_target = null;

	/**
	 * Resource path (optional).
	 */
	@Nullable
	@XmlAttribute(name = "source")
	private URN m_source = null;

	/**
	 * Action handler class name (optional).
	 */
	@Nullable
	@XmlAttribute(name = "action")
	private String m_action = null;

	/**
	 * View type (optional).
	 */
	@Nullable
	@XmlAttribute(name = "view")
	private String m_viewType = null;

	/**
	 * Children routes list.
	 */
	@NotNull
	@XmlList
	@XmlElement(name = "action")
	private List<RoutingNodeConfiguration> m_children = Collections.emptyList();

	@NotNull
	@Contract(pure = true)
	public Optional<String> name() {
		return Optional.ofNullable(m_name);
	}

	@NotNull
	@Contract(pure = true)
	public Optional<Pattern> pattern() {
		return Optional.ofNullable(m_pattern);
	}

	@NotNull
	@Contract(pure = true)
	public List<RouteParameter> parameters() {
		return m_parameters;
	}

	@NotNull
	@Contract(pure = true)
	public Optional<String> key() {
		return Optional.ofNullable(m_key);
	}

	@NotNull
	@Contract(pure = true)
	public List<String> tags() {
		return m_tags;
	}

	@NotNull
	@Contract(pure = true)
	public Optional<URN> target() {
		return Optional.ofNullable(m_target);
	}

	@NotNull
	@Contract(pure = true)
	public Optional<URN> source() {
		return Optional.ofNullable(m_source);
	}

	@NotNull
	@Contract(pure = true)
	public Optional<String> action() {
		return Optional.ofNullable(m_action);
	}

	@NotNull
	@Contract(pure = true)
	public List<String> filters() {
		return m_filters;
	}

	@NotNull
	@Contract(pure = true)
	public List<RoutingNodeConfiguration> children() {
		return m_children;
	}
}
