package autist.server.configuration.adapter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import autist.configuration.ConfigurationAdapter;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class MailAddressAdapter extends ConfigurationAdapter<String, InternetAddress> {
	@Override
	@NotNull
	@Contract("_ -> new")
	public InternetAddress unmarshal(final @NotNull String address)
			throws AddressException
	{
		return new InternetAddress(address, true);
	}
}
