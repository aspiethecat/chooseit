package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.Optional;

import java.net.URI;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import autist.server.application.ResourceRoot;

@XmlAccessorType(XmlAccessType.FIELD)
public class ResourceRootSpecification<
			EntityType extends ResourceRoot,
			ConfigurationType extends ResourceRootConfiguration<
					EntityType,
					? extends ResourceRootSpecification<EntityType, ConfigurationType>>>
			extends NamedEntitySpecification<EntityType, ConfigurationType>
{
	@Nullable
	@XmlAttribute(name = "uri")
	private URI m_uri = null;

	@NotNull
	@Contract(pure = true)
	public final Optional<URI> uri() {
		return Optional.ofNullable(m_uri);
	}

	@Override
	@Contract(pure = true)
	public boolean isPreConfigured() {
		return Objects.nonNull(m_uri);
	}
}
