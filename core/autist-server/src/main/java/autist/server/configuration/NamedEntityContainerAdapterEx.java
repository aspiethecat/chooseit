package autist.server.configuration;

import org.jetbrains.annotations.NotNull;

public class NamedEntityContainerAdapterEx<
		ContainerType extends NamedEntityContainerEx<EntityType>,
		EntityType extends NamedEntityConfiguration>
		extends NamedEntityContainerAdapter<ContainerType, EntityType>
{
		@Override
		public NamedEntityMapEx<EntityType> unmarshal(
				final @NotNull ContainerType container)
				throws Exception
		{
			return new NamedEntityMapEx<>(buildMap(container.getItems()), container.getDefaultItemName());
		}
}
