package autist.server.configuration.adapter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.net.URISyntaxException;

import autist.configuration.ConfigurationAdapter;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class URIAdapter extends ConfigurationAdapter<String, URI> {
	@Override
	@NotNull
	@Contract("_ -> new")
	public URI unmarshal(final @NotNull String path)
			throws URISyntaxException
	{
		return new URI(path);
	}
}
