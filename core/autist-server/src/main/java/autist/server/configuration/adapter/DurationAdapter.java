package autist.server.configuration.adapter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;
import java.time.format.DateTimeParseException;

import autist.configuration.ConfigurationAdapter;

@SuppressWarnings({"WeakerAccess", "unused"})
public class DurationAdapter extends ConfigurationAdapter<String, Duration> {
	@Override
	@NotNull
	@Contract("_ -> new")
	public Duration unmarshal(final @NotNull String duration)
			throws DateTimeParseException
	{
		return Duration.parse(duration);
	}
}
