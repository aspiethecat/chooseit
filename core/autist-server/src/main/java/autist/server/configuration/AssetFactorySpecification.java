package autist.server.configuration;

import org.jetbrains.annotations.Contract;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
public final class AssetFactorySpecification {
	/**
	 * Resource roots.
	 */
	@XmlElement(name = "roots")
	@XmlJavaTypeAdapter(AssetRootSpecification.ContainerAdapter.class)
	private NamedEntityMap<AssetRootSpecification> m_roots = new NamedEntityMap<>();

	/**
	 * Resource families.
	 */
	@XmlElement(name = "types")
	@XmlJavaTypeAdapter(AssetTypeSpecification.ContainerAdapter.class)
	private NamedEntityMapEx<AssetTypeSpecification> m_types = new NamedEntityMapEx<>();

	@Contract(pure = true)
	public NamedEntityMap<AssetRootSpecification> getRoots() {
		return m_roots;
	}

	@Contract(pure = true)
	public NamedEntityMapEx<AssetTypeSpecification> getTypes() {
		return m_types;
	}
}
