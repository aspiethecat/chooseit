package autist.server.configuration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class AssetMatchSpecification {
	@XmlAttribute(name = "extension")
	private String m_extension = null;

	@XmlAttribute(name = "pattern")
	private String m_pattern = null;

	public String getExtension() {
		return m_extension;
	}

	public String getPattern() {
		return m_pattern;
	}
}
