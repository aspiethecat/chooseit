package autist.server.configuration.adapter;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;

import autist.configuration.ConfigurationAdapter;

public class AdapterFactory {
	@Nullable
	private static List<ConfigurationAdapter> s_adapters = null;

	public static List<ConfigurationAdapter> getAdapters()
			throws ServiceConfigurationError
	{
		if (s_adapters == null) {
			final var adapters = new ArrayList<ConfigurationAdapter>();

			for (final var item : ServiceLoader.load(ConfigurationAdapter.class))
				adapters.add(item);

			s_adapters = Collections.unmodifiableList(adapters);

		}

		return s_adapters;
	}
}
