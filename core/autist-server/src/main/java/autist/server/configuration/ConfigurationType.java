package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import autist.commons.util.EnumResolver;
import autist.commons.util.KeySupplier;
import autist.configuration.ConfigurationRoot;

import autist.server.application.ActionNodeImpl;

public enum ConfigurationType
		implements KeySupplier<Class<? extends ConfigurationRoot>>
{
	ROUTING(ActionNodeImpl.class),
	SYSTEM(ServerConfiguration.class),
	APPLICATION(ApplicationSpecification.class);

	@NotNull
	private static final EnumResolver<Class<? extends ConfigurationRoot>, ConfigurationType> s_resolver =
			new EnumResolver<>(ConfigurationType.class);

	@NotNull
	private final Class<? extends ConfigurationRoot> m_key;

	ConfigurationType(final Class<? extends ConfigurationRoot> configurationClass) {
		m_key = configurationClass;
	}

	@NotNull
	@Contract(pure = true)
	public Class<? extends ConfigurationRoot> key() {
		return m_key;
	}

	@NotNull
	@Contract(pure = true)
	public String schemaName() {
		return m_key.getSimpleName();
	}

	@NotNull
	public static Optional<ConfigurationType> resolve(
			final Class<? extends ConfigurationRoot> configurationClass)
	{
		return s_resolver.resolve(configurationClass);
	}
}
