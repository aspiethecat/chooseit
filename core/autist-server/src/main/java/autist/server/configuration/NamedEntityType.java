package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;

import autist.commons.util.EnumResolver;
import autist.commons.util.KeySupplier;

import autist.server.application.AssetRoot;
import autist.server.application.AssetType;
import autist.server.application.DataSource;
import autist.server.application.TemplateRoot;

public enum NamedEntityType
		implements KeySupplier<Class<? extends NamedEntity>>
{
	GENERIC(NamedEntity.class, "entity"),
	DATA_SOURCE(DataSource.class, "data source"),
	TEMPLATE_ROOT(TemplateRoot.class, "template root"),
	ASSET_ROOT(AssetRoot.class, "asset root"),
	ASSET_TYPE(AssetType.class, "asset type");

	@NotNull
	private static final EnumResolver<Class<? extends NamedEntity>, NamedEntityType> s_resolver =
			new EnumResolver<>(NamedEntityType.class);

	@NotNull
	private final Class<? extends NamedEntity> m_key;

	@NotNull
	private final String m_description;

	NamedEntityType(
			final @NotNull Class<? extends NamedEntity> entityClass,
			final @NotNull String description)
	{
		m_key = entityClass;
		m_description = description;
	}

	@NotNull
	@Contract(pure = true)
	public Class<? extends NamedEntity> key() {
		return m_key;
	}

	@NotNull
	@Contract(pure = true)
	public String description() {
		return m_description;
	}

	@NotNull
	@Contract(pure = true)
	public static String description(
			final @NotNull Class<? extends NamedEntity> key)
	{
		Objects.requireNonNull(key, "key == null");
		return resolve(key).orElse(GENERIC).description();
	}

	@NotNull
	@Contract(pure = true)
	public static Optional<NamedEntityType> resolve(
			@NotNull Class<? extends NamedEntity> key)
	{
		return s_resolver.resolve(key);
	}
}
