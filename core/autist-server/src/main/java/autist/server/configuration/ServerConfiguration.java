package autist.server.configuration;

import org.jetbrains.annotations.Contract;

import java.util.List;
import java.nio.file.Path;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import autist.configuration.ConfigurationException;
import autist.configuration.ConfigurationRoot;

import autist.server.journal.LogConfiguration;

/**
 * Complete configuration.
 */
@XmlRootElement(name = "configuration")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServerConfiguration implements ConfigurationRoot {
	/**
	 * Server core settings.
	 */
	@XmlElement(name = "server")
	private CoreConfiguration m_threadPoolConfiguration = null;

	/**
	 * Connectors settings.
	 */
	@XmlElement(name = "channel")
	@XmlElementWrapper(name = "channels")
	private List<ChannelConfiguration> m_channels = null;

	/**
	 * System log settings.
	 */
	@XmlElement(name = "systemLog")
	private LogConfiguration m_systemLogConfiguration = null;

	/**
	 * Access log settings.
	 */
	@XmlElement(name = "requestLog")
	private LogConfiguration m_requestLogConfiguration = null;

	/**
	 * Application instance configuration.
	 */
	@XmlElement(name = "application")
	private ApplicationConfiguration m_applicationConfiguration = null;

	public static ServerConfiguration load(Path path) throws ConfigurationException {
		return ConfigurationLoader.load(
				ServerConfiguration.class,
				ConfigurationLoader.createExternalSource(path));
	}

	@Contract(pure = true)
	public CoreConfiguration getThreadPoolConfiguration() {
		return m_threadPoolConfiguration;
	}

	@Contract(pure = true)
	public List<ChannelConfiguration> getChannels() {
		return m_channels;
	}

	@Contract(pure = true)
	public LogConfiguration getSystemLogConfiguration() {
		return m_systemLogConfiguration;
	}

	@Contract(pure = true)
	public LogConfiguration getRequestLogConfiguration() {
		return m_requestLogConfiguration;
	}

	@Contract(pure = true)
	public ApplicationConfiguration getApplicationConfiguration() {
		return m_applicationConfiguration;
	}
}
