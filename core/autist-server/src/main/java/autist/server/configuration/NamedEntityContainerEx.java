package autist.server.configuration;

public interface NamedEntityContainerEx<
		EntityType extends NamedEntityConfiguration>
		extends NamedEntityContainer<EntityType>
{
	String getDefaultItemName();
}
