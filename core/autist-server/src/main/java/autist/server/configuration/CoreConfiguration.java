package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Server core configuration settings.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class CoreConfiguration extends SocketConfiguration {
	/**
	 * Boss thread pool size.
	 * @see #bossThreadCount()
	 */
	@Nullable
	@XmlAttribute(name = "bossThreadCount")
	private Integer m_bossThreadCount = null;

	/**
	 * Worker thread pool size.
	 * @see #workerThreadCount()
	 */
	@Nullable
	@XmlAttribute(name = "workerThreadCount")
	private Integer m_workerThreadCount = null;

	@NotNull
	@Contract(pure = true)
	public Optional<Integer> bossThreadCount() {
		return Optional.ofNullable(m_bossThreadCount);
	}

	@NotNull
	@Contract(pure = true)
	public Optional<Integer> workerThreadCount() {
		return Optional.ofNullable(m_workerThreadCount);
	}
}
