package autist.server.configuration;

import java.util.Locale;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
public final class TemplateFactorySpecification {
	public static final Locale DEFAULT_LOCALE = Locale.US;
	public static final Charset DEFAULT_ENCODING = StandardCharsets.UTF_8;

	/**
	 * Template roots.
	 *
	 * @see #getRoots()
	 */
	@XmlElement(name = "roots")
	@XmlJavaTypeAdapter(TemplateRootSpecification.ContainerAdapter.class)
	private NamedEntityMap<TemplateRootSpecification> m_roots = new NamedEntityMap<>();

	/**
	 * Default locale name.
	 *
	 * @see #getDefaultLocale()
	 */
	@XmlAttribute(name = "defaultLocale")
	private Locale m_defaultLocale = DEFAULT_LOCALE;

	/**
	 * Default encoding name.
	 *
	 * @see #getDefaultEncoding()
	 */
	@XmlAttribute(name = "defaultEncoding")
	private Charset m_defaultEncoding = DEFAULT_ENCODING;

	public NamedEntityMap<TemplateRootSpecification> getRoots() {
		return m_roots;
	}

	public Locale getDefaultLocale() {
		return m_defaultLocale;
	}

	public Charset getDefaultEncoding() {
		return m_defaultEncoding;
	}
}
