package autist.server.configuration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
public class TemplateFactoryConfiguration {
	/**
	 * Template roots.
	 */
	@XmlElement(name = "roots")
	@XmlJavaTypeAdapter(TemplateRootConfiguration.ContainerAdapter.class)
	private NamedEntityMap<TemplateRootConfiguration> m_roots = new NamedEntityMap<>();

	public NamedEntityMap<TemplateRootConfiguration> getRoots() {
		return m_roots;
	}
}
