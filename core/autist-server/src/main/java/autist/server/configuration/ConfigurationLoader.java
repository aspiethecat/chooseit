package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import java.io.FileNotFoundException;
import java.nio.file.Path;

import java.net.URI;
import java.net.URISyntaxException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import autist.commons.util.DeferredException;
import autist.configuration.ConfigurationException;
import autist.configuration.ConfigurationRoot;

import autist.server.configuration.adapter.AdapterFactory;

/**
 * Base class for root configuration objects.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class ConfigurationLoader {
	public static final String SCHEMA_EXTENSION = "xsd";

	/**
	 * Reads configuration object from XML file.
	 */
	@NotNull
	public static <
			ConfigurationRootType extends ConfigurationRoot>
	ConfigurationRootType load(
			final @NotNull Class<ConfigurationRootType> configurationClass,
			final @NotNull Source source)
			throws ConfigurationException
	{
		return load(configurationClass, source,null);
	}

	/**
	 * Reads configuration object from XML file.
	 */
	@NotNull
	@SuppressWarnings("unchecked")
	public static <
			ConfigurationRootType extends ConfigurationRoot>
	ConfigurationRootType load(
			final @NotNull Class<ConfigurationRootType> clazz,
			final @NotNull Source source,
			final @Nullable Unmarshaller.Listener listener)
			throws ConfigurationException
	{
		Objects.requireNonNull(clazz, "clazz == null");
		Objects.requireNonNull(source, "source == null");

		final var type = ConfigurationType
				.resolve(clazz)
				.orElseThrow(() -> new IllegalArgumentException(
						String.format("Unsupported configuration type: %s", clazz.getName())));

		final var typeName = type.name().toLowerCase();

		try {
			final var unmarshaller =
					JAXBContext.newInstance(clazz).createUnmarshaller();

			for (final var adapter : AdapterFactory.getAdapters())
					unmarshaller.setAdapter(adapter);

			if (listener != null)
				unmarshaller.setListener(listener);

			final var exception = new DeferredException<>(ConfigurationException.class);

			unmarshaller.setSchema(loadSchema(type));
			unmarshaller.setEventHandler((ValidationEvent event) -> {
				exception.bind(new ConfigurationException(
						String.format(
							"Invalid %s configuration file: %s",
							typeName, event.getMessage())));
				return false;
			});

			exception.throwIfPresent();

			return (ConfigurationRootType) unmarshaller.unmarshal(source);
		} catch (ConfigurationException | JAXBException e) {
			throw new ConfigurationException(
					String.format(
							"Can't load %s configuration: %s",
							typeName, source),
					e);
		}
	}

	@NotNull
	@Contract(pure = true)
	private static Schema loadSchema(
			final @NotNull ConfigurationType type)
			throws ConfigurationException
	{
		Objects.requireNonNull(type, "type == null");

		final URI uri = getSchemaURI(type);
		try {
			final Source configurationSource = createInternalSource(type.key(), uri);

			return SchemaFactory
					.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
					.newSchema(configurationSource);
		} catch (final FileNotFoundException e) {
			throw new ConfigurationException(
					String.format("Configuration schema file not found: %s", uri),
					e);
		} catch (final SAXException e) {
			throw new ConfigurationException(
					String.format("Invalid configuration schema file: %s", uri),
					e);
		}
	}

	@NotNull
	@Contract(pure = true)
	private static URI getSchemaURI(
			final @NotNull ConfigurationType type)
	{
		final var path = String.format("%s.%s", type.schemaName(), SCHEMA_EXTENSION);
		try {
			return new URI(path);
		} catch (final URISyntaxException e) {
			throw new AssertionError(
					String.format("Invalid configuration schema URI: %s", path), e);
		}
	}

	@NotNull
	@Contract("_ -> new")
	public static ConfigurationSource createExternalSource(
			final @NotNull Path path)
	{
		Objects.requireNonNull(path, "path == null");
		return new ConfigurationSource(path);
	}

	@NotNull
	@Contract("_ -> new")
	public static Source createInternalSource(
			final @NotNull URI uri)
			throws FileNotFoundException
	{
		Objects.requireNonNull(uri, "uri == null");

		final var path = getResourcePath(uri);
		final var stream = ClassLoader.getSystemResourceAsStream(path);

		if (Objects.isNull(stream))
			throw new FileNotFoundException(path);

		return new StreamSource(stream);
	}

	@NotNull
	@Contract("_, _ -> new")
	public static <
			ConfigurationRootType extends ConfigurationRoot>
	Source createInternalSource(
			final @NotNull Class<ConfigurationRootType> owner,
			final @NotNull URI uri)
			throws FileNotFoundException
	{
		Objects.requireNonNull(owner, "owner == null");
		Objects.requireNonNull(uri, "uri == null");

		final var path = getResourcePath(uri);
		final var stream = owner.getResourceAsStream(path);

		if (stream == null)
			throw new FileNotFoundException(String.format("%s: %s", owner.getName(), path));

		return new StreamSource(stream);
	}

	private static String getResourcePath(final @NotNull URI uri) {
		Objects.requireNonNull(uri, "uri == null");

		String resourcePath = uri.toString();
		if (resourcePath.startsWith("/"))
			resourcePath = resourcePath.substring(1);

		return resourcePath;
	}
}
