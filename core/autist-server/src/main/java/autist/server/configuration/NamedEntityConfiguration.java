package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import javax.xml.bind.annotation.XmlAttribute;

@SuppressWarnings({"WeakerAccess", "unused"})
public class NamedEntityConfiguration<
		EntityType extends NamedEntity,
		SpecificationType extends NamedEntitySpecification<
				EntityType, ? extends NamedEntityConfiguration>>
{
	@XmlAttribute(name = "name", required = true)
	private String m_name = null;

	@Contract(pure = true)
	public final String name() {
		return m_name;
	}
}
