package autist.server.configuration;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import autist.server.application.TemplateRoot;

@XmlAccessorType(XmlAccessType.FIELD)
public class TemplateRootSpecification
		extends ResourceRootSpecification<TemplateRoot, TemplateRootConfiguration>
{
	@XmlType(name = "TemplateRootSpecificationContainer")
	public static class Container
			implements NamedEntityContainer<TemplateRootSpecification>
	{
		@XmlElement(name = "root", type = TemplateRootSpecification.class)
		private final List<TemplateRootSpecification> m_items = new ArrayList<>();

		public List<TemplateRootSpecification> getItems() {
			return m_items;
		}
	}

	public static class ContainerAdapter
			extends NamedEntityContainerAdapter<Container, TemplateRootSpecification>
	{
	}
}
