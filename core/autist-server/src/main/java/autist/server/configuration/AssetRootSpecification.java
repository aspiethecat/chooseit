package autist.server.configuration;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import autist.server.application.AssetRoot;

@XmlAccessorType(XmlAccessType.FIELD)
public class AssetRootSpecification
		extends ResourceRootSpecification<AssetRoot, AssetRootConfiguration>
{
	@XmlType(name = "ResourceRootSpecificationContainer")
	public static class Container
			implements NamedEntityContainer<AssetRootSpecification>
	{
		@XmlElement(
				name = "root",
				type = AssetRootSpecification.class
		)
		private final List<AssetRootSpecification> m_items = new ArrayList<>();

		public List<AssetRootSpecification> getItems() {
			return m_items;
		}
	}

	public static class ContainerAdapter
			extends NamedEntityContainerAdapter<Container, AssetRootSpecification>
	{
	}

	/**
	 * Resource root prefix.
	 */
	@XmlAttribute(name = "prefix")
	private String m_prefix = null;

	@Nullable
	public String getPrefix() {
		return m_prefix;
	}
}
