package autist.server.configuration.adapter;

import org.jetbrains.annotations.NotNull;

import java.nio.file.InvalidPathException;
import java.nio.file.Paths;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;

import autist.commons.net.DomainSocketAddress;

import autist.configuration.ConfigurationAdapter;

@SuppressWarnings({"WeakerAccess", "unused"})
public class SocketAddressAdapter extends ConfigurationAdapter<String, SocketAddress> {
	private static final int PORT_NUMBER_MIN = 0;
	private static final int PORT_NUMBER_MAX = 65535;

	/**
	 * @throws InvalidPathException invalid domain socket path
	 * @throws UnknownHostException IP address of the host could not be determined
	 * @throws IllegalArgumentException invalid address
	 */
	@Override
	@NotNull
	public SocketAddress unmarshal(final @NotNull String address)
			throws InvalidPathException, UnknownHostException, IllegalArgumentException
	{
		if (address.isEmpty())
			throw new IllegalArgumentException("Empty address");

		final String host;
		final int port;

		final char start = address.charAt(0);

		if ((start == '/') || (start == '\\'))
			return new DomainSocketAddress(Paths.get(address));

		final InetAddress inetAddress;
		if (start == '[') {
			final int end = address.indexOf(']');
			if (end < 0)
				throw new IllegalArgumentException(String.format("Unterminated IPv6 address: %s", address));

			host = address.substring(start, end);

			if (end < address.length()) {
				final int suffix = end + 1;
				if (address.charAt(suffix) != ':')
					throw new IllegalArgumentException(String.format("Invalid IPv6 address suffix: %s", address));

				port = parsePortNumber(address.substring(suffix + 1));
			} else {
				port = 0;
			}

			inetAddress = Inet6Address.getByName(host);
		} else {
			final int suffix = address.indexOf(':');
			if (suffix < 0) {
				host = address;
				port = 0;
			} else {
				host = address.substring(0, suffix);
				port = parsePortNumber(address.substring(suffix + 1));
			}

			inetAddress = Inet4Address.getByName(host);
		}

		return new InetSocketAddress(inetAddress, port);
	}

	private static int parsePortNumber(final @NotNull String value)
			throws NumberFormatException
	{
		if (value.isEmpty())
			throw new NumberFormatException("Port number expected");

		for (final char ch : value.toCharArray()) {
			if ((ch < '0') || (ch > '9'))
				throw new NumberFormatException("Invalid port number");
		}

		final int port = Integer.valueOf(value);

		if ((port < PORT_NUMBER_MIN) || (port > PORT_NUMBER_MAX))
			throw new NumberFormatException("Port number out of range");

		return port;
	}
}
