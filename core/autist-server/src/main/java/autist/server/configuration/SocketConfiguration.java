package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelOption;

/**
 * Socket configuration settings.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@SuppressWarnings({"WeakerAccess", "unused"})
class SocketConfiguration {
	/** Backlog queue size */
	@Nullable
	@XmlAttribute(name = "backlog")
	private Integer m_backlog = null;

	/** Send buffer size in bytes */
	@Nullable
	@XmlAttribute(name = "sendBuffer")
	private Integer m_sendBufferSize = null;

	/** Receive buffer size in bytes */
	@Nullable
	@XmlAttribute(name = "receiveBuffer")
	private Integer m_receiveBufferSize = null;

	public final void configure(
			final @NotNull ServerBootstrap bootstrap,
			final @NotNull SocketConfiguration parent)
	{
		setBootstrapOption(
				bootstrap,
				ChannelOption.SO_BACKLOG,
				m_backlog,
				parent.m_backlog);
		setBootstrapChildOption(
				bootstrap,
				ChannelOption.SO_SNDBUF,
				m_sendBufferSize,
				parent.m_sendBufferSize);
		setBootstrapChildOption(
				bootstrap,
				ChannelOption.SO_RCVBUF,
				m_receiveBufferSize,
				parent.m_receiveBufferSize);
	}

	@Nullable
	@Contract(pure = true)
	public final Optional<Integer> backlog() {
		return Optional.ofNullable(m_backlog);
	}

	@Nullable
	@Contract(pure = true)
	public final Optional<Integer> sendBufferSize() {
		return Optional.ofNullable(m_sendBufferSize);
	}

	@Nullable
	@Contract(pure = true)
	public final Optional<Integer> receiveBufferSize() {
		return Optional.ofNullable(m_receiveBufferSize);
	}

	private static <OptionType> void setBootstrapOption(
			final @NotNull ServerBootstrap bootstrap,
			final @NotNull ChannelOption<OptionType> option,
			final @Nullable OptionType channelSetting,
			final @Nullable OptionType serverSetting)
	{
		if (channelSetting != null)
			bootstrap.option(option, channelSetting);
		else if (serverSetting != null)
			bootstrap.option(option, serverSetting);
	}

	private static <OptionType> void setBootstrapChildOption(
			final @NotNull ServerBootstrap bootstrap,
			final @NotNull ChannelOption<OptionType> option,
			final @Nullable OptionType channelSetting,
			final @Nullable OptionType serverSetting)
	{
		if (channelSetting != null)
			bootstrap.childOption(option, channelSetting);
		else if (serverSetting != null)
			bootstrap.childOption(option, serverSetting);
	}
}
