package autist.server.configuration.adapter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import autist.configuration.ConfigurationAdapter;

@SuppressWarnings({"WeakerAccess", "unused"})
public class PatternAdapter extends ConfigurationAdapter<String, Pattern> {
	@Override
	@NotNull
	@Contract("_ -> new")
	public Pattern unmarshal(final @NotNull String regex)
			throws PatternSyntaxException
	{
		return Pattern.compile(regex);
	}
}
