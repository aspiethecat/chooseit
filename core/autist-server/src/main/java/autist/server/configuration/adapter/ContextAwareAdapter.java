package autist.server.configuration.adapter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import autist.configuration.ConfigurationAdapter;

@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class ContextAwareAdapter<ValueType, BoundType, ContextType>
		extends ConfigurationAdapter<ValueType, BoundType>
{
	@NotNull
	private final ContextType m_context;

	public ContextAwareAdapter(final @NotNull ContextType context) {
		m_context = Objects.requireNonNull(context, "context == null");
	}

	@NotNull
	@Contract(pure = true)
	public final ContextType context() {
		return m_context;
	}
}
