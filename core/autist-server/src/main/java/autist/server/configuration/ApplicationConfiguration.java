package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

import java.nio.file.Path;

import java.net.URI;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import autist.application.EnvironmentType;

@XmlAccessorType(XmlAccessType.FIELD)
public final class ApplicationConfiguration extends ConfigurationLoader {
	/**
	 * Default environment type.
	 */
	public static final EnvironmentType DEFAULT_ENVIRONMENT = EnvironmentType.PRODUCTION;

	/**
	 * Application instance name.
	 */
	@XmlAttribute(name = "name", required = true)
	private String m_name = null;

	/**
	 * Runtime root path.
	 */
	@XmlAttribute(name = "cacheRoot", required = true)
	private Path m_cacheRoot = null;

	/**
	 * Application specification file URI.
	 */
	@XmlAttribute(name = "specification", required = true)
	private URI m_specificationUri = null;

	/**
	 * ActionContext type.
	 */
	@NotNull
	@XmlAttribute(name = "environment")
	@XmlJavaTypeAdapter(EnvironmentType.Adapter.class)
	private EnvironmentType m_environment = DEFAULT_ENVIRONMENT;

	/**
	 * Mailer configuration.
	 */
	@Nullable
	@XmlElement(name = "mailer")
	private MailerConfiguration m_mailerConfiguration = null;

	/**
	 * Data source settings.
	 */
	@NotNull
	@XmlElement(name = "dataSources")
	@XmlJavaTypeAdapter(DataSourceConfiguration.ContainerAdapter.class)
	private NamedEntityMap<DataSourceConfiguration> m_dataSources = new NamedEntityMap<>();

	/**
	 * Template settings.
	 */
	@Nullable
	@XmlElement(name = "templates")
	private TemplateFactoryConfiguration m_templateFactoryConfiguration = null;

	/**
	 * Asset settings.
	 */
	@Nullable
	@XmlElement(name = "assets")
	private AssetFactoryConfiguration m_assetFactoryConfiguration = null;

	@Contract(pure = true)
	public String getName() {
		return m_name;
	}

	@Contract(pure = true)
	public Path getCacheRoot() {
		return m_cacheRoot;
	}

	@Contract(pure = true)
	public URI getSpecificationUri() {
		return m_specificationUri;
	}

	@NotNull
	@Contract(pure = true)
	public EnvironmentType getEnvironment() {
		return m_environment;
	}

	@NotNull
	@Contract(pure = true)
	public Optional<MailerConfiguration> getMailerConfiguration() {
		return Optional.ofNullable(m_mailerConfiguration);
	}

	@NotNull
	@Contract(pure = true)
	public NamedEntityMap<DataSourceConfiguration> getDataSources() {
		return m_dataSources;
	}

	@NotNull
	@Contract(pure = true)
	public Optional<TemplateFactoryConfiguration> getTemplateFactoryConfiguration() {
		return Optional.ofNullable(m_templateFactoryConfiguration);
	}

	@NotNull
	@Contract(pure = true)
	public Optional<AssetFactoryConfiguration> getAssetFactoryConfiguration() {
		return Optional.ofNullable(m_assetFactoryConfiguration);
	}
}
