package autist.server.configuration;

import java.util.List;

public interface NamedEntityContainer<T extends NamedEntityConfiguration>
{
	List<T> getItems();
}
