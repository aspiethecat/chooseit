package autist.server.configuration;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import autist.server.application.AssetType;

@XmlAccessorType(XmlAccessType.FIELD)
public class AssetTypeConfiguration
		extends NamedEntityConfiguration<AssetType, AssetTypeSpecification>
{
	@XmlType(name = "ResourceTypeConfigurationContainer")
	public static class Container
			implements NamedEntityContainerEx<AssetTypeConfiguration>
	{
		@XmlAttribute(name = "default")
		private String m_defaultItemName = null;

		@XmlElement(name = "type", type = AssetTypeConfiguration.class)
		private final List<AssetTypeConfiguration> m_items = new ArrayList<>(0);

		@Override
		public final String getDefaultItemName() {
			return m_defaultItemName;
		}

		@Override
		public final List<AssetTypeConfiguration> getItems() {
			return m_items;
		}
	}

	public static class ContainerAdapter
			extends NamedEntityContainerAdapterEx<Container, AssetTypeConfiguration>
	{
	}

	/**
	 * MIME type.
	 */
	@XmlAttribute(name = "mimeType")
	private String m_mimeType = null;

	/**
	 * Match list.
	 */
	@XmlElement(name = "match")
	private List<AssetMatchSpecification> m_matches = new ArrayList<>();

	@Nullable
	public final String getMimeType() {
		return m_mimeType;
	}

	public final List<AssetMatchSpecification> getMatches() {
		return Collections.unmodifiableList(m_matches);
	}
}
