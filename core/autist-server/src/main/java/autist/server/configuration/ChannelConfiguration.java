package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.net.InetAddress;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Connector configuration settings.
 */
public class ChannelConfiguration extends SocketConfiguration {
	/** Name, used in logs */
	@XmlAttribute(name = "name", required = true)
	private String m_name = null;

	/** Port number */
	@XmlAttribute(name = "port", required = true)
	private Integer m_port = null;

	/** SSL enable flag */
	@XmlAttribute(name = "secure")
	private boolean m_secure = false;

	/** SSL private key path */
	@XmlAttribute(name = "privateKey")
	private String m_privateKeyPath = null;

	/** SSL certificate path */
	@XmlAttribute(name = "certificate")
	private String m_certificatePath = null;

	/** Network address */
	@XmlAttribute(name = "address")
	private InetAddress m_address = null;

	@Nullable
	@Contract(pure = true)
	public final String getName() {
		return m_name;
	}

	@Nullable
	@Contract(pure = true)
	public final Integer getPort() {
		return m_port;
	}

	@Contract(pure = true)
	public final boolean isSecure() {
		return m_secure;
	}

	@Nullable
	@Contract(pure = true)
	public final String getPrivateKeyPath() {
		return m_privateKeyPath;
	}

	@Nullable
	@Contract(pure = true)
	public final String getCertificatePath() {
		return m_certificatePath;
	}

	public final InetAddress getAddress() {
		if (m_address == null)
			return InetAddress.getLoopbackAddress();
		return m_address;
	}
}
