package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

import java.nio.file.Path;

import java.net.URI;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import autist.server.application.ResourceRoot;

@XmlAccessorType(XmlAccessType.FIELD)
public class ResourceRootConfiguration<
		EntityType extends ResourceRoot,
		SpecificationType extends ResourceRootSpecification<
				EntityType, ? extends ResourceRootConfiguration<EntityType, SpecificationType>>>
		extends NamedEntityConfiguration<EntityType, SpecificationType>
{
	@Nullable
	@XmlAttribute(name = "uri")
	private URI m_uri = null;

	@Nullable
	@XmlAttribute(name = "path")
	private Path m_path = null;

	@NotNull
	@Contract(pure = true)
	public final Optional<URI> uri() {
		return Optional.ofNullable(m_uri);
	}

	@NotNull
	@Contract(pure = true)
	public Optional<Path> path() {
		return Optional.ofNullable(m_path);
	}
}
