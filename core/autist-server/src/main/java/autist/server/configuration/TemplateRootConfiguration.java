package autist.server.configuration;

import java.util.List;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import autist.server.application.TemplateRoot;

@XmlAccessorType(XmlAccessType.FIELD)
public class TemplateRootConfiguration
		extends ResourceRootConfiguration<TemplateRoot, TemplateRootSpecification>
{
	@XmlType(name = "TemplateRootConfigurationContainer")
	public static class Container
			implements NamedEntityContainer<TemplateRootConfiguration>
	{
		@XmlElement(name = "root", type = TemplateRootConfiguration.class)
		private final List<TemplateRootConfiguration> m_items = new ArrayList<>();

		public List<TemplateRootConfiguration> getItems() {
			return m_items;
		}
	}

	public static class ContainerAdapter
			extends NamedEntityContainerAdapter<Container, TemplateRootConfiguration>
	{
	}
}
