package autist.server.configuration;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import autist.server.application.AssetRoot;

@XmlAccessorType(XmlAccessType.FIELD)
public class AssetRootConfiguration
		extends ResourceRootConfiguration<AssetRoot, AssetRootSpecification>
{
	@XmlType(name = "ResourceRootConfigurationContainer")
	public static class Container
			implements NamedEntityContainer<AssetRootConfiguration>
	{
		@XmlElement(name = "root", type = AssetRootConfiguration.class)
		private final List<AssetRootConfiguration> m_items = new ArrayList<>(0);

		public final List<AssetRootConfiguration> getItems() {
			return m_items;
		}
	}

	public static class ContainerAdapter
			extends NamedEntityContainerAdapter<Container, AssetRootConfiguration>
	{
	}
}
