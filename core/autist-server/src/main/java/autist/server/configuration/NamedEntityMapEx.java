package autist.server.configuration;

import java.util.Collections;
import java.util.Map;

public class NamedEntityMapEx<T extends NamedEntityConfiguration>
	extends NamedEntityMap<T>
{
	private final String m_defaultItemName;

	public NamedEntityMapEx() {
		this(Collections.emptyMap());
	}

	public NamedEntityMapEx(Map<String, T> entityMap) {
		this(entityMap, null);
	}

	public NamedEntityMapEx(
			Map<String, T> entityMap,
			String defaultItemName)
	{
		super(entityMap);
		m_defaultItemName = defaultItemName;
	}

	public String getDefaultItemName() {
		return m_defaultItemName;
	}
}
