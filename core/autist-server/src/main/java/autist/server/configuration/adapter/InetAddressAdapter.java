package autist.server.configuration.adapter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.net.InetAddress;
import java.net.UnknownHostException;

import autist.configuration.ConfigurationAdapter;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class InetAddressAdapter extends ConfigurationAdapter<String, InetAddress> {
	@Override
	@NotNull
	@Contract("_ -> new")
	public InetAddress unmarshal(final @NotNull String address)
			throws UnknownHostException
	{
		return InetAddress.getByName(address);
	}
}
