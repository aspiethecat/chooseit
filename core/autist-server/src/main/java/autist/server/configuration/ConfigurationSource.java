package autist.server.configuration;

import java.util.Objects;
import java.io.InputStream;
import java.nio.file.Path;

import javax.xml.transform.stream.StreamSource;

class ConfigurationSource extends StreamSource {
	private final Path m_path;

	public ConfigurationSource(Path path) {
		super(Objects.requireNonNull(path).toFile());
		m_path = path;
	}

	public ConfigurationSource(Path path, InputStream stream) {
		super(Objects.requireNonNull(stream));
		m_path = Objects.requireNonNull(path);
	}

	public Path getPath() {
		return m_path;
	}

	@Override
	public String toString() {
		return m_path.toString();
	}
}
