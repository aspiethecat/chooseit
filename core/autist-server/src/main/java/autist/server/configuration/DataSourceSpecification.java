package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import autist.server.application.DataSource;

public class DataSourceSpecification
		extends NamedEntitySpecification<DataSource, DataSourceConfiguration>
{
	@XmlAttribute(name = "specification", required = true)
	private String m_specificationPath = null;

	@XmlType(name = "DataSourceSpecificationContainer")
	public static class Container
			implements NamedEntityContainerEx<DataSourceSpecification>
	{
		@Nullable
		@XmlAttribute(name = "default")
		private String m_defaultItemName = null;

		@XmlElement(name = "dataSource", type = DataSourceSpecification.class)
		final List<DataSourceSpecification> m_items = new ArrayList<>();

		@Nullable
		@Contract(pure = true)
		public final String getDefaultItemName() {
			return m_defaultItemName;
		}

		@NotNull
		@Contract(pure = true)
		public final List<DataSourceSpecification> getItems() {
			return m_items;
		}
	}

	public static class ContainerAdapter
			extends NamedEntityContainerAdapterEx<Container, DataSourceSpecification>
	{
	}

	@Override
	public boolean isPreConfigured() {
		return false;
	}

	public String getSpecificationPath() {
		return m_specificationPath;
	}
}
