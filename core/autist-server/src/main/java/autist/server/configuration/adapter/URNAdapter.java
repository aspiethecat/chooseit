package autist.server.configuration.adapter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import autist.commons.net.URN;

import autist.configuration.ConfigurationAdapter;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class URNAdapter extends ConfigurationAdapter<String, URN> {
	@Override
	@NotNull
	@Contract("_ -> new")
	public URN unmarshal(final @NotNull String urn) {
		return new URN(urn);
	}
}
