package autist.server.configuration.adapter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.IllformedLocaleException;
import java.util.Locale;
import java.util.StringTokenizer;

import autist.configuration.ConfigurationAdapter;

@SuppressWarnings({"WeakerAccess", "unused"})
public class LocaleAdapter extends ConfigurationAdapter<String, Locale> {
	@Override
	@NotNull
	@Contract("_ -> new")
	public Locale unmarshal(final @NotNull String localeName)
			throws IllformedLocaleException
	{
		if (localeName.isEmpty())
			return Locale.getDefault();

		final Locale.Builder builder = new Locale.Builder();
		final StringTokenizer tokenizer = new StringTokenizer(localeName, "-");

		builder.setLanguage(tokenizer.nextToken());
		if (tokenizer.hasMoreTokens())
			builder.setRegion(tokenizer.nextToken());
		if (tokenizer.hasMoreTokens())
			builder.setVariant(tokenizer.nextToken());

		return builder.build();
	}
}
