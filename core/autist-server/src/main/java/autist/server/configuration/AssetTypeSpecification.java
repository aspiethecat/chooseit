package autist.server.configuration;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import autist.server.application.AssetBuilder;
import autist.server.application.AssetType;

@XmlAccessorType(XmlAccessType.FIELD)
public final class AssetTypeSpecification
		extends NamedEntitySpecification<AssetType, AssetTypeConfiguration>
{
	@XmlType(name = "ResourceTypeSpecificationContainer")
	public static class Container
			implements NamedEntityContainerEx<AssetTypeSpecification>
	{
		@XmlAttribute(name = "default")
		private String m_defaultItemName = null;

		@XmlElement(name = "type", type = AssetTypeSpecification.class)
		private final List<AssetTypeSpecification> m_items = new ArrayList<>();

		@Override
		public final String getDefaultItemName() {
			return m_defaultItemName;
		}

		@Override
		public List<AssetTypeSpecification> getItems() {
			return m_items;
		}
	}

	public static class ContainerAdapter
			extends NamedEntityContainerAdapterEx<Container, AssetTypeSpecification>
	{
	}

	/**
	 * MIME type.
	 */
	@XmlAttribute(name = "mimeType")
	private String m_mimeType = null;

	/**
	 * Match list.
	 */
	@XmlElement(name = "match")
	private List<AssetMatchSpecification> m_matches = new ArrayList<>(0);

	/**
	 * Builder list.
	 */
	@XmlElement(name = "builder", type = AssetBuilderSpecification.class)
	@XmlJavaTypeAdapter(AssetBuilder.Adapter.class)
	private List<Class<? extends AssetBuilder>> m_builders = new ArrayList<>(0);

	@Override
	public boolean isPreConfigured() {
		return true;
	}

	@Nullable
	public final String getMimeType() {
		return m_mimeType;
	}

	public final List<AssetMatchSpecification> getMatches() {
		return Collections.unmodifiableList(m_matches);
	}

	public final List<Class<? extends AssetBuilder>> getBuilders() {
		return Collections.unmodifiableList(m_builders);
	}
}
