package autist.server.configuration;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import autist.server.application.DataSource;

/**
 * Database connection settings.
 */
public class DataSourceConfiguration
		extends NamedEntityConfiguration<DataSource, DataSourceSpecification>
{
	@XmlType(name = "DataSourceConfigurationContainer")
	public static class Container
			implements NamedEntityContainer<DataSourceConfiguration>
	{
		@XmlElement(name = "dataSource", type = DataSourceConfiguration.class)
		private final List<DataSourceConfiguration> m_items = new ArrayList<>();

		public final List<DataSourceConfiguration> getItems() {
			return m_items;
		}
	}

	public static class ContainerAdapter
			extends NamedEntityContainerAdapter<Container, DataSourceConfiguration>
	{
	}

	/**
	 * Database driver class name.
	 */
	@XmlAttribute(name = "driver")
	private String m_driverClass = null;

	/**
	 * Database URL.
	 */
	@XmlAttribute(name = "url")
	private String m_url = null;

	/**
	 * User name.
	 */
	@XmlAttribute(name = "userName")
	private String m_userName = null;

	/**
	 * Password.
	 */
	@XmlAttribute(name = "password")
	private String m_password = null;

	public final String getDriverClass() {
		return m_driverClass;
	}

	public final String getUrl() {
		return m_url;
	}

	public final String getUserName() {
		return m_userName;
	}

	public final String getPassword() {
		return m_password;
	}
}
