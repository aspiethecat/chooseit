package autist.server.configuration;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

import java.io.FileNotFoundException;

import java.net.URI;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import autist.configuration.ConfigurationException;
import autist.configuration.ConfigurationRoot;

@XmlRootElement(name = "application")
@XmlAccessorType(XmlAccessType.FIELD)
public final class ApplicationSpecification implements ConfigurationRoot {
	@XmlAttribute(name = "routeKeyType", required = true)
	private String m_routeKeyType = null;

	@XmlAttribute(name = "routeTagType", required = true)
	private String m_routeTagType = null;

	/**
	 * Path to routing map file.
	 */
	@XmlAttribute(name = "routingMap", required = true)
	private URI m_routingMapPath = null;

	/**
	 * Data source settings.
	 */
	@NotNull
	@XmlElement(name = "dataSources")
	@XmlJavaTypeAdapter(DataSourceSpecification.ContainerAdapter.class)
	private NamedEntityMapEx<DataSourceSpecification> m_dataSources = new NamedEntityMapEx<>();

	/**
	 * Template settings.
	 */
	@Nullable
	@XmlElement(name = "templates")
	private TemplateFactorySpecification m_templateFactorySpecification = null;

	/**
	 * Resource settings.
	 */
	@Nullable
	@XmlElement(name = "assets")
	private AssetFactorySpecification m_assetFactorySpecification = null;

	@Contract(pure = true)
	public Optional<String> routeKeyType() {
		return Optional.ofNullable(m_routeKeyType);
	}

	@Contract(pure = true)
	public Optional<String> routeTagType() {
		return Optional.ofNullable(m_routeTagType);
	}

	@Contract(pure = true)
	public Optional<URI> routingMapPath() {
		return Optional.ofNullable(m_routingMapPath);
	}

	@NotNull
	@Contract(pure = true)
	public NamedEntityMapEx<DataSourceSpecification> getDataSources() {
		return m_dataSources;
	}

	@NotNull
	@Contract(pure = true)
	public Optional<TemplateFactorySpecification> getTemplateFactorySpecification() {
		return Optional.ofNullable(m_templateFactorySpecification);
	}

	@NotNull
	@Contract(pure = true)
	public Optional<AssetFactorySpecification> getAssetFactorySpecification() {
		return Optional.ofNullable(m_assetFactorySpecification);
	}

	public static ApplicationSpecification load(final @NotNull URI uri)
			throws ConfigurationException
	{
		try {
			return ConfigurationLoader.load(
					ApplicationSpecification.class,
					ConfigurationLoader.createInternalSource(uri));
		} catch (final FileNotFoundException e) {
			throw new ConfigurationException("Can't load application specification", e);
		}
	}
}
