package autist.server.configuration.adapter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

import autist.configuration.ConfigurationAdapter;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class PathAdapter extends ConfigurationAdapter<String, Path> {
	@Override
	@NotNull
	@Contract("_ -> new")
	public Path unmarshal(final @NotNull String path)
			throws InvalidPathException
	{
		return Paths.get(path).toAbsolutePath();
	}
}
