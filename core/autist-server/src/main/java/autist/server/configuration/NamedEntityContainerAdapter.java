package autist.server.configuration;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import autist.configuration.ConfigurationAdapter;

public class NamedEntityContainerAdapter<
		ContainerType extends NamedEntityContainer<EntityType>,
		EntityType extends NamedEntityConfiguration>
		extends ConfigurationAdapter<ContainerType, NamedEntityMap<EntityType>>
{
		@Override
		public NamedEntityMap<EntityType> unmarshal(
				final @NotNull ContainerType container)
				throws Exception
		{
			return new NamedEntityMap<>(buildMap(container.getItems()));
		}

	protected static <
			EntityType extends NamedEntityConfiguration>
	Map<String, EntityType> buildMap(
			final @NotNull List<EntityType> items)
	{
		final int size = items.size();
		final Map<String, EntityType> map = new LinkedHashMap<>(size);

		items.forEach(item -> map.put(item.name(), item));

		return map;
	}
}
