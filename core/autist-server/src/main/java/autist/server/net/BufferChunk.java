package autist.server.net;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

final class BufferChunk {
	private final BufferPool m_pool;
	private char[] m_data;
	private BufferChunk m_next;

	BufferChunk(final int size) {
		m_pool = null;
		m_data = new char[size];
	}

	BufferChunk(BufferPool pool) {
		m_pool = pool;
		m_data = new char[pool.getChunkSize()];
	}

	@Nullable
	@Contract(pure = true)
	BufferPool getPool() {
		return m_pool;
	}

	@NotNull
	@Contract(pure = true)
	char[] getData() {
		return m_data;
	}

	@Nullable
	@Contract(pure = true)
	BufferChunk getNext() {
		return m_next;
	}

	void setNext(@Nullable final BufferChunk next) {
		m_next = next;
	}

	void release() {
		if (m_pool == null)
			throw new IllegalStateException();

		m_pool.releaseChunk(this);
	}
}
