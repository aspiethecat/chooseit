package autist.server.net;

import org.jetbrains.annotations.NotNull;

import java.io.Reader;
import java.io.Writer;

public final class Buffer {
	public static final int DEFAULT_CHUNK_SIZE = 8192;

	private final BufferPool m_pool;
	private final int m_chunkSize;

	private int m_writeOffset = 0;
	private int m_readOffset = 0;
	private BufferChunk m_firstChunk = null;
	private BufferChunk m_lastChunk = null;

	public Buffer() {
		this(DEFAULT_CHUNK_SIZE);
	}

	public Buffer(final int chunkSize) {
		if (chunkSize <= 0)
			throw new IllegalArgumentException();

		m_pool = null;
		m_chunkSize = chunkSize;
	}

	public Buffer(final @NotNull BufferPool pool) {
		m_pool = pool;
		m_chunkSize = pool.getChunkSize();
	}

	public Writer getWriter() {
		return new WriterImpl();
	}

	public Reader getReader() {
		return new ReaderImpl();
	}

	private class WriterImpl extends Writer {
		@Override
		public void write(final @NotNull char[] data, int offset, int length) {
			while (length > 0) {
				int delta = Math.min(length, m_chunkSize);

				System.arraycopy(data, offset, getChunk().getData(), m_writeOffset, delta);

				length -= delta;
				offset += delta;

				m_writeOffset += delta;
			}
		}

		@Override
		public void write(final @NotNull String string, int offset, int length) {
			while (length > 0) {
				final int delta = Math.min(length, m_chunkSize);

				string.getChars(offset, offset + delta, getChunk().getData(), m_writeOffset);

				length -= delta;
				offset += delta;

				m_writeOffset += delta;
			}
		}

		private BufferChunk getChunk() {
			BufferChunk chunk = m_lastChunk;

			if ((chunk == null) || (m_writeOffset == m_chunkSize)) {
				chunk = new BufferChunk(m_chunkSize);

				if (m_lastChunk == null)
					m_firstChunk = chunk;
				else
					m_lastChunk.setNext(chunk);

				m_lastChunk = chunk;
				m_writeOffset = 0;
			}

			return m_lastChunk;
		}

		@Override
		public void flush() {
		}

		@Override
		public void close() {
		}
	}

	private class ReaderImpl extends Reader {
		@Override
		public int read(final @NotNull char[] data, int offset, int length) {
			int bytesRead = 0;

			while (length > 0) {
				final int remaining;
				if (m_firstChunk != m_lastChunk)
					remaining = m_chunkSize - m_readOffset;
				else
					remaining = m_writeOffset - m_readOffset;

				final int delta = Math.min(length, remaining);

				System.arraycopy(m_firstChunk.getData(), m_readOffset, data, offset, delta);

				offset += delta;
				length -= delta;

				m_readOffset += delta;
				if (m_readOffset == m_chunkSize) {
					final BufferChunk chunk = m_firstChunk;

					m_firstChunk = chunk.getNext();
					if (m_firstChunk == null)
						m_lastChunk = null;
					chunk.setNext(null);

					m_readOffset = 0;
				}

				bytesRead += delta;
			}

			return bytesRead;
		}

		@Override
		public void close() {
		}
	}
}
