package autist.server.net;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public final class BufferPool {
	public static final int DEFAULT_MAX_FREE_CHUNK_COUNT = 32;

	private final int m_chunkSize;
	private final int m_maxFreeChunkCount;

	private BufferChunk m_free;
	private int m_totalChunkCount = 0;
	private int m_freeChunkCount = 0;

	public BufferPool() {
		this(Buffer.DEFAULT_CHUNK_SIZE, DEFAULT_MAX_FREE_CHUNK_COUNT);
	}

	public BufferPool(final int chunkSize) {
		this(chunkSize, DEFAULT_MAX_FREE_CHUNK_COUNT);
	}

	public BufferPool(final int chunkSize, final int maxFreeChunkCount) {
		if ((chunkSize <= 0) || (maxFreeChunkCount <= 0))
			throw new IllegalArgumentException();

		m_chunkSize = chunkSize;
		m_maxFreeChunkCount = maxFreeChunkCount;
	}

	@Contract(pure = true)
	public int getChunkSize() {
		return m_chunkSize;
	}

	@Contract(pure = true)
	public int getMaxFreeChunkCount() {
		return m_maxFreeChunkCount;
	}

	@Contract(pure = true)
	public int getTotalChunkCount() {
		return m_totalChunkCount;
	}

	@Contract(pure = true)
	public int getFreeChunkCount() {
		return m_freeChunkCount;
	}

	public BufferChunk acquireChunk() {
		final BufferChunk chunk;

		if (m_freeChunkCount == 0) {
			chunk = new BufferChunk(this);
			m_totalChunkCount++;
		} else {
			chunk = m_free;
			m_free = chunk.getNext();
			chunk.setNext(null);
			m_freeChunkCount--;
		}

		return chunk;
	}

	void releaseChunk(final @NotNull BufferChunk chunk) {
		if (chunk.getPool() != this)
			throw new IllegalArgumentException();

		if (m_freeChunkCount < m_maxFreeChunkCount) {
			chunk.setNext(m_free);
			m_freeChunkCount++;
		} else {
			chunk.setNext(null);
		}
	}
}
