package autist.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class HttpDateFormat {
	public static final String FORMAT = "EEE, dd MMM yyyy HH:mm:ss zzz";
	public static final TimeZone TIMEZONE = TimeZone.getTimeZone("GMT");

	private static final ThreadLocal<DateFormat> format = ThreadLocal.withInitial(() -> {
		DateFormat format = new SimpleDateFormat(FORMAT, Locale.US);
		format.setTimeZone(TIMEZONE);
		return format;
	});

	@Nullable
	public static Date parse(final @NotNull String source) {
		Date date = null;

		try {
			date = format.get().parse(source);
		} catch (ParseException ignored) { }

		return date;
	}

	public static String format() {
		return format(null);
	}

	public static String format(@Nullable Date date) {
		return format.get().format((date == null) ?
				(new GregorianCalendar()).getTime() :
				date);
	}
}
