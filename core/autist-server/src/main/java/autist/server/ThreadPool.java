package autist.server;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.logging.Level;

import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;

import autist.journal.Journal;
import autist.journal.JournalType;
import autist.server.configuration.CoreConfiguration;

/**
 * Server thread pool.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class ThreadPool {
	private static final Journal s_journal = Journal.get(JournalType.SERVER);

	/**
	 * Boss thread group.
	 */
	@NotNull
	private final EventLoopGroup m_bossGroup;

	/**
	 * Worker thread group.
	 */
	@NotNull
	private final EventLoopGroup m_workerGroup;

	private ThreadPool(final @NotNull CoreConfiguration configuration) {
		m_bossGroup = configuration.bossThreadCount()
				.map(NioEventLoopGroup::new)
				.orElseGet(NioEventLoopGroup::new);

		m_workerGroup = configuration.workerThreadCount()
				.map(NioEventLoopGroup::new)
				.orElseGet(NioEventLoopGroup::new);
	}

	@NotNull
	@Contract(pure = true)
	public EventLoopGroup bossGroup() {
		return m_bossGroup;
	}

	@NotNull
	@Contract(pure = true)
	public EventLoopGroup workerGroup() {
		return m_workerGroup;
	}

	public void shutdownGracefully() {
		m_workerGroup.shutdownGracefully();
		m_bossGroup.shutdownGracefully();
	}

	@NotNull
	@Contract("_ -> new")
	public static ThreadPool createInstance(final @NotNull CoreConfiguration configuration) {
		s_journal.log(Level.INFO, "Creating thread pool...");

		if (s_journal.isLoggable(Level.CONFIG)) {
			s_journal.log(Level.CONFIG, String.format(
					"Boss threads: %s",
					configuration.bossThreadCount().map(Number::toString).orElse("default")));
			s_journal.log(Level.CONFIG, String.format(
					"Worker threads: %s",
					configuration.workerThreadCount().map(Number::toString).orElse("default")));
		}

		return new ThreadPool(configuration);
	}
}
