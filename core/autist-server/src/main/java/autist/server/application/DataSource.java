package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.logging.Level;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import autist.commons.reflection.ClassUtil;
import autist.commons.reflection.InvocationException;
import autist.configuration.ConfigurationException;

import autist.server.configuration.DataSourceConfiguration;
import autist.server.configuration.DataSourceSpecification;
import autist.server.configuration.NamedEntity;

public class DataSource extends NamedEntity {
	private final SqlSessionFactory m_factory;

	private DataSource(
			final @NotNull ApplicationContext context,
			final @NotNull DataSourceSpecification specification)
			throws ConfigurationException
	{
		super(specification.name());

		final DataSourceConfiguration configuration = specification
				.configuration()
				.orElseThrow(AssertionError::new);

		final String driverClass = configuration.getDriverClass();
		final String url         = configuration.getUrl();
		final String userName    = configuration.getUserName();
		final String password    = configuration.getPassword();

		context.log(Level.INFO, String.format(
				"Creating [%s] data source...",
				specification.name()));

		if (context.isLoggable(Level.CONFIG)) {
			context.log(Level.CONFIG, String.format("Driver: %s", driverClass));
			context.log(Level.CONFIG, String.format("URL: %s", url));
			context.log(Level.CONFIG, String.format("Username: %s", userName));
			context.log(Level.CONFIG, String.format(
					"Password: %s",
					(password != null) && (!password.isEmpty()) ?
							"[hidden]" :
							"[isAbsent]"));
		}

		registerDriver(driverClass);

		final Properties properties = new Properties();

		properties.setProperty("driver", driverClass);
		properties.setProperty("url", url);
		properties.setProperty("username", userName);
		properties.setProperty("password", password);

		final SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();

		m_factory = builder.build(
				getClass().getResourceAsStream(
						specification.getSpecificationPath()),
				context.environmentType().name().toLowerCase(),
				properties);
	}

	private static void registerDriver(
			final @NotNull String className)
			throws ConfigurationException
	{
		Objects.requireNonNull(className);

		final Class<?> driverClass;
		try {
			driverClass = Class.forName(className);
		} catch (ClassNotFoundException e) {
			throw new ConfigurationException(
					String.format("Database driver class not found: %s", className),
					e);
		}

		if (!Driver.class.isAssignableFrom(driverClass)) {
			throw new ConfigurationException(
					String.format("Invalid database driver class: %s", className));
		}

		try {
			DriverManager.registerDriver((Driver) ClassUtil.createInstance(driverClass));
		} catch (final InvocationException | SQLException e) {
			throw new ConfigurationException(
					String.format("Can't register database driver class: %s", className),
					e);
		}
	}

	static List<DataSource> buildList(
			final @NotNull ApplicationContext context,
			final @NotNull Map<String, DataSourceSpecification> specificationMap,
			final @NotNull Map<String, DataSourceConfiguration> configurationMap)
			throws ConfigurationException
	{
		return buildList(
				context, DataSource.class, DataSource::configure, specificationMap, configurationMap);
	}

	@NotNull
	@Contract("_, _ -> new")
	private static DataSource configure(
			final @NotNull ApplicationContext context,
			final @NotNull DataSourceSpecification specification)
			throws ConfigurationException
	{
		return new DataSource(context, specification);
	}

	SqlSession openSession() {
		return m_factory.openSession();
	}
}
