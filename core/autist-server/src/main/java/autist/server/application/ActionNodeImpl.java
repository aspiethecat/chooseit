package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import autist.commons.net.URN;

import autist.configuration.ConfigurationException;
import autist.configuration.ConfigurationRoot;
import autist.configuration.adapter.AbstractAdapter;

import autist.application.action.ActionInvocation;

import autist.server.configuration.NamedEntityConfiguration;

@XmlRootElement(name = "action")
public final class ActionNodeImpl
		extends NamedEntityConfiguration
		implements ActionNode, ConfigurationRoot
{
	/**
	 * Action invocation descriptor.
	 * @see #action()
	 */
	private @Nullable ActionInvocation<?, ?, ?> m_action = null;


	@Override
	@Contract(pure = true)
	public @NotNull ActionInvocation<?, ?, ?> action() {
		return m_action;
	}

	@Override
	@Contract(pure = true)
	public @NotNull Collection<ActionInvocation<?, ?, ?>> filters() {
		return m_filters;
	}

	private void bind(final @NotNull ActionNodeImpl parent) {
		if (m_parent != null)
			throw new IllegalStateException("Action node parent already bound");

		m_parent = parent;
	}

	private void configure(
			final @NotNull Set<ActionInvocation<?, ?, ?>> actions,
			final @NotNull List<ConfigurationException> errors)
	{
		if (Objects.nonNull(m_action))
			throw new IllegalStateException("Action node already configured");

		m_urn = buildURN(m_parent, name());

		if (Objects.nonNull(m_parent)) {
			m_classNamePrefix = buildClassNamePrefix(m_parent.m_classNamePrefix, m_classNamePrefix);
			m_filters.addAll(m_parent.filters());
		} else {
			m_classNamePrefix = buildClassNamePrefix(m_classNamePrefix);
		}

		try {
			if (m_className != null) {
				String actionClass = m_className;
				if (m_classNamePrefix != null)
					actionClass = String.join(
							".", m_classNamePrefix, m_className);

				m_action = ActionInvocationImpl.create(actionClass);
			} else if (m_templateName != null) {
				m_action = ActionInvocationImpl.create(TemplateHandler.class);
			}

			actions.add(m_action);
		} catch (final ConfigurationException e) {
			errors.add(e);
		}

		m_filterClassNames.forEach((item) -> {
			try {
				final ActionInvocationImpl<?, ?, ?> filter =
						ActionInvocationImpl.create(item);

				m_filters.add(filter);
				actions.add(filter);
			} catch (final ConfigurationException e) {
				errors.add(e);
			}
		});

		m_children.forEach((item) -> {
			item.configure(actions, errors);
			m_childrenMap.put(item.name(), item);
		});
	}

	@NotNull
	@Contract("_, _ -> new")
	private static URN buildURN(
			@Nullable final ActionNode parent,
			final @NotNull String name)
	{
		if (Objects.isNull(parent))
			return new URN("/");

		return parent.urn().append(name);
	}

	private static @Nullable String buildClassNamePrefix(
			final @Nullable String prefix)
	{
		return buildClassNamePrefix(null, prefix);
	}

	private static @Nullable String buildClassNamePrefix(
			final @Nullable String parentPrefix,
			final @Nullable String prefix)
	{
		if (Objects.isNull(prefix) || prefix.isEmpty())
			return parentPrefix;

		if (prefix.startsWith(".")) {
			if (prefix.length() > 1)
				return prefix.substring(1);

			return null;
		}

		if (Objects.isNull(parentPrefix))
			return prefix;

		return String.join(".", parentPrefix, prefix);
	}

	public static class Filter {
		@XmlAttribute(name = "handler")
		private String m_handler = null;

		@Nullable
		@Contract(pure = true)
		public String getHandler() {
			return m_handler;
		}
	}

	public static class FilterAdapter extends AbstractAdapter<Filter, String> {
		@Override
		public String unmarshal(final @NotNull Filter filter) {
			return filter.getHandler();
		}
	}
}
