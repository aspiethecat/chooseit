package autist.server.application.routing;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;

import autist.commons.util.EnumResolver;

import autist.application.routing.RouteKey;
import autist.application.routing.RouteTag;
import autist.application.routing.RoutingContext;

@SuppressWarnings({"WeakerAccess", "unused"})
final class DefaultRoutingContext<
		KeyType extends Enum<KeyType> & RouteKey<KeyType>,
		TagType extends Enum<TagType> & RouteTag<TagType>>
		implements RoutingContext<DefaultRoutingContext<KeyType, TagType>, KeyType, TagType>
{
	@NotNull
	private final Class<KeyType> m_keyClass;

	@NotNull
	private final EnumResolver<String, KeyType> m_keyResolver;

	@NotNull
	private final Class<TagType> m_tagClass;

	@NotNull
	private final EnumResolver<String, TagType> m_tagResolver;

	DefaultRoutingContext(
			final @NotNull Class<KeyType> keyClass,
			final @NotNull Class<TagType> tagClass)
	{
		Objects.requireNonNull(keyClass, "keyClass == null");
		Objects.requireNonNull(tagClass, "tagClass == null");

		m_keyClass = keyClass;
		m_keyResolver = new EnumResolver<>(m_keyClass);
		m_tagClass = tagClass;
		m_tagResolver = new EnumResolver<>(m_tagClass);
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Class<KeyType> keyClass() {
		return m_keyClass;
	}

	@NotNull
	@Contract(pure = true)
	public Optional<KeyType> resolveKey(final @NotNull String key) {
		Objects.requireNonNull(key, "key == null");
		return m_keyResolver.resolve(key);
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Class<TagType> tagClass() {
		return m_tagClass;
	}

	@NotNull
	@Contract(pure = true)
	public Optional<TagType> resolveTag(final @NotNull String tag) {
		Objects.requireNonNull(tag, "tag == null");
		return m_tagResolver.resolve(tag);
	}
}
