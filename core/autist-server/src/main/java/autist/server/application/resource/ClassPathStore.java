package autist.server.application.resource;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;

import java.io.IOException;
import java.nio.file.NoSuchFileException;

import java.net.URI;

import autist.commons.net.URN;

import autist.application.resource.Resource;

public final class ClassPathStore extends AbstractResourceStore {
	public ClassPathStore(
			final @NotNull String name,
			final @NotNull URI uri) {
		super(
				StoreType.CLASSPATH,
				Objects.requireNonNull(name, "name"),
				Objects.requireNonNull(uri, "uri"));
	}

	@Override
	@Contract(pure = true)
	public boolean isCacheRequired() {
		return true;
	}

	@Override
	@NotNull
	@Contract("_ -> new")
	public Optional<Resource> fetch(
			final @NotNull URN urn)
			throws IOException
	{
		Objects.requireNonNull(urn, "urn");

		try {
			return Optional.of(new ClassPathResource(this, urn));
		} catch (final NoSuchFileException e) {
			return Optional.empty();
		}
	}

	@Override
	@NotNull
	@Contract("_, _ -> fail")
	public Optional<Resource> store(
			final @NotNull URN urn,
			final @NotNull Resource source)
	{
		throw new UnsupportedOperationException();
	}
}
