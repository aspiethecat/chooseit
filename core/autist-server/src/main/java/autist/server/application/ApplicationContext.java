package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.logging.Level;

import autist.commons.context.AbstractContextRoot;

import autist.journal.Journal;
import autist.journal.JournalContext;
import autist.journal.JournalType;

import autist.application.EnvironmentType;
import autist.application.routing.RoutingContext;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class ApplicationContext
		extends AbstractContextRoot<ApplicationContext>
		implements JournalContext
{
	@NotNull
	private final String m_name;

	@NotNull
	private final EnvironmentType m_environmentType;

	@NotNull
	private final RoutingContext<?, ?, ?> m_routingContext;

	@NotNull
	private final Journal m_journal;

	ApplicationContext(
			final @NotNull String name,
			final @NotNull EnvironmentType environmentType,
			final @NotNull RoutingContext<?, ?, ?>  routingContext)
	{
		m_name = name;
		m_environmentType = environmentType;
		m_routingContext = routingContext;

		m_journal = new Journal(JournalType.APPLICATION, this);
	}

	@NotNull
	@Contract(pure = true)
	public String name() {
		return m_name;
	}

	@NotNull
	@Contract(pure = true)
	public EnvironmentType environmentType() {
		return m_environmentType;
	}

	@NotNull
	@Contract(pure = true)
	public RoutingContext<?, ?, ?> routingContext() {
		return m_routingContext;
	}

	public boolean isLoggable(final @NotNull Level level) {
		return m_journal.isLoggable(level);
	}

	public void log(
			final @NotNull Level level,
			final @NotNull String message)
	{
		m_journal.log(level, message);
	}

	public void log(
			final @NotNull Level level,
			final @NotNull String message,
			final @NotNull Throwable cause)
	{
		m_journal.log(level, message, cause);
	}
}
