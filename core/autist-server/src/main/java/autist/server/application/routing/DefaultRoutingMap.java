package autist.server.application.routing;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import autist.commons.net.URN;

import autist.application.routing.RouteKey;
import autist.application.routing.RouteTag;
import autist.application.routing.RoutingContext;
import autist.application.routing.RoutingMap;
import autist.application.routing.RoutingNode;

@SuppressWarnings({"WeakerAccess", "unused"})
final class DefaultRoutingMap<
		NodeType extends RoutingNode<NodeType, ContextType, KeyType, TagType>,
		ContextType extends RoutingContext<ContextType, KeyType, TagType>,
		KeyType extends Enum<KeyType> & RouteKey<KeyType>,
		TagType extends Enum<TagType> & RouteTag<TagType>>
		implements RoutingMap<
				DefaultRoutingMap<NodeType, ContextType, KeyType, TagType>,
				NodeType,
				ContextType,
				KeyType,
				TagType>
{
	private static final int DEFAULT_CAPACITY = 128;

	@NotNull
	private final Map<KeyType, NodeType> m_routeKeyMap = new ConcurrentHashMap<>();

	@NotNull
	private final Map<TagType, List<NodeType>> m_routeTagMap = new ConcurrentHashMap<>();

	@NotNull
	private final ContextType m_context;

	@NotNull
	private final NodeType m_root;

	DefaultRoutingMap(
			final @NotNull ContextType context,
			final @NotNull NodeType root)
	{
		Objects.requireNonNull(context, "context == null");
		Objects.requireNonNull(root, "root == null");

		m_context = context;
		m_root = root;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public ContextType context() {
		return m_context;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public NodeType root() {
		return m_root;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Optional<NodeType> find(final @NotNull KeyType key) {
		Objects.requireNonNull(key, "key == null");
		return Optional.ofNullable(m_routeKeyMap.get(key));
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Optional<NodeType> find(final @NotNull URN urn) {
		Objects.requireNonNull(urn, "urn == null");

		if (urn.isEmpty())
			return Optional.of(m_root);

		int depth = 0;
		var result = Optional.of(m_root);

		for (final var key : urn) {
			result = result.flatMap(itemNode -> itemNode.findChild(key));
			if (!result.isPresent())
				break;

			depth++;
		}

		if (depth < urn.componentCount())
			return Optional.empty();

		return result;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public List<NodeType> findAll(final @NotNull TagType tag) {
		Objects.requireNonNull(tag, "tag == null");

		return Optional.ofNullable(m_routeTagMap.get(tag))
				.map(Collections::unmodifiableList)
				.orElseGet(Collections::emptyList);
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Iterator<NodeType> iterator() {
		return new MapIterator();
	}

	private final class MapIterator implements Iterator<NodeType> {
		@Nullable
		private NodeIterator m_iterator = new NodeIterator();

		@Override
		public boolean hasNext() {
			while (Objects.nonNull(m_iterator)) {
				if (m_iterator.hasNext())
					return true;

				m_iterator = m_iterator.parent();
			}

			return false;
		}

		@Override
		@NotNull
		public NodeType next() throws NoSuchElementException {
			while (Objects.nonNull(m_iterator)) {
				if (m_iterator.hasNext())
					return m_iterator.next();

				m_iterator = m_iterator.parent();
			}

			throw new NoSuchElementException();
		}
	}

	private final class NodeIterator implements Iterator<NodeType> {
		@Nullable
		private final NodeIterator m_parent;

		@NotNull
		private final NodeType m_node;

		@Nullable
		private Iterator<NodeType> m_iterator = null;

		NodeIterator() {
			m_parent = null;
			m_node = m_root;
		}

		NodeIterator(
				final @NotNull NodeIterator parent,
				final @NotNull NodeType node)
		{
			m_parent = parent;
			m_node = node;
		}

		@Nullable
		@Contract(pure = true)
		public NodeIterator parent() {
			return m_parent;
		}

		@Override
		public boolean hasNext() {
			return Objects.isNull(m_iterator) || m_iterator.hasNext();
		}

		@Override
		@NotNull
		public NodeType next() throws NoSuchElementException {
			if (Objects.isNull(m_iterator)) {
				m_iterator = m_node.iterator();
				return m_node;
			}

			if (!m_iterator.hasNext())
				throw new NoSuchElementException();

			return m_iterator.next();
		}
	}
}
