package autist.server.application.view;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import java.nio.charset.Charset;

import io.netty.handler.codec.http.HttpResponseStatus;

import autist.application.Request;
import autist.application.Response;
import autist.application.handler.HandlerContext;
import autist.application.view.AbstractView;
import autist.application.view.ViewException;

import autist.server.HttpException;

public final class ErrorView extends AbstractView {
	private final HttpException m_error;

	public ErrorView(final @NotNull HttpException error) {
		m_error = Objects.requireNonNull(error, "error == null");
	}

	@Override
	public void render(
			final @NotNull HandlerContext context,
			final @NotNull Request request,
			final @NotNull Response response)
			throws ViewException
	{
		super.render(context, request, response);

		renderFallback(response, m_error);
	}

	public static void renderFallback(
			final @NotNull Response response,
			final @NotNull HttpException error)
	{
		final Charset charset = Charset.defaultCharset();
		final HttpResponseStatus status = error.getStatus();

		response.setContentType(String.format("text/plain; charset=%s", charset.name()));
		response.setContent(
				String.format("%d %s\n", status.code(), status.reasonPhrase()),
				charset);
	}
}
