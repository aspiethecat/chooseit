package autist.server.application.view.template;

import java.util.Date;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;

import freemarker.template.AdapterTemplateModel;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.WrappingTemplateModel;

public class LocalTimeWrapper
		extends WrappingTemplateModel
		implements TemplateDateModel, AdapterTemplateModel
{
	private final LocalTime m_time;

	public LocalTimeWrapper(
			final LocalTime time,
			final ObjectWrapper wrapper)
	{
		super(wrapper);
		m_time = time;
	}

	@Override
	public Object getAdaptedObject(Class<?> hint) {
		return m_time;
	}

	@Override
	public Date getAsDate() throws TemplateModelException {
		return Date.from(m_time
				.atDate(LocalDate.ofEpochDay(0))
				.atZone(ZoneOffset.systemDefault())
				.toInstant());
	}

	@Override
	public int getDateType() {
		return TIME;
	}
}
