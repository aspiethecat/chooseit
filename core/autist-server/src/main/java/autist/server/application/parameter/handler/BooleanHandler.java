package autist.server.application.parameter.handler;

import org.jetbrains.annotations.NotNull;

public class BooleanHandler implements TypeHandler<Boolean> {
	@Override
	public Boolean parse(
			final @NotNull Class<Boolean> actualType,
			final @NotNull String value)
	{
		return Boolean.parseBoolean(value);
	}
}
