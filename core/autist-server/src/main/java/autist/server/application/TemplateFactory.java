package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateNotFoundException;

import autist.commons.net.URN;
import autist.configuration.ConfigurationException;

import autist.application.resource.Resource;
import autist.application.resource.ResourceStore;

import autist.server.configuration.TemplateFactoryConfiguration;
import autist.server.configuration.TemplateFactorySpecification;

import autist.server.application.resource.AbstractResourceStore;
import autist.server.application.view.template.ObjectWrapperEx;

public final class TemplateFactory {
	private final Configuration m_configuration;

	private TemplateFactory(
			final @NotNull Collection<TemplateRoot> templateRoots,
			final @NotNull Locale defaultLocale,
			final @NotNull Charset defaultEncoding)
			throws ConfigurationException
	{
		if (templateRoots.isEmpty())
			throw new ConfigurationException("Empty template roots list");

		final List<TemplateLoader> templateLoaderList = new LinkedList<>();
		final ConfigurationException error = templateRoots.stream()
				.map((root) -> {
					try {
						templateLoaderList.add(
								new Loader(AbstractResourceStore.create(root.name(), root.uri())));
					} catch (final IllegalArgumentException | IOException e) {
						return new ConfigurationException(
								String.format("Template loader initialization failed: %s", root.uri()),
								e);
					}
					return null;
				})
				.filter(Objects::nonNull)
				.findFirst()
				.orElse(null);

		if (error != null)
			throw error;

		TemplateLoader templateLoader = new MultiTemplateLoader(
				templateLoaderList.toArray(new TemplateLoader[0]));

		m_configuration = new Configuration(Configuration.VERSION_2_3_23);
		m_configuration.setDefaultEncoding(defaultEncoding.name());
		m_configuration.setLocale(defaultLocale);
		m_configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		m_configuration.setTemplateLoader(templateLoader);
		m_configuration.setObjectWrapper(
				new ObjectWrapperEx(m_configuration.getIncompatibleImprovements()));
	}

	@NotNull
	public Optional<Template> getTemplate(
			final @NotNull String name)
			throws IOException
	{
		Template template = null;
		try {
			template = m_configuration.getTemplate(name);
		} catch (final TemplateNotFoundException ignore) { }

		return Optional.ofNullable(template);
	}

	@NotNull
	public Optional<Template> getTemplate(
			final @NotNull String name,
			final @NotNull Locale locale)
			throws IOException
	{
		Template template = null;
		try {
			template = m_configuration.getTemplate(name, locale);
		} catch (final TemplateNotFoundException ignore) { }

		return Optional.ofNullable(template);

	}

	@NotNull
	@Contract("_, _, _ -> new")
	public static TemplateFactory build(
			final @NotNull ApplicationContext context,
			final @NotNull TemplateFactorySpecification specification,
			final @NotNull TemplateFactoryConfiguration configuration)
			throws ConfigurationException
	{
		return new TemplateFactory(
				TemplateRoot.buildList(
						context,
						specification.getRoots(),
						configuration.getRoots()),
				specification.getDefaultLocale(),
				specification.getDefaultEncoding());
	}

	private class Loader implements TemplateLoader {
		@NotNull
		private ResourceStore m_store;

		Loader(final @NotNull ResourceStore store) {
			m_store = store;
		}

		@Override
		@NotNull
		public Object findTemplateSource(
				final @NotNull String urn)
				throws IOException
		{
			try {
				return m_store
						.fetch(new URN(urn))
						.orElseThrow(() -> new FileNotFoundException(urn));
			} catch (final IOException e) {
				throw new IOException(e);
			}
		}

		@Override
		public long getLastModified(
				final @NotNull Object source)
				throws IllegalArgumentException
		{
			if (source instanceof Resource)
				return ((Resource) source).attributes().lastModifiedTime().toMillis();

			throw new IllegalArgumentException(
					String.format("Invalid template object class: %s", source.getClass()));
		}

		@Override
		public Reader getReader(
				final @NotNull Object source,
				final @NotNull String charsetName)
				throws IOException
		{
			if (!Resource.class.isAssignableFrom(source.getClass())) {
				throw new IllegalArgumentException(
						String.format("Invalid template object class: %s", source.getClass()));
			}

			return new InputStreamReader(
					((Resource) source).inputStream(), charsetName);
		}

		@Override
		public void closeTemplateSource(Object source)
				throws IOException
		{
			if (!Resource.class.isAssignableFrom(source.getClass()))
				throw new IllegalArgumentException(
						String.format("Invalid template object class: %s", source.getClass()));

			((Resource) source).inputStream().close();
		}
	}
}
