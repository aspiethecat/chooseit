package autist.server.application;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

import java.net.URI;

import autist.configuration.ConfigurationException;

import autist.server.configuration.NamedEntity;
import autist.server.configuration.TemplateRootConfiguration;
import autist.server.configuration.TemplateRootSpecification;

public final class TemplateRoot extends ResourceRoot {
	private TemplateRoot(
			final @NotNull String name,
			final @NotNull URI uri)
			throws ConfigurationException
	{
		super(name, uri);
	}

	public static List<TemplateRoot> buildList(
			final @NotNull ApplicationContext context,
			final @NotNull Map<String, TemplateRootSpecification> specificationMap,
			final @NotNull Map<String, TemplateRootConfiguration> configurationMap)
			throws ConfigurationException
	{
		return NamedEntity.buildList(
				context,
				TemplateRoot.class,
				TemplateRoot::configure,
				specificationMap,
				configurationMap);
	}

	private static TemplateRoot configure(
			final @NotNull ApplicationContext context,
			final @NotNull TemplateRootSpecification specification)
			throws ConfigurationException
	{
		return new TemplateRoot(
				specification.name(),
				configureURI(TemplateRoot.class, specification));
	}
}
