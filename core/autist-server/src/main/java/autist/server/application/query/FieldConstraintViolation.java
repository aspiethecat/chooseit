package autist.server.application.query;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import javax.validation.ConstraintViolation;
import javax.validation.Path;
import javax.validation.metadata.ConstraintDescriptor;

import autist.application.parameter.Parameters;

public final class FieldConstraintViolation<Q extends Parameters<Q>>
		implements ConstraintViolation<Q>
{
	private final FieldValidator<Q> m_parent;
	private final Class<Q> m_queryClass;

	private ConstraintDescriptor<?> m_descriptor = null;
	private Q m_query = null;
	private Object m_invalidValue = null;
	private String m_message = null;

	private FieldConstraintViolation(
			final @NotNull FieldValidator<Q> parent,
			final @NotNull Class<Q> queryClass)
	{
		m_parent = parent;
		m_queryClass = queryClass;
	}

	@Override
	public @Nullable ConstraintDescriptor<?> getConstraintDescriptor() {
		return m_descriptor;
	}

	@Override
	public <U> U unwrap(Class<U> aClass) {
		return null;
	}

	@Override
	public Q getRootBean() {
		return m_query;
	}

	@Override
	public Class<Q> getRootBeanClass() {
		return m_queryClass;
	}

	@Override
	public Object getLeafBean() {
		return m_query;
	}

	@Override
	public Object[] getExecutableParameters() {
		return new Object[0];
	}

	@Override
	public Object getExecutableReturnValue() {
		return null;
	}

	@Override
	public Path getPropertyPath() {
		return m_parent.getPropertyPath();
	}

	@Override
	public @Nullable Object getInvalidValue() {
		return m_invalidValue;
	}

	@Override
	public String getMessage() {
		return m_message;
	}

	@Override
	public String getMessageTemplate() {
		return (String) m_descriptor.getAttributes().get("message");
	}

	private void setQuery(final Q query) {
		m_query = Objects.requireNonNull(query);
	}

	private void setDescriptor(final ConstraintDescriptor<?> descriptor) {
		m_descriptor = Objects.requireNonNull(descriptor);
	}

	private void setInvalidValue(final Object invalidValue) {
		m_invalidValue = invalidValue;
	}

	private void setMessage(final String message) {
		m_message = Objects.requireNonNull(message);
	}

	private void validate() {
		if (m_query == null)
			throw new IllegalStateException("Query is not set");
		if (m_message == null)
			throw new IllegalStateException("Message is not set");
	}

	public static final class Builder<T extends Parameters<T>> {
		private final FieldValidator<T> m_parent;
		private final Class<T> m_queryClass;

		private FieldConstraintViolation<T> m_instance = null;

		public Builder(
				final FieldValidator<T> parent,
				final Class<T> queryClass)
		{
			m_parent = Objects.requireNonNull(parent);
			m_queryClass = Objects.requireNonNull(queryClass);

			reset();
		}

		private FieldConstraintViolation<T> reset() {
			final FieldConstraintViolation<T> old = m_instance;

			m_instance = new FieldConstraintViolation<>(
					m_parent, m_queryClass);

			return old;
		}

		public Builder<T> query(final T query) {
			m_instance.setQuery(Objects.requireNonNull(query));
			return this;
		}

		public Builder<T> descriptor(final ConstraintDescriptor<?> descriptor) {
			m_instance.setDescriptor(Objects.requireNonNull(descriptor));
			return this;
		}

		public Builder<T> invalidValue(final Object invalidValue) {
			m_instance.setInvalidValue(invalidValue);
			return this;
		}

		public Builder<T> message(final String message) {
			m_instance.setMessage(Objects.requireNonNull(message));
			return this;
		}

		public FieldConstraintViolation<T> build() {
			m_instance.validate();
			return reset();
		}
	}
}
