package autist.server.application.view.template;

import java.util.Arrays;
import java.util.IdentityHashMap;
import java.util.Map;

import java.util.function.BiFunction;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import freemarker.template.DefaultObjectWrapper;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.Version;

public class ObjectWrapperEx extends DefaultObjectWrapper {
	private enum Types {
		LOCAL_TIME(LocalTime.class, LocalTimeWrapper::new),
		LOCAL_DATE(LocalDate.class, LocalDateWrapper::new),
		LOCAL_DATE_TIME(LocalDateTime.class, LocalDateTimeWrapper::new);

		private static Map<Class<?>, BiFunction<?, ObjectWrapper, TemplateModel>>
				s_typeMap = new IdentityHashMap<>();

		static {
			Arrays.stream(values()).forEach(
					type -> s_typeMap.put(type.m_type, type.m_constructor));
		}

		private final Class<?> m_type;
		private final BiFunction<?, ObjectWrapper, TemplateModel> m_constructor;

		<T> Types(
				final Class<T> type,
				final BiFunction<T, ObjectWrapper, TemplateModel> constructor)
		{
			m_type = type;
			m_constructor = constructor;
		}

		@SuppressWarnings("unchecked")
		static BiFunction<Object, ObjectWrapper, TemplateModel> get(final Class<?> type) {
			return (BiFunction<Object, ObjectWrapper, TemplateModel>) s_typeMap.get(type);
		}
	}

	public ObjectWrapperEx(Version incompatibleImprovements) {
		super(incompatibleImprovements);
	}

	@Override
	protected TemplateModel handleUnknownType(final Object obj)
			throws TemplateModelException
	{
		final BiFunction<Object, ObjectWrapper, TemplateModel> constructor =
				Types.get(obj.getClass());

		if (constructor == null)
			return super.handleUnknownType(obj);

		return constructor.apply(obj, this);
	}
}
