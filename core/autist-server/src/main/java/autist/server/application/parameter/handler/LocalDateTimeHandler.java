package autist.server.application.parameter.handler;

import org.jetbrains.annotations.Nullable;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeHandler
		extends LocalDateTimeHandlerBase<LocalDateTime>
{
	public LocalDateTimeHandler() { }

	public LocalDateTimeHandler(final String format) {
		super(format);
	}

	@Override
	protected LocalDateTime parse(
			@Nullable final DateTimeFormatter format,
			final String value)
	{
		if (format == null)
			return LocalDateTime.parse(value);

		return LocalDateTime.parse(value, format);
	}
}
