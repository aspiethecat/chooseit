package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import java.io.File;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;

import java.net.URI;

import autist.commons.net.URN;

import autist.application.resource.Resource;
import autist.application.resource.ResourceStore;

import autist.server.application.resource.FileSystemStore;

@SuppressWarnings({"WeakerAccess", "unchecked"})
public class ResourceCache implements ResourceStore {
	private final FileSystemStore m_store;

	public ResourceCache(
			final @NotNull Path path)
			throws IOException
	{
		m_store = new FileSystemStore("cache", path.toUri());
	}

	public void cleanup()
			throws IOException
	{
		final var rootPath = m_store.systemPath();

		try {
			Files
					.walk(rootPath)
					.filter(Files::isRegularFile)
					.map(Path::toFile)
					.forEach(File::delete);
		} catch (final IOException e) {
			throw new IOException(
					String.format("Cache cleanup failed: %s", rootPath),
					e);
		}
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public final String name() {
		return m_store.name();
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public final URI uri() {
		return m_store.uri();
	}

	@Override
	public boolean isCacheRequired() {
		return false;
	}

	@Override
	@NotNull
	public Optional<Resource> fetch(
			final @NotNull URN urn)
			throws IOException
	{
		return m_store.fetch(urn);
	}

	@Override
	@NotNull
	public Optional<Resource> store(
			final @NotNull URN urn,
			final @NotNull Resource source)
			throws IOException
	{
		return m_store.store(urn, source);
	}
}
