package autist.server.application.parameter.handler;

import org.jetbrains.annotations.Nullable;

import java.text.DecimalFormat;
import java.text.ParseException;

public class DoubleHandler extends FloatHandlerBase<Double> {
	public DoubleHandler() { }

	public DoubleHandler(final String format) {
		super(format);
	}

	@Override
	protected Double parse(@Nullable final DecimalFormat format, final String value)
			throws ParseException
	{
		if (format == null)
			return Double.valueOf(value);

		return format.parse(value).doubleValue();
	}
}
