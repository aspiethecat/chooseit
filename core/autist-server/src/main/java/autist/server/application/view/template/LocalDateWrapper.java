package autist.server.application.view.template;

import java.util.Date;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;

import freemarker.template.AdapterTemplateModel;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.WrappingTemplateModel;

public class LocalDateWrapper
		extends WrappingTemplateModel
		implements TemplateDateModel, AdapterTemplateModel
{
	private final LocalDate m_date;

	public LocalDateWrapper(
			final LocalDate date,
			final ObjectWrapper wrapper)
	{
		super(wrapper);
		m_date = date;
	}

	@Override
	public Object getAdaptedObject(Class<?> hint) {
		return m_date;
	}

	@Override
	public Date getAsDate() throws TemplateModelException {
		return Date.from(m_date.atTime(LocalTime.MIN).toInstant(ZoneOffset.UTC));
	}

	@Override
	public int getDateType() {
		return DATE;
	}
}
