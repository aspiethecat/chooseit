package autist.server.application.parameter.handler;

import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;

import java.text.DecimalFormat;
import java.text.ParseException;

import autist.application.parameter.ParameterHandlerException;

public class BigDecimalHandler
		extends NumberHandler<BigDecimal>
{
	public BigDecimalHandler() { }

	public BigDecimalHandler(final String format) {
		super(format);
	}

	@Override
	protected void setupFormat(final DecimalFormat format) {
		format.setParseBigDecimal(true);
		format.setParseIntegerOnly(false);
	}

	@Override
	protected BigDecimal parse(@Nullable final DecimalFormat format, final String value)
			throws ParseException
	{
		try {
			if (format == null)
				return new BigDecimal(value);

			return (BigDecimal) format.parse(value);
		} catch (final NumberFormatException e) {
			throw new ParameterHandlerException(e);
		}
	}
}
