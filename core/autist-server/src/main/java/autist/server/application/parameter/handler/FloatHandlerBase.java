package autist.server.application.parameter.handler;

import org.jetbrains.annotations.NotNull;
import java.text.DecimalFormat;

abstract class FloatHandlerBase<T extends Number> extends NumberHandler<FloatHandlerBase<T>, T> {
	public FloatHandlerBase() { }

	public FloatHandlerBase(final String format) {
		super(format);
	}

	@Override
	protected void setupFormat(final @NotNull DecimalFormat format) {
		format.setParseBigDecimal(false);
		format.setParseIntegerOnly(false);
	}
}
