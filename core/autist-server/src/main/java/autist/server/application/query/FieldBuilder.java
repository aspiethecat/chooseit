package autist.server.application.query;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import java.text.ParseException;
import java.time.format.DateTimeParseException;

import autist.application.handler.HandlerException;
import autist.application.parameter.Parameters;
import autist.application.parameter.ParameterDescriptor;

final class FieldBuilder<Q extends Parameters<Q>> extends ParameterBuilder<Q> {
	private final FieldValidator<Q> m_validator;

	@SuppressWarnings("unchecked")
	FieldBuilder(
			final ParameterDescriptor parameter,
			final FieldValidator<Q> validator)
	{
		super(requireFieldDescriptor(parameter));
		m_validator = validator;
	}

	@SuppressWarnings("unchecked")
	void build(
			final Q query,
			@Nullable final List<String> stringValues)
			throws HandlerException
	{
		Objects.requireNonNull(query);

		final List<String> source;

		if (stringValues != null)
			source = stringValues;
		else
			source = Collections.emptyList();

		final Object value;

		if (getParameter().isCollection()) {
			final Collection<?> valueSet = getCollection().createInstance(source.size());

			source.stream()
					.map(item -> buildValue(query, item))
					.filter(Objects::nonNull)
					.forEach(((Collection<Object>) valueSet)::add);

			value = valueSet;
		} else {
			value = buildValue(query, source.isEmpty() ? null : source.get(source.size() - 1));
		}

		getParameter().setValue(query, value);
	}

	@SuppressWarnings("unchecked")
	private Object buildValue(
			final @NotNull Q query,
			@Nullable final String source)
	{
		String stringValue = trimValue(source);
		Object objectValue = null;

		boolean parseError = false;

		if (stringValue != null) {
			try {
				objectValue = parseValue(stringValue);
			} catch (final ParseException | DateTimeParseException | IllegalArgumentException e) {
				m_validator.addConstraintViolation(
						query, stringValue, getParameter().getMessage());
				parseError = true;
			}
		}

		if (!parseError && m_validator != null)
			m_validator.validate(query, objectValue);

		return objectValue;
	}

	private static ParameterDescriptor requireFieldDescriptor(
			final ParameterDescriptor parameter)
	{
		Objects.requireNonNull(parameter);

		if (parameter.isCookie()) {
			throw new IllegalArgumentException(String.format(
					"Passing cookie parameter descriptor to field builder: %s",
					parameter.getName()));
		}

		return parameter;
	}
}
