package autist.server.application.parameter.handler;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import autist.application.parameter.ParameterDescriptor;
import autist.application.parameter.ParameterHandler;
import autist.application.parameter.ParameterHandlerContext;

public class EnumHandler<
		HandlerType extends EnumHandler<HandlerType, ValueType>,
		ValueType extends Enum<ValueType>>
		implements ParameterHandler<HandlerType, EnumHandler.Context, ValueType>
{
	@Override
	@NotNull
	@SuppressWarnings("unchecked")
	public Class<ValueType> type() {
		return (Class<ValueType>) Enum.class;
	}

	@NotNull
	@Override
	public EnumHandler.Context getContext(final @NotNull ParameterDescriptor descriptor) {
		return new Context(descriptor);
	}

	@NotNull
	@Override
	@SuppressWarnings("unchecked")
	public ValueType parseValue(
			@NotNull final EnumHandler.Context context,
			final @NotNull String value)
			throws IllegalArgumentException
	{
		return Enum.valueOf((Class<ValueType>) context.parameterClass(), value);
	}

	static final class Context implements ParameterHandlerContext<Context> {
		@NotNull
		private final Class m_parameterClass;

		Context(final @NotNull ParameterDescriptor descriptor) {
			m_parameterClass = descriptor.valueType();
		}

		@NotNull
		@Contract(pure = true)
		Class parameterClass() {
			return m_parameterClass;
		}
	}
}
