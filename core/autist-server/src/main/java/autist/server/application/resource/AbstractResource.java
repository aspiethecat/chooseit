package autist.server.application.resource;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import autist.commons.net.URN;
import autist.application.resource.Resource;
import autist.application.resource.ResourceStore;

@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class AbstractResource<StoreType extends ResourceStore>
		implements Resource
{
	@NotNull
	private final StoreType m_store;

	@NotNull
	private final URN m_urn;

	protected AbstractResource(
			final @NotNull StoreType root,
			final @NotNull URN urn)
	{
		m_store = Objects.requireNonNull(root, "root == null");
		m_urn = Objects.requireNonNull(urn, "urn == null");
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public final StoreType store() {
		return m_store;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public final URN urn() {
		return m_urn;
	}
}
