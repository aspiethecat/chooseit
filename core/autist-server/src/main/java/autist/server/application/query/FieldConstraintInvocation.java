package autist.server.application.query;

import org.jetbrains.annotations.Nullable;

import java.lang.annotation.Annotation;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.metadata.ConstraintDescriptor;

import autist.commons.reflection.ClassUtil;

final class FieldConstraintInvocation<A extends Annotation, T> {
	private final ConstraintDescriptor<A> m_descriptor;
	private final ConstraintValidator<A, T> m_validator;

	FieldConstraintInvocation(
			final ConstraintDescriptor<A> descriptor,
			final Class<ConstraintValidator<A, T>> validatorClass,
			final A validatorParams)
	{
		m_descriptor = Objects.requireNonNull(descriptor);
		m_validator = ClassUtil.createInstance(
				Objects.requireNonNull(validatorClass));

		m_validator.initialize(Objects.requireNonNull(validatorParams));
	}

	public ConstraintDescriptor<A> getDescriptor() {
		return m_descriptor;
	}

	boolean isValid(@Nullable final T value) {
		return m_validator.isValid(value, null);
	}
}
