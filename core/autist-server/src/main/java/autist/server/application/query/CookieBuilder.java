package autist.server.application.query;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.text.ParseException;
import java.time.format.DateTimeParseException;

import autist.application.parameter.Parameters;
import autist.application.parameter.ParameterDescriptor;

public class CookieBuilder<Q extends Parameters<Q>> extends ParameterBuilder<Q> {
	public CookieBuilder(final ParameterDescriptor descriptor) {
		super(requireCookieDescriptor(descriptor));
	}

	@SuppressWarnings("unchecked")
	Object build(
			final @NotNull Q query,
			@Nullable final String source)
	{
		Objects.requireNonNull(query);

		Object objectValue = null;
		String stringValue = trimValue(source);

		if (stringValue != null) {
			try {
				objectValue = parseValue(stringValue);
			} catch (final ParseException | DateTimeParseException | IllegalArgumentException ignored) { }
		}

		return objectValue;
	}

	private static ParameterDescriptor requireCookieDescriptor(
			final ParameterDescriptor parameter)
	{
		Objects.requireNonNull(parameter);

		if (!parameter.isCookie()) {
			throw new IllegalArgumentException(String.format(
					"Passing non-cookie parameter descriptor to cookie builder: %s",
					parameter.getName()));
		}
		if (parameter.isCollection()) {
			throw new IllegalArgumentException(String.format(
					"Collections not allowed as cookie parameter values: %s",
					parameter.getName()));
		}

		return parameter;
	}
}
