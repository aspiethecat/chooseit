package autist.server.application.query;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;

import autist.application.parameter.ParameterDescriptor;
import autist.application.parameter.Parameters;

class QueryDescriptor {
	private final Class<? extends Parameters<?>> m_queryClass;
	private final Collection<ParameterDescriptor> m_parameters;

	QueryDescriptor(final Class<? extends Parameters<?>> queryClass) {
		m_queryClass = Objects.requireNonNull(queryClass);
		m_parameters = findParameters(m_queryClass);
	}

	@SuppressWarnings("unused")
	final Class<? extends Parameters<?>> getQueryClass() {
		return m_queryClass;
	}

	@SuppressWarnings("unused")
	final int getParameterCount() {
		return m_parameters.size();
	}

	@SuppressWarnings("unused")
	final Collection<ParameterDescriptor> getParameters() {
		return Collections.unmodifiableCollection(m_parameters);
	}

	private static Collection<ParameterDescriptor> findParameters(
			final @NotNull Class<? extends Parameters<?>> queryClass)
	{
		final BeanInfo queryInfo = getQueryInfo(queryClass);

		final PropertyDescriptor properties[] = queryInfo.getPropertyDescriptors();
		final Collection<ParameterDescriptor> parameters = new ArrayList<>(properties.length);

		for (final PropertyDescriptor property : properties) {
			final ParameterDescriptor descriptor;
			try {
				descriptor = new ParameterDescriptor(queryClass, property);
			} catch (final NoSuchFieldException e) {
				continue;
			}

			parameters.add(descriptor);
		}

		return parameters;
	}

	private static BeanInfo getQueryInfo(
			final @NotNull Class<? extends Parameters<?>> queryClass)
	{
		final BeanInfo info;
		try {
			info = Introspector.getBeanInfo(queryClass);
		} catch (final IntrospectionException e) {
			throw new AssertionError(
					String.format("getBeanInfo() failed: %s", queryClass.getName()), e);
		}

		return info;
	}
}
