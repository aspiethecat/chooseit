package autist.server.application.mail;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

import autist.server.configuration.MailerConfiguration;

class MailerAuthenticator extends Authenticator {
	@NotNull
	private final PasswordAuthentication m_authentication;

	MailerAuthenticator(final @NotNull MailerConfiguration configuration) {
		Objects.requireNonNull(configuration, "configuration == null");

		m_authentication = new PasswordAuthentication(
				configuration.getUserName().orElse(null),
				configuration.getPassword().orElse(null));
	}

	@Override
	@NotNull
	protected PasswordAuthentication getPasswordAuthentication() {
		return m_authentication;
	}
}
