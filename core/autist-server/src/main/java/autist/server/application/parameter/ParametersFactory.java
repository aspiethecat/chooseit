package autist.server.application.parameter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.ServiceLoader;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import autist.commons.reflection.InvocationException;
import autist.commons.reflection.ReflectionUtil;
import autist.commons.resource.ResourceLoader;

import autist.application.Request;
import autist.application.handler.HandlerException;
import autist.application.parameter.ParameterDescriptor;
import autist.application.parameter.ParameterHandler;
import autist.application.parameter.Parameters;

import autist.server.application.InstanceCache;

@SuppressWarnings({"WeakerAccess", "unused"})
public class ParametersFactory<
		FactoryType extends ParametersFactory<FactoryType, ParametersType>,
		ParametersType extends Parameters<ParametersType>>
{
	@NotNull
	private static List<ParameterHandler> s_typeHandlers = new ArrayList<>();

	@NotNull
	private static final InstanceCache<Parameters<?>, ResourceBundle>
			s_resourceCache = new InstanceCache<>();

	@NotNull
	private static InstanceCache<Parameters, ParametersFactory> s_factoryCache = new InstanceCache<>();

	static {
		for (final var handler : ServiceLoader.load(ParameterHandler.class))
			s_typeHandlers.add(handler);
	}

	@NotNull
	private final ResourceBundle m_resourceBundle;

	@NotNull
	private final IdentityHashMap<Method, ParameterDescriptor> m_descriptorMap = new IdentityHashMap<>();

	@NotNull
	private final IdentityHashMap<Method, ParameterBuilder> m_builderMap = new IdentityHashMap<>();

	@NotNull
	private final Class<ParametersType> m_type;

	private ParametersFactory(
			final @NotNull Class<ParametersType> type)
	{
		Objects.requireNonNull(type, "type == null");
		if (!type.isInterface())
			throw new IllegalArgumentException("!type.isInterface()");

		m_type = type;

		m_resourceBundle = s_resourceCache.createIfAbsent(
				m_type, () -> getResourceBundle(m_type));

		final var methods = new ArrayList<Method>();
		ReflectionUtil
				.findInterfaces(m_type, Parameters.class)
				.forEach(clazz -> {
					for (final var method : clazz.getDeclaredMethods()) {
						if (method.getParameterCount() > 0)
							continue;
						if (method.getReturnType().equals(Void.TYPE))
							continue;
						methods.add(method);
					}
				});

		final var methodNameMap = new HashMap<Method, ParameterDescriptorImpl>();
		methods.forEach(method ->
			methodNameMap.computeIfAbsent(method, ParameterDescriptorImpl::new));
	}

	@NotNull
	@Contract(pure = true)
	public Class<ParametersType> type() {
		return m_type;
	}

	@SuppressWarnings("unchecked")
	public Optional<Parameters> create(
			final @NotNull Request request,
			final @NotNull Collection<HandlerException> errors)
	{
		try {
			return Optional.of((ParametersType) Proxy.newProxyInstance(
					getClass().getClassLoader(),
					new Class[] { m_type },
					new ParametersProxy<>(request, errors)));
		} catch (final HandlerException e) {
			errors.add(e);
		}

		return Optional.empty();
	}

	@NotNull
	@SuppressWarnings("unchecked")
	public static <
			FactoryType extends ParametersFactory<FactoryType, ParametersType>,
			ParametersType extends Parameters<ParametersType>>
	FactoryType getInstance(
			final @NotNull Class<ParametersType> parametersClass)
			throws IllegalArgumentException, InvocationException
	{
		try {
			return (FactoryType) s_factoryCache.createIfAbsent(
					parametersClass, ParametersFactory.class, parametersClass);
		} catch (final InvocationException e) {
			final var cause = e.getCause();
			if (cause instanceof InvocationTargetException) {
				final var targetCause = cause.getCause();
				if (targetCause instanceof RuntimeException)
					throw (RuntimeException) targetCause;
			}

			throw e;
		}
	}

	@Nullable
	private static ResourceBundle getResourceBundle(
			final @NotNull Class<?> parametersClass) {
		try {
			return ResourceLoader.getResourceBundle(parametersClass);
		} catch (final MissingResourceException e) {
			return null;
		}
	}
}
