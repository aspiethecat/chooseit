package autist.server.application.query;

import org.jetbrains.annotations.NotNull;

import javax.validation.ElementKind;
import javax.validation.Path;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;

import java.util.stream.Collectors;

public final class FieldPath implements Path {
	private final Node m_items[];

	public FieldPath(final String... items) {
		m_items = new Node[items.length];

		for (int i = 0; i < items.length; i++)
			m_items[i] = new Node(this, i, items[i]);
	}

	public Iterator<Path.Node> iterator() {
		return new NodeIterator(this, 0);
	}

	public String toString() {
		return Arrays.stream(m_items)
				.map(Node::getName)
				.collect(Collectors.joining("."));
	}

	private static final class Node
			implements Path.Node, Iterable<Path.Node>
	{
		private final FieldPath m_parent;
		private final Integer m_index;
		private final String m_name;

		Node(
				final FieldPath parent,
			 	final Integer index,
				final String name)
		{
			m_parent = Objects.requireNonNull(parent);
			m_index = Objects.requireNonNull(index);
			m_name = Objects.requireNonNull(name);
		}

		FieldPath getParent() {
			return m_parent;
		}

		@Override
		public String getName() {
			return m_name;
		}

		@Override
		public boolean isInIterable() {
			return true;
		}

		@Override
		public Integer getIndex() {
			return m_index;
		}

		@Override
		public Object getKey() {
			return null;
		}

		@Override
		public ElementKind getKind() {
			return null;
		}

		@Override
		public <T extends Path.Node> T as(Class<T> aClass) {
			return null;
		}

		@Override
		public Iterator<Path.Node> iterator() {
			return new NodeIterator(m_parent, m_index + 1);
		}
	}

	private static final class NodeIterator implements Iterator<Path.Node> {
		private FieldPath m_path;
		private int m_index;

		NodeIterator(final @NotNull FieldPath path, final int index) {
			m_path = Objects.requireNonNull(path);
			m_index = index;
		}

		public boolean hasNext() {
			return (m_index < m_path.m_items.length);
		}

		public Node next() {
			return m_path.m_items[m_index++];
		}
	}
}
