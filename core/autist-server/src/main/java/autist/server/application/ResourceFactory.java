package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;

import java.io.IOException;

import autist.commons.net.URN;
import autist.commons.util.DeferredException;
import autist.configuration.ConfigurationException;

import autist.application.resource.Resource;
import autist.application.resource.ResourceStore;

import autist.server.application.resource.AbstractResourceStore;

@SuppressWarnings({"WeakerAccess", "unused"})
public class ResourceFactory {
	@NotNull
	private final ApplicationContext m_context;

	@NotNull
	private final List<ResourceStore> m_stores = new ArrayList<>();

	protected ResourceFactory(
			final @NotNull ApplicationContext context,
			final @NotNull List<? extends ResourceRoot> roots)
			throws ConfigurationException
	{
		m_context = context;

		final var errors = new ArrayList<ConfigurationException>();

		roots.forEach(
				root -> {
					try {
						m_stores.add(AbstractResourceStore.create(root.name(), root.uri()));
					} catch (final IllegalArgumentException| IOException e) {
						errors.add(new ConfigurationException(e.getMessage(), e.getCause()));
					}
				});
		Collections.reverse(m_stores);

		if (!errors.isEmpty()) {
			errors.forEach(
					error -> m_context.log(Level.SEVERE, error.getMessage()));
			throw new ConfigurationException("Resource factory initialization failed");
		}
	}

	@NotNull
	@Contract(pure = true)
	protected ApplicationContext context() {
		return m_context;
	}

	@NotNull
	@Contract(pure = true)
	protected Optional<ResourceCache> cache() {
		return Optional.empty();
	}

	@NotNull
	protected Optional<Resource> get(
			final @NotNull URN urn)
	{
		Objects.requireNonNull(urn, "urn == null");

		try {
			for (final var store : m_stores) {
				final var resource = fetch(store, urn);

				if (resource.isPresent())
					return resource;
			}
		} catch (final IOException e) {
			m_context.log(Level.SEVERE, String.format(
					"Can't get resource: %s: %s", urn.toString(), e.getMessage()));
		}

		return Optional.empty();
	}

	@NotNull
	private Optional<Resource> fetch(
			final @NotNull ResourceStore store,
			final @NotNull URN urn)
			throws IOException
	{
		if (store.isCacheRequired()) {
			final var cachedResource = fetchFromCache(urn);
			if (cachedResource.isPresent())
				return cachedResource;
		}

		final var exception = new DeferredException<>(IOException.class);

		final var result = store
				.fetch(urn)
				.map(resource -> exception.wrap(
						() -> store.isCacheRequired() ?
								storeToCache(urn, resource).orElse(null) :
								resource));

		exception.throwIfPresent();

		return result;
	}

	@NotNull
	protected Optional<Resource> storeToCache(
			final @NotNull URN urn,
			final @NotNull Resource source)
			throws IOException
	{
		Objects.requireNonNull(urn, "path == null");
		Objects.requireNonNull(source, "source == null");

		final var exception = new DeferredException<>(IOException.class);

		final var result = cache()
				.map(cache -> exception.wrap(
						() -> cache.store(urn, source)))
				.orElse(Optional.of(source));

		exception.throwIfPresent();

		return result;
	}

	@NotNull
	protected Optional<Resource> fetchFromCache(
			final @NotNull URN urn)
			throws IOException
	{
		Objects.requireNonNull(urn, "urn == null");

		final var exception = new DeferredException<>(IOException.class);

		final var result = cache()
				.map(cache -> exception.wrap(
						() -> cache.fetch(urn).orElse(null)));

		exception.throwIfPresent();

		return result;
	}
}
