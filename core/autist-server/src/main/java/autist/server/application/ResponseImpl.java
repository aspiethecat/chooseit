package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

import java.nio.charset.Charset;

import java.net.URI;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.cookie.DefaultCookie;
import io.netty.handler.codec.http.cookie.ServerCookieEncoder;

import autist.application.util.Cookie;
import autist.application.resource.Resource;
import autist.application.Response;

import autist.server.HttpDateFormat;

public class ResponseImpl implements Response {
	public static final String DEFAULT_CONTENT_TYPE = "text/plain";

	static final HttpResponseStatus[] TERMINATORS = {
		HttpResponseStatus.BAD_REQUEST,
		HttpResponseStatus.REQUEST_TIMEOUT,
		HttpResponseStatus.LENGTH_REQUIRED,
		HttpResponseStatus.REQUEST_ENTITY_TOO_LARGE,
		HttpResponseStatus.REQUEST_URI_TOO_LONG,
		HttpResponseStatus.INTERNAL_SERVER_ERROR,
		HttpResponseStatus.SERVICE_UNAVAILABLE,
		HttpResponseStatus.NOT_IMPLEMENTED
	};

	static {
		Arrays.sort(TERMINATORS);
	}

	private String m_contentType;

	private Date m_date;

	private Date m_lastModified;

	private HttpResponseStatus m_status;

	private Set<Cookie> m_cookies;

	private Object m_content = Unpooled.EMPTY_BUFFER;

	private boolean m_keepAlive;

	public ResponseImpl(HttpResponseStatus status, boolean keepAlive) {
		m_status    = status;
		m_keepAlive = keepAlive;
	}

	@Contract(pure = true)
	@SuppressWarnings("unused")
	public String getContentType() {
		return m_contentType;
	}

	@Override
	public void setContentType(final String contentType) {
		m_contentType = contentType;
	}

	@Override
	public void setDate(final Date date) {
		m_date = date;
	}

	@Override
	public void setLastModified(final Date lastModified) {
		m_lastModified = lastModified;
	}

	@Contract(pure = true)
	public HttpResponseStatus getStatus() {
		return m_status;
	}

	@SuppressWarnings("unused")
	public void setStatus(final HttpResponseStatus status) {
		m_status = status;

		if (Arrays.binarySearch(TERMINATORS, status) >= 0)
			m_keepAlive = false;
	}

	@NotNull
	@Contract(pure = true)
	@SuppressWarnings("unused")
	public Set<Cookie> getCookies() {
		if (m_cookies == null)
			return Collections.emptySet();

		return m_cookies;
	}

	@Override
	@SuppressWarnings("unused")
	public void setCookies(final @NotNull Set<Cookie> cookies) {
		m_cookies = Objects.requireNonNull(cookies);
	}

	@NotNull
	@Contract(pure = true)
	@SuppressWarnings("unused")
	public Object getContent() {
		return m_content;
	}

	@Override
	@SuppressWarnings("unused")
	public void setContent(final @NotNull URI content) {
		m_content = Objects.requireNonNull(content);
	}

	@Override
	@SuppressWarnings("unused")
	public void setContent(@Nullable final byte[] content) {
		if ((content != null) && (content.length > 0))
			m_content = Unpooled.copiedBuffer(content);
		else
			m_content = Unpooled.EMPTY_BUFFER;
	}

	@Override
	@SuppressWarnings("unused")
	public void setContent(@Nullable final String content, final Charset charset) {
		if ((content != null) && !content.isEmpty())
			m_content = Unpooled.copiedBuffer(content, charset);
		else
			m_content = Unpooled.EMPTY_BUFFER;
	}

	@Override
	@SuppressWarnings("unused")
	public void setContent(@Nullable final Resource content) {
		if (content != null)
			m_content = content;
		else
			m_content = Unpooled.EMPTY_BUFFER;
	}

	public boolean isKeepAlive() {
		return m_keepAlive;
	}

	public HttpResponse build() {
		DefaultHttpResponse response = new DefaultHttpResponse(
			HttpVersion.HTTP_1_1, m_status);

		HttpHeaders headers = response.headers();

		headers.set(HttpHeaderNames.DATE, HttpDateFormat.format(m_date));

		if (m_keepAlive) {
			headers.set(
					HttpHeaderNames.CONNECTION,
					HttpHeaderValues.KEEP_ALIVE);
		}

		buildCookies(response);
		buildRedirect(response);
		buildContent(response);

		return response;
	}

	private void buildCookies(final HttpResponse response) {
		if ((m_cookies == null) || m_cookies.isEmpty())
			return;

		m_cookies.forEach(cookie -> {
			final DefaultCookie cookieImpl =
					new DefaultCookie(cookie.getName(), cookie.getValue());

			if (cookie.getDomain() != null)
				cookieImpl.setDomain(cookie.getDomain());
			if (cookie.getPath() != null)
				cookieImpl.setPath(cookie.getPath());
			if (cookie.getMaxAge() != null)
				cookieImpl.setMaxAge(cookie.getMaxAge());

			cookieImpl.setHttpOnly(cookie.isHttpOnly());
			cookieImpl.setSecure(cookie.isSecure());

			response.headers().add(
					HttpHeaderNames.SET_COOKIE,
					ServerCookieEncoder.STRICT.encode(cookieImpl));
		});
	}

	private void buildRedirect(final @NotNull HttpResponse response) {
		if (!m_content.getClass().equals(URI.class))
			return;

		response.setStatus(HttpResponseStatus.FOUND);
		response.headers().add(HttpHeaderNames.LOCATION, m_content.toString());

		m_lastModified = null;
		m_content = Unpooled.EMPTY_BUFFER;
		m_contentType = null;
	}

	private void buildContent(final @NotNull HttpResponse response) {
		Objects.requireNonNull(response);

		HttpHeaders headers = response.headers();

		if (m_lastModified != null) {
			headers.set(
					HttpHeaderNames.LAST_MODIFIED,
					HttpDateFormat.format(m_lastModified));
		}

		long contentLength;
		if (m_content instanceof ByteBuf)
			contentLength = (long) ((ByteBuf) m_content).readableBytes();
		else if (m_content instanceof Resource)
			contentLength = ((Resource) m_content).attributes().length();
		else
			throw new AssertionError("Unsupported content object type");

		headers.set(
				HttpHeaderNames.CONTENT_LENGTH,
				contentLength);

		if (m_contentType != null) {
			headers.set(
					HttpHeaderNames.CONTENT_TYPE,
					m_contentType);
		} else if (contentLength > 0) {
			headers.set(
					HttpHeaderNames.CONTENT_TYPE,
					DEFAULT_CONTENT_TYPE);
		}
	}
}
