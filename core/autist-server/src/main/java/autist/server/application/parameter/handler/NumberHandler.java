package autist.server.application.parameter.handler;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import java.util.Optional;
import java.util.function.BiConsumer;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

import autist.application.parameter.ParameterDescriptor;

@SuppressWarnings({"WeakerAccess", "unused"})
abstract class NumberHandler<
		HandlerType extends NumberHandler<HandlerType, ValueType>,
		ValueType>
		extends AbstractHandler<HandlerType, ValueType>
{
	static final class DefaultContext extends AbstractHandler.DefaultContext {
		@Nullable
		private final DecimalFormat m_numberFormat;

		DefaultContext(final @NotNull ParameterDescriptor descriptor) {
			super(descriptor);

			m_numberFormat = descriptor.format()
					.map(this::parseFormat)
					.map(this::setupFormat)
					.orElse(null);
		}

		@NotNull
		@Contract(pure = true)
		Optional<DecimalFormat> numberFormat() {
			return Optional.ofNullable(m_numberFormat);
		}
	}

	@Override
	public final HandlerType parse(
			final @NotNull Class<HandlerType> actualType,
			final @NotNull String value)
			throws ParseException
	{
		Objects.requireNonNull(actualType);
		Objects.requireNonNull(value);

		return parse(m_format, value);
	}

	protected abstract void setupFormat(DecimalFormat format);

	protected abstract HandlerType parse(@Nullable DecimalFormat format, String value)
			throws ParseException;

	DecimalFormat parseFormat(final @NotNull String format) {
		final String formatString;
		final DecimalFormatSymbols formatSymbols;

		int pos = format.indexOf(";;");
		if (pos < 0) {
			formatString = format;
			formatSymbols = null;
		} else {
			formatString = format.substring(0, pos);

			formatSymbols = new DecimalFormatSymbols();
			Arrays.stream(format.substring(pos + 2).split("\\s+"))
					.forEach(option -> parseOption(formatSymbols, option));
		}

		final DecimalFormat decimalFormat = new DecimalFormat(formatString);
		if (formatSymbols != null)
			decimalFormat.setDecimalFormatSymbols(formatSymbols);

		setupFormat(decimalFormat);

		return decimalFormat;
	}

	private void parseOption(
			final @NotNull DecimalFormatSymbols symbols,
			final @NotNull String optionString)
	{
		if (optionString.isEmpty())
			return;

		final int pos = optionString.indexOf('=');
		if (pos < 1) {
			throw new IllegalArgumentException(
					String.format("Invalid format option specification: '%s'", optionString));
		}

		final String optionName = optionString.substring(0, pos);
		final String optionValue = optionString.substring(pos + 1);

		if (optionValue.isEmpty()) {
			throw new IllegalArgumentException(
					String.format("Missing format option value: %s", optionName));
		}
		if (!optionValue.matches("^'[^']+'$")) {
			throw new IllegalArgumentException(
					String.format("Invalid format option value: %s: %s", optionName, optionValue));
		}

		final BiConsumer<DecimalFormatSymbols, String>
				optionHandler = Option.getHandler(optionName);
		if (optionHandler == null) {
			throw new IllegalArgumentException(
					String.format("Unexpected option name: %s", optionName));
		}

		optionHandler.accept(symbols, optionValue.substring(1, optionValue.length() - 1));
	}

	private enum Option {
		CURRENCY(
				"currency",
				(symbols, value) -> {
					symbols.setCurrency(Currency.getInstance(value));
				}),

		CURRENCY_SYMBOL(
				"currencySymbol",
				DecimalFormatSymbols::setCurrencySymbol),

		DECIMAL_SEPARATOR(
				"decimalSeparator",
				(symbols, value) -> {
					if (value.length() > 1)
						throw new IllegalArgumentException("Decimal separator should be a character");
					symbols.setDecimalSeparator(value.charAt(0));
				}),

		DIGIT(
				"digit",
				(symbols, value) -> {
					if (value.length() > 1)
						throw new IllegalArgumentException("Digit should be a character");
					symbols.setDigit(value.charAt(0));
				}),

		EXPONENT_SEPARATOR(
				"exponentSeparator",
				DecimalFormatSymbols::setExponentSeparator),

		GROUPING_SEPARATOR(
				"groupingSeparator",
				(symbols, value) -> {
					if (value.length() > 1)
						throw new IllegalArgumentException("Grouping separator should be a character");
					symbols.setGroupingSeparator(value.charAt(0));
				}),

		INFINITY(
				"infinity",
				DecimalFormatSymbols::setInfinity),

		INTERNATIONAL_CURRENCY_SYMBOL(
				"internationalCurrencySymbol",
				DecimalFormatSymbols::setInternationalCurrencySymbol),

		MINUS_SIGN(
				"minusSign",
				(symbols, value) -> {
					if (value.length() > 1)
						throw new IllegalArgumentException("Minus sign should be a character");
					symbols.setMinusSign(value.charAt(0));
				}),

		MONETARY_DECIMAL_SEPARATOR(
				"monetaryDecimalSeparator",
				(symbols, value) -> {
					if (value.length() > 1)
						throw new IllegalArgumentException("Monetary decimal separator should be a character");
					symbols.setMonetaryDecimalSeparator(value.charAt(0));
				}),

		NAN(
				"nan",
				DecimalFormatSymbols::setNaN),

		PATTERN_SEPARATOR(
				"patternSeparator",
				(symbols, value) -> {
					if (value.length() > 1)
						throw new IllegalArgumentException("Pattern separator should be a character");
					symbols.setPatternSeparator(value.charAt(0));
				}),

		PERCENT(
				"percent",
				(symbols, value) -> {
					if (value.length() > 1)
						throw new IllegalArgumentException("Percent symbol should be a character");
					symbols.setPercent(value.charAt(0));
				}),

		PER_MILL(
				"perMill",
				(symbols, value) -> {
					if (value.length() > 1)
						throw new IllegalArgumentException("Per-mill symbol should be a character");
					symbols.setPerMill(value.charAt(0));
				}),

		ZERO_DIGIT(
				"zeroDigit",
				(symbols, value) -> {
					if (value.length() > 1)
						throw new IllegalArgumentException("Zero digit symbol should be a character");
					symbols.setZeroDigit(value.charAt(0));
				});

		private String m_name;
		private BiConsumer<DecimalFormatSymbols, String> m_handler;

		private static final Map<String, BiConsumer<DecimalFormatSymbols, String>>
				s_instanceMap = new HashMap<>();

		static {
			Arrays.stream(values()).forEach(
					option -> s_instanceMap.put(option.m_name, option.m_handler));
		}

		Option(
				final String name,
				final BiConsumer<DecimalFormatSymbols, String> handler)
		{
			m_name = name;
			m_handler = handler;
		}

		public static BiConsumer<DecimalFormatSymbols, String> getHandler(String name) {
			return s_instanceMap.get(name);
		}
	}
}
