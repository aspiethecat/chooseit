package autist.server.application.query;

import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.net.URI;
import javax.mail.internet.InternetAddress;

import org.apache.ibatis.type.TypeHandler;

import autist.server.application.parameter.handler.BigDecimalHandler;
import autist.server.application.parameter.handler.BigIntegerHandler;
import autist.server.application.parameter.handler.BooleanHandler;
import autist.server.application.parameter.handler.DoubleHandler;
import autist.server.application.parameter.handler.EmailHandler;
import autist.server.application.parameter.handler.EnumHandler;
import autist.server.application.parameter.handler.FloatHandler;
import autist.server.application.parameter.handler.IntegerHandler;
import autist.server.application.parameter.handler.LocalDateHandler;
import autist.server.application.parameter.handler.LocalDateTimeHandler;
import autist.server.application.parameter.handler.LocalTimeHandler;
import autist.server.application.parameter.handler.LongHandler;
import autist.server.application.parameter.handler.ShortHandler;
import autist.server.application.parameter.handler.StringHandler;
import autist.server.application.parameter.handler.URIHandler;

enum TypeAdapter {
	STRING(String.class, new StringHandler(), StringHandler::new),

	SHORT(Short.class, new ShortHandler(), ShortHandler::new),
	INTEGER(Integer.class, new IntegerHandler(), IntegerHandler::new),
	LONG(Long.class, new LongHandler(), LongHandler::new),
	FLOAT(Float.class, new FloatHandler(), FloatHandler::new),
	DOUBLE(Double.class, new DoubleHandler(), DoubleHandler::new),
	BIG_DECIMAL(BigDecimal.class, new BigDecimalHandler(), BigDecimalHandler::new),
	BIG_INTEGER(BigInteger.class, new BigIntegerHandler(), BigIntegerHandler::new),

	BOOLEAN(Boolean.class, new BooleanHandler(), null),
	ENUM(Enum.class, new EnumHandler(), null),

	LOCAL_TIME(LocalTime.class, new LocalTimeHandler(), LocalTimeHandler::new),
	LOCAL_DATE(LocalDate.class, new LocalDateHandler(), LocalDateHandler::new),
	LOCAL_DATE_TIME(LocalDateTime.class, new LocalDateTimeHandler(), LocalDateTimeHandler::new),

	URL(URI.class, new URIHandler(), null),
	EMAIL(InternetAddress.class, new EmailHandler(), null);

	private static final Map<Class<?>, TypeAdapter> s_instanceMap =
			new IdentityHashMap<>(values().length);

	static {
		Arrays.stream(values()).forEach(
				(adapter) -> s_instanceMap.put(adapter.m_type, adapter));
	}

	private final Class<?> m_type;
	private final TypeHandler<?> m_defaultHandler;
	private final Function<String, ? extends  TypeHandler<?>> m_constructor;

	<T, H extends TypeHandler<T>, C extends Function<String, ? extends TypeHandler<T>>>
	TypeAdapter(
			final Class<T> type,
			final H defaultHandler,
			@Nullable final C constructor)
	{
		m_type = Objects.requireNonNull(type);
		m_defaultHandler = Objects.requireNonNull(defaultHandler);
		m_constructor = constructor;
	}

	@SuppressWarnings("unchecked")
	static <T> TypeHandler<T> getDefaultHandler(final Class<T> type) {
		Objects.requireNonNull(type);

		final TypeAdapter adapter = s_instanceMap.get(type);
		if (adapter == null)
			return null;

		return (TypeHandler<T>) adapter.m_defaultHandler;
	}

	@SuppressWarnings("unchecked")
	static <T> TypeHandler<T> createHandler(final Class<T> type, final String format) {
		Objects.requireNonNull(type);
		Objects.requireNonNull(format);

		final TypeAdapter adapter =
				s_instanceMap.get(Objects.requireNonNull(type));

		if ((adapter == null) || (adapter.m_constructor == null))
			return null;

		return (TypeHandler<T>) adapter.m_constructor.apply(format);
	}
}
