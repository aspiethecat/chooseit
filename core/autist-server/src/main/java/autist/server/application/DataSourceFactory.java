package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import autist.configuration.ConfigurationException;

import autist.application.model.DataSourceProvider;

import autist.server.configuration.DataSourceConfiguration;
import autist.server.configuration.DataSourceSpecification;
import autist.server.configuration.NamedEntityMap;
import autist.server.configuration.NamedEntityMapEx;

public final class DataSourceFactory implements DataSourceProvider {
	@Nullable
	private final DataSource m_defaultDataSource;

	private final Map<String, DataSource> m_dataSourceMap =
			new LinkedHashMap<>(0);

	private DataSourceFactory(
			final @NotNull Collection<DataSource> dataSources,
			final @Nullable String defaultItemName)
			throws ConfigurationException
	{
		dataSources.forEach((dataSource) ->
				m_dataSourceMap.put(dataSource.name(), dataSource));

		m_defaultDataSource = findDefault(m_dataSourceMap, defaultItemName);
	}

	@Override
	@NotNull
	public SqlSession openSession() {
		if (m_defaultDataSource == null)
			throw new IllegalStateException("No data sources configured");

		return m_defaultDataSource.openSession();
	}

	@Override
	@NotNull
	public SqlSession openSession(final @NotNull String dataSource) {
		final DataSource factory = m_dataSourceMap.get(dataSource);
		if (factory == null)
			throw new IllegalArgumentException(String.format("Invalid data source specified: %s", dataSource));

		return factory.openSession();
	}

	@Nullable
	private static DataSource findDefault(
			final @NotNull Map<String, DataSource> dataSources,
			final @Nullable String defaultItemName)
			throws ConfigurationException
	{
		String dataSourceName = defaultItemName;

		if (dataSourceName == null) {
			if (dataSources.size() > 1) {
				throw new ConfigurationException(
						"Multiple data sources configured without specifying default one");
			} else if (!dataSources.isEmpty()) {
				dataSourceName = dataSources.keySet().iterator().next();
			}
		}

		if (dataSourceName == null)
			return null;

		final DataSource dataSource = dataSources.get(dataSourceName);
		if (dataSource == null) {
			throw new ConfigurationException(String.format(
					"Unknown data source specified as default: %s",
					dataSourceName));
		}

		return dataSource;
	}

	@NotNull
	@Contract("_, _, _ -> new")
	public static DataSourceFactory build(
			final @NotNull ApplicationContext context,
			final @NotNull NamedEntityMapEx<DataSourceSpecification> specificationMap,
			final @NotNull NamedEntityMap<DataSourceConfiguration> configurationMap)
			throws ConfigurationException
	{
		return new DataSourceFactory(
				DataSource.buildList(
						context,
						specificationMap,
						configurationMap),
				specificationMap.getDefaultItemName());
	}
}
