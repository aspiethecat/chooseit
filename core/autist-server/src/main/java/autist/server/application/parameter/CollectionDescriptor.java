package autist.server.application.parameter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

@SuppressWarnings({"WeakerAccess", "unused"})
public class CollectionDescriptor {
	@SuppressWarnings("unchecked")
	private static final List<CollectionDescriptor> COLLECTION_TYPES = List.of(
			new CollectionDescriptor(Collection.class, ArrayList.class),
			new CollectionDescriptor(List.class, ArrayList.class),
			new CollectionDescriptor(Set.class, HashSet.class),
			new CollectionDescriptor(Set.class, Enum.class, EnumSet.class),
			new CollectionDescriptor(SortedSet.class, TreeSet.class),
			new CollectionDescriptor(Queue.class, ArrayDeque.class),
			new CollectionDescriptor(Deque.class, ArrayDeque.class));

	@NotNull
	private final Class<? extends Collection<?>> m_interfaceType;

	private final int m_interfaceRank;

	private final Class<?> m_elementType;

	private final int m_elementRank;

	private final Class<? extends Collection<?>> m_providerType;

	public <T extends Collection<T>> CollectionDescriptor(
			final @NotNull Class<T> interfaceType,
			final @NotNull Class<? extends T> providerType)
	{
		this(interfaceType, Object.class, providerType);
	}

	public <T extends Collection<T>> CollectionDescriptor(
			final @NotNull Class<T> interfaceType,
			final Class<?> elementType,
			final @NotNull Class<? extends T> providerType)
	{
		Objects.requireNonNull(interfaceType, "interfaceType == null");
		Objects.requireNonNull(elementType, "elementType == null");
		Objects.requireNonNull(providerType, "providerType == null");

		if (!interfaceType.isInterface())
			throw new IllegalArgumentException("!interfaceType.isInterface()");

		if (providerType.isInterface())
			throw new IllegalArgumentException("providerType.isInterface()");

		m_interfaceType = interfaceType;
		m_elementType = elementType;
		m_providerType = providerType;

		m_interfaceRank = getInterfaceDepth(m_interfaceType);
		m_elementRank = getClassDepth(m_elementType);
	}

	@NotNull
	@Contract(pure = true)
	final Class<? extends Collection<?>> interfaceType() {
		return m_interfaceType;
	}

	@Contract(pure = true)
	final int interfaceRank() {
		return m_interfaceRank;
	}

	@NotNull
	@Contract(pure = true)
	final Class<?> elementType() {
		return m_elementType;
	}

	@Contract(pure = true)
	final int elementRank() {
		return m_elementRank;
	}

	@NotNull
	@Contract(pure = true)
	final Class<? extends Collection<?>> providerType() {
		return m_providerType;
	}

	@NotNull
	public static Optional<CollectionDescriptor> findType(
			final @NotNull Class<?> collectionType,
			final @NotNull Class<?> elementType)
	{
		return COLLECTION_TYPES.stream()
				.filter((candidate) -> candidate.isCompatible(collectionType, elementType))
				.min((left, right) -> {
					final int r = Integer.compare(right.m_elementRank, left.m_elementRank);
					if (r != 0)
						return r;

					return Integer.compare(left.m_interfaceRank, right.m_interfaceRank);
				});
	}

	private boolean isCompatible(
			final @NotNull Class<?> interfaceType,
			final @NotNull Class<?> elementType)
	{
		return interfaceType.isAssignableFrom(m_interfaceType) &&
						m_elementType.isAssignableFrom(elementType);
	}

	private static int getClassDepth(final @NotNull Class<?> clazz) {
		if (clazz.isInterface())
			throw new AssertionError();

		int depth = 0;

		Class<?> item = clazz;
		while (item != null) {
			depth++;
			item = item.getSuperclass();
		}

		return depth;
	}

	private static int getInterfaceDepth(final @NotNull Class<?> clazz) {
		if (!clazz.isInterface())
			throw new AssertionError();

		final Class<?> interfaces[] = clazz.getInterfaces();

		if (interfaces.length == 0)
			return 0;

		int depth = 0;
		for (final Class<?> item : interfaces) {
			final int itemDepth = getInterfaceDepth(item);
			if (depth < itemDepth)
				depth = itemDepth;
		}

		return depth + 1;
	}
}
