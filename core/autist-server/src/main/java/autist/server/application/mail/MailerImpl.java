package autist.server.application.mail;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.Properties;

import java.io.UnsupportedEncodingException;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import autist.configuration.ConfigurationException;

import autist.application.EnvironmentType;
import autist.application.mail.Mailer;


import autist.server.configuration.MailerConfiguration;
import autist.server.application.ApplicationContext;

public class MailerImpl implements Mailer {
	private final Session m_session;
	private final InternetAddress m_defaultSender;

	private MailerImpl(
			final @NotNull Session session,
			final @Nullable InternetAddress defaultSender)
	{
		m_session = Objects.requireNonNull(session);
		m_defaultSender = defaultSender;
	}

	@Override
	@NotNull
	public MimeMessage createMessage() {
		return new MimeMessage(m_session);
	}

	@Override
	public void sendMessage(
			final @NotNull MimeMessage message)
			throws MessagingException
	{
		Objects.requireNonNull(message);

		if ((message.getSender() == null) && (m_defaultSender != null))
			message.setSender(m_defaultSender);

		try {
			Transport.send(message);
		} catch (final NoSuchProviderException e) {
			throw new AssertionError("Can't get SMTP transport provider", e);
		}
	}

	@NotNull
	@Contract("_, _ -> new")
	public static MailerImpl build(
			final @NotNull ApplicationContext context,
			final @NotNull MailerConfiguration configuration)
			throws ConfigurationException
	{
		final Properties properties = new Properties();

		final SocketAddress serverAddress = configuration.getServerAddress();
		if (serverAddress instanceof InetSocketAddress) {
			final InetSocketAddress inetAddress = (InetSocketAddress) serverAddress;

			final int port;
			if (inetAddress.getPort() > 0) {
				port = inetAddress.getPort();
			} else {
				port = configuration.isSecure() ?
						MailerConfiguration.DEFAULT_SECURE_PORT :
						MailerConfiguration.DEFAULT_INSECURE_PORT;
			}

			properties.put("mail.smtp.host", inetAddress.getHostName());
			properties.put("mail.smtp.port", port);
		} else {
			throw new ConfigurationException(String.format(
					"Unsupported SMTP server address type: %s", serverAddress.getClass().getName()));
		}

		configuration.getConnectionTimeout().ifPresent(
				timeout -> properties.put("mail.smtp.connectiontimeout", timeout.toMillis()));
		configuration.getReadTimeout().ifPresent(
				timeout -> properties.put("mail.smtp.timeout", timeout.toMillis()));
		configuration.getWriteTimeout().ifPresent(
				timeout -> properties.put("mail.smtp.writetimeout", timeout.toMillis()));

		if (configuration.isSecure())
			properties.put("mail.smtp.ssl.enable", true);

		if (context.environmentType() == EnvironmentType.DEVELOPMENT) {
			properties.put("mail.debug", true);
			properties.put("mail.debug.auth", true);
		}

		final Authenticator authenticator;
		if (configuration.getUserName().isPresent()) {
			properties.put("mail.smtp.auth", true);
			authenticator = new MailerAuthenticator(configuration);
		} else {
			authenticator = null;
		}

		final InternetAddress defaultSender =
				configuration.getSenderAddress().orElse(null);

		if (defaultSender != null) {
			configuration.getSenderName().ifPresent(
					name -> {
						try {
							defaultSender.setPersonal(name, configuration.getCharset().name());
						} catch (final UnsupportedEncodingException e) {
							throw new AssertionError(e);
						}
					});
		}

		return new MailerImpl(Session.getInstance(properties, authenticator), defaultSender);
	}
}
