package autist.server.application.parameter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Optional;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import autist.application.parameter.Parameter;
import autist.application.parameter.ParameterDescriptor;
import autist.application.parameter.ParameterHandler;
import autist.application.parameter.ParameterSource;

@SuppressWarnings({"WeakerAccess", "unused"})
final class ParameterDescriptorImpl implements ParameterDescriptor {
	public static final String DEFAULT_MESSAGE = "{parameter.invalid}";
	public static final ParameterSource[] DEFAULT_SOURCE = new ParameterSource[] { };

	@NotNull
	private final Method m_accessor;

	@NotNull
	private final String m_name;

	@Nullable
	private final Class<? extends Collection> m_collectionType;

	@NotNull
	private final Class<?> m_valueType;

	@Nullable
	private final Class<? extends ParameterHandler> m_valueTypeHandler;

	private final boolean m_optional;

	private final boolean m_collection;

	@Nullable
	private final String m_format;

	@NotNull
	private final String m_message;

	@NotNull
	private final ParameterSource[] m_source;

	@SuppressWarnings("unchecked")
	public ParameterDescriptorImpl(final @NotNull Method accessor) {
		m_accessor = accessor;

		final var methodName = accessor.getName();
		final var methodReturnType = accessor.getReturnType();

		if (methodReturnType.equals(Void.TYPE)) {
			throw new IllegalArgumentException(
					String.format("Method return type is void: %s", methodName));
		}

		final var annotation = accessor.getAnnotation(Parameter.class);

		var name = methodName;
		var collectionType = (Class<? extends Collection>) null;
		var valueType = methodReturnType;
		var valueTypeHandler = (Class<? extends ParameterHandler>) null;
		var source = DEFAULT_SOURCE;
		var format = (String) null;
		var message = DEFAULT_MESSAGE;

		if (annotation != null) {
			if (!annotation.name().isEmpty())
				name = annotation.name();

			if (!annotation.type().equals(Void.TYPE))
				valueType = annotation.type();

			if (!annotation.typeHandler().equals(ParameterHandler.class))
				valueTypeHandler = annotation.typeHandler();

			if (annotation.source().length > 0)
				source = annotation.source();

			if (!annotation.format().isEmpty())
				format = annotation.format();

			if (!annotation.message().isEmpty())
				message = annotation.message();
		}

		m_optional = valueType.isAssignableFrom(Optional.class);

		m_collection = valueType.isAssignableFrom(Iterable.class);
		if (m_collection) {
			if (!collectionType.isInterface()) {
				throw new IllegalArgumentException(
						String.format("Non-interface collection type: %s", methodName));
			}

			collectionType = (Class<? extends Collection>) valueType;
			valueType = getTypeVariable(collectionType);
		}

		m_name = name;
		m_collectionType = collectionType;
		m_valueType = valueType;
		m_valueTypeHandler = valueTypeHandler;
		m_format = format;
		m_source = source;
		m_message = message;
	}

	@NotNull
	@Contract(pure = true)
	public Method accessor() {
		return m_accessor;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public String name() {
		return m_name;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Optional<Class<? extends Collection>> collectionType() {
		return Optional.ofNullable(m_collectionType);
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Class<?> valueType() {
		return m_valueType;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Optional<Class<? extends ParameterHandler>> valueTypeHandler() {
		return Optional.ofNullable(m_valueTypeHandler);
	}

	@Override
	@Contract(pure = true)
	public boolean isOptional() {
		return m_optional;
	}

	@Override
	@Contract(pure = true)
	public boolean isCollection() {
		return m_collection;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Optional<String> format() {
		return Optional.ofNullable(m_format);
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public String message() {
		return m_message;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public ParameterSource[] source() {
		return m_source.clone();
	}

	@SuppressWarnings("unchecked")
	private static Class<?> getTypeVariable(
			final @NotNull Class<?> clazz)
	{
		return (Class<?>) ((ParameterizedType) (Type) clazz).getActualTypeArguments()[0];
	}
}
