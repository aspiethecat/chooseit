package autist.server.application.resource;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.net.URI;

import autist.commons.net.URN;

import autist.application.resource.Resource;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class FileSystemStore
		extends AbstractResourceStore
{
	@NotNull
	private final Path m_systemPath;

	public FileSystemStore(
			final @NotNull String name,
			final @NotNull URI uri)
			throws IOException
	{
		super(
				StoreType.FILESYSTEM,
				Objects.requireNonNull(name, "name == null"),
				Objects.requireNonNull(uri, "uri == null"));

		m_systemPath = Paths.get(uri);

		if (!Files.exists(m_systemPath))
			throw new NoSuchFileException(m_systemPath.toString());
		if (!Files.isDirectory(m_systemPath))
			throw new NotDirectoryException(m_systemPath.toString());
	}

	@NotNull
	@Contract(pure = true)
	public Path systemPath() {
		return m_systemPath;
	}

	@Override
	@Contract(pure = true)
	public boolean isCacheRequired() {
		return false;
	}

	@Override
	@NotNull
	@Contract("_ -> new")
	public Optional<Resource> fetch(
			final @NotNull URN urn)
			throws IOException
	{
		Objects.requireNonNull(urn,"urn == null");

		try {
			return Optional.of(new FileSystemResource(this, urn));
		} catch (final NoSuchFileException e) {
			return Optional.empty();
		}
	}

	@Override
	@NotNull
	@Contract("_, _ -> new")
	public Optional<Resource> store(
			final @NotNull URN urn,
			final @NotNull Resource source)
			throws IOException
	{
		Objects.requireNonNull(urn, "urn == null");
		Objects.requireNonNull(source, "source == null");

		try {
			return Optional.of(new FileSystemResource(this, urn, source));
		} catch (final NoSuchFileException e) {
			return Optional.empty();
		}
	}
}
