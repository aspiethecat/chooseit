package autist.server.application.parameter.handler;

import org.jetbrains.annotations.Nullable;

import java.text.DecimalFormat;
import java.text.ParseException;

public class FloatHandler extends FloatHandlerBase<Float> {
	public FloatHandler() { }

	public FloatHandler(final String format) {
		super(format);
	}

	@Override
	protected Float parse(@Nullable final DecimalFormat format, final String value)
			throws ParseException
	{
		if (format == null)
			return Float.valueOf(value);

		return format.parse(value).floatValue();
	}
}
