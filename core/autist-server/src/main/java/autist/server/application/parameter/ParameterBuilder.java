package autist.server.application.parameter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

import java.lang.reflect.Constructor;
import java.text.ParseException;
import java.time.format.DateTimeParseException;

import autist.commons.reflection.ClassUtil;
import autist.commons.reflection.InvocationException;

import autist.application.Request;
import autist.application.handler.HandlerException;
import autist.application.parameter.ParameterDescriptor;
import autist.application.parameter.ParameterHandler;
import autist.application.parameter.ParameterHandlerContext;
import autist.application.parameter.Parameters;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class ParameterBuilder {
	@NotNull
	private  final Class<? extends Parameters> m_parent;

	@Nullable
	private final Class<? extends Collection<?>> m_collectionClass;

	@Nullable
	private final Constructor<? extends Collection<?>> m_collectionConstructor;

	@NotNull
	private final ParameterDescriptor m_descriptor;

	@NotNull
	private final ParameterHandler m_handler;

	@NotNull
	private final ParameterHandlerContext m_context;

	public <T extends Collection<T>> ParameterBuilder(
			final @NotNull Class<? extends Parameters> parent,
			final @NotNull ParameterDescriptor descriptor,
			final @NotNull ParameterHandler handler)
	{
		m_parent = parent;
		m_descriptor = descriptor;
		m_handler = handler;
		m_context = m_handler.getContext(descriptor);

		if (descriptor.isCollection()) {
			final var parentName = m_parent.getName();
			final var parameterName = descriptor.name();

			final Class<? extends Collection> collectionType = descriptor
					.collectionType()
					.orElseThrow(() -> new IllegalArgumentException(
							String.format(
									"Parameter is not a collection: %s: %s",
									parentName, parameterName)));

			final CollectionDescriptor collectionDescriptor = CollectionDescriptor
					.findType(collectionType, descriptor.valueType())
					.orElseThrow(() -> new IllegalArgumentException(String.format(
							"Unsupported collection type: %s: %s: %s",
							parentName, parameterName, collectionType.getName())));

			m_collectionClass = collectionDescriptor.providerType();
			m_collectionConstructor = ClassUtil.getConstructor((Class<T>) m_collectionClass, Integer.TYPE)
					.or(() -> ClassUtil.getConstructor((Class<T>) m_collectionClass))
					.orElseThrow(() -> new AssertionError(
							String.format("Collection constructor not found: %s", m_collectionClass.getName())));
		} else {
			m_collectionClass = null;
			m_collectionConstructor = null;
		}
	}

	@NotNull
	@Contract(pure = true)
	public Class<? extends Parameters> parent() {
		return m_parent;
	}

	@NotNull
	@Contract(pure = true)
	public ParameterDescriptor descriptor() {
		return m_descriptor;
	}

	public Object build(
			final @NotNull Request request)
			throws HandlerException
	{
		Objects.requireNonNull(request, "request == null");

		if (m_descriptor.isCollection()) {
			Collection<?> collection = createCollection(size);
		}

	}

	private ArrayList<String> getValues() {
	}

	@Nullable
	@Contract(pure = true)
	private String trimValue(final @Nullable String source) {
		String value = null;

		if (source != null) {
			final String trimmedValue = source.trim();
			if (!trimmedValue.isEmpty())
				value = trimmedValue;
		}

		return value;
	}

	@NotNull
	@Contract(pure = true)
	@SuppressWarnings("unchecked")
	private Object parseValue(
			final @NotNull String value)
			throws ParseException, DateTimeParseException
	{
		Objects.requireNonNull(value, "value == null");
		return m_handler.parseValue(m_context, value);
	}

	private Collection<?> createCollection(final int initialSize)
			throws HandlerException
	{
		if (m_collectionConstructor == null)
			throw new IllegalStateException();

		final Collection<?> instance;
		try {
			instance = (m_collectionConstructor.getParameterCount() > 0) ?
					ClassUtil.createInstance(m_collectionConstructor, initialSize) :
					ClassUtil.createInstance(m_collectionConstructor);
		} catch (final InvocationException e) {
			throw new HandlerException(String.format(
					"Can't create collection: %s", e.getMessage()), e.getCause());
		}

		return instance;
	}
}
