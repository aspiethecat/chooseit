package autist.server.application;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;

import autist.application.Model;
import autist.application.action.AbstractAction;
import autist.application.action.DefaultActionContext;
import autist.application.model.DefaultModel;
import autist.application.action.parameters.DefaultQuery;

public class TemplateHandler
		extends AbstractAction<DefaultActionContext, DefaultQuery>
{
	@Override
	@NotNull
	public final Optional<Model> invoke(
			final @NotNull DefaultQuery query)
	{
		Objects.requireNonNull(query, "query == null");
		return Optional.of(new DefaultModel());
	}
}
