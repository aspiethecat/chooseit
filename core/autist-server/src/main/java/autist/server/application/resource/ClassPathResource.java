package autist.server.application.resource;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.jar.JarEntry;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.NoSuchFileException;
import java.nio.file.attribute.FileTime;

import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;

import autist.commons.net.URN;

import autist.application.resource.ResourceAttributes;

class ClassPathResource extends AbstractResource<ClassPathStore> {
	private final URL m_url;
	private final ResourceAttributes m_attributes;
	private final InputStream m_stream;

	ClassPathResource(
			final @NotNull ClassPathStore root,
			final @NotNull URN urn)
			throws IOException
	{
		super(root, urn);

		final var baseURN = new URN(root.uri());
		final var fullURN = baseURN.append(urn).toString();

		m_url = getClass().getResource(fullURN);
		if (m_url == null)
			throw new NoSuchFileException("Resource not found", root.name(), fullURN);

		try {
			final var connection = m_url.openConnection();
			if (connection instanceof JarURLConnection)
				m_attributes = new Attributes(((JarURLConnection) connection).getJarEntry());
			else
				m_attributes = new Attributes(connection);

			m_stream = connection.getInputStream();
		} catch (final IOException e) {
			throw new IOException(
					String.format("Can't open resource: %s: %s", root.name(), urn),
					e);
		}
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public URL url() {
		return m_url;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public ResourceAttributes attributes() {
		return m_attributes;
	}

	@Override
	@NotNull
	public InputStream inputStream() {
		return m_stream;
	}

	@Override
	public void close() throws IOException {
		m_stream.close();
	}

	private class Attributes implements ResourceAttributes {
		private final long m_length;
		private final FileTime m_lastModifiedTime;

		Attributes(final @NotNull JarEntry jarEntry) {
			m_length = jarEntry.getSize();
			m_lastModifiedTime = FileTime.fromMillis(jarEntry.getTime());
		}

		Attributes(final @NotNull URLConnection connection) {
			m_length = connection.getContentLength();
			m_lastModifiedTime = FileTime.fromMillis(connection.getLastModified());
		}

		@Contract(pure = true)
		public long length() {
			return m_length;
		}

		@Contract(pure = true)
		public FileTime lastModifiedTime() {
			return m_lastModifiedTime;
		}
	}
}
