package autist.server.application.resource;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;

import java.io.IOException;

import autist.commons.net.URN;

import autist.application.resource.Resource;

import autist.server.application.ApplicationContext;
import autist.server.application.AssetBuilder;
import autist.server.application.AssetBuilderInvocation;
import autist.server.application.AssetFactory;

public class Compressor implements AssetBuilder {
	private static final String MINIFIED_EXTENSION = "min";
	private static final String JAVASCRIPT_EXTENSION = "js";
	private static final String STYLESHEET_EXTENSION = "css";

	private final ApplicationContext m_context;

	public Compressor(final @NotNull ApplicationContext application) {
		m_context = Objects.requireNonNull(application);
	}

	@Override
	public Optional<AssetBuilderInvocation> findInvocation(
			final @NotNull AssetFactory factory,
			final @NotNull URN urn)
	{
		final var location = urn.parent();
		final var assetName = urn.lastComponent()
				.orElseThrow(() -> new IllegalArgumentException("Empty asset URN"));

		final int typeExtensionPos = assetName.lastIndexOf('.');
		if (typeExtensionPos < 0)
			return Optional.empty();

		final int minifiedExtensionPos = assetName.substring(0, typeExtensionPos).lastIndexOf('.');
		if (minifiedExtensionPos < 0)
			return Optional.empty();
		if (!assetName.substring(minifiedExtensionPos + 1, typeExtensionPos).equals(MINIFIED_EXTENSION))
			return Optional.empty();

		final String sourceName = assetName.substring(0, minifiedExtensionPos);
		final String typeExtension = assetName.substring(typeExtensionPos + 1);
		final String sourceFileName = String.format("%s.%s", sourceName, typeExtension);

		return factory
				.getAsset(location
						.map(locationUrn -> locationUrn.append(sourceFileName))
						.orElse(URN.EMPTY))
				.map(source -> {
					if (typeExtension.equals(STYLESHEET_EXTENSION))
						return new StyleSheetInvocation(source);
					else if (typeExtension.equals(JAVASCRIPT_EXTENSION))
						return new JavaScriptInvocation(source);
					return null;
				});
	}

	private abstract class GenericInvocation implements AssetBuilderInvocation {
		private final Resource m_source;

		GenericInvocation(final Resource source) {
			m_source = source;
		}

		@Override
		public boolean isCacheValid(final @NotNull Resource cachedResource) {
			final var cacheLastModifiedionTime = cachedResource.attributes().lastModifiedTime().toMillis();
			final var sourceLastModifiedTime = m_source.attributes().lastModifiedTime().toMillis();

			return sourceLastModifiedTime <= cacheLastModifiedionTime;
		}

		Resource getSource() {
			return m_source;
		}
	}

	private class StyleSheetInvocation extends GenericInvocation {
		StyleSheetInvocation(final Resource source) {
			super(source);
		}

		@Override
		public Resource build() throws IOException {
			return getSource();
		}
	}

	private class JavaScriptInvocation extends GenericInvocation {
		JavaScriptInvocation(final Resource source) {
			super(source);
		}

		@Override
		public Resource build() throws IOException {
			return getSource();
		}
	}
}
