package autist.server.application.parameter.handler;

import org.jetbrains.annotations.Nullable;

import java.text.DecimalFormat;
import java.text.ParseException;

public class IntegerHandler extends IntegerHandlerBase<Integer> {
	public IntegerHandler() { }

	public IntegerHandler(final String format) {
		super(format);
	}

	@Override
	protected Integer parse(@Nullable final DecimalFormat format, final String value)
			throws ParseException
	{
		if (format == null)
			return Integer.valueOf(value);

		return format.parse(value).intValue();
	}
}
