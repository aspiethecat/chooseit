package autist.server.application;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import java.io.IOException;

import autist.commons.net.URN;
import autist.configuration.ConfigurationAdapter;
import autist.configuration.ConfigurationException;

import autist.server.configuration.AssetBuilderSpecification;

public interface AssetBuilder {
	Optional<AssetBuilderInvocation> findInvocation(
			final @NotNull AssetFactory factory,
			final @NotNull URN urn)
			throws IOException;

	class Adapter extends ConfigurationAdapter<
			AssetBuilderSpecification,
			Class<? extends AssetBuilder>>
	{
		public Class<? extends AssetBuilder> unmarshal(
				final AssetBuilderSpecification container)
				throws Exception
		{
			return container.getBuilderClass();
		}
	}

	class ClassAdapter extends ConfigurationAdapter<String, Class<? extends AssetBuilder>> {
		@Override
		@SuppressWarnings("unchecked")
		public Class<? extends AssetBuilder> unmarshal(
				final String className)
				throws Exception
		{
			final Class<?> builderClass;

			try {
				builderClass = Class.forName(className);
			} catch (final ClassNotFoundException e) {
				throw new ConfigurationException(
						String.format("Resource builder class not found: %s", className));
			}

			if (!AssetBuilder.class.isAssignableFrom(builderClass)) {
				throw new ConfigurationException(
						String.format("Invalid resource builder superclass: %s", className));
			}

			return (Class<? extends AssetBuilder>) builderClass;
		}
	}
}
