package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import java.net.URI;

import autist.configuration.ConfigurationException;

import autist.server.configuration.AssetRootConfiguration;
import autist.server.configuration.AssetRootSpecification;
import autist.server.configuration.NamedEntity;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class AssetRoot extends ResourceRoot {
	private final String m_prefix;

	private AssetRoot(
			final @NotNull String name,
			final @NotNull URI uri,
			final @Nullable String prefix)
			throws ConfigurationException
	{
		super(
				Objects.requireNonNull(name, "name == null"),
				Objects.requireNonNull(uri, "uri == null"));

		m_prefix = prefix;
	}

	@Contract(pure = true)
	public final String getPrefix() {
		return m_prefix;
	}

	public static List<AssetRoot> buildList(
			final @NotNull ApplicationContext context,
			final @NotNull Map<String, AssetRootSpecification> specificationMap,
			final @NotNull Map<String, AssetRootConfiguration> configurationMap)
			throws ConfigurationException
	{
		return NamedEntity.buildList(
				context,
				AssetRoot.class,
				AssetRoot::configure,
				specificationMap,
				configurationMap);
	}

	@NotNull
	@Contract("_, _ -> new")
	private static AssetRoot configure(
			final @NotNull ApplicationContext context,
			final @NotNull AssetRootSpecification specification)
			throws ConfigurationException
	{
		return new AssetRoot(
				specification.name(),
				configureURI(AssetRoot.class, specification),
				specification.getPrefix());
	}
}
