package autist.server.application.parameter.handler;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.regex.Pattern;

public class StringHandler implements TypeHandler<String> {
	private final Pattern m_pattern;

	public StringHandler() {
		m_pattern = null;
	}

	public StringHandler(final @NotNull String format) {
		m_pattern = Pattern.compile(Objects.requireNonNull(format));
	}

	@Override
	public String parse(
			final @NotNull Class<String> actualType,
			final @NotNull String value)
	{
		Objects.requireNonNull(actualType);
		Objects.requireNonNull(value);

		if ((m_pattern != null) && !m_pattern.matcher(value).matches())
			return null;

		return value;
	}
}
