package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;

import autist.commons.reflection.ReflectionUtil;
import autist.configuration.ConfigurationException;

import autist.application.Request;
import autist.application.action.AbstractAction;
import autist.application.handler.HandlerContext;
import autist.application.handler.HandlerException;
import autist.application.action.ActionInvocation;
import autist.application.parameter.Parameters;
import autist.application.view.AbstractView;

@SuppressWarnings("unused")
final class ActionInvocationImpl<
		ActionType extends AbstractAction<ActionContextType, QueryType>,
		ActionContextType extends HandlerContext<ActionContextType>,
		QueryType extends Parameters<QueryType>>
		implements ActionInvocation<ActionType, ActionContextType, QueryType>
{
	@NotNull
	private final ApplicationContext m_context;

	@NotNull
	private final Class<ActionType> m_actionType;

	@NotNull
	private final Class<ActionContextType> m_actionContextType;

	@NotNull
	private final Class<QueryType> m_queryType;

	@NotNull
	private final ActionType m_action;

	@NotNull
	private final ActionContextType m_actionContext;

	@SuppressWarnings("unchecked")
	private ActionInvocationImpl(
			final @NotNull ApplicationContext context,
			final @NotNull Class<ActionType> actionType)
	{
		m_context = context;
		m_actionType = actionType;

		m_actionContextType = ReflectionUtil.getGenericParameterType(
				m_actionType, AbstractAction.class, 0);
		m_queryType = ReflectionUtil.getGenericParameterType(
				m_actionType, AbstractAction.class, 1);
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Class<ActionType> actionType() {
		return m_actionType;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Class<ActionContextType> actionContextType() {
		return m_actionContextType;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Class<QueryType> queryType() {
		return m_queryType;
	}

	@NotNull
	@Contract(pure = true)
	public ActionType action() {
		return m_action;
	}

	@NotNull
	@Contract(pure = true)
	public ActionContextType context() {
		return m_actionContext;
	}

	@NotNull
	public Optional<AbstractView> invoke(
			final @NotNull Request request)
			throws HandlerException
	{
	}

	@Override
	@Contract(value = "null -> false", pure = true)
	public boolean equals(final Object object) {
		if (object == this)
			return true;
		if (object == null)
			return false;

		if (object instanceof ActionInvocation)
			return m_actionType.equals(((ActionInvocationImpl) object).actionType());

		return false;
	}

	@Override
	public int hashCode() {
		return m_actionType.hashCode();
	}

	@NotNull
	@Contract("_ -> new")
	@SuppressWarnings("unchecked")
	public static <
			ActionType extends AbstractAction<ContextType, QueryType>,
			ContextType extends HandlerContext<ContextType>,
			QueryType extends Parameters<QueryType>>
	ActionInvocationImpl<ActionType, ContextType, QueryType> create(
			final @NotNull String actionClass)
			throws ConfigurationException
	{
		Objects.requireNonNull(actionClass, "actionClass == null");
		return new ActionInvocationImpl(getActionClass(actionClass));
	}

	@NotNull
	@Contract("_ -> new")
	@SuppressWarnings("unchecked")
	public static <
		ActionType extends AbstractAction<ActionContextType, QueryType>,
		ActionContextType extends HandlerContext<ActionContextType>,
		QueryType extends Parameters<QueryType>>
	ActionInvocationImpl<ActionType, ActionContextType, QueryType> create(
			final @NotNull Class<ActionType> actionClass)
	{
		Objects.requireNonNull(actionClass, "actionClass == null");
		return new ActionInvocationImpl(actionClass);
	}

	@SuppressWarnings("unchecked")
	private static <
		ActionType extends AbstractAction<ContextType, QueryType>,
		ContextType extends HandlerContext<ContextType>,
		QueryType extends Parameters<QueryType>>
	Class<ActionType> getActionClass(final @NotNull String className)
			throws ConfigurationException
	{
		final Class<ActionType> actionClass;
		try {
			actionClass = (Class<ActionType>) Class.forName(className);
		} catch (final ClassNotFoundException e) {
			throw new ConfigurationException(
					String.format("Action handler class not found: %s", className));
		}

		if (!AbstractAction.class.isAssignableFrom(actionClass)) {
			throw new ConfigurationException(
					String.format("Invalid action handler superclass: %s", className));
		}

		return actionClass;
	}
}
