package autist.server.application;

import autist.application.action.AbstractAction;

class ActionHandlerCache extends InstanceCache<AbstractAction<?, ?>, ActionHandler<?, ?, ?>>
{
}
