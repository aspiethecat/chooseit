package autist.server.application.query;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.metadata.BeanDescriptor;
import javax.validation.metadata.PropertyDescriptor;

import autist.commons.resource.ResourceLoader;
import autist.server.application.InstanceCache;

import autist.application.Request;
import autist.application.handler.HandlerException;
import autist.application.query.Parameters;


public final class QueryBuilder<QueryType extends Parameters<QueryType>> {
	private static final ValidatorFactory m_validatorFactory =
			Validation.buildDefaultValidatorFactory();

	private final QueryDescriptor m_descriptor;
	private final Map<String, ParameterBuilder<QueryType>> m_builderMap;

	@SuppressWarnings("unchecked")
	public QueryBuilder(final Class<QueryType> queryClass) {
		m_descriptor = new QueryDescriptor(queryClass);

		m_builderMap = createBuilderMap(m_queryClass, createValidatorMap(m_queryClass));
	}

	private QueryType parseRequest(
			final @NotNull QueryType query,
			final @NotNull Request request,
			final @NotNull Collection<HandlerException> errors)
	{
		final Map<String, List<String>> parameters = request.getParameters();
		final Map<String, String> cookies = request.getCookies();

		m_builderMap.forEach((propertyName, builder) -> {
			try {
				if (builder instanceof CookieBuilder) {
					((CookieBuilder<QueryType>) builder).build(
							query, cookies.get(builder.getParameterName()));
				} else if (builder instanceof FieldBuilder) {
					((FieldBuilder<QueryType>) builder).build(
							query, parameters.get(builder.getParameterName()));
				} else {
					throw new AssertionError(String.format(
							"Unsupported parameter builder type: %s",
							builder.getClass().getName()));
				}
			} catch (final HandlerException e) {
				errors.add(e);
			}
		});

		return query;
	}

	private Optional<QueryType> createQuery(
			final @NotNull Request request,
			final @NotNull Collection<HandlerException> errors)
	{
		QueryType query = null;
		try {
			query = Parameters.create(m_queryClass, request);
		} catch (final HandlerException e) {
			errors.add(e);
		}

		return Optional.ofNullable(query);
	}

	private Map<String, ParameterBuilder<QueryType>> createBuilderMap(
			final @NotNull Class<QueryType> queryClass,
			final Map<String, FieldValidator<QueryType>> validatorMap)
	{
		final Map<String, ParameterBuilder<QueryType>> builderMap =
				new HashMap<>(m_descriptor.getParameterCount());

		m_descriptor
				.getParameters()
				.forEach(parameter -> {
					final String name = parameter.getName();

					final ParameterBuilder<QueryType> builder;

					if (parameter.isCookie()) {
						builder = new CookieBuilder<>(parameter);
					} else {
						final FieldValidator<QueryType> fieldValidator =
								(validatorMap != null) ? validatorMap.get(name) : null;
						builder = new FieldBuilder<>(parameter, fieldValidator);
					}

					builderMap.put(name, builder);
				});

		return builderMap;
	}

	private
	<T extends Parameters<T>> Map<String, FieldValidator<T>>
	createValidatorMap(final @NotNull Class<T> queryClass)
	{
		final Validator validator = m_validatorFactory.getValidator();
		final BeanDescriptor validationDescriptor =
				validator.getConstraintsForClass(queryClass);

		if (!validationDescriptor.isBeanConstrained())
			return null;

		final Map<String, FieldValidator<T>> validatorMap =
				new HashMap<>(m_descriptor.getParameterCount());

		m_descriptor
				.getParameters()
				.forEach(item -> {
					PropertyDescriptor property = validationDescriptor.getConstraintsForProperty(item.getPropertyName());
					if (property == null) {
						property = validationDescriptor.getConstraintsForProperty("m_" + item.getPropertyName());
					}
					if (property == null)
						return;

					final FieldValidator<T> fieldValidator =
							new FieldValidator<>(queryClass, property, item.getName(), m_resourceBundle);

					validatorMap.put(item.getPropertyName(), fieldValidator);
				});

		return validatorMap;
	}
}
