package autist.server.application.resource;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import java.io.IOException;

import java.net.URI;

import autist.application.resource.ResourceStore;

@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class AbstractResourceStore
		implements ResourceStore
{
	private final String m_name;
	private final URI m_uri;

	protected AbstractResourceStore(
			final @NotNull StoreType type,
			final @NotNull String name,
			final @NotNull URI uri)
	{
		Objects.requireNonNull(type, "type == null");
		Objects.requireNonNull(name, "name == null");
		Objects.requireNonNull(uri, "uri == null");

		if (!uri.isAbsolute())
			throw new IllegalArgumentException("Resource store URI should be absolute");
		if (!uri.getScheme().equals(type.key()))
			throw new IllegalArgumentException(String.format("Invalid asset URI scheme: %s", uri.toString()));

		m_name = name;
		m_uri = uri;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public final String name() {
		return m_name;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public final URI uri() {
		return m_uri;
	}

	@Override
	@Contract(pure = true)
	public abstract boolean isCacheRequired();

	@NotNull
	public static ResourceStore create(
			final @NotNull String name,
			final @NotNull URI uri)
			throws IOException
	{
		Objects.requireNonNull(name, "name == null");
		Objects.requireNonNull(uri,"uri == null");

		return StoreType
				.resolve(uri)
				.orElseThrow(() -> new IllegalArgumentException(
						String.format("Unsupported asset URI scheme: %s", uri.toString())))
				.storeBuilder()
				.create(name, uri);
	}
}
