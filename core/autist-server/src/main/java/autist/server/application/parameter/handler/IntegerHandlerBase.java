package autist.server.application.parameter.handler;

import java.text.DecimalFormat;

abstract class IntegerHandlerBase<T extends Number> extends NumberHandler<T> {
	IntegerHandlerBase() { }

	IntegerHandlerBase(final String format) {
		super(format);
	}

	@Override
	protected void setupFormat(final DecimalFormat format) {
		format.setParseBigDecimal(false);
		format.setParseIntegerOnly(true);
	}
}
