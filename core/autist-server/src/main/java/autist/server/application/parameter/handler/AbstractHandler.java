package autist.server.application.parameter.handler;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import autist.application.parameter.ParameterDescriptor;
import autist.application.parameter.ParameterHandler;
import autist.application.parameter.ParameterHandlerContext;

@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class AbstractHandler<
		HandlerType extends AbstractHandler<HandlerType, ValueType>, ValueType>
		implements ParameterHandler<HandlerType, AbstractHandler.DefaultContext, ValueType>
{
	@NotNull
	@Override
	public DefaultContext getContext(final @NotNull ParameterDescriptor descriptor) {
		return new DefaultContext(descriptor);
	}

	static class DefaultContext
			implements ParameterHandlerContext<DefaultContext>
	{
		@NotNull
		private final ParameterDescriptor m_descriptor;

		DefaultContext(final @NotNull ParameterDescriptor descriptor) {
			m_descriptor = descriptor;
		}

		@NotNull
		@Contract(pure = true)
		ParameterDescriptor descriptor() {
			return m_descriptor;
		}
	}
}
