package autist.server.application.parameter.handler;

import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LocalDateHandler
		extends LocalDateTimeHandlerBase<LocalDate>
{
	public LocalDateHandler() { }

	public LocalDateHandler(final String format) {
		super(format);
	}

	@Override
	protected LocalDate parse(
			@Nullable final DateTimeFormatter format,
			final String value)
	{
		if (format == null)
			return LocalDate.parse(value);

		return LocalDate.parse(value, format);
	}
}
