package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

import java.lang.reflect.Constructor;

import autist.commons.reflection.ClassUtil;
import autist.commons.reflection.InvocationException;

@SuppressWarnings({"WeakerAccess", "unused"})
public class InstanceCache<KeyType, ValueType>
{
	private static final int DEFAULT_CAPACITY = 128;

	@NotNull
	private final Map<Class<? extends KeyType>, ValueType> m_registry;

	public InstanceCache() {
		this(DEFAULT_CAPACITY);
	}

	public InstanceCache(int initialCapacity) {
		if (initialCapacity < 0)
			throw new IllegalArgumentException("initialCapacity < 0");

		m_registry = new ConcurrentHashMap<>(initialCapacity);
	}

	@Contract(pure = true)
	public boolean isEmpty() {
		return m_registry.isEmpty();
	}

	@Contract(pure = true)
	public int size() {
		return m_registry.size();
	}

	@Contract(pure = true)
	public boolean contains(
			final @NotNull Class<? extends KeyType> key)
	{
		Objects.requireNonNull(key, "key == null");
		return m_registry.containsKey(key);
	}

	@NotNull
	@Contract(pure = true)
	public Optional<ValueType> get(
			final @NotNull Class<? extends KeyType> key)
	{
		Objects.requireNonNull(key, "key == null");
		return Optional.ofNullable(m_registry.get(key));
	}

	@NotNull
	@Contract(pure = true)
	public ValueType getOrDefault(
			final @NotNull Class<? extends KeyType> key,
			final @NotNull ValueType defaultValue)
	{
		Objects.requireNonNull(key, "key == null");
		Objects.requireNonNull(defaultValue, "defaultValue == null");

		return get(key).orElse(defaultValue);
	}

	public Optional<ValueType> put(
			final @NotNull Class<? extends KeyType> key,
			final @NotNull ValueType value)
	{
		Objects.requireNonNull(key, "key == null");
		Objects.requireNonNull(value, "value == null");

		return Optional.ofNullable(m_registry.put(key, value));
	}

	public Optional<ValueType> putIfAbsent(
			final @NotNull Class<? extends KeyType> key,
			final @NotNull ValueType value)
	{
		Objects.requireNonNull(key, "key == null");
		Objects.requireNonNull(value, "value == null");

		return Optional.ofNullable(m_registry.putIfAbsent(key, value));
	}

	@NotNull
	public Optional<ValueType> create(
			final @NotNull Class<? extends KeyType> key,
			final @NotNull Class<? extends ValueType> clazz,
			final Object... constructorArgs)
			throws InvocationException, SecurityException
	{
		Objects.requireNonNull(key, "key == null");
		Objects.requireNonNull(clazz, "clazz == null");

		return Optional.ofNullable(
				m_registry.put(key, ClassUtil.createInstance(clazz, constructorArgs)));
	}

	@NotNull
	public Optional<ValueType> create(
			final @NotNull Class<? extends KeyType> key,
			final @NotNull Constructor<? extends ValueType> constructor,
			final Object... constructorArgs)
			throws InvocationException, SecurityException
	{
		Objects.requireNonNull(key, "key == null");
		Objects.requireNonNull(constructor, "constructor == null");

		return Optional.ofNullable(
				m_registry.put(key, ClassUtil.createInstance(constructor, constructorArgs)));
	}

	@NotNull
	public ValueType createIfAbsent(
			final @NotNull Class<? extends KeyType> key,
			final @NotNull Class<? extends ValueType> clazz,
			final Object... constructorArgs)
			throws InvocationException, SecurityException
	{
		Objects.requireNonNull(key, "key == null");
		Objects.requireNonNull(clazz, "clazz == null");

		return m_registry.computeIfAbsent(key, arg -> ClassUtil.createInstance(clazz, constructorArgs));
	}

	@NotNull
	public ValueType createIfAbsent(
			final @NotNull Class<? extends KeyType> key,
			final @NotNull Constructor<? extends ValueType> constructor,
			final Object... constructorArgs)
			throws InvocationException, SecurityException
	{
		Objects.requireNonNull(key, "key == null");
		Objects.requireNonNull(constructor, "constructor == null");

		return m_registry.computeIfAbsent(key, arg -> ClassUtil.createInstance(constructor, constructorArgs));
	}

	@NotNull
	public ValueType createIfAbsent(
			final @NotNull Class<? extends KeyType> key,
			final @NotNull Supplier<ValueType> supplier)
	{
		Objects.requireNonNull(key, "key == null");
		Objects.requireNonNull(supplier, "supplier == null");

		return m_registry.computeIfAbsent(key, arg -> supplier.get());
	}

	@NotNull
	public Optional<ValueType> computeIfAbsent(
			final @NotNull Class<? extends KeyType> key,
			final @NotNull Supplier<ValueType> supplier)
	{
		Objects.requireNonNull(key, "key == null");
		Objects.requireNonNull(supplier, "supplier == null");

		return Optional.ofNullable(
				m_registry.computeIfAbsent(key, arg -> supplier.get()));
	}

	@NotNull
	public Optional<ValueType> remove(
			final @NotNull Class<? extends KeyType> key)
	{
		Objects.requireNonNull(key, "key == null");
		return Optional.ofNullable(m_registry.remove(key));
	}

	public void clear() {
		m_registry.clear();
	}
}
