package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import java.nio.file.Path;

import java.net.URI;
import java.net.URISyntaxException;

import autist.commons.util.DeferredOptional;
import autist.configuration.ConfigurationException;

import autist.server.configuration.NamedEntity;
import autist.server.configuration.NamedEntityType;
import autist.server.configuration.ResourceRootConfiguration;
import autist.server.configuration.ResourceRootSpecification;
import autist.server.application.resource.StoreType;

public abstract class ResourceRoot extends NamedEntity {
	public static final StoreType DEFAULT_STORE_TYPE = StoreType.CLASSPATH;
	public static final String DEFAULT_URI_SCHEME = DEFAULT_STORE_TYPE.key();

	@NotNull
	private URI m_uri;

	ResourceRoot(
			final @NotNull String name,
			final @NotNull URI uri)
	{
		super(Objects.requireNonNull(name, "name"));
		m_uri = Objects.requireNonNull(uri, "uri");
	}

	@NotNull
	@Contract(pure = true)
	public URI uri() {
		return m_uri;
	}

	@NotNull
	static <
			ResourceType extends ResourceRoot,
			SpecificationType extends ResourceRootSpecification<ResourceType, ConfigurationType>,
			ConfigurationType extends ResourceRootConfiguration<ResourceType, SpecificationType>>
	URI configureURI(
			@NotNull Class<ResourceType> entityClass,
			@NotNull SpecificationType specification)
			throws ConfigurationException
	{
		final var entityName = NamedEntityType
				.resolve(entityClass)
				.orElse(NamedEntityType.GENERIC)
				.description();

		final var uriFuture = new DeferredOptional<URI>();
		final var pathFuture = new DeferredOptional<Path>();

		specification.configuration().ifPresent(configuration -> {
			configuration.uri().ifPresent(uriFuture::bind);
			configuration.path().ifPresent(pathFuture::bind);
		});

		if (uriFuture.isAbsent()) {
			uriFuture.bind(pathFuture
					.map(path -> path.toAbsolutePath().toUri())
					.or(specification::uri)
					.orElseThrow(() -> new ConfigurationException(
							String.format("Unspecified %s URI: %s", entityName, specification.name()))));
		}

		final URI uri = uriFuture.get();

		if (!uri.isAbsolute()) {
			try {
				return new URI(DEFAULT_URI_SCHEME, uri.getSchemeSpecificPart(), null);
			} catch (final URISyntaxException e) {
				throw new ConfigurationException(String.format(
						"Invalid %s URI syntax: %s", entityName, uri));
			}
		}

		return uri;
	}
}