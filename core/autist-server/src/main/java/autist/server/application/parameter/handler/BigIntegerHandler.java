package autist.server.application.parameter.handler;

import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.text.DecimalFormat;
import java.text.ParseException;

public class BigIntegerHandler
		extends NumberHandler<BigInteger>
{
	public BigIntegerHandler() { }

	public BigIntegerHandler(final String format) {
		super(format);
	}

	@Override
	protected void setupFormat(final DecimalFormat format) {
		format.setParseBigDecimal(true);
		format.setParseIntegerOnly(true);
	}

	@Override
	protected BigInteger parse(@Nullable final DecimalFormat format, final String value)
			throws ParseException
	{
		if (format == null)
			return new BigInteger(value);

		return ((BigDecimal) format.parse(value)).toBigInteger();
	}
}
