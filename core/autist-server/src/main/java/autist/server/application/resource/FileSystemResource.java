package autist.server.application.resource;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;

import java.net.MalformedURLException;
import java.net.URL;

import autist.commons.net.URN;

import autist.application.resource.Resource;
import autist.application.resource.ResourceAttributes;

class FileSystemResource extends AbstractResource<FileSystemStore> {
	private final URL m_url;
	private final ResourceAttributes m_attributes;
	private final RandomAccessFile m_handle;
	private final FileChannel m_channel;
	private final InputStream m_stream;

	FileSystemResource(
			final @NotNull FileSystemStore root,
			final @NotNull URN urn)
			throws IOException
	{
		this(
				Objects.requireNonNull(root, "root"),
				Objects.requireNonNull(urn, "urn"),
				null);
	}

	FileSystemResource(
			final @NotNull FileSystemStore root,
			final @NotNull URN urn,
			@Nullable final Resource source)
			throws IOException
	{
		super(
				Objects.requireNonNull(root, "root"),
				Objects.requireNonNull(urn, "urn"));

		final var systemPath = buildSystemPath();

		m_url = buildURL(systemPath);
		m_handle = open(systemPath, source);
		m_channel = m_handle.getChannel();
		m_stream = Channels.newInputStream(m_channel);

		if (source != null) {
			copyContent(source);
			copyAttributes(systemPath, source);
		}

		m_attributes = readAttributes(systemPath);
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public URL url() {
		return m_url;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public ResourceAttributes attributes() {
		return m_attributes;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public InputStream inputStream() {
		return m_stream;
	}

	@Override
	public void close() throws IOException {
		m_handle.close();
	}

	@NotNull
	private Path buildSystemPath() {
		return store().systemPath().resolve(urn().toPath());
	}

	@NotNull
	private URL buildURL(final @NotNull Path systemPath) {
		try {
			return systemPath.toUri().toURL();
		} catch (final MalformedURLException e) {
			throw new AssertionError(e);
		}
	}

	@NotNull
	@Contract("_, _ -> new")
	private RandomAccessFile open(
			final @NotNull Path systemPath,
			final @Nullable Resource source)
			throws IOException
	{
		try {
			if (source != null) {
				final var dir = systemPath.getParent();

				if (!Files.exists(dir))
					Files.createDirectories(dir);

				return new RandomAccessFile(systemPath.toFile(), "rw");
			} else {
				return new RandomAccessFile(systemPath.toFile(), "r");
			}
		} catch (final FileNotFoundException e) {
			throw new NoSuchFileException(urn().toString());
		}
	}

	@NotNull
	@Contract("_ -> new")
	private Attributes readAttributes(
			final @NotNull Path systemPath)
			throws IOException
	{
		try {
			return new Attributes(systemPath);
		} catch (final IOException e) {
			throw new IOException(
					String.format("Can't get resource attributes: %s: %s", store().name(), urn()),
					e);
		}
	}

	private void copyAttributes(
			final @NotNull Path systemPath,
			final @NotNull Resource source)
			throws IOException
	{
		try {
			Files.setLastModifiedTime(systemPath, source.attributes().lastModifiedTime());
		} catch (final IOException e) {
			throw new IOException(
					String.format("Can't copy resource attributes: %s: %s", store().name(), urn()),
					e);
		}
	}

	private void copyContent(
			final @NotNull Resource source)
			throws IOException
	{
		final var sourceStream = source.inputStream();
		final var sourceChannel = Channels.newChannel(sourceStream);

		try {
			m_channel.truncate(0);
			m_channel.transferFrom(sourceChannel, 0, source.attributes().length());
		} catch (final IOException e) {
			throw new IOException(
					String.format("Can't copy resource data: %s: %s", store().name(), urn()),
					e);
		}
	}

	private static class Attributes implements ResourceAttributes {
		private BasicFileAttributes m_attributes;

		Attributes(final @NotNull Path systemPath) throws IOException {
			m_attributes = Files.readAttributes(systemPath, BasicFileAttributes.class);
		}

		@Override
		@Contract(pure = true)
		public long length() {
			return m_attributes.size();
		}

		@Override
		@Contract(pure = true)
		public FileTime lastModifiedTime() {
			return m_attributes.lastModifiedTime();
		}
	}
}
