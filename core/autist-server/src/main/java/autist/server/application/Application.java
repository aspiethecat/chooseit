package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.Optional;

import java.util.logging.Level;

import java.io.IOException;

import io.netty.handler.codec.http.HttpResponseStatus;

import autist.configuration.ConfigurationException;

import autist.journal.JournalContext;

import autist.application.Request;
import autist.application.view.ViewException;
import autist.application.mail.Mailer;

import autist.server.HttpException;

import autist.server.configuration.ApplicationConfiguration;
import autist.server.configuration.ApplicationSpecification;
import autist.server.configuration.AssetFactoryConfiguration;
import autist.server.configuration.TemplateFactoryConfiguration;

import autist.server.application.mail.MailerImpl;
import autist.server.application.routing.Router;
import autist.server.application.view.ErrorView;

/**
 * Application.
 */
@SuppressWarnings("javadoc")
public final class Application implements JournalContext {
	@NotNull
	private final ApplicationSpecification m_specification;

	@NotNull
	private final ApplicationConfiguration m_configuration;

	@NotNull
	private final ApplicationContext m_context;

	@NotNull
	private final Router m_router;

	@SuppressWarnings("LeakingThisInConstructor")
	public Application(
			final ApplicationSpecification specification,
			final ApplicationConfiguration configuration)
			throws ConfigurationException
	{
		m_specification = Objects.requireNonNull(specification, "specification == null");
		m_configuration = Objects.requireNonNull(configuration, "configuration == null");

		m_context = setupContext();
		m_router = setupRouter();

		m_context.log(Level.INFO,"Warming up...");
		m_router.warmUp();
	}

	@NotNull
	@Contract(" -> new")
	private ApplicationContext setupContext()
			throws ConfigurationException
	{
		final ApplicationContext context = new ApplicationContext(
				m_configuration.getName(),
				m_configuration.getEnvironment());

		context.put(ActionCache.class, new ActionCache());
		context.put(ActionHandlerCache.class, new ActionHandlerCache());
		context.put(QueryBuilderCache.class, new QueryBuilderCache());

		context.put(createAssetCache());
		context.put(createDataSourceFactory());

		createAssetFactory().ifPresent(context::put);
		createTemplateFactory().ifPresent(context::put);
		createMailer().ifPresent(context::put);

		return context;
	}

	@NotNull
	private ResourceCache createAssetCache() throws ConfigurationException {
		m_context.log(Level.INFO, "Configuring asset cache...");

		final ResourceCache cache;
		try {
			cache = new ResourceCache(m_configuration.getCacheRoot());
		} catch (final IOException e) {
			throw new ConfigurationException("Asset cache initialization failed", e);
		}

		cleanupAssetCache(cache);

		return cache;
	}

	private void cleanupAssetCache(
			final @NotNull ResourceCache cache)
			throws ConfigurationException
	{
		m_context.log(Level.INFO, "Cleaning up asset cache...");

		try {
			cache.cleanup();
		} catch (final IOException e) {
			throw new ConfigurationException("Asset cache cleanup failed", e);
		}
	}

	@NotNull
	private Optional<TemplateFactory> createTemplateFactory()
			throws ConfigurationException
	{
		try {
			return m_specification
					.getTemplateFactorySpecification()
					.map(templateFactorySpecification -> {
						m_context.log(Level.INFO, "Configuring template factory...");

						TemplateFactoryConfiguration templateFactoryConfiguration = m_configuration
								.getTemplateFactoryConfiguration()
								.orElseGet(TemplateFactoryConfiguration::new);

						try {
							return TemplateFactory.build(
									m_context,
									templateFactorySpecification,
									templateFactoryConfiguration);
						} catch (final ConfigurationException e) {
							throw new RuntimeException(e);
						}
					});
		} catch (final RuntimeException e) {
			if (e.getCause() instanceof ConfigurationException)
				throw (ConfigurationException) e.getCause();
			throw e;
		}
	}

	@NotNull
	private DataSourceFactory createDataSourceFactory()
			throws ConfigurationException
	{
		m_context.log(Level.INFO, "Configuring data sources...");

		return DataSourceFactory.build(
				m_context,
				m_specification.getDataSources(),
				m_configuration.getDataSources());
	}

	@NotNull
	private Optional<AssetFactory> createAssetFactory()
			throws ConfigurationException
	{
		try {
			return m_specification
					.getAssetFactorySpecification()
					.map(assetFactorySpecification -> {
						m_context.log(Level.INFO, "Configuring asset factory...");

						AssetFactoryConfiguration assetFactoryConfiguration = m_configuration
								.getAssetFactoryConfiguration()
								.orElseGet(AssetFactoryConfiguration::new);

						try {
							return AssetFactory.build(
									m_context,
									assetFactorySpecification,
									assetFactoryConfiguration);
						} catch (final ConfigurationException e) {
							throw new RuntimeException(e);
						}
					});
		} catch (final RuntimeException e) {
			if (e.getCause() instanceof ConfigurationException)
				throw (ConfigurationException) e.getCause();
			throw e;
		}
	}

	@NotNull
	private Optional<Mailer> createMailer()
			throws ConfigurationException
	{
		try {
			return m_configuration
					.getMailerConfiguration()
					.map(mailerConfiguration -> {
						m_context.log(Level.INFO, "Configuring mailer...");

						try {
							return MailerImpl.build(m_context, mailerConfiguration);
						} catch (final ConfigurationException e) {
							throw new RuntimeException(e);
						}
					});
		} catch (final RuntimeException e) {
			if (e.getCause() instanceof ConfigurationException)
				throw (ConfigurationException) e.getCause();
			throw e;
		}
	}

	@NotNull
	@Contract(" -> new")
	private Router setupRouter()
			throws ConfigurationException
	{
		m_context.log(Level.INFO, "Configuring router...");
		return new Router(m_context, m_specification.routingMapPath());
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public String name() {
		return m_configuration.getName();
	}

	public ResponseImpl handleRequest(
			final RequestImpl request)
			throws HttpException
	{
		Objects.requireNonNull(request);

		final ResponseImpl response =
				new ResponseImpl(HttpResponseStatus.OK, request.isKeepAlive());
		m_router.route(request, response);

		return response;
	}

	public ResponseImpl handleError(
			final @Nullable Request request,
			final @NotNull HttpException error)
	{
		final Level level = error.isServerError() ? Level.SEVERE : Level.WARNING;
		final Throwable cause = Objects.nonNull(error.getCause()) ? error : null;
		final String message = String.format(
				"%s: %s", error.getStatus().reasonPhrase(), error.getMessage());

		m_context.log(level, message, cause);

		final HttpResponseStatus status = error.getStatus();

		boolean keepAlive = false;
		if (request != null)
			keepAlive = request.isKeepAlive();

		ResponseImpl response = new ResponseImpl(status, keepAlive);
		try {
			(new ErrorView(error)).render(m_router.actionContext(), request, response);
		} catch (final ViewException | RuntimeException e) {
			m_context.log(Level.SEVERE, String.format("Can't render error view: %s", e.getMessage()), e);

			response = new ResponseImpl(status, keepAlive);
			ErrorView.renderFallback(response, error);
		}

		return response;
	}
}
