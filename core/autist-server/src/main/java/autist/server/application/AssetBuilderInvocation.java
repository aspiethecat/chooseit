package autist.server.application;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import autist.application.resource.Resource;

public interface AssetBuilderInvocation {
	boolean isCacheValid(final @NotNull Resource cachedResource);

	Resource build() throws IOException;
}
