package autist.server.application.parameter.handler;

import org.jetbrains.annotations.NotNull;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class URIHandler extends AbstractHandler<URIHandler, URI> {
	@Override
	@NotNull
	public Class<URI> type() {
		return URI.class;
	}

	@Override
	@NotNull
	public URI parseValue(
			final @NotNull DefaultContext context,
			final @NotNull String value)
			throws IllegalArgumentException
	{
		try {
			return new URL(value).toURI();
		} catch (final MalformedURLException | URISyntaxException e) {
			throw  new IllegalArgumentException(e);
		}
	}
}
