package autist.server.application.parameter.handler;

import org.jetbrains.annotations.Nullable;

import java.text.DecimalFormat;
import java.text.ParseException;

public class LongHandler extends IntegerHandlerBase<Long> {
	public LongHandler() { }

	public LongHandler(final String format) {
		super(format);
	}

	@Override
	protected Long parse(@Nullable final DecimalFormat format, final String value)
			throws ParseException
	{
		if (format == null)
			return Long.valueOf(value);

		return format.parse(value).longValue();
	}
}
