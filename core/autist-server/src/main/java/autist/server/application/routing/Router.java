package autist.server.application.routing;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;

import java.net.URI;

import autist.configuration.ConfigurationException;

import autist.application.handler.HandlerContext;
import autist.application.handler.HandlerEnvironment;
import autist.application.action.DefaultActionContext;
import autist.application.routing.RoutingContext;
import autist.application.routing.RoutingMap;
import autist.application.routing.RoutingNode;

import autist.server.configuration.ConfigurationLoader;
import autist.server.configuration.RoutingNodeConfiguration;

import autist.server.application.ActionEnvironmentImpl;
import autist.server.application.ApplicationContext;
import autist.server.application.AssetFactory;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class Router {
	/**
	 * Application context.
	 */
	@NotNull
	private final ApplicationContext m_context;

	/**
	 * Asset factory.
	 */
	@NotNull
	private final AssetFactory m_assetFactory;

	/**
	 * Routing map.
	 */
	@NotNull
	private final RoutingMap m_routingMap;

	/**
	 * Action environment context.
	 * @see #actionEnvironment()
	 */
	@NotNull
	private final HandlerEnvironment m_actionEnvironment;

	/**
	 * Default action context.
	 */
	@NotNull
	private final HandlerContext<DefaultActionContext> m_actionContext;

	Router(
			final @NotNull ApplicationContext context,
			final @NotNull URI routingMap)
			throws ConfigurationException
	{
		m_context = context;

		m_assetFactory = context.require(AssetFactory.class);
		m_routingMap = loadRoutingMap(context.routingContext(), routingMap);
		m_actionEnvironment = new ActionEnvironmentImpl(context, this);
		m_actionContext = new DefaultActionContext(m_actionEnvironment);
	}

	@NotNull
	@SuppressWarnings("unchecked")
	private static DefaultRoutingMap loadRoutingMap(
			final @NotNull RoutingContext<?, ?, ?> context,
			final @NotNull URI routingMap)
			throws ConfigurationException
	{
		final RoutingNode root;

		try {
			final var configuration = ConfigurationLoader.load(
					RoutingNodeConfiguration.class,
					ConfigurationLoader.createInternalSource(routingMap));

			root = new DefaultRoutingNode(context, configuration);
		} catch (final FileNotFoundException e) {
			throw new ConfigurationException(
					String.format("Routing map not found: %s", e.getMessage()), e);
		}

		return new DefaultRoutingMap(context, root);
	}
/*
	@NotNull
	private RoutingNode setupRoutingMap(
			final @NotNull URI uri,
			final @NotNull DeferredOptional<Map<String, ActionNode>> identifierMapFuture)
			throws ConfigurationException
	{
		final List<ConfigurationException> errors = new ArrayList<>(0);

		rootNode.configure(actionSet, errors);

		if (!errors.isEmpty()) {
			errors.forEach((item) -> {
				final Throwable cause = item.getCause();
				if (cause == null)
					context.log(Level.SEVERE, item.getMessage());
				else
					context.log(Level.SEVERE, item.getMessage(), item.getCause());
			});

			throw new ConfigurationException("Invalid routing map configuration");
		}

		return rootNode;
	}
*/
	@Contract(pure = true)
	public @NotNull HandlerEnvironment actionEnvironment() {
		return m_actionEnvironment;
	}

	@Contract(pure = true)
	public @NotNull HandlerContext actionContext() {
		return m_actionContext;
	}

	@Contract(pure = true)
	public @NotNull RoutingMap routingMap() {
		return m_routingMap;
	}
/*
	void route(
			final @NotNull RequestImpl request,
			final @NotNull ResponseImpl response)
			throws HttpException
	{
		final Optional<RoutingNode> action = m_routingMap.find(request.getURN());

		if (action.isPresent())
			processAction(action.get(), request, response);
		else
			processResource(request, response);
	}

	public @NotNull Optional<AbstractView> invokeAction(
			final @NotNull RoutingNode routingNode,
			final @NotNull Request request)
			throws IllegalStateException, ActionException
	{
		final ActionInvocation<?, ?, ?> invocation = routingNode.action()
				.orElseThrow(() -> new IllegalStateException("routing node action not configured"));

		try {
			return ActionHandler
					.get(m_context, invocation)
					.invoke(request, routingNode.asset().orElse(null));
		} catch (final ConfigurationException | ActionException | RuntimeException e) {
			throw new ActionException(
					String.format(
							"Action execution failed: %s",
							invocation.actionType().getName()),
					e);
		}
	}

	@SuppressWarnings("unchecked")
	void warmUp() throws ConfigurationException {
		final var exception = new DeferredException<ConfigurationException>();

		m_actionSet.stream()
				.filter(Objects::nonNull)
				.map(invocation -> {
					try {
						ActionHandler.get(m_context, invocation);
					} catch (final ConfigurationException e) {
						exception.bind(e);
						return null;
					}

					return invocation;
				})
				.forEach(invocation -> m_context.log(Level.CONFIG, String.format(
						"Instantiating %s (%s)",
						invocation.handlerClass().getName(),
						invocation.queryClass().getName())));

		exception.throwIfPresent();
	}

	private void processAction(
			final @NotNull RoutingNode routingNode,
			final @NotNull Request request,
			final @NotNull Response response)
			throws HttpException
	{
		if (!routingNode.isConfigured())
			throw new HttpException(HttpException.FORBIDDEN, request.getURN().toString());

		Optional<AbstractView> view;
		try {
			view = invokeFilters(routingNode, request);

			if (!view.isPresent())
				view = invokeAction(routingNode, request);
		} catch (final ActionException | RuntimeException e) {
			m_context.log(Level.SEVERE, e.getMessage(), e.getCause());

			throw new HttpException(
					HttpException.INTERNAL_SERVER_ERROR, request.getURI().toString());
		}

		if (!view.isPresent()) {
			throw new HttpException(
					HttpException.NOT_FOUND, request.getURI().toString());
		}

		renderView(view.get(), request, response);
	}

	@NotNull
	private Optional<AbstractView> invokeFilters(
			final @NotNull RoutingNode routingNode,
			final @NotNull Request request)
			throws ActionException
	{
		final var exception = new DeferredException<ActionException>();

		final Optional<AbstractView> filteredView = routingNode.filters()
				.stream()
				.map(filter -> {
					try {
						return ActionHandler.get(m_context, filter).invoke(request);
					} catch (final ConfigurationException | ActionException | RuntimeException e) {
						exception.bind(new ActionException(
								String.format(
									"Filter execution failed: %s",
									filter.action().getClass().getName()),
								e));
						return Optional.<AbstractView>empty();
					}
				})
				.filter(Optional::isPresent)
				.findFirst()
				.orElse(Optional.empty());

		exception.throwIfPresent();

		return filteredView;
	}

	private void processResource(
			final @NotNull Request request,
			final @NotNull Response response)
			throws HttpException
	{
		final var method = request.getMethod();

		if (!ResourceView.isMethodSupported(method)) {
			throw new HttpException(
					HttpException.METHOD_NOT_ALLOWED,
					String.format("Request method not allowed: %s", method.name()));
		}

		final var urn = request.getURN();
		final var mimeType = new DeferredOptional<String>();

		final Resource resource;
		try {
			resource = m_assetFactory.getAsset(urn, mimeType);
		} catch (final ResourceException e) {
			throw new HttpException(HttpException.INTERNAL_SERVER_ERROR, e.getMessage());
		}

		if (resource == null)
			throw new HttpException(HttpException.NOT_FOUND, urn.toString());

		final var attributes = resource.getAttributes();
		final long lastModified = attributes.getLastModifiedTime().toMillis() / 1000L;
		final boolean modified =
				getOptionalDateHeader(request, HttpHeaderNames.IF_MODIFIED_SINCE)
					.map((date) -> (lastModified >= (date.getTime() / 1000L)))
					.orElse(true) &&
				getOptionalDateHeader(request, HttpHeaderNames.IF_UNMODIFIED_SINCE)
					.map((date) -> (lastModified <= (date.getTime() / 1000L)))
					.orElse(true);

		if (!modified)
			throw HttpException.NOT_MODIFIED;

		final AbstractView view = new ResourceView(method, resource, mimeType.get());

		renderView(view, request, response);
	}

	private static Optional<Date> getOptionalDateHeader(
			final @NotNull Request request,
			final @NotNull CharSequence name)
			throws HttpException
	{
		Objects.requireNonNull(request);
		Objects.requireNonNull(name);

		return getOptionalHeader(request, name, Date.class, HttpDateFormat::parse);
	}

	@SuppressWarnings("unchecked")
	private static <T> Optional<T> getOptionalHeader(
			final @NotNull Request request,
			final @NotNull CharSequence name,
			final @NotNull Class<T> valueClass,
			@Nullable final Function<String, T> converter)
			throws HttpException
	{
		Objects.requireNonNull(request);
		Objects.requireNonNull(name);
		Objects.requireNonNull(valueClass);

		final String source = request.getHeader(name);

		T value = null;
		if ((source != null) && !source.isEmpty()) {
			if (converter != null) {
				value = converter.apply(source);
				if (value == null) {
					throw new HttpException(HttpException.BAD_REQUEST, String.format(
							"Incorrect HTTP header value: %s: %s",
							name, source));
				}
			} else if (String.class.isAssignableFrom(valueClass)) {
				value = (T) source;
			} else {
				throw new AssertionError("Converter not specified for non-string result class");
			}
		}

		return Optional.ofNullable(value);
	}

	private static void renderView(
			final @NotNull AbstractView view,
			final @NotNull Request request,
			final @NotNull Response response)
			throws HttpException
	{
		Objects.requireNonNull(view);
		Objects.requireNonNull(request);
		Objects.requireNonNull(response);

		try {
			view.render(request, response);
		} catch (final ViewException e) {
			if (e.getCause() instanceof HttpException)
				throw (HttpException) e.getCause();

			throw new HttpException(
					HttpException.INTERNAL_SERVER_ERROR,
					"Can't render view",
					e);
		}
	}
	*/
}
