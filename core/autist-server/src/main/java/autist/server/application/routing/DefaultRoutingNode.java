package autist.server.application.routing;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import autist.commons.net.URN;

import autist.application.routing.RouteParameter;
import autist.application.routing.RoutingContext;
import autist.application.routing.RoutingNode;
import autist.application.routing.RouteKey;
import autist.application.routing.RouteTag;

import autist.server.configuration.RoutingNodeConfiguration;

// TODO: Unmodifiable views here!
@SuppressWarnings({"WeakerAccess", "unused"})
final class DefaultRoutingNode<
		ContextType extends RoutingContext<ContextType, KeyType, TagType>,
		KeyType extends Enum<KeyType> & RouteKey<KeyType>,
		TagType extends Enum<TagType> & RouteTag<TagType>>
		implements RoutingNode<
				DefaultRoutingNode<ContextType, KeyType, TagType>,
				ContextType,
				KeyType,
				TagType>
{
	@NotNull
	private final ContextType m_context;

	@Nullable
	private final DefaultRoutingNode<ContextType, KeyType, TagType> m_parent;

	@NotNull
	private final String m_name;

	@NotNull
	private final URN m_path;

	@Nullable
	private final Pattern m_pattern;

	@NotNull
	private final List<RouteParameter> m_parameters;

	@Nullable
	private final KeyType m_key;

	@NotNull
	private final List<TagType> m_tags;

	@Nullable
	private final URN m_target;

	@Nullable
	private final URN m_source;

	@Nullable
	private final Class<?> m_action;

	@NotNull
	private final List<Class<?>> m_filters;

	@NotNull
	private final Map<String, DefaultRoutingNode<ContextType, KeyType, TagType>> m_children;

	DefaultRoutingNode(
			final @NotNull ContextType context,
			final @NotNull RoutingNodeConfiguration configuration)
	{
		this(context, null, configuration);
	}

	DefaultRoutingNode(
			final @NotNull DefaultRoutingNode<ContextType, KeyType, TagType> parent,
			final @NotNull RoutingNodeConfiguration configuration)
	{
		this(parent.context(), parent, configuration);
	}

	private DefaultRoutingNode(
			final @NotNull ContextType context,
			final @Nullable DefaultRoutingNode<ContextType, KeyType, TagType> parent,
			final @NotNull RoutingNodeConfiguration configuration)
	{
		Objects.requireNonNull(context, "context == null");
		Objects.requireNonNull(configuration, "configuration == null");

		if (parent != null) {
			if (!parent.context().equals(context))
				throw new IllegalArgumentException("parent.context() != context");
		}

		m_context = context;
		m_parent = parent;

		if (m_parent != null) {
			m_name = configuration.name()
					.orElseThrow(() -> new IllegalArgumentException("Node name not specified"));
			m_path = m_parent.path().append(m_name);
		} else {
			m_name = null;
			m_path = URN.EMPTY;
		}

		m_pattern = configuration.pattern().orElse(null);
		m_parameters = configuration.parameters();

		m_key = configuration.key()
				.map(key -> m_context
						.resolveKey(key)
						.orElseThrow(() -> new IllegalArgumentException(
								String.format("Invalid node key: %s: %s", m_path.toString(), key))))
				.orElse(null);

		m_tags = configuration.tags()
				.stream()
				.map(tag -> m_context
						.resolveTag(tag)
						.orElseThrow(() -> new IllegalArgumentException(
								String.format("Invalid node tag: %s: %s", m_path.toString(), tag))))
				.collect(Collectors.toList());

		m_target = configuration.target().orElse(null);
		m_source = configuration.source().orElse(null);
		m_action = configuration.action()
				.map(className -> {
					try {
						return Class.forName(className);
					} catch (final ClassNotFoundException e) {
						throw new IllegalArgumentException(
								String.format("Action class not found: %s: %s", m_path.toString(), className));
					}
				})
				.orElse(null);

		m_filters = configuration.filters()
				.stream()
				.map(className -> {
					try {
						return Class.forName(className);
					} catch (final ClassNotFoundException e) {
						throw new IllegalArgumentException(
								String.format("Filter class not found: %s: %s", m_path.toString(), className));
					}
				})
				.collect(Collectors.toList());

		if (!configuration.children().isEmpty()) {
			m_children = new HashMap<>();
			configuration.children()
					.forEach(route -> {
						final var node = new DefaultRoutingNode<>(this, route);
						m_children.put(node.name(), node);
					});
		} else {
			m_children = Collections.emptyMap();
		}
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public ContextType context() {
		return m_context;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Optional<DefaultRoutingNode<ContextType, KeyType, TagType>> parent() {
		return Optional.ofNullable(m_parent);
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Optional<KeyType> key() {
		return Optional.ofNullable(m_key);
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public List<TagType> tags() {
		return m_tags;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public URN path() {
		return m_path;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public String name() {
		return m_name;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Optional<Pattern> pattern() {
		return Optional.ofNullable(m_pattern);
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public List<RouteParameter> parameters() {
		return m_parameters;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Optional<URN> target() {
		return Optional.ofNullable(m_target);
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Optional<URN> payload() {
		return Optional.ofNullable(m_source);
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public Optional<Class<?>> action() {
		return Optional.ofNullable(m_action);
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public List<Class<?>> filters() {
		return m_filters;
	}

	@NotNull
	@Contract(pure = true)
	public Optional<DefaultRoutingNode<ContextType, KeyType, TagType>> findChild(
			final @NotNull String key)
	{
		return Optional.of(m_children.get(key));
	}

	@NotNull
	@Contract(pure = true)
	public Iterator<DefaultRoutingNode<ContextType, KeyType, TagType>> iterator() {
		return m_children.values().iterator();
	}
}
