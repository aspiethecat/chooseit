package autist.server.application.resource;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import java.net.URI;

import autist.application.resource.ResourceStore;

@FunctionalInterface
public interface StoreBuilder<StoreType extends ResourceStore> {
	@NotNull
	StoreType create(
			final @NotNull String name,
			final @NotNull URI uri)
			throws IOException;
}
