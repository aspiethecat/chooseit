package autist.server.application.resource;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import java.net.URI;
import java.util.Optional;

import autist.commons.util.EnumResolver;
import autist.application.resource.ResourceStore;
import autist.commons.util.KeySupplier;

public enum StoreType implements KeySupplier<String> {
	CLASSPATH("classpath", ClassPathStore.class, ClassPathStore::new),
	FILESYSTEM("file", FileSystemStore.class, FileSystemStore::new);

	@NotNull
	private final static EnumResolver<String, StoreType> s_resolver =
			new EnumResolver<>(StoreType.class);

	@NotNull
	private final String m_key;

	@NotNull
	private final Class<? extends ResourceStore> m_storeClass;

	@NotNull
	private final StoreBuilder<?> m_storeBuilder;

	<T extends ResourceStore> StoreType(
			final @NotNull String key,
			final @NotNull Class<T> storeClass,
			final @NotNull StoreBuilder<T> storeBuilder)
	{
		m_key = key;
		m_storeClass = storeClass;
		m_storeBuilder = storeBuilder;
	}

	@NotNull
	@Contract(pure = true)
	public String key() {
		return m_key;
	}

	@NotNull
	@Contract(pure = true)
	public Class<? extends ResourceStore> storeClass() {
		return m_storeClass;
	}

	@NotNull
	@Contract(pure = true)
	public StoreBuilder<?> storeBuilder() {
		return m_storeBuilder;
	}

	@NotNull
	public static Optional<StoreType> resolve(final @NotNull URI uri) {
		Objects.requireNonNull(uri, "uri == null");

		if (!uri.isAbsolute())
			throw new IllegalArgumentException("Resource store URI should be absolute");

		return s_resolver.resolve(uri.getScheme());
	}
}
