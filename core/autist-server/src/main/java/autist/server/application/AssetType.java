package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import autist.commons.reflection.ClassUtil;

import autist.configuration.ConfigurationException;

import autist.server.configuration.AssetMatchSpecification;
import autist.server.configuration.AssetTypeConfiguration;
import autist.server.configuration.AssetTypeSpecification;
import autist.server.configuration.NamedEntity;

public final class AssetType extends NamedEntity {
	@NotNull
	private final String m_mimeType;

	@NotNull
	private final Collection<AssetMatchSpecification> m_matches;

	@NotNull
	private final Collection<AssetBuilder> m_builders;

	private AssetType(
			final @NotNull String name,
			final @NotNull String mimeType,
			final @NotNull Collection<AssetMatchSpecification> matches,
			final @NotNull Collection<AssetBuilder> builders)
	{
		super(name);

		m_mimeType = mimeType;
		m_matches  = matches;
		m_builders = builders;
	}

	@NotNull
	@Contract(pure = true)
	public final String mimeType() {
		return m_mimeType;
	}

	@NotNull
	@Contract(pure = true)
	public Collection<AssetMatchSpecification> matches() {
		return m_matches;
	}

	@Contract(pure = true)
	public boolean hasBuilders() {
		return !m_builders.isEmpty();
	}

	@NotNull
	@Contract(pure = true)
	public Collection<AssetBuilder> builders() {
		return m_builders;
	}

	static List<AssetType> buildList(
			final @NotNull ApplicationContext context,
			final @NotNull Map<String, AssetTypeSpecification> specificationMap,
			final @NotNull Map<String, AssetTypeConfiguration> configurationMap)
			throws ConfigurationException
	{
		return buildList(
				context,
				AssetType.class,
				AssetType::configure,
				specificationMap,
				configurationMap);
	}

	@NotNull
	@Contract("_, _ -> new")
	private static AssetType configure(
			final @NotNull ApplicationContext context,
			final @NotNull AssetTypeSpecification specification)
			throws ConfigurationException
	{
		final Collection<AssetMatchSpecification> matches =
				new ArrayList<>(specification.getMatches());

		String mimeType = specification.configuration()
				.map(configuration -> {
					matches.addAll(configuration.getMatches());
					return configuration.getMimeType();
				})
				.orElse(specification.getMimeType());

		if (mimeType == null) {
			throw new ConfigurationException(String.format(
					"Unspecified asset type MIME-type: %s", specification.name()));
		}

		final List<AssetBuilder> builders =
				new ArrayList<>(specification.getBuilders().size());

		for (Class<? extends AssetBuilder> builderClass : specification.getBuilders()) {
			try {
				builders.add(ClassUtil.createInstance(builderClass, context));
			} catch (final IllegalArgumentException e) {
				throw new ConfigurationException(String.format(
						"Can't instantiate asset builder: %s", e.getMessage()));
			}
		}

		return new AssetType(specification.name(), mimeType, matches, builders);
	}
}
