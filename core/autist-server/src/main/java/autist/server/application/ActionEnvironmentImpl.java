package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import autist.commons.context.AbstractContextRoot;

import autist.application.EnvironmentType;
import autist.application.routing.RoutingMap;

import autist.application.handler.HandlerEnvironment;

import autist.server.application.routing.Router;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class ActionEnvironmentImpl
		extends AbstractContextRoot<HandlerEnvironment>
		implements HandlerEnvironment
{
	@NotNull
	private final ApplicationContext m_context;

	@NotNull
	private final Router m_router;

	public ActionEnvironmentImpl(
			final @NotNull ApplicationContext context,
			final @NotNull Router router)
	{
		m_context = context;
		m_router = router;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public EnvironmentType type() {
		return m_context.environmentType();
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public RoutingMap routingMap() {
		return m_router.routingMap();
	}
}
