package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import java.io.IOException;

import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.handler.codec.http.QueryStringDecoder;
import io.netty.handler.codec.http.cookie.ServerCookieDecoder;
import io.netty.handler.codec.http.multipart.Attribute;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;

import autist.commons.net.URN;

import autist.application.Request;
import autist.application.RequestMethod;

public class RequestImpl implements Request
{
	/** Request method */
	private final RequestMethod m_method;

	/** Keep-alive connection flag */
	private final boolean m_keepAlive;

	/** Client address */
	private final InetSocketAddress m_clientAddress;

	/** Server address */
	private final InetSocketAddress m_serverAddress;

	/** Server name */
	private final String m_serverName;

	/** Request URI (normalized and sanitized) */
	private final URI m_uri;

	/** Request URN (normalized and sanitized) */
	private final URN m_urn;

	/** Query string */
	private final String m_queryString;

	/** Headers */
	private final HttpHeaders m_headers;

	/** Parameters */
	private final Map<String, List<String>> m_parameters;

	/** Cookies */
	private final Map<String, String> m_cookies = new HashMap<>();

	public RequestImpl(
			final @NotNull ChannelHandlerContext handlerContext,
			final @NotNull HttpRequest rawRequest,
			@Nullable final HttpPostRequestDecoder postRequestDecoder)
			throws URISyntaxException, IOException
	{
		Objects.requireNonNull(handlerContext);
		Objects.requireNonNull(rawRequest);

		m_method     = RequestMethod.valueOf(rawRequest.method().name());
		m_keepAlive  = HttpUtil.isKeepAlive(rawRequest);

		m_uri         = parseUri(rawRequest.uri());
		m_urn         = (new URN(m_uri)).normalize();
		m_queryString = m_uri.getQuery();

		m_headers = rawRequest.headers();

		m_clientAddress = (InetSocketAddress) handlerContext.channel().remoteAddress();
		m_serverAddress = (InetSocketAddress) handlerContext.channel().localAddress();
		m_serverName    = m_headers.get(HttpHeaderNames.HOST, m_serverAddress.getHostString());

		m_parameters = new HashMap<>((new QueryStringDecoder(m_uri)).parameters());

		if (postRequestDecoder != null) {
			for (final InterfaceHttpData data : postRequestDecoder.getBodyHttpDatas()) {
				if (data.getHttpDataType() == InterfaceHttpData.HttpDataType.Attribute) {
					Attribute attribute = (Attribute) data;

					final List<String> parameters = m_parameters.computeIfAbsent(
							attribute.getName(),
							(key) -> new ArrayList<>());
					parameters.add(attribute.getString());
				}
			}
		}

		String cookieString = rawRequest.headers().get(HttpHeaderNames.COOKIE);
		if (cookieString != null) {
			ServerCookieDecoder.STRICT.decode(cookieString).forEach(cookie ->
				m_cookies.put(cookie.name(), cookie.value()));
		}
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public RequestMethod getMethod() {
		return m_method;
	}

	@Override
	@Contract(pure = true)
	public boolean isKeepAlive() {
		return m_keepAlive;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public InetSocketAddress getClientAddress() {
		return m_clientAddress;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public InetSocketAddress getServerAddress() {
		return m_serverAddress;
	}

	@Override
	@Contract(pure = true)
	public String getServerName() {
		return m_serverName;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public URI getURI() {
		return m_uri;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public final URN getURN() {
		return m_urn;
	}

	@Override
	@Contract(pure = true)
	public final String getQueryString() {
		return m_queryString;
	}

	@Override
	@Nullable
	@Contract(pure = true)
	public final String getHeader(final @NotNull CharSequence name) {
		return m_headers.get(Objects.requireNonNull(name));
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public final Map<String, List<String>> getParameters() {
		return m_parameters;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public final Map<String, String> getCookies() {
		return m_cookies;
	}

	private static URI parseUri(final @NotNull String str) throws URISyntaxException {
		final URI result = new URI(str);

		if ((result.getScheme() != null) || (result.getAuthority() != null))
			throw new URISyntaxException(str, "Request URI should not contain scheme or authority");

		return result;
	}
}
