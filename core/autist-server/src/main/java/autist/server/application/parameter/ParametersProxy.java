package autist.server.application.parameter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import javax.validation.ConstraintViolation;

import autist.application.Request;
import autist.application.handler.HandlerException;
import autist.application.parameter.Parameters;

@SuppressWarnings({"WeakerAccess", "unused"})
public class ParametersProxy<
		ParametersType extends Parameters<ParametersType>>
		implements InvocationHandler, Parameters<ParametersType>
{
	@NotNull
	private Request m_request;

	@Nullable
	private Map<String, Object> m_values = null;

	@Nullable
	private Set<ConstraintViolation<ParametersType>> m_constraintViolations = null;

	public ParametersProxy(
			final @NotNull Request request,
			final @NotNull Collection<HandlerException> errors)
			throws HandlerException
	{
		Objects.requireNonNull(request, "request == null");
		m_request = request;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public final Request request() {
		return m_request;
	}

	@Override
	@Contract(pure = true)
	public final boolean hasConstraintViolations() {
		return ((m_constraintViolations == null) || (m_constraintViolations.isEmpty()));
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public final Set<ConstraintViolation<ParametersType>> constraintViolations() {
		if (m_constraintViolations == null)
			return Collections.emptySet();

		return m_constraintViolations;
	}

	@Override
	public Object invoke(
			final @NotNull Object proxy,
			final @NotNull Method method,
			final @NotNull Object[] args)
			throws Throwable
	{
		if (method.getDeclaringClass().equals(Parameters.class))
			return method.invoke(this);

		return null;
	}
}
