package autist.server.application.parameter.handler;

import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.time.format.DateTimeFormatter;

import org.apache.ibatis.type.TypeHandler;

abstract class LocalDateTimeHandlerBase<T>
		implements TypeHandler<T>
{
	private final DateTimeFormatter m_format;

	LocalDateTimeHandlerBase() {
		m_format = null;
	}

	LocalDateTimeHandlerBase(final String format) {
		m_format = DateTimeFormatter.ofPattern(Objects.requireNonNull(format));
	}

	@Override
	public final T parse(
			final Class<T> actualType,
			final String value)
	{
		Objects.requireNonNull(actualType);
		Objects.requireNonNull(value);

		return parse(m_format, value);
	}

	protected abstract T parse(@Nullable DateTimeFormatter format, String value);
}
