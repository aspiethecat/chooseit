package autist.server.application.query;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.annotation.Annotation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Path;
import javax.validation.metadata.ConstraintDescriptor;
import javax.validation.metadata.PropertyDescriptor;

import autist.application.parameter.Parameters;

final class FieldValidator<Q extends Parameters<Q>> {
	private static final Pattern s_attributePattern = Pattern.compile("\\$\\{(.+?)\\}");

	private final Class<Q> m_queryClass;
	private final PropertyDescriptor m_propertyDescriptor;
	private final FieldPath m_propertyPath;
	private final ResourceBundle m_resourceBundle;

	private final FieldConstraintViolation.Builder<Q> m_violationBuilder;
	private final List<FieldConstraintInvocation<?, ?>> m_constraints;

	@SuppressWarnings("unchecked")
	FieldValidator(
			final Class<Q> queryClass,
			final PropertyDescriptor descriptor,
			final String fieldName,
			final ResourceBundle resourceBundle)
	{
		m_queryClass = Objects.requireNonNull(queryClass);
		m_propertyDescriptor = Objects.requireNonNull(descriptor);
		m_propertyPath = new FieldPath(fieldName);
		m_resourceBundle = resourceBundle;

		m_violationBuilder = new FieldConstraintViolation.Builder<>(this, m_queryClass);

		final Set<ConstraintDescriptor<?>> constraints =
				descriptor.getConstraintDescriptors();

		m_constraints = new ArrayList<>(constraints.size());

		descriptor
				.getConstraintDescriptors()
				.forEach(constraint -> {
					final Class<? extends Annotation> constraintClass =
							constraint.getAnnotation().annotationType();

					final FieldConstraintInvocation<?, ?> invocation =
							constraint.getConstraintValidatorClasses()
								.stream()
								.map(validator ->
									new FieldConstraintInvocation(
											constraint, validator, constraint.getAnnotation()))
								.findFirst()
								.orElse(null);

					if (invocation == null)
						throw new AssertionError(String.format(
								"Validator class not found: %s",
								constraintClass.getName()));

					m_constraints.add(invocation);
				});
	}

	@Contract(pure = true)
	Class<Q> getQueryClass() {
		return m_queryClass;
	}

	@Contract(pure = true)
	PropertyDescriptor getPropertyDescriptor() {
		return m_propertyDescriptor;
	}

	@Contract(pure = true)
	Path getPropertyPath() {
		return m_propertyPath;
	}

	void addConstraintViolation(
			final Q query,
			@Nullable final Object value,
			final String message)
	{
		Objects.requireNonNull(query);
		Objects.requireNonNull(message);

		query.getConstraintViolations().add(
				m_violationBuilder
					.query(query)
					.invalidValue(value)
					.message(formatMessage(message))
					.build());
	}

	void validate(final Q query, @Nullable final Object value) {
		Objects.requireNonNull(query);

		m_constraints.stream()
				.filter(invocation -> !isValueValid(invocation, value))
				.map(invocation -> {
					final ConstraintDescriptor<?> descriptor =
							invocation.getDescriptor();

					return m_violationBuilder
							.query(query)
							.descriptor(descriptor)
							.invalidValue(value)
							.message(formatMessage(descriptor))
							.build();
				})
				.forEach(query.getConstraintViolations()::add);
	}

	@SuppressWarnings("unchecked")
	private static boolean isValueValid(
			final @NotNull FieldConstraintInvocation<?, ?> invocation,
			@Nullable final Object value)
	{
		return ((FieldConstraintInvocation<?, Object>) invocation).isValid(value);
	}

	@NotNull
	private String formatMessage(final @NotNull String message) {
		return formatMessage(message, Collections.emptyMap());
	}

	@NotNull
	private String formatMessage(final @NotNull ConstraintDescriptor<?> descriptor) {
		final Map<String, Object> attributes = descriptor.getAttributes();
		return formatMessage((String) attributes.get("message"), attributes);
	}

	@NotNull
	private String formatMessage(
			final @NotNull String message,
			@NotNull Map<String, Object> attributes)
	{
		final String format;

		if (message.startsWith("{") && message.endsWith("}")) {
			if (m_resourceBundle == null)
				return "(null)";

			try {
				format = m_resourceBundle.getString(
						message.substring(1, message.length() - 1));
			} catch (final MissingResourceException e) {
				return "(null)";
			}
		} else {
			format = message;
		}

		final Matcher matcher = s_attributePattern.matcher(format);
		final StringBuffer buffer = new StringBuffer();

		while (matcher.find()) {
			final Object replacement = attributes.get(matcher.group(1));

			final String replacementStr;
			if (replacement != null)
				replacementStr = Matcher.quoteReplacement(replacement.toString());
			else
				replacementStr = "(null)";

			matcher.appendReplacement(buffer, replacementStr);
		}
		matcher.appendTail(buffer);

		return buffer.toString();
	}
}
