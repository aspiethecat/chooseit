package autist.server.application.routing;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;

import java.net.URI;

import autist.configuration.ConfigurationException;

import autist.application.routing.RouteKey;
import autist.application.routing.RouteTag;
import autist.application.routing.RoutingContext;
import autist.application.routing.RoutingMap;
import autist.application.routing.RoutingMapFactory;
import autist.application.routing.RoutingNode;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class DefaultRoutingMapFactory
		implements RoutingMapFactory
{
	@Override
	@NotNull
	@Contract(value = "_, _, _ -> new", pure = true)
	@SuppressWarnings("unchecked")
	public 	<
			MapType extends RoutingMap<MapType, NodeType, ContextType, KeyType, TagType>,
			NodeType extends RoutingNode<NodeType, ContextType, KeyType, TagType>,
			ContextType extends RoutingContext<ContextType, KeyType, TagType>,
			KeyType extends Enum<KeyType> & RouteKey<KeyType>,
			TagType extends Enum<TagType> & RouteTag<TagType>>
	MapType create(
			final @NotNull Class<KeyType> keyClass,
			final @NotNull Class<TagType> tagClass,
			final @NotNull NodeType root)
			throws ConfigurationException
	{
		Objects.requireNonNull(keyClass, "keyClass == null");
		Objects.requireNonNull(tagClass, "tagClass == null");
		Objects.requireNonNull(root, "root == null");

		return (MapType) new DefaultRoutingMap<>(
				(ContextType) new DefaultRoutingContext<>(keyClass, tagClass), root);
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public 	<
			MapType extends RoutingMap<MapType, NodeType, ContextType, KeyType, TagType>,
			NodeType extends RoutingNode<NodeType, ContextType, KeyType, TagType>,
			ContextType extends RoutingContext<ContextType, KeyType, TagType>,
			KeyType extends Enum<KeyType> & RouteKey<KeyType>,
			TagType extends Enum<TagType> & RouteTag<TagType>>
	Optional<MapType> load(
			final @NotNull Class<KeyType> keyClass,
			final @NotNull Class<TagType> tagClass,
			final @NotNull URI uri)
			throws ConfigurationException
	{
		Objects.requireNonNull(keyClass, "keyClass == null");
		Objects.requireNonNull(tagClass, "tagClass == null");
		Objects.requireNonNull(uri, "uri == null");

		return Optional.empty();
	}
}
