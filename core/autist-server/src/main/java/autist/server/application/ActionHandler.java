package autist.server.application;

import autist.application.parameter.Parameters;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

import autist.configuration.ConfigurationException;
import autist.journal.Journal;

import autist.application.Model;
import autist.application.Request;
import autist.application.action.AbstractAction;
import autist.application.handler.HandlerContext;
import autist.application.handler.HandlerException;
import autist.application.action.ActionInvocation;
import autist.application.model.DefaultJsonModel;
import autist.application.query.Parameters;
import autist.application.view.AbstractView;
import autist.application.view.JsonView;
import autist.application.view.TemplateView;

import autist.server.application.query.QueryBuilder;

final class ActionHandler<
		ActionType extends AbstractAction<ActionContextType, QueryType>,
		ActionContextType extends HandlerContext<ActionContextType>,
		QueryType extends Parameters<QueryType>>
{
	@NotNull
	private final ApplicationContext m_context;

	@NotNull
	private final ActionType m_action;

	@NotNull
	private final ActionContextType m_actionContext;

	@NotNull
	private final QueryBuilder<QueryType> m_queryBuilder;

	ActionHandler(
			final @NotNull ApplicationContext context,
			final @NotNull ActionType action,
			final @NotNull ActionContextType actionContext,
			final @NotNull QueryBuilder<QueryType> queryBuilder)
	{
		m_context = context;

		m_action = action;
		m_actionContext = actionContext;
		m_queryBuilder = queryBuilder;
	}

	@NotNull
	Optional<AbstractView> invoke(final @NotNull Request request) throws HandlerException {
		return invoke(request, null);
	}

	@NotNull
	@SuppressWarnings("unchecked")
	Optional<AbstractView> invoke(
			final @NotNull Request request,
			@Nullable final String templateName)
			throws HandlerException
	{
		Objects.requireNonNull(request);

		final Collection<HandlerException> errors = new ArrayList<>(0);

		final QueryType query;
		try {
			query = m_queryBuilder
					.build(request, errors)
					.orElseThrow(() -> new HandlerException(String.format(
							"Can't build query: %d error(s)", errors.size())));
		} catch (final RuntimeException e) {
			throw new AssertionError("Internal error in query builder", e);
		} finally {
			final Journal logger = m_context.getLogger();
			if (!errors.isEmpty())
				errors.forEach((e) -> logger.severe(e.getMessage(), e.getCause()));
		}

		Optional<? extends Model> model = m_action.invoke(query);
		if (!model.isPresent())
			return Optional.empty();

		final Model data = model.get();
		final Class<? extends Model> type = data.getClass();

		final AbstractView view;

		if (AbstractView.class.isAssignableFrom(type))
			view = (AbstractView) data;
		else if (DefaultJsonModel.class.isAssignableFrom(type))
			view = new JsonView(data);
		else if (templateName != null)
			view = new TemplateView(templateName, data);
		else
			throw new HandlerException(String.format("Unexpected model type: %s", type.getName()));

		return Optional.of(view);
	}

	@SuppressWarnings("unchecked")
	static <
			HandlerType extends ActionHandler<ActionType, ContextType, QueryType>,
			InvocationType extends ActionInvocation<ActionType, ContextType, QueryType>,
			ActionType extends AbstractAction<ContextType, QueryType>,
			ContextType extends HandlerContext<ContextType>,
			QueryType extends Parameters<QueryType>>
	HandlerType get(
			final @NotNull ApplicationContext context,
			final @NotNull InvocationType invocation)
			throws ConfigurationException
	{
		final ActionHandlerCache cache = context.require(ActionHandlerCache.class);

		try {
			return (HandlerType) cache.createIfAbsent(
					invocation.handlerClass(),
					handlerClass -> {
						try {
							return new ActionHandler<>(application, invocation);
						} catch (final ConfigurationException e) {
							throw new RuntimeException(e);
						}
					});
		} catch (final RuntimeException e) {
			if (e.getCause() instanceof ConfigurationException)
				throw (ConfigurationException) e.getCause();
			throw e;
		}
	}

	private static <
			ActionType extends AbstractAction<ContextType, QueryType>,
			ContextType extends HandlerContext<ContextType>,
			QueryType extends Parameters<QueryType>>
	ActionType getAction(
			final @NotNull Application application,
			final @NotNull ActionInvocation<ActionType, ContextType, QueryType> invocation)
			throws ConfigurationException
	{
		final Class<ActionType> actionClass = invocation.getHandlerClass();

		return application.getCachedInstance(
				AbstractAction.class,
				actionClass,
				actionType -> AbstractAction.create(actionClass));
	}

	private static <
			QueryType extends Parameters<QueryType>,
			QueryBuilderType extends QueryBuilder<QueryType>>
	QueryBuilderType getQueryBuilder(
			final @NotNull Application application,
			final @NotNull Class<QueryType> queryClass)
			throws ConfigurationException
	{
		return application.getCachedInstance(
				QueryBuilder.class,
				queryClass,
				key -> new QueryBuilder<QueryType>(queryClass));
	}
}
