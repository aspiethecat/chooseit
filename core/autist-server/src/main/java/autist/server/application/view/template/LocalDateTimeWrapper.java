package autist.server.application.view.template;

import java.util.Date;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import freemarker.template.AdapterTemplateModel;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.WrappingTemplateModel;

public class LocalDateTimeWrapper
		extends WrappingTemplateModel
		implements TemplateDateModel, AdapterTemplateModel
{
	private final LocalDateTime m_dateTime;

	public LocalDateTimeWrapper(
			final LocalDateTime dateTime,
			final ObjectWrapper wrapper)
	{
		super(wrapper);
		m_dateTime = dateTime;
	}

	@Override
	public Object getAdaptedObject(Class<?> hint) {
		return m_dateTime;
	}

	@Override
	public Date getAsDate() throws TemplateModelException {
		return Date.from(m_dateTime.toInstant(ZoneOffset.UTC));
	}

	@Override
	public int getDateType() {
		return DATE;
	}
}
