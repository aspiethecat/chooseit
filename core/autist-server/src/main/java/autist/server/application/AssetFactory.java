package autist.server.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import java.util.Optional;
import java.util.logging.Level;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import java.io.IOException;

import autist.commons.util.DeferredException;
import autist.commons.util.DeferredOptional;
import autist.commons.net.URN;

import autist.configuration.ConfigurationException;

import autist.application.resource.ResourceStore;
import autist.application.resource.Resource;

import autist.server.configuration.AssetFactoryConfiguration;
import autist.server.configuration.AssetFactorySpecification;
import autist.server.application.resource.AbstractResourceStore;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class AssetFactory extends ResourceFactory {
	private static final String DEFAULT_MIME_TYPE = "application/octet-stream";

	@NotNull
	private final List<ResourceStore> m_stores = new ArrayList<>();

	@NotNull
	private final ResourceCache m_cache;

	@NotNull
	private final List<AssetType> m_types;

	@NotNull
	private final String m_defaultMimeType;

	@NotNull
	private final Map<String, AssetType> m_extensionMap = new HashMap<>();

	@NotNull
	private final Map<Pattern, AssetType> m_patternMap = new HashMap<>();

	private AssetFactory(
			final @NotNull ApplicationContext context,
			final @NotNull List<AssetRoot> roots,
			final @NotNull List<AssetType> types,
			final @Nullable String defaultMimeType)
			throws ConfigurationException
	{
		super(context, roots);

		m_cache = context.require(ResourceCache.class);

		final var errors = new ArrayList<ConfigurationException>();

		roots.forEach(
				root -> {
					try {
						m_stores.add(AbstractResourceStore.create(root.name(), root.uri()));
					} catch (final IllegalArgumentException | IOException e) {
						errors.add(new ConfigurationException(e.getMessage(), e.getCause()));
					}
				});
		Collections.reverse(m_stores);

		m_types = Objects.requireNonNull(types, "types");
		m_types.forEach(
				item -> item.matches().forEach(
						match -> {
							if (match.getExtension() != null) {
								m_extensionMap.put(match.getExtension(), item);
							} else if (match.getPattern() != null) {
								try {
									m_patternMap.put(Pattern.compile(match.getPattern()), item);
								} catch (final PatternSyntaxException e) {
									errors.add(new ConfigurationException(String.format(
											"Invalid resource type pattern syntax: %s: %s",
											item.name(), match.getPattern())));
								}
							} else {
								errors.add(new ConfigurationException(String.format(
										"Missing resource type extension or pattern: %s",
										item.name())));
							}
						}));

		m_defaultMimeType = Objects.requireNonNullElse(
				defaultMimeType, DEFAULT_MIME_TYPE);

		if (!errors.isEmpty()) {
			errors.forEach(
					error -> context.log(Level.SEVERE, error.getMessage()));
			throw new ConfigurationException("Resource factory initialization failed");
		}
	}

	@Override
	@NotNull
	@Contract(pure = true)
	protected Optional<ResourceCache> cache() {
		return Optional.of(m_cache);
	}

	public Optional<Resource> getAsset(
			final @NotNull URN urn)
			throws IOException
	{
		return getAsset(urn, null);
	}

	public Optional<Resource> getAsset(
			final @NotNull URN urn,
			final @Nullable DeferredOptional<String> mimeType)
			throws IOException
	{
		Objects.requireNonNull(urn, "path");

		final var type = getAssetType(urn);

		if (mimeType != null) {
			mimeType.bind(type
					.map(AssetType::mimeType)
					.orElse(m_defaultMimeType));
		}

		if (type.map(AssetType::hasBuilders).orElse(false))
			return buildAsset(urn, type.get());

		return get(urn);
	}

	@NotNull
	private Optional<Resource> buildAsset(
			final URN urn,
			final AssetType type)
			throws IOException
	{
		final AssetBuilderInvocation invocation = type
				.builders()
				.stream()
				.map(builder -> {
					try {
						return builder.findInvocation(this, urn).orElse(null);
					} catch (final IOException e) {
						context().log(Level.SEVERE, String.format(
								"Resource builder error: %s", e.getMessage()));
						return null;
					}
				})
				.filter(Objects::nonNull)
				.findFirst()
				.orElse(null);

		if (invocation == null)
			return get(urn);

		final var exception  = new DeferredException<>(IOException.class);

		final Optional<Resource> result = fetchFromCache(urn)
				.filter(invocation::isCacheValid)
				.or(() -> Optional.ofNullable(exception.wrap(
						() -> storeToCache(urn, invocation.build()).orElse(null))));

		exception.throwIfPresent();

		return result;
	}

	@NotNull
	private Optional<AssetType> getAssetType(
			final @NotNull URN urn)
	{
		return urn.extension()
				.map(m_extensionMap::get)
				.or(() -> urn.lastComponent().flatMap(
						name -> m_patternMap.entrySet()
								.stream()
								.filter(entry -> entry.getKey().matcher(name).matches())
								.map(Map.Entry::getValue)
								.findFirst()));
	}

	public static AssetFactory build(
			final @NotNull ApplicationContext context,
			final @NotNull AssetFactorySpecification specification,
			final @NotNull AssetFactoryConfiguration configuration)
			throws ConfigurationException
	{
		final String defaultMimeType;

		if (configuration.getTypes().getDefaultItemName() != null)
			defaultMimeType = configuration.getTypes().getDefaultItemName();
		else if (specification.getTypes().getDefaultItemName() != null)
			defaultMimeType = specification.getTypes().getDefaultItemName();
		else
			defaultMimeType = DEFAULT_MIME_TYPE;

		return new AssetFactory(
				context,
				AssetRoot.buildList(
						context,
						specification.getRoots(),
						configuration.getRoots()),
				AssetType.buildList(
						context,
						specification.getTypes(),
						configuration.getTypes()),
				defaultMimeType);
	}
}
