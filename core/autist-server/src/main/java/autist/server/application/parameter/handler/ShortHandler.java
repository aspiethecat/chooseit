package autist.server.application.parameter.handler;

import org.jetbrains.annotations.Nullable;

import java.text.DecimalFormat;
import java.text.ParseException;

public class ShortHandler extends IntegerHandlerBase<Short> {
	public ShortHandler() { }

	public ShortHandler(final String format) {
		super(format);
	}

	@Override
	protected Short parse(@Nullable final DecimalFormat format, final String value)
			throws ParseException
	{
		if (format == null)
			return Short.valueOf(value);

		return format.parse(value).shortValue();
	}
}
