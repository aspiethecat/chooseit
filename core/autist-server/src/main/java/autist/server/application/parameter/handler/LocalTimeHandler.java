package autist.server.application.parameter.handler;

import org.jetbrains.annotations.Nullable;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class LocalTimeHandler
		extends LocalDateTimeHandlerBase<LocalTime>
{
	public LocalTimeHandler() { }

	public LocalTimeHandler(final String format) {
		super(format);
	}

	@Override
	protected LocalTime parse(
			@Nullable final DateTimeFormatter format,
			final String value)
	{
		if (format == null)
			return LocalTime.parse(value);

		return LocalTime.parse(value, format);
	}
}
