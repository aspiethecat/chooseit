package autist.server.application.template;

import autist.server.net.Buffer;

public interface Template<T> {
	void render(final Buffer buffer, final T model);
}
