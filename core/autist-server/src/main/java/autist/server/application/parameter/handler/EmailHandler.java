package autist.server.application.parameter.handler;

import org.jetbrains.annotations.NotNull;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class EmailHandler implements TypeHandler<InternetAddress> {
	@Override
	public InternetAddress parse(
			final @NotNull Class<InternetAddress> actualType,
			final @NotNull String value)
	{
		final InternetAddress address;

		try {
			address = new InternetAddress(value);
			address.validate();
		} catch (final AddressException e) {
			return null;
		}

		return address;
	}
}
