package autist.server.application.view;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.Objects;

import autist.application.Request;
import autist.application.handler.HandlerContext;
import autist.application.view.ViewException;
import autist.application.view.AbstractView;
import autist.application.RequestMethod;
import autist.application.resource.Resource;
import autist.application.Response;

public class ResourceView extends AbstractView {
	@NotNull
	private final RequestMethod m_method;

	@NotNull
	private final Resource m_content;

	@NotNull
	private final String m_mimeType;

	public ResourceView(
			final @NotNull RequestMethod method,
			final @NotNull Resource content,
			final @NotNull String mimeType)
	{
		m_method = Objects.requireNonNull(method, "method == null");
		m_content = Objects.requireNonNull(content, "content == null");
		m_mimeType = Objects.requireNonNull(mimeType, "mimeType == null");
	}

	@NotNull
	@Contract(pure = true)
	public RequestMethod getMethod() {
		return m_method;
	}

	@NotNull
	@Contract(pure = true)
	public Resource getContent() {
		return m_content;
	}

	@NotNull
	@Contract(pure = true)
	public String getMimeType() {
		return m_mimeType;
	}

	@Override
	public void render(
			final @NotNull HandlerContext context,
			final @NotNull Request request,
			final @NotNull Response response)
			throws ViewException
	{
		super.render(context, request, response);

		response.setLastModified(new Date(m_content.attributes().lastModifiedTime().toMillis()));
		response.setContent(m_content);
		response.setContentType(m_mimeType);
	}

	public static boolean isMethodSupported(final @NotNull RequestMethod method) {
		return method.equals(RequestMethod.GET) || method.equals(RequestMethod.HEAD);
	}
}
