package autist.server;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;

import io.netty.handler.codec.http.HttpResponseStatus;

import java.util.Objects;

@SuppressWarnings("javadoc")
public class HttpException extends Exception {
	private final HttpExceptionType m_type;
	private final HttpResponseStatus m_status;

	public static HttpException NOT_MODIFIED =
			new HttpException(HttpResponseStatus.NOT_MODIFIED);
	public static HttpException BAD_REQUEST =
			new HttpException(HttpResponseStatus.BAD_REQUEST);
	public static HttpException FORBIDDEN =
			new HttpException(HttpResponseStatus.FORBIDDEN);
	public static HttpException NOT_FOUND =
			new HttpException(HttpResponseStatus.NOT_FOUND);
	public static HttpException METHOD_NOT_ALLOWED =
			new HttpException(HttpResponseStatus.METHOD_NOT_ALLOWED);
	public static HttpException INTERNAL_SERVER_ERROR =
			new HttpException(HttpResponseStatus.INTERNAL_SERVER_ERROR);

	public HttpException(
			final HttpException type,
			final String message)
	{
		this(type.getStatus(), message, null);
	}

	public HttpException(
			@NotNull HttpException type,
			@Nullable String message,
			@Nullable Throwable cause)
	{
		this(type.m_status, message, cause);
	}

	private HttpException(final HttpResponseStatus status) {
		this(Objects.requireNonNull(status), null, null);
	}

	/**
	 * Private constructor which actually creates object.
	 */
	private HttpException(
			@NotNull HttpResponseStatus status,
			@Nullable String message,
			@Nullable Throwable cause)
	{
		super(message, cause);

		m_status = status;
		m_type   = HttpExceptionType.getErrorType(this);
	}

	public HttpExceptionType getType() {
		return m_type;
	}

	public HttpResponseStatus getStatus() {
		return m_status;
	}

	public boolean isServerError() {
		return (m_type == HttpExceptionType.SERVER_ERROR);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;

		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;

		HttpException error = (HttpException) obj;

		return (error.m_status.code() == m_status.code());
	}
}
