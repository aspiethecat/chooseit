package autist.server;

import java.util.Objects;

import autist.server.application.Application;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

import io.netty.handler.ssl.SslContext;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.stream.ChunkedWriteHandler;

public class HttpChannelInitializer extends ChannelInitializer<SocketChannel> {
	private final Application m_application;

	private final SslContext m_sslContext;

	public HttpChannelInitializer(
			final Application application,
			final SslContext sslContext)
	{
		m_application = Objects.requireNonNull(application);
		m_sslContext = sslContext;
	}

	@Override
	public void initChannel(SocketChannel ch) throws Exception {
		ChannelPipeline pipeline = ch.pipeline();

		if (m_sslContext != null)
			pipeline.addLast(m_sslContext.newHandler(ch.alloc()));

		pipeline.addLast(new HttpRequestDecoder());
		pipeline.addLast(new HttpResponseEncoder());
		pipeline.addLast(new ChunkedWriteHandler());

		pipeline.addLast(new HttpChannelHandler(m_application));
	}
}
