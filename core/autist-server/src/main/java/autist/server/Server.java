package autist.server;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Enumeration;
import java.util.Objects;

import java.util.jar.Attributes;
import java.util.jar.Manifest;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.LogManager;

import java.io.File;
import java.io.IOException;

import java.nio.file.Path;
import java.nio.file.Paths;

import java.net.InetAddress;
import java.net.URL;
import java.net.URLClassLoader;

import javax.net.ssl.SSLException;

import io.netty.bootstrap.ServerBootstrap;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelOption;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslProvider;

import io.netty.util.concurrent.GlobalEventExecutor;

import autist.configuration.ConfigurationException;

import autist.journal.Journal;
import autist.journal.JournalType;

import autist.application.EnvironmentType;

import autist.server.configuration.ApplicationConfiguration;
import autist.server.configuration.ApplicationSpecification;
import autist.server.configuration.ChannelConfiguration;
import autist.server.configuration.CoreConfiguration;
import autist.server.configuration.ServerConfiguration;

import autist.server.journal.LogFormatter;
import autist.server.journal.LogHandler;
import autist.server.journal.LogConfiguration;
import autist.server.journal.RequestLogFormatter;

import autist.server.application.Application;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class Server implements Runnable {
	private static final Journal s_journal = Journal.get(JournalType.SERVER);

	/**
	 * Configuration file path property name (without package name).
	 */
	public static final String CONFIG_PROPERTY = "configuration";

	/**
	 * Default configuration file path.
	 */
	public static final String CONFIG_PATH = "configuration.xml";

	/**
	 * Environment type.
	 */
	private final @NotNull EnvironmentType m_environmentType;

	/**
	 * Server configuration.
	 */
	private final @NotNull ServerConfiguration m_configuration;

	/**
	 * Application instance.
	 */
	private final @NotNull Application m_application;

	/**
	 * Thread pool.
	 */
	private ThreadPool m_threadPool = null;

	private final ChannelGroup m_channels;

	private Server() throws ConfigurationException {
		m_environmentType = detectEnvironmentType();

		showBanner();
		setupLogger();

		try {
			m_configuration = loadConfiguration();

			setupLogFiles();

			m_application  = createApplication(m_configuration.getApplicationConfiguration());
			m_threadPool   = ThreadPool.createInstance(m_configuration.getThreadPoolConfiguration());
			m_channels     = createChannels();
		} catch (final ConfigurationException e) {
			if (m_threadPool != null) {
				s_journal.log(Level.INFO, "Shutting down thread pool...");
				m_threadPool.shutdownGracefully();
			}

			s_journal.log(Level.SEVERE, e.getMessage());
			if (e.getCause() != null)
				s_journal.log(Level.SEVERE, e.getCause().getMessage(), e.getCause());

			s_journal.log(Level.SEVERE, "Server initialization failed");
			throw e;
		}

		s_journal.log(Level.INFO, "Server initialization completed");
	}

	@NotNull
	private EnvironmentType detectEnvironmentType() {
		final URL serverURL = getClass().getResource(
				String.format("%s.class", getClass().getSimpleName()));

		if (serverURL.getProtocol().equals("file")) {
			System.err.println("[Running in debug mode]");
			System.err.println();

			return EnvironmentType.DEVELOPMENT;
		}

		return EnvironmentType.PRODUCTION;
	}

	private void showBanner() {
		if (m_environmentType == EnvironmentType.DEVELOPMENT)
			return;

		final Attributes attributes = getManifestAttributes();

		System.err.println();
		System.err.println(String.format(
				"%s (%s, build %s)",
				attributes.getValue("Product-Name"),
				attributes.getValue("Product-Version"),
				attributes.getValue("Product-Build")));
		System.err.println(String.format(
				"Copyright (c) %s, %s. All rights reserved.",
				attributes.getValue("Product-Copyright"),
				attributes.getValue("Product-Vendor-Name")));
		System.err.println();
	}

	@NotNull
	private Attributes getManifestAttributes() {
		final URLClassLoader classLoader = (URLClassLoader) getClass().getClassLoader();

		try {
			final Enumeration<URL> manifestList =
					classLoader.findResources("META-INF/MANIFEST.MF");

			while (manifestList.hasMoreElements()) {
				final Manifest manifest =
						new Manifest(manifestList.nextElement().openStream());

				final Attributes attributes = manifest.getMainAttributes();

				final String productId = attributes.getValue("Product-Id");
				if (Objects.isNull(productId) || !productId.equals(getClass().getPackage().getName()))
					continue;

				return attributes;
			}
		} catch (final IOException e) {
			throw new AssertionError("Can't load manifest", e);
		}

		throw new AssertionError("Root manifest not found");
	}

	@Override
	public void run() {
		s_journal.log(Level.INFO,"Handling connections...");

		// Wait for server termination
		try {
			m_channels.newCloseFuture().sync();
		} catch (InterruptedException e) {
			s_journal.log(Level.WARNING, "Can't close channel", e);
		} finally {
			m_threadPool.shutdownGracefully();
		}
	}

	private void setupLogger() {
		final String serverPackageName = getClass().getPackage().getName();
		final String configurationPath = String.format(
				"/%s/logging.properties", serverPackageName.replace(".", "/"));

		try {
			final LogManager logManager = LogManager.getLogManager();

			logManager.reset();
			logManager.readConfiguration(
						getClass().getResourceAsStream(configurationPath));
		} catch (IOException | SecurityException e) {
			throw new AssertionError("Can't load logging configuration", e);
		}
	}

	private ServerConfiguration loadConfiguration() throws ConfigurationException {
		final Path path = Paths.get(System.getProperty(
				String.join(".", getClass().getPackage().getName(), CONFIG_PROPERTY),
				CONFIG_PATH));

		s_journal.log(Level.INFO, String.format("Loading configuration (%s)...", path.toString()));

		return ServerConfiguration.load(path);
	}

	private void setupLogFiles() throws ConfigurationException {
		try {
			setupSystemLog(m_configuration.getSystemLogConfiguration());
			setupRequestLog(m_configuration.getRequestLogConfiguration());
		} catch (final IOException e) {
			throw new ConfigurationException("Can't initialize log", e);
		}
	}

	private void setupSystemLog(final LogConfiguration configuration)
			throws IOException
	{
		if (configuration == null)
			return;

		final LogHandler handler = LogHandler.createInstance(configuration);

		handler.setLevel(configuration.getLevel());
		handler.setFormatter(new LogFormatter(configuration.getFormat()));

		Logger.getLogger(JournalType.SERVER.loggerName()).addHandler(handler);
		Logger.getLogger(JournalType.APPLICATION.loggerName()).addHandler(handler);
	}

	private void setupRequestLog(final LogConfiguration configuration)
		throws IOException
	{
		final Logger requestLogger = Logger.getLogger(JournalType.REQUEST.loggerName());

		for (final Handler handler : requestLogger.getHandlers())
			requestLogger.removeHandler(handler);
		requestLogger.setUseParentHandlers(false);

		if (configuration == null) {
			requestLogger.setLevel(Level.OFF);
			return;
		}

		final LogHandler handler = LogHandler.createInstance(configuration);

		handler.setLevel(Level.ALL);
		handler.setFormatter(new RequestLogFormatter(configuration.getFormat()));

		requestLogger.addHandler(handler);
	}

	private Application createApplication(
			final ApplicationConfiguration configuration)
			throws ConfigurationException
	{
		s_journal.log(Level.INFO, String.format(
				"Initializing application instance: %s (%s)...",
				configuration.getName(),
				configuration.getEnvironment().name().toLowerCase()));

		s_journal.log(Level.CONFIG, String.format(
				"Loading application specification: %s (%s)...",
				configuration.getName(),
				configuration.getSpecificationUri()
		));

		final ApplicationSpecification specification =
				ApplicationSpecification.load(configuration.getSpecificationUri());

		return new Application(specification, configuration);
	}

	private ChannelGroup createChannels() throws ConfigurationException {
		s_journal.log(Level.INFO, "Creating channels...");

		final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
		final ConfigurationException error = m_configuration.getChannels().stream()
				.map((configuration) -> {
					try {
						channels.add(createChannel(m_threadPool, configuration));
					} catch (final ConfigurationException e) {
						return e;
					}

					return null;
				})
				.filter(Objects::nonNull)
				.findFirst()
				.orElse(null);

		if (error != null) {
			s_journal.log(Level.SEVERE, error.getMessage());

			s_journal.log(Level.INFO, "Shutting down channels...");
			channels.close().awaitUninterruptibly();

			throw new ConfigurationException("Channel creation failure");
		}

		return channels;
	}

	private Channel createChannel(
			final ThreadPool threadPool,
			final ChannelConfiguration channelConfiguration)
			throws ConfigurationException
	{
		final String      channelName    = channelConfiguration.getName();
		final InetAddress channelAddress = channelConfiguration.getAddress();
		final Integer     channelPort    = channelConfiguration.getPort();

		s_journal.log(Level.CONFIG, String.format(
				"Creating channel: %s (%s:%d)...",
				channelName, channelAddress.getHostAddress(), channelPort));

		final ServerBootstrap bootstrap = new ServerBootstrap();
		final CoreConfiguration serverConfiguration = m_configuration.getThreadPoolConfiguration();

		final SslContext sslContext =
				channelConfiguration.isSecure() ?
					createSslContext(channelConfiguration) :
					null;

		final ChannelHandler initializer =
				new HttpChannelInitializer(m_application, sslContext);

		bootstrap
				.group(threadPool.bossGroup(), threadPool.workerGroup())
				.channel(NioServerSocketChannel.class)
				.option(ChannelOption.SO_REUSEADDR, true)
				.childHandler(initializer)
				.childOption(ChannelOption.SO_KEEPALIVE, true);

		channelConfiguration.configure(bootstrap, serverConfiguration);

		final ChannelFuture future = bootstrap.bind(channelAddress, channelPort);

		Throwable cause = null;
		try {
			if (!future.await().isSuccess())
				cause = future.cause();
		} catch (final InterruptedException e) {
			cause = e;
		}

		if (cause != null) {
			throw new ConfigurationException(String.format(
					"%s: %s (%s:%d)",
					cause.getMessage(), channelName, channelAddress.getHostAddress(), channelPort));
		}

		return future.channel();
	}

	private SslContext createSslContext(final ChannelConfiguration configuration)
			throws ConfigurationException
	{
		final String privateKeyPath = configuration.getPrivateKeyPath();
		final String certificatePath = configuration.getCertificatePath();

		if ((privateKeyPath == null) || privateKeyPath.isEmpty())
			throw new ConfigurationException(String.format(
					"SSL private key path not specified: %s",
					configuration.getName()));
		if ((certificatePath == null) || certificatePath.isEmpty())
			throw new ConfigurationException(String.format(
					"SSL certificate path not specified: %s",
					configuration.getName()));

		try {
			return SslContextBuilder
					.forServer(
						new File(certificatePath),
						new File(privateKeyPath))
					.sslProvider(SslProvider.JDK)
					.build();
		} catch (SSLException e) {
			throw new ConfigurationException("Can't create SSL context", e);
		}
	}

	@NotNull
	@Contract(pure = true)
	public ServerConfiguration getConfiguration() {
		return m_configuration;
	}

	@NotNull
	@Contract(pure = true)
	public Application getApplication() {
		return m_application;
	}

	@Contract(pure = true)
	public ThreadPool getThreadPool() {
		return m_threadPool;
	}

	public static void main(final String args[]) {
		try {
			(new Server()).run();
		} catch (final ConfigurationException e) {
			System.exit(1);
		}
	}
}
