package autist.server.database;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

@SuppressWarnings("unused")
public abstract class StringTypeHandler<T> extends BaseTypeHandler<T> {
	@NotNull
	@Contract(value = "null -> fail; !null -> _", pure = true)
	public abstract String marshal(@NotNull T value) throws SQLException;

	@NotNull
	@Contract(value = "null -> fail; !null -> _", pure = true)
	public abstract T unmarshal(@NotNull String value) throws SQLException;

	@Nullable
	@Contract(value = "null -> null; !null -> _", pure = true)
	private T unmarshalEx(@Nullable String value) throws SQLException {
		if (value == null)
			return null;

		return unmarshal(value);
	}

	@Override
	@Contract("null, _, _, _ -> fail; _, _, null, _ -> fail; _, _, _, null -> fail")
	public final void setNonNullParameter(
			final @NotNull PreparedStatement statement,
			final int parameterIndex,
			final @NotNull T parameterValue,
			final @NotNull JdbcType parameterType)
			throws SQLException
	{
		statement.setString(
				checkParameter(statement, parameterIndex, parameterValue, parameterType),
				marshal(parameterValue));
	}

	@Override
	@Nullable
	@Contract(value = "null, _ -> fail; _, null -> fail; !null, !null -> _")
	public final T getNullableResult(
			final @NotNull ResultSet resultSet,
			final @NotNull String columnLabel)
			throws SQLException
	{
		return unmarshalEx(resultSet.getString(checkColumnByLabel(resultSet, columnLabel)));
	}

	@Override
	@Nullable
	@Contract(value = "null, _ -> fail; !null, _ -> _")
	public final T getNullableResult(
			final @NotNull ResultSet resultSet,
			final int columnIndex)
			throws SQLException
	{
		return unmarshalEx(resultSet.getString(checkColumnByIndex(resultSet, columnIndex)));
	}

	@Override
	@Nullable
	@Contract(value = "null, _ -> fail; !null, _ -> _")
	public final T getNullableResult(
			final @NotNull CallableStatement statement,
			final int columnIndex)
			throws SQLException
	{
		return unmarshalEx(statement.getString(checkColumnByIndex(statement, columnIndex)));
	}

	@Contract(
			value = "_, _, _, _ -> param2",
			pure = true
	)
	private int checkParameter(
			final @NotNull PreparedStatement statement,
			final int parameterIndex,
			final @NotNull T parameterValue,
			final @NotNull JdbcType parameterType)
			throws IndexOutOfBoundsException, SQLException
	{
		Objects.requireNonNull(statement);
		Objects.requireNonNull(parameterValue);
		Objects.requireNonNull(parameterType);

		if ((parameterIndex <= 0) || (parameterIndex > statement.getParameterMetaData().getParameterCount()))
			throw new IndexOutOfBoundsException();

		return parameterIndex;
	}

	@NotNull
	@Contract(value = "null, _ -> fail; _, null -> fail; !null, !null -> param2", pure = true)
	private String checkColumnByLabel(
			final @NotNull ResultSet resultSet,
			final @NotNull String columnLabel)
	{
		Objects.requireNonNull(resultSet);
		Objects.requireNonNull(columnLabel);

		return columnLabel;
	}

	@Contract(value = "_, _ -> param2", pure = true)
	private int checkColumnByIndex(
			final @NotNull ResultSet resultSet,
			final int columnIndex)
			throws SQLException
	{
		Objects.requireNonNull(resultSet);

		if ((columnIndex <= 0) || (columnIndex > resultSet.getMetaData().getColumnCount()))
			throw new IndexOutOfBoundsException();

		return columnIndex;
	}

	@Contract(value = "null, _ -> fail; !null, _ -> param2", pure = true)
	private int checkColumnByIndex(
			final @NotNull CallableStatement statement,
			final int columnIndex)
			throws SQLException
	{
		Objects.requireNonNull(statement);

		if ((columnIndex <= 0) || (columnIndex > statement.getMetaData().getColumnCount()))
			throw new IndexOutOfBoundsException();

		return columnIndex;
	}
}
