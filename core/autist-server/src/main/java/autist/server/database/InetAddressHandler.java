package autist.server.database;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.Objects;

@SuppressWarnings("unused")
@MappedTypes(InetAddress.class)
@MappedJdbcTypes(JdbcType.VARCHAR)
public final class InetAddressHandler extends StringTypeHandler<InetAddress> {
	@Override
	@NotNull
	@Contract(value = "null -> fail; !null -> _", pure = true)
	public String marshal(final @NotNull InetAddress value) {
		return Objects.requireNonNull(value).getHostAddress();
	}

	@Override
	@NotNull
	@Contract(value = "null -> fail; !null -> _", pure = true)
	public InetAddress unmarshal(final @NotNull String value) throws SQLException {
		try {
			return InetAddress.getByName(Objects.requireNonNull(value));
		} catch (final UnknownHostException e) {
			throw new SQLException(String.format("Invalid network address value: %s", value), e);
		}
	}
}
