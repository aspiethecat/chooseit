package autist.server.database;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.sql.SQLException;
import java.util.Objects;

@SuppressWarnings("unused")
@MappedTypes(InternetAddress.class)
@MappedJdbcTypes(JdbcType.VARCHAR)
public final class EmailHandler extends StringTypeHandler<InternetAddress> {
	@Override
	@NotNull
	@Contract(value = "null -> fail; !null -> _", pure = true)
	public String marshal(final @NotNull InternetAddress value) {
		return Objects.requireNonNull(value).toString();
	}

	@Override
	@NotNull
	@Contract(value = "null -> fail; !null -> _", pure = true)
	public InternetAddress unmarshal(final @NotNull String value) throws SQLException {
		try {
			return new InternetAddress(Objects.requireNonNull(value));
		} catch (final AddressException e) {
			throw new SQLException(String.format("Invalid e-mail address value: %s", value), e);
		}
	}
}
