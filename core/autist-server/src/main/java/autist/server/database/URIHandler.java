package autist.server.database;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.Objects;

@SuppressWarnings("unused")
@MappedTypes(URI.class)
@MappedJdbcTypes(JdbcType.VARCHAR)
public final class URIHandler extends StringTypeHandler<URI> {
	@Override
	@NotNull
	@Contract(value = "null -> fail; !null -> _", pure = true)
	public String marshal(final @NotNull URI value) {
		return Objects.requireNonNull(value).toString();
	}

	@Override
	@NotNull
	@Contract(value = "null -> fail; !null -> _", pure = true)
	public URI unmarshal(final @NotNull String value) throws SQLException {
		try {
			return new URI(Objects.requireNonNull(value));
		} catch (final URISyntaxException e) {
			throw new SQLException(String.format("Invalid URI value: %s", value), e);
		}
	}
}
