package autist.server.journal;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.logging.Level;
import java.util.logging.LogRecord;

import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;

@SuppressWarnings({"WeakerAccess", "unused"})
public class RequestLogRecord extends LogRecord {
	@FunctionalInterface
	public interface FieldExtractor {
		Object extract(final @NotNull RequestLogRecord record);
	}

	public enum Field implements FieldExtractor {
		TIMESTAMP {
			@Override
			public Object extract(final @NotNull RequestLogRecord record) {
				return record.getMillis();
			}
		},
		PROTOCOL_VERSION {
			@Override
			public Object extract(final @NotNull RequestLogRecord record) {
				return record.m_request.protocolVersion().text();
			}
		},
		SERVER_NAME {
			@Override
			public Object extract(final @NotNull RequestLogRecord record) {
				return record.m_request.headers().get(HttpHeaderNames.HOST);
			}
		},
		REQUEST_METHOD {
			@Override
			public Object extract(final @NotNull RequestLogRecord record) {
				return record.m_request.method().name();
			}
		},
		REQUEST_URI {
			@Override
			public Object extract(final @NotNull RequestLogRecord record) {
				return record.m_request.uri();
			}
		},
		STATUS_CODE {
			@Override
			public Object extract(final @NotNull RequestLogRecord record) {
				return Integer.toString(record.m_response.status().code());
			}
		},
		STATUS_TEXT {
			@Override
			public Object extract(final @NotNull RequestLogRecord record) {
				return record.m_response.status().reasonPhrase();
			}
		},
		CONTENT_LENGTH {
			@Override
			public Object extract(final @NotNull RequestLogRecord record) {
				return record.m_response.headers().get(HttpHeaderNames.CONTENT_LENGTH);
			}
		},
		CONTENT_TYPE {
			@Override
			public Object extract(final @NotNull RequestLogRecord record) {
				return record.m_response.headers().get(HttpHeaderNames.CONTENT_TYPE);
			}
		}
	}

	private final @NotNull HttpRequest m_request;
	private final @NotNull HttpResponse m_response;

	RequestLogRecord(
			final @NotNull HttpRequest request,
			final @NotNull HttpResponse response)
	{
		super(Level.INFO, request.uri());

		m_request = request;
		m_response = response;
	}

	@NotNull
	@Contract(pure = true)
	public final HttpRequest getRequest() {
		return m_request;
	}

	@NotNull
	@Contract(pure = true)
	public final HttpResponse getResponse() {
		return m_response;
	}
}
