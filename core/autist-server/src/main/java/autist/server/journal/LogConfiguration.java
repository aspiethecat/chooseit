package autist.server.journal;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.logging.Level;
import java.nio.file.Path;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * Logging configuration settings.
 */
public class LogConfiguration {
	/**
	 * Default logging level.
	 */
	public static Level DEFAULT_LEVEL = Level.CONFIG;

	private static class LevelAdapter extends XmlAdapter<String, Level> {
		@Override
		public Level unmarshal(String level) throws Exception {
			return Level.parse(level.toUpperCase());
		}

		@Override
		public String marshal(Level level) throws Exception {
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * Log file path pattern.
	 */
	@XmlAttribute(name = "path")
	private Path m_path = null;

	/**
	 * Logging level.
	 */
	@XmlAttribute(name = "level")
	@XmlJavaTypeAdapter(LevelAdapter.class)
	private Level m_level = DEFAULT_LEVEL;

	/**
	 * Logging format.
	 */
	@Nullable
	@XmlAttribute(name = "format")
	private String m_format = null;

	/**
	 * Maximum log file size in bytes.
	 */
	@Nullable
	@XmlAttribute(name = "maxSize")
	private Long m_maxSize = null;

	/**
	 * Maximum log file count.
	 */
	@Nullable
	@XmlAttribute(name = "maxCount")
	private Integer m_maxCount = null;

	@Nullable
	@Contract(pure = true)
	public Path getPath() {
		return m_path;
	}

	@NotNull
	@Contract(pure = true)
	public Level getLevel() {
		return m_level;
	}

	@Nullable
	@Contract(pure = true)
	public String getFormat() {
		return m_format;
	}

	@Nullable
	@Contract(pure = true)
	public Long getMaxSize() {
		return m_maxSize;
	}

	@Nullable
	@Contract(pure = true)
	public Integer getMaxCount() {
		return m_maxCount;
	}
}
