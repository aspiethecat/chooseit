package autist.server.journal;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

public class RequestLogFormatter extends SimpleFormatter {
	private final String format;

	public static final String FORMAT_DEFAULT = "[%1$tF %1$tT] %3$s %4$s %5$s %2$s %6$s %8$s %9$s%n";
	public static final String FORMAT_PROPERTY = "RequestLogFormatter.format";

	public RequestLogFormatter() {
		this(null);
	}

	public RequestLogFormatter(@Nullable String format) {
		this.format = ((format == null) || format.isEmpty()) ?
			System.getProperty(FORMAT_PROPERTY, FORMAT_DEFAULT) :
			format;
	}

	@Override
	public synchronized String format(LogRecord record) {
		if (!RequestLogRecord.class.isAssignableFrom(record.getClass())) {
			throw new IllegalArgumentException(
					String.format("Invalid request log record class: %s", record.getClass()));
		}

		List<Object> fieldList = new ArrayList<>(RequestLogRecord.Field.values().length);
		for (RequestLogRecord.Field field : RequestLogRecord.Field.values()) {
			Object value = field.extract((RequestLogRecord) record);
			fieldList.add(value != null ? value : "-");
		}

		return String.format(format, fieldList.toArray());
	}
}
