package autist.server.journal;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.logging.FileHandler;

import java.io.IOException;
import java.nio.file.Path;

public final class LogHandler extends FileHandler {
	private LogHandler(
			final @NotNull Path path,
			final long size,
			final int count)
			throws IOException
	{
		super(path.toString(), size, count, true);
	}

	@NotNull
	@Contract("_ -> new")
	public static LogHandler createInstance(
			final @NotNull LogConfiguration configuration)
			throws IOException
	{
		Objects.requireNonNull(configuration);

		return new LogHandler(
				configuration.getPath(),
				configuration.getMaxSize(),
				configuration.getMaxCount());
	}
}
