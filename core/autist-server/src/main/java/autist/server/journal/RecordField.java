package autist.server.journal;

public interface RecordField<RecordType> {
	Class<?> getType();
	Object extract(RecordType record);
}
