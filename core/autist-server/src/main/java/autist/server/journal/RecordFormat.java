package autist.server.journal;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"WeakerAccess", "unused"})
public class RecordFormat<
		RecordType,
		FieldType extends Enum<FieldType> & RecordField>
{
	private enum ParserState {
		DEFAULT,
		TOKEN_START,
		TOKEN_NAME,
		FORMAT_START,
		FORMAT_CONTENT,
		FORMAT_END,
		ERROR
	}

	private final Class<FieldType> m_fieldClass;

	private final String m_formatString;

	private final List<Object> m_format = new ArrayList<>();

	public RecordFormat(final Class<FieldType> fieldClass, final String formatString) {
		m_fieldClass = fieldClass;
		m_formatString = formatString;
	}

	@SuppressWarnings("unchecked")
	public String format(RecordType record) {
		final StringBuilder result = new StringBuilder();

		m_format.forEach((token) -> {
			if (m_fieldClass.isAssignableFrom(token.getClass()))
				result.append(((FieldType) token).extract(record));
			else
				result.append(token);
		});

		return result.toString();
	}

	private List<Object> parseFormatString(String formatString) {
		final List<Object> format = new ArrayList<>();
		final StringBuilder buffer = new StringBuilder();

		ParserState state = ParserState.DEFAULT;
		for (int i = 0; i < formatString.length(); i++) {
			char ch = formatString.charAt(i);

			switch (state) {
				case DEFAULT:
					if (ch == '$')
						state = ParserState.TOKEN_START;
					else
						buffer.append(ch);
					break;

				case TOKEN_START:
					if (ch == '$') {
						state = ParserState.DEFAULT;
						buffer.append(ch);
					} else if (ch == '{') {
						state = ParserState.TOKEN_NAME;
						if (buffer.length() > 0) {
							format.add(flushToken(buffer));
						}
					} else {
						state = ParserState.ERROR;
					}
					break;

				case TOKEN_NAME:
					break;

				default:
					throw new AssertionError("Unhandled parser state");
			}

			if (state == ParserState.ERROR)
				throw new IllegalArgumentException("Invalid format string");
		}

		return format;
	}

	private String flushToken(final @NotNull StringBuilder buffer) {
		final String content = buffer.toString();

		buffer.setLength(0);

		return content;
	}

	@Contract(pure = true)
	public Class<FieldType> getFieldClass() {
		return m_fieldClass;
	}

	@Contract(pure = true)
	public String getFormatString() {
		return m_formatString;
	}
}
