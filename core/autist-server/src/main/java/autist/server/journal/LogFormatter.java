package autist.server.journal;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.Objects;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

import java.io.PrintWriter;
import java.io.StringWriter;

import autist.journal.JournalRecord;

/**
 * Generic log formatter.
 */
public class LogFormatter extends SimpleFormatter {
	public static final String FORMAT_DEFAULT = "[%1$tF %1$tT] %3$s%%%4$s%7$s: %5$s%6$s%n";
	public static final String FORMAT_PROPERTY = "LogFormatter.format";

	/**
	 * Logging format string.
	 */
	private final String format;

	private final Date date = new Date();

	public LogFormatter() {
		this(null);
	}

	public LogFormatter(@Nullable String format) {
		this.format = ((format == null) || format.isEmpty()) ?
			System.getProperty(FORMAT_PROPERTY, FORMAT_DEFAULT) :
			format;
	}

	@Override
	public synchronized String format(@NotNull LogRecord record) {
		Objects.requireNonNull(record);

		date.setTime(record.getMillis());

		final String levelName = record.getLevel().getName();

		final String loggerName;
		final String contextName;

		if (record instanceof JournalRecord) {
			final JournalRecord journalRecord = (JournalRecord) record;

			loggerName = journalRecord.type().key();
			contextName = journalRecord.context()
					.map(context -> String.format("[%s]", context.name()))
					.orElse("");
		} else {
			loggerName = record.getLoggerName();
			contextName = "";
		}

		return String.format(
				format,
				date,
				getSource(record),
				loggerName,
				levelName,
				formatMessage(record),
				getThrowable(record),
				contextName);
	}

	private String getSource(@NotNull LogRecord record) {
		Objects.requireNonNull(record);

		String source;
		if (record.getSourceClassName() != null) {
			source = record.getSourceClassName();

			if (record.getSourceMethodName() != null) {
				source += " " + record.getSourceMethodName();
			}
		} else {
			source = record.getLoggerName();
		}

		return source;
	}

	private String getThrowable(@NotNull LogRecord record) {
		Objects.requireNonNull(record);

		if (record.getThrown() == null)
			return "";

		StringWriter stringWriter = new StringWriter();

		try (PrintWriter printWriter = new PrintWriter(stringWriter)) {
			printWriter.println();
			record.getThrown().printStackTrace(printWriter);
		}

		return stringWriter.toString();
	}
}
