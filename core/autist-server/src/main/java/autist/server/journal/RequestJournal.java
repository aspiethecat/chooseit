package autist.server.journal;

import org.jetbrains.annotations.NotNull;

import java.util.logging.LogRecord;

import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;

import autist.journal.Journal;
import autist.journal.JournalType;

public class RequestJournal {
	private final Journal m_journal = Journal.get(JournalType.REQUEST);

	public void log(
			final @NotNull HttpRequest request,
			final @NotNull HttpResponse response)
	{
		LogRecord record;

		record = new RequestLogRecord(request, response);
		record.setLoggerName(m_journal.type().loggerName());

		m_journal.log(record);
	}
}
