package autist.server;

public enum HttpExceptionType {
	UNKNOWN,
	INFORMATIONAL,
	SUCCESSFUL,
	REDIRECTION,
	CLIENT_ERROR,
	SERVER_ERROR;

	public static HttpExceptionType getErrorType(HttpException error) {
		final int prefix = error.getStatus().code() / 100;

		if (prefix >= values().length)
			return UNKNOWN;

		return values()[prefix];
	}
}
