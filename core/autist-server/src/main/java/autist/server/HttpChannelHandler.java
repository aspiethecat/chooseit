package autist.server;

import autist.journal.Journal;
import autist.journal.JournalType;
import autist.server.journal.RequestJournal;
import autist.server.application.Application;
import autist.server.application.RequestImpl;
import autist.server.application.ResponseImpl;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.logging.Level;

import java.io.IOException;

import java.net.URISyntaxException;

import io.netty.buffer.ByteBuf;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import io.netty.handler.codec.DecoderResult;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.HttpChunkedInput;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.LastHttpContent;
import io.netty.handler.codec.http.multipart.DefaultHttpDataFactory;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import io.netty.handler.stream.ChunkedStream;

import autist.application.resource.Resource;

public class HttpChannelHandler extends SimpleChannelInboundHandler<Object> {
	private static final Journal s_journal = Journal.get(JournalType.SERVER);
	private static final RequestJournal s_requestJournal = new RequestJournal();

	/**
	 * Server instance.
	 */
	private final Application m_application;

	/**
	 * Raw HTTP request.
	 */
	private HttpRequest m_rawRequest = null;

	/**
	 * POST request decoder.
	 */
	private HttpPostRequestDecoder m_postRequestDecoder = null;

	HttpChannelHandler(final @NotNull Application application) {
		m_application = Objects.requireNonNull(application);
	}

	@Override
	public final void channelReadComplete(
			final @NotNull ChannelHandlerContext handlerContext)
	{
		handlerContext.flush();
	}

	@Override
	protected final void channelRead0(
			final ChannelHandlerContext handlerContext,
			final Object message)
	{
		if (message instanceof HttpRequest) {
			m_rawRequest = (HttpRequest) message;

			if (HttpUtil.is100ContinueExpected(m_rawRequest)) {
				send100Continue(handlerContext);
			}

			if (m_rawRequest.method().equals(HttpMethod.POST)) {
				m_postRequestDecoder = new HttpPostRequestDecoder(
						new DefaultHttpDataFactory(DefaultHttpDataFactory.MINSIZE),
						m_rawRequest);
			}

			return;
		}

		if (message instanceof HttpContent) {
			if (m_postRequestDecoder != null)
				m_postRequestDecoder.offer((HttpContent) message);
		}

		if (message instanceof LastHttpContent) {
			final LastHttpContent trailer = (LastHttpContent) message;
			final HttpHeaders trailingHeaders = trailer.trailingHeaders();

			if (!trailingHeaders.isEmpty())
				m_rawRequest.headers().add(trailingHeaders);

			RequestImpl request = null;
			ResponseImpl response;
			try {
				request = decodeRequest(handlerContext);
				response = m_application.handleRequest(request);
			} catch (final RuntimeException e) {
				response = m_application.handleError(request, new HttpException(
						HttpException.INTERNAL_SERVER_ERROR,
						(e.getMessage() != null) ? e.getMessage() : e.getClass().getName(),
						e));
			} catch (final HttpException e) {
				response = m_application.handleError(request, e);
			} finally {
				if (m_postRequestDecoder != null) {
					m_postRequestDecoder.destroy();
					m_postRequestDecoder = null;
				}
			}

			s_requestJournal.log(
					m_rawRequest,
					sendResponse(handlerContext, response));
		}
	}

	private HttpResponse sendResponse(
			final @NotNull ChannelHandlerContext handlerContext,
			final @NotNull ResponseImpl response)
	{
		Objects.requireNonNull(handlerContext);
		Objects.requireNonNull(response);

		final HttpResponse rawResponse = response.build();

		handlerContext.write(rawResponse);

		Object  content = response.getContent();
		boolean keepAlive = response.isKeepAlive();

		ChannelFuture future;
		if (content instanceof ByteBuf) {
			future = sendBufferedContent(handlerContext, (ByteBuf) content);
		} else if (content instanceof Resource) {
			try {
				future = sendResourceContent(handlerContext, (Resource) content);
			} catch (IOException e) {
				future = handlerContext.channel().disconnect();
				keepAlive = false;

				s_journal.log(Level.SEVERE, "Can't process chunked file content", e);
			}
		} else {
			throw new AssertionError(String.format(
					"Unsupported content class: %s",
					content.getClass().getName()));
		}

		future.addListener((ChannelFutureListener) result -> {
			if (!result.isSuccess()) {
				s_journal.log(Level.SEVERE, result.cause().getMessage());
				result.channel().close();
			}
		});

		if (!keepAlive)
			future.addListener(ChannelFutureListener.CLOSE);

		return rawResponse;
	}

	private static ChannelFuture sendBufferedContent(
			final ChannelHandlerContext handlerContext,
			final ByteBuf content)
	{
		Objects.requireNonNull(handlerContext);
		Objects.requireNonNull(content);

		handlerContext.write(content);

		return handlerContext.writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT);
	}

	private static ChannelFuture sendResourceContent(
			final @NotNull ChannelHandlerContext handlerContext,
			final @NotNull Resource content)
			throws IOException
	{
		Objects.requireNonNull(handlerContext);
		Objects.requireNonNull(content);

		return handlerContext.writeAndFlush(new HttpChunkedInput(
				new ChunkedStream(content.inputStream(), 8192)));
	}

	private RequestImpl decodeRequest(
			final @NotNull ChannelHandlerContext handlerContext)
			throws HttpException
	{
		Objects.requireNonNull(handlerContext);

		final DecoderResult decoderResult = m_rawRequest.decoderResult();
		if (!decoderResult.isSuccess()) {
			throw new HttpException(
					HttpException.BAD_REQUEST,
					String.format("Request decoding failed: %s", m_rawRequest.uri()),
					decoderResult.cause());
		}

		final RequestImpl request;
		try {
			request = new RequestImpl(handlerContext, m_rawRequest, m_postRequestDecoder);
		} catch (final URISyntaxException e) {
			throw new HttpException(
					HttpException.BAD_REQUEST,
					String.format("Invalid request URI: %s", m_rawRequest.uri()),
					e);
		} catch (final IOException e) {
			throw new HttpException(
					HttpException.INTERNAL_SERVER_ERROR,
					String.format("Can't read POST data: %s", m_rawRequest.uri()),
					e);
		}

		return request;
	}

	private static void send100Continue(
			final @NotNull ChannelHandlerContext handlerContext)
	{
		handlerContext.write(new DefaultFullHttpResponse(
				HttpVersion.HTTP_1_1, HttpResponseStatus.CONTINUE));
	}

	@Override
	public final void exceptionCaught(
			final @NotNull ChannelHandlerContext handlerContext,
			final Throwable cause)
	{
		s_journal.log(Level.WARNING, "An exception caught during request processing", cause);
		handlerContext.close();
	}
}
