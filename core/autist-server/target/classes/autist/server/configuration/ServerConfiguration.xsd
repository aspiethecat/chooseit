<?xml version="1.0" encoding="UTF-8"?>
<xs:schema
		version="1.1"
		elementFormDefault="qualified"
		xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:simpleType name="networkAddress">
		<xs:annotation>
			<xs:documentation>IPv4 network address</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:token">
			<xs:pattern value="(25[0-5]|2[0-4][0-9]|[0-1]?[0-9]?[0-9])\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9]?[0-9])\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9]?[0-9])\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9]?[0-9])" />
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="networkPort">
		<xs:annotation>
			<xs:documentation>Network port number</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:unsignedShort">
			<xs:minInclusive value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="socketConfiguration">
		<xs:attribute name="backlog" type="xs:positiveInteger">
			<xs:annotation>
				<xs:documentation>Backlog queue size</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="sendBuffer" type="xs:positiveInteger">
			<xs:annotation>
				<xs:documentation>Socket-level send buffer size</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="receiveBuffer" type="xs:positiveInteger">
			<xs:annotation>
				<xs:documentation>Socket-level receive buffer size</xs:documentation>
			</xs:annotation>
		</xs:attribute>
	</xs:complexType>

	<xs:complexType name="serverConfiguration">
		<xs:complexContent>
			<xs:extension base="socketConfiguration">
				<xs:attribute name="bossThreadCount" type="xs:positiveInteger">
					<xs:annotation>
						<xs:documentation>Boss thread pool size</xs:documentation>
					</xs:annotation>
				</xs:attribute>

				<xs:attribute name="workerThreadCount" type="xs:positiveInteger">
					<xs:annotation>
						<xs:documentation>Worker thread pool size</xs:documentation>
					</xs:annotation>
				</xs:attribute>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<xs:complexType name="channelConfiguration">
		<xs:complexContent>
			<xs:extension base="socketConfiguration">
				<xs:attribute name="name" type="xs:string" use="required">
					<xs:annotation>
						<xs:documentation>Channel name</xs:documentation>
					</xs:annotation>
				</xs:attribute>

				<xs:attribute name="address" type="networkAddress" default="127.0.0.1">
					<xs:annotation>
						<xs:documentation>Network address (loopback if unspecified)</xs:documentation>
					</xs:annotation>
				</xs:attribute>

				<xs:attribute name="port" type="networkPort" use="required">
					<xs:annotation>
						<xs:documentation>Port number</xs:documentation>
					</xs:annotation>
				</xs:attribute>

				<xs:attribute name="secure" type="xs:boolean" default="false">
					<xs:annotation>
						<xs:documentation>SSL enable flag</xs:documentation>
					</xs:annotation>
				</xs:attribute>

				<xs:attribute name="privateKey" type="xs:string">
					<xs:annotation>
						<xs:documentation>SSL private key path</xs:documentation>
					</xs:annotation>
				</xs:attribute>

				<xs:attribute name="certificate" type="xs:string">
					<xs:annotation>
						<xs:documentation>SSL certificate path</xs:documentation>
					</xs:annotation>
				</xs:attribute>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<xs:complexType name="channelList">
		<xs:sequence>
			<xs:element name="channel" type="channelConfiguration" maxOccurs="unbounded" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="logLevel">
		<xs:annotation>
			<xs:documentation>Logging level</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:enumeration value="severe" />
			<xs:enumeration value="warning" />
			<xs:enumeration value="info" />
			<xs:enumeration value="config" />
			<xs:enumeration value="fine" />
			<xs:enumeration value="finer" />
			<xs:enumeration value="finest" />
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="logConfiguration">
		<xs:annotation>
			<xs:documentation>Log configuration</xs:documentation>
		</xs:annotation>

		<xs:attribute name="path" type="xs:string" use="required">
			<xs:annotation>
				<xs:documentation>Log file path pattern</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="level" type="logLevel">
			<xs:annotation>
				<xs:documentation>Logging level</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="format" type="xs:string">
			<xs:annotation>
				<xs:documentation>Logging format</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="maxSize" type="xs:positiveInteger">
			<xs:annotation>
				<xs:documentation>Maximum log file size in bytes</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="maxCount" type="xs:positiveInteger" default="1">
			<xs:annotation>
				<xs:documentation>Maximum log file count</xs:documentation>
			</xs:annotation>
		</xs:attribute>
	</xs:complexType>

	<xs:simpleType name="environmentType">
		<xs:annotation>
			<xs:documentation>Environment type</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:enumeration value="production" />
			<xs:enumeration value="development" />
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="namedEntityConfiguration">
		<xs:annotation>
			<xs:documentation>Named entity configuration</xs:documentation>
		</xs:annotation>

		<xs:attribute name="name" type="xs:string" use="required">
			<xs:annotation>
				<xs:documentation>Entity name</xs:documentation>
			</xs:annotation>
		</xs:attribute>
	</xs:complexType>

	<xs:complexType name="mailerConfiguration">
		<xs:annotation>
			<xs:documentation>Mailer configuration</xs:documentation>
		</xs:annotation>

		<xs:attribute name="server" type="xs:string">
			<xs:annotation>
				<xs:documentation>SMTP server address (IPv4, IPv6 or domain)</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="userName" type="xs:string">
			<xs:annotation>
				<xs:documentation>SMTP server user name</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="password" type="xs:string">
			<xs:annotation>
				<xs:documentation>SMTP server password</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="connectionTimeout" type="xs:long">
			<xs:annotation>
				<xs:documentation>SMTP server connection timeout</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="readTimeout" type="xs:long">
			<xs:annotation>
				<xs:documentation>SMTP server read timeout</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="writeTimeout" type="xs:long">
			<xs:annotation>
				<xs:documentation>SMTP server write timeout</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="secure" type="xs:boolean">
			<xs:annotation>
				<xs:documentation>Secure connection flag</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="senderAddress" type="xs:string">
			<xs:annotation>
				<xs:documentation>Default sender e-mail address</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="senderName" type="xs:string">
			<xs:annotation>
				<xs:documentation>Default sender name</xs:documentation>
			</xs:annotation>
		</xs:attribute>
	</xs:complexType>

	<xs:complexType name="dataSourceConfiguration">
		<xs:annotation>
			<xs:documentation>Database connection settings</xs:documentation>
		</xs:annotation>

		<xs:complexContent>
			<xs:extension base="namedEntityConfiguration">
				<xs:attribute name="driver" type="xs:string" use="required">
					<xs:annotation>
						<xs:documentation>Database driver class name</xs:documentation>
					</xs:annotation>
				</xs:attribute>

				<xs:attribute name="url" type="xs:string" use="required">
					<xs:annotation>
						<xs:documentation>Database URL</xs:documentation>
					</xs:annotation>
				</xs:attribute>

				<xs:attribute name="userName" type="xs:string" use="required">
					<xs:annotation>
						<xs:documentation>User name</xs:documentation>
					</xs:annotation>
				</xs:attribute>

				<xs:attribute name="password" type="xs:string">
					<xs:annotation>
						<xs:documentation>Password</xs:documentation>
					</xs:annotation>
				</xs:attribute>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<xs:complexType name="dataSourceList">
		<xs:annotation>
			<xs:documentation>Data sources list</xs:documentation>
		</xs:annotation>

		<xs:sequence>
			<xs:element name="dataSource" type="dataSourceConfiguration" maxOccurs="unbounded" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="templateRootConfiguration">
		<xs:annotation>
			<xs:documentation>Template root settings</xs:documentation>
		</xs:annotation>

		<xs:complexContent>
			<xs:extension base="namedEntityConfiguration">
				<xs:attribute name="uri" type="xs:string">
					<xs:annotation>
						<xs:documentation>Template root URI</xs:documentation>
					</xs:annotation>
				</xs:attribute>
				<xs:attribute name="path" type="xs:string">
					<xs:annotation>
						<xs:documentation>Template root path</xs:documentation>
					</xs:annotation>
				</xs:attribute>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<xs:complexType name="templatesConfiguration">
		<xs:annotation>
			<xs:documentation>Templates configuration</xs:documentation>
		</xs:annotation>

		<xs:sequence>
			<xs:element name="roots" type="templateRootList" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="templateRootList">
		<xs:annotation>
			<xs:documentation>Template roots list</xs:documentation>
		</xs:annotation>

		<xs:sequence>
			<xs:element name="root" type="templateRootConfiguration" maxOccurs="unbounded" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="assetsConfiguration">
		<xs:annotation>
			<xs:documentation>Assets configuration</xs:documentation>
		</xs:annotation>

		<xs:sequence>
			<xs:element name="roots" type="assetRootList" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="assetRootList">
		<xs:annotation>
			<xs:documentation>Asset roots list</xs:documentation>
		</xs:annotation>

		<xs:sequence>
			<xs:element name="root" type="assetRootConfiguration" maxOccurs="unbounded" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="assetRootConfiguration">
		<xs:annotation>
			<xs:documentation>Asset root settings</xs:documentation>
		</xs:annotation>

		<xs:complexContent>
			<xs:extension base="namedEntityConfiguration">
				<xs:attribute name="uri" type="xs:string">
					<xs:annotation>
						<xs:documentation>Asset root URI</xs:documentation>
					</xs:annotation>
				</xs:attribute>
				<xs:attribute name="path" type="xs:string">
					<xs:annotation>
						<xs:documentation>Asset root path</xs:documentation>
					</xs:annotation>
				</xs:attribute>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<xs:complexType name="applicationConfiguration">
		<xs:annotation>
			<xs:documentation>Application settings</xs:documentation>
		</xs:annotation>

		<xs:sequence>
			<xs:element name="mailer" type="mailerConfiguration" minOccurs="0" />
			<xs:element name="dataSources" type="dataSourceList" minOccurs="0" />
			<xs:element name="templates" type="templatesConfiguration" minOccurs="0" />
			<xs:element name="assets" type="assetsConfiguration" minOccurs="0" />
		</xs:sequence>

		<xs:attribute name="name" type="xs:string" use="required">
			<xs:annotation>
				<xs:documentation>Application instance name</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="cacheRoot" type="xs:string" use="required">
			<xs:annotation>
				<xs:documentation>Path to cache directory</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="specification" type="xs:string" use="required">
			<xs:annotation>
				<xs:documentation>Path to application specification file</xs:documentation>
			</xs:annotation>
		</xs:attribute>

		<xs:attribute name="environment" type="environmentType" default="production">
			<xs:annotation>
				<xs:documentation>Environment type</xs:documentation>
			</xs:annotation>
		</xs:attribute>
	</xs:complexType>

	<xs:element name="configuration">
		<xs:annotation>
			<xs:documentation>Server configuration root</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:all>
				<xs:element name="server" type="serverConfiguration" minOccurs="0" />
				<xs:element name="channels" type="channelList" />
				<xs:element name="systemLog" type="logConfiguration" minOccurs="0" />
				<xs:element name="requestLog" type="logConfiguration" minOccurs="0"  />
				<xs:element name="application" type="applicationConfiguration" />
			</xs:all>
		</xs:complexType>
	</xs:element>
</xs:schema>
