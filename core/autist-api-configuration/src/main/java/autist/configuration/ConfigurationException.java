package autist.configuration;

import org.jetbrains.annotations.Nullable;

/**
 * Configuration exception class.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class ConfigurationException extends Exception {
	public ConfigurationException() { }

	public ConfigurationException(final @Nullable String message) {
		super(message);
	}

	public ConfigurationException(final @Nullable String message, final @Nullable Throwable cause) {
		super(message, cause);
	}

	public ConfigurationException(final @Nullable Throwable cause) {
		super(cause);
	}
}
