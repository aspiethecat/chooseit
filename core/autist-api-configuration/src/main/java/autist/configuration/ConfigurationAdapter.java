package autist.configuration;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public abstract class ConfigurationAdapter<ValueType, BoundType>
		extends XmlAdapter<ValueType, BoundType>
{
	@Override
	public ValueType marshal(final BoundType value) throws Exception {
		throw new UnsupportedOperationException();
	}
}
