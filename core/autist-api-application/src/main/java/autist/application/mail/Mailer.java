package autist.application.mail;

import org.jetbrains.annotations.NotNull;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

public interface Mailer {
	@NotNull
	MimeMessage createMessage();

	void sendMessage(
			final @NotNull MimeMessage message)
			throws MessagingException;
}
