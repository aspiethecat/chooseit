package autist.application;

import org.jetbrains.annotations.Nullable;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Set;

import autist.application.resource.Resource;
import autist.application.util.Cookie;

public interface Response {
	void setDate(@Nullable Date date);
	void setLastModified(@Nullable Date date);

	void setCookies(Set<Cookie> cookies);

	void setContentType(@Nullable String contentType);

	void setContent(URI uri);
	void setContent(byte[] content);
	void setContent(String content, Charset charset);
	void setContent(Resource content);
}
