package autist.application;

@SuppressWarnings("unused")
public enum RequestMethod {
	OPTIONS,
	GET,
	HEAD,
	POST,
	PUT,
	PATCH,
	DELETE,
	TRACE,
	CONNECT
}
