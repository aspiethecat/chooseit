package autist.application.resource;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import java.io.IOException;

import java.net.URI;

import autist.commons.net.URN;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface ResourceStore {
	@NotNull
	@Contract(pure = true)
	String name();

	@NotNull
	@Contract(pure = true)
	URI uri();

	@Contract(pure = true)
	boolean isCacheRequired();

	@NotNull
	Optional<Resource> fetch(
			final @NotNull URN urn)
			throws IllegalArgumentException, IOException;

	@NotNull
	Optional<Resource> store(
			final @NotNull URN urn,
			final @NotNull Resource source)
			throws IllegalArgumentException, IOException;
}
