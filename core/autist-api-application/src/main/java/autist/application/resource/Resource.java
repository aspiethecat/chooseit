package autist.application.resource;

import autist.commons.net.URN;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.InputStream;
import java.net.URL;

@SuppressWarnings("unused")
public interface Resource extends AutoCloseable {
	@NotNull
	@Contract(pure = true)
	ResourceStore store();

	@NotNull
	@Contract(pure = true)
	URN urn();

	@NotNull
	@Contract(pure = true)
	URL url();

	@NotNull
	@Contract(pure = true)
	ResourceAttributes attributes();

	@NotNull
	@Contract(pure = true)
	InputStream inputStream();
}
