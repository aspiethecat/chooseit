package autist.application.resource;

import java.nio.file.attribute.FileTime;

public interface ResourceAttributes {
	long length();
	FileTime lastModifiedTime();
}
