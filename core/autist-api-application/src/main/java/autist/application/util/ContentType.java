package autist.application.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@SuppressWarnings("unused")
public class ContentType {
	@NotNull
	private final String m_type;

	@NotNull
	private final String m_subtype;

	@Nullable
	private final String m_suffix;

	@Nullable
	private Map<String, String> m_parameters;

	public ContentType(
			final @NotNull String type,
			final @NotNull String subtype)
	{
		this(type, subtype, null);
	}

	public ContentType(
			final @NotNull String type,
			final @NotNull String subtype,
			@Nullable final String suffix)
	{
		m_type = Objects.requireNonNull(type, "type == null");
		m_subtype = Objects.requireNonNull(subtype, "type == null");
		m_suffix = suffix;
	}

	@NotNull
	@Contract(pure = true)
	public final String getType() {
		return m_type;
	}

	@NotNull
	@Contract(pure = true)
	public final String getSubtype() {
		return m_subtype;
	}

	@NotNull
	@Contract(pure = true)
	public final Optional<String> getSuffix() {
		return Optional.ofNullable(m_suffix);
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();

		builder.append(m_type).append('/').append(m_subtype);

		if (m_suffix != null)
			builder.append('+').append(m_suffix);

		return builder.toString();
	}

	public static final ContentType valueOf(final @NotNull String contentTypeString) {
		throw new UnsupportedOperationException();
	}
}
