package autist.application.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.Optional;

@SuppressWarnings("unused")
public class HostInfo {
	@NotNull
	private final String m_hostName;

	@Nullable
	private final Integer m_portNumber;

	public HostInfo(final @NotNull String hostName) {
		this(hostName, null);
	}

	public HostInfo(
			final @NotNull String hostName,
			@Nullable final Integer portNumber)
	{
		m_hostName = Objects.requireNonNull(hostName, "hostName == null");
		m_portNumber = portNumber;
	}

	@NotNull
	@Contract(pure = true)
	public final String getHostName() {
		return m_hostName;
	}

	@NotNull
	@Contract(pure = true)
	public final Optional<Integer> getPortNumber() {
		return Optional.ofNullable(m_portNumber);
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();

		builder.append(m_hostName);

		if (m_portNumber != null)
			builder.append(':').append(m_portNumber);

		return builder.toString();
	}

	public static HostInfo valueOf(final @NotNull String hostNameString) {
		throw new UnsupportedOperationException();
	}
}
