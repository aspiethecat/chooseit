package autist.application.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public final class Cookie {
	/**
	 * Cookie name.
	 *
	 * @see #getName()
	 */
	private final String m_name;

	/**
	 * Cookie value.
	 *
	 * @see #getValue()
	 */
	private String m_value;

	/**
	 * Domain.
	 *
	 * @see #getDomain()
	 * @see #setDomain(String)
	 */
	private String m_domain = null;

	/**
	 * Path.
	 *
	 * @see #getPath()
	 * @see #setPath(String)
	 */
	private String m_path = null;

	/**
	 * Maximum age in seconds.
	 *
	 * @see #getMaxAge()
	 * @see #setMaxAge(Long)
	 */
	private Long m_maxAge = null;

	/**
	 * HTTP-only cookie flag.
	 *
	 * @see #isHttpOnly()
	 * @see #setHttpOnly(boolean)
	 */
	private boolean m_httpOnly = false;

	/**
	 * Secure cookie flag.
	 *
	 * @see #isSecure()
	 * @see #setSecure(boolean)
	 */
	private boolean m_secure = false;

	public Cookie(
			final @NotNull String name,
			final @NotNull String value)
	{
		m_name = Objects.requireNonNull(name);
		m_value = Objects.requireNonNull(value);
	}

	@Contract(pure = true)
	public String getName() {
		return m_name;
	}

	@Contract(pure = true)
	public String getValue() {
		return m_value;
	}

	public void setValue(final String value) {
		m_value = Objects.requireNonNull(value);
	}

	@Contract(pure = true)
	public String getDomain() {
		return m_domain;
	}

	public void setDomain(@Nullable final String domain) {
		m_domain = domain;
	}

	@Contract(pure = true)
	public String getPath() {
		return m_path;
	}

	public void setPath(@Nullable final String path) {
		m_path = path;
	}

	@Contract(pure = true)
	public Long getMaxAge() {
		return m_maxAge;
	}

	public void setMaxAge(@Nullable final Long maxAge) {
		m_maxAge = maxAge;
	}

	@Contract(pure = true)
	public boolean isHttpOnly() {
		return m_httpOnly;
	}

	public void setHttpOnly(final boolean httpOnly) {
		m_httpOnly = httpOnly;
	}

	@Contract(pure = true)
	public boolean isSecure() {
		return m_secure;
	}

	public void setSecure(final boolean secure) {
		m_secure = secure;
	}
}
