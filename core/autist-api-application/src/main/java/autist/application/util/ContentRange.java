package autist.application.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

import autist.commons.util.Bounds;

@SuppressWarnings("unused")
public abstract class ContentRange {
	@NonNls
	private static final char FIELD_SEPARATOR = ' ';

	@NonNls
	private static final char BOUND_SEPARATOR = '-';

	@NotNull
	private final ContentRangeType m_type;

	@Nullable
	private final Bounds<Long> m_range;

	private ContentRange() {
		m_type = ContentRangeType.NONE;
		m_range = null;
	}

	private ContentRange(
			final @NotNull Long lowerBound,
			final @NotNull Long upperBound)
	{
		m_type = ContentRangeType.BYTES;
		m_range = Bounds.range(lowerBound, upperBound);
	}

	@NotNull
	@Contract(pure = true)
	public final ContentRangeType getType() {
		return m_type;
	}

	@NotNull
	@Contract(pure = true)
	public final Optional<Bounds<Long>> getRange() {
		return Optional.ofNullable(m_range);
	}

	public static class Request extends ContentRange {
		public Request() {
			super();
		}

		public Request(
				final @NotNull Long lowerBound,
				final @NotNull Long upperBound)
		{
			super(upperBound, lowerBound);
		}

		@NonNls
		@NotNull
		@Override
		public String toString() {
			final StringBuilder builder = new StringBuilder();

			builder.append(getType().name().toLowerCase());

			getRange().ifPresent(
					(range) -> builder
							.append(FIELD_SEPARATOR)
							.append(range.lowerBound())
							.append(BOUND_SEPARATOR)
							.append(range.upperBound())
			);

			return builder.toString();
		}
	}

	public static class Response extends ContentRange {
		@Nullable
		private final Long m_size;

		public Response() {
			super();
			m_size = null;
		}

		public Response(
				final @NotNull Long lowerBound,
				final @NotNull Long upperBound,
				@Nullable final Long size)
		{
			super(upperBound, lowerBound);
			m_size = size;
		}

		@NotNull
		@Contract(pure = true)
		public final Optional<Long> getSize() {
			return Optional.ofNullable(m_size);
		}
	}
}
