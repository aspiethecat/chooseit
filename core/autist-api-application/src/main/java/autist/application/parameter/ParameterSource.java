package autist.application.parameter;

@SuppressWarnings({"WeakerAccess", "unused"})
public enum ParameterSource {
	PATH,
	QUERY,
	COOKIE
}
