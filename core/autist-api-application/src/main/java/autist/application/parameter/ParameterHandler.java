package autist.application.parameter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface ParameterHandler<
		HandlerType extends ParameterHandler<HandlerType, ContextType, ValueType>,
		ContextType extends ParameterHandlerContext<ContextType>,
		ValueType>
{
	@NotNull
	@Contract(pure = true)
	Class<ValueType> type();

	@NotNull
	@Contract(pure = true)
	ContextType getContext(final @NotNull ParameterDescriptor descriptor);

	@NotNull
	@Contract(pure = true)
	ValueType parseValue(
			final @NotNull ContextType context,
			final @NotNull String stringValue)
			throws ParameterHandlerException;
}
