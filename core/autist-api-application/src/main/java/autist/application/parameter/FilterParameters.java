package autist.application.parameter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface FilterParameters<
		ParametersType extends FilterParameters<ParametersType>>
		extends Parameters<ParametersType>
{
	@NotNull
	@Contract(pure = true)
	Optional<String> filter();
}
