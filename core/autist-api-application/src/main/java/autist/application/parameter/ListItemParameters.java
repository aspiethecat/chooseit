package autist.application.parameter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface ListItemParameters<
		ParametersType extends ListItemParameters<ParametersType, FieldType>,
		FieldType extends Enum<FieldType>>
		extends Parameters<ParametersType>
{
	enum SortOrder {
		ASC,
		DESC
	}

	@NotNull
	@Contract(pure = true)
	Optional<FieldType> sortField();

	@NotNull
	@Contract(pure = true)
	default Optional<SortOrder> sortOrder() {
		return Optional.of(SortOrder.ASC);
	}
}
