package autist.application.parameter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Set;

import javax.validation.ConstraintViolation;

import autist.application.Request;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface Parameters<
		ParametersType extends Parameters<ParametersType>>
{
	@NotNull
	@Contract(pure = true)
	Request request();

	@Contract(pure = true)
	boolean hasConstraintViolations();

	@NotNull
	@Contract(pure = true)
	Set<ConstraintViolation<ParametersType>> constraintViolations();
}
