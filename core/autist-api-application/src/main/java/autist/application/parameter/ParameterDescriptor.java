package autist.application.parameter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Optional;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface ParameterDescriptor {
	@NotNull
	@Contract(pure = true)
	String name();

	@NotNull
	@Contract(pure = true)
	Optional<Class<? extends Collection>> collectionType();

	@NotNull
	@Contract(pure = true)
	Class<?> valueType();

	@NotNull
	@Contract(pure = true)
	Optional<Class<? extends ParameterHandler>> valueTypeHandler();

	@Contract(pure = true)
	boolean isOptional();

	@Contract(pure = true)
	boolean isCollection();

	@NotNull
	@Contract(pure = true)
	Optional<String> format();

	@NotNull
	@Contract(pure = true)
	String message();

	@NotNull
	@Contract(pure = true)
	ParameterSource[] source();
}
