package autist.application.parameter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface DictionaryEntityParameters<
		ParametersType extends DictionaryEntityParameters<ParametersType>>
		extends NamedEntityParameters<ParametersType>
{
	@NotNull
	@Contract(pure = true)
	Optional<String> description();
}
