package autist.application.parameter;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface CoreEntityParameters<
		ParametersType extends CoreEntityParameters<ParametersType>>
		extends DictionaryEntityParameters<ParametersType>
{
}
