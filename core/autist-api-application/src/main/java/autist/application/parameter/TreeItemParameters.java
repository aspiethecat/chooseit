package autist.application.parameter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface TreeItemParameters<
		ParametersType extends TreeItemParameters<ParametersType, FieldType>,
		FieldType extends Enum<FieldType>>
		extends ListItemParameters<ParametersType, FieldType>
{
	@NotNull
	@Contract(pure = true)
	Optional<Long> parentId();
}
