package autist.application.parameter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Parameter {
	/** Parameter name */
	String name() default "";

	/** Parameter type */
	Class<?> type() default Void.class;

	/** Value format */
	String format() default "";

	/** Acceptable parameter sources (ordered) */
	ParameterSource[] source() default {};

	/** Parsing error message */
	String message() default "";

	/** Parameter type resolver */
	Class<? extends ParameterHandler> typeHandler()
			default ParameterHandler.class;
}
