package autist.application.parameter;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface NamedEntityParameters<
		ParametersType extends NamedEntityParameters<ParametersType>>
		extends EntityParameters<ParametersType>
{
	@NotNull
	@Contract(pure = true)
	Optional<String> name();
}
