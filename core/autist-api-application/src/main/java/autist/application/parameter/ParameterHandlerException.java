package autist.application.parameter;

@SuppressWarnings("unused")
public class ParameterHandlerException extends Exception {
	public ParameterHandlerException() { }

	public ParameterHandlerException(final String message) {
		super(message);
	}

	public ParameterHandlerException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public ParameterHandlerException(final Throwable cause) {
		super(cause);
	}
}
