package autist.application;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.InetSocketAddress;
import java.net.URI;
import java.util.List;
import java.util.Map;

import autist.commons.net.URN;

@SuppressWarnings("unused")
public interface Request {
	/** Returns request method */
	@NotNull
	@Contract(pure = true)
	RequestMethod getMethod();

	/** Returns client address */
	@NotNull
	@Contract(pure = true)
	InetSocketAddress getClientAddress();

	/** Returns server address */
	@NotNull
	@Contract(pure = true)
	InetSocketAddress getServerAddress();

	/** Returns server name */
	@Contract(pure = true)
	String getServerName();

	/** Returns full request URI */
	@NotNull
	@Contract(pure = true)
	URI getURI();

	/** Returns request URN */
	@NotNull
	@Contract(pure = true)
	URN getURN();

	/** Returns query string */
	@Contract(pure = true)
	@SuppressWarnings("unused")
	String getQueryString();

	/** Returns parameters map */
	@NotNull
	@Contract(pure = true)
	Map<String, List<String>> getParameters();

	/** Returns cookies map */
	@NotNull
	@Contract(pure = true)
	Map<String, String> getCookies();

	/** Returns header value */
	@Nullable
	@Contract(pure = true)
	String getHeader(final @NotNull CharSequence name);

	@Contract(pure = true)
	boolean isKeepAlive();
}
