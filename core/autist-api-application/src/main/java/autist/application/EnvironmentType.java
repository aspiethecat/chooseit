package autist.application;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public enum EnvironmentType {
	PRODUCTION,
	TESTING,
	DEVELOPMENT;

	public static class Adapter extends XmlAdapter<String, EnvironmentType> {
		@Override
		public EnvironmentType unmarshal(String environmentType) throws Exception {
			return EnvironmentType.valueOf(environmentType.toUpperCase());
		}

		@Override
		public String marshal(EnvironmentType environmentType) throws Exception {
			throw new UnsupportedOperationException();
		}
	}
}
