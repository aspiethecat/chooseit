package autist.application.action;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import org.apache.ibatis.session.SqlSession;

import autist.database.EntityMapper;
import autist.database.Entity;

import autist.application.handler.HandlerException;
import autist.application.Model;
import autist.application.model.DefaultStoreModel;
import autist.application.parameter.EntityParameters;
import autist.application.view.JsonView;

@SuppressWarnings("unused")
public abstract class DefaultStoreAction<
		ContextType extends GenericActionContext<ContextType>,
		EntityType extends Entity,
		EntityMapperType extends EntityMapper<EntityType>,
		QueryType extends EntityParameters<QueryType>>
		extends GenericAction<
				DefaultStoreAction<ContextType, EntityType, EntityMapperType, QueryType>,
				ContextType, EntityType, EntityMapperType, QueryType>
{
	@Override
	@NotNull
	@Contract("null -> fail; !null -> _")
	public final Optional<Model> invoke(
			final @NotNull QueryType query)
			throws HandlerException
	{
		return Optional.of(new JsonView(invokeTransaction(
				session -> {
					final DefaultStoreModel<EntityType, EntityMapperType, QueryType> model = createModel(session, query);

					model.setStatus(DefaultFormAction.validate(model, query));
					model.check();

					if (model.getStatus())
						model.store();

					return model;
				})));
	}

	@Contract("null, _ -> fail; _, null -> fail; !null, !null -> _")
	protected abstract DefaultStoreModel<EntityType, EntityMapperType, QueryType> createModel(
			final @NotNull SqlSession session,
			final @NotNull QueryType query);
}
