package autist.application.action;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import autist.database.EntityMapper;
import autist.database.Entity;

import autist.application.handler.HandlerException;
import autist.application.Model;
import autist.application.model.DefaultDeleteModel;
import autist.application.parameter.EntityParameters;
import autist.application.view.JsonView;

@SuppressWarnings("unused")
public abstract class DefaultDeleteAction<
		ContextType extends GenericActionContext<ContextType>,
		EntityType extends Entity,
		EntityMapperType extends EntityMapper<EntityType>,
		ParametersType extends EntityParameters<ParametersType>>
		extends GenericAction<
				DefaultDeleteAction<
						ContextType, EntityType, EntityMapperType, ParametersType>,
				ContextType, EntityType, EntityMapperType, ParametersType>
{
	@Override
	@NotNull
	@Contract("null -> fail; !null -> _")
	public Optional<Model> invoke(
			final @NotNull ParametersType query)
			throws HandlerException
	{
		if (!query.id().isPresent())
			return Optional.empty();

		return Optional.of(new JsonView(invokeTransaction(
				session -> {
					return new DefaultDeleteModel<>(session, getMapperClass(), query.id().get());
				})));
	}
}
