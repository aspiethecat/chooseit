package autist.application.action;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import autist.database.TreeEntityMapper;
import autist.database.TreeEntity;

import autist.application.handler.HandlerException;
import autist.application.Model;
import autist.application.model.DefaultTreeSearchModel;
import autist.application.parameter.TreeItemParameters;

@SuppressWarnings("unused")
public abstract class DefaultTreeSearchAction<
		ContextType extends GenericActionContext<ContextType>,
		EntityType extends TreeEntity<EntityType, EntityType>,
		EntityMapperType extends TreeEntityMapper<EntityType, EntityType>,
		QueryType extends TreeItemParameters<QueryType, FieldType>,
		FieldType extends Enum<FieldType>>
		extends GenericAction<
				DefaultTreeSearchAction<
						ContextType, EntityType, EntityMapperType, QueryType, FieldType>,
				ContextType, EntityType, EntityMapperType, QueryType>
{
	@Override
	@NotNull
	@Contract("null -> fail; !null -> _")
	public Optional<Model> invoke(
			final @NotNull QueryType query)
			throws HandlerException
	{
		return Optional.of(invokeTransaction(session -> {
			return new DefaultTreeSearchModel<>(session, getMapperClass(), query);
		}));
	}
}
