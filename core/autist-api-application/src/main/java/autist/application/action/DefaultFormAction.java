package autist.application.action;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import autist.application.model.AbstractFormModel;
import autist.application.parameter.Parameters;

import javax.validation.ConstraintViolation;
import java.util.Set;

@SuppressWarnings("unused")
public abstract class DefaultFormAction<
		ContextType extends GenericActionContext<ContextType>,
		ParametersType extends Parameters<ParametersType>>
		extends PersistenceAction<
				DefaultFormAction<ContextType, ParametersType>, ContextType, ParametersType>
{
	@Contract("null, _ -> fail; _, null -> fail; !null, !null -> _")
	public static <
			QueryType extends Parameters<QueryType>>
	boolean validate(
			final @NotNull AbstractFormModel model,
			final @NotNull QueryType query)
	{
		final Set<ConstraintViolation<QueryType>> violations = query.constraintViolations();
		if (violations.isEmpty())
			return true;

		violations.forEach((item) -> model.addError(
				item.getPropertyPath().toString(),
				item.getMessage()));

		return false;
	}
}
