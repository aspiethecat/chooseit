package autist.application.action;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import autist.commons.context.Context;
import autist.commons.reflection.ReflectionUtil;
import autist.configuration.ConfigurationException;

import autist.application.handler.ActionHandler;
import autist.application.handler.HandlerContext;
import autist.application.parameter.Parameters;

public abstract class AbstractAction<
		HandlerType extends AbstractAction<HandlerType, ContextType, ParametersType>,
		ContextType extends AbstractActionContext<ContextType>,
		ParametersType extends Parameters<ParametersType>>
		implements ActionHandler<HandlerType, ContextType, ParametersType>
{
	@NotNull
	private final ContextType m_context;

	@SuppressWarnings("unchecked")
	protected AbstractAction() {
		final Class<ContextType> contextType = (Class<ContextType>) ReflectionUtil
				.getGenericParameterType(getClass(), AbstractAction.class, 0);

		m_context = Context.requireInstance(contextType);
	}

	protected void configure() throws ConfigurationException { }

	@Override
	@NotNull
	@Contract(pure = true)
	public final ContextType context() {
		return m_context;
	}
}
