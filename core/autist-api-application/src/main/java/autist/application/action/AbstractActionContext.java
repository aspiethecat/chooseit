package autist.application.action;

import autist.commons.context.AbstractContextNode;
import org.jetbrains.annotations.NotNull;

import autist.application.handler.HandlerContext;
import autist.application.handler.HandlerEnvironment;

@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class AbstractActionContext<
		ContextType extends AbstractActionContext<ContextType>>
		extends AbstractContextNode<ContextType, HandlerEnvironment>
		implements HandlerContext<ContextType>
{
	public AbstractActionContext(final @NotNull HandlerEnvironment parent) {
		super(parent);
	}
}
