package autist.application.action;

import autist.database.NamedEntityMapper;
import autist.database.NamedEntity;

import autist.application.handler.HandlerException;
import autist.application.Model;
import autist.application.model.DefaultFilterModel;
import autist.application.parameter.FilterParameters;
import autist.application.view.JsonView;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@SuppressWarnings("unused")
public class DefaultFilterAction<
		ContextType extends GenericActionContext<ContextType>,
		EntityType extends NamedEntity,
		EntityMapperType extends NamedEntityMapper<EntityType>,
		ParametersType extends FilterParameters<ParametersType>>
		extends GenericAction<
				DefaultFilterAction<
						ContextType, EntityType, EntityMapperType, ParametersType>,
				ContextType, EntityType, EntityMapperType, ParametersType>
{
	@Override
	@NotNull
	@Contract("null -> fail; !null -> _")
	public Optional<Model> invoke(
			final @NotNull ParametersType query)
			throws HandlerException
	{
		final DefaultFilterModel<EntityType, EntityMapperType, ParametersType> model = invokeTransaction(
				session -> {
					return new DefaultFilterModel<>(session, getMapperClass(), query);
				});

		return Optional.of(new JsonView(model.getItems()));
	}
}
