package autist.application.action;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import autist.database.EntityMapper;
import autist.database.Entity;

import autist.application.handler.HandlerException;
import autist.application.Model;
import autist.application.model.DefaultListSearchModel;
import autist.application.parameter.ListItemParameters;

@SuppressWarnings("unused")
public abstract class DefaultListSearchAction<
		ContextType extends GenericActionContext<ContextType>,
		EntityType extends Entity,
		EntityMapperType extends EntityMapper<EntityType>,
		FieldType extends Enum<FieldType>,
		ParametersType extends ListItemParameters<ParametersType, FieldType>>
		extends GenericAction<
				DefaultListSearchAction<
						ContextType, EntityType, EntityMapperType, FieldType, ParametersType>,
				ContextType, EntityType, EntityMapperType, ParametersType>
{
	@Override
	@NotNull
	@Contract("null -> fail; !null -> _")
	public Optional<Model> invoke(
			final @NotNull ParametersType parameters)
			throws HandlerException
	{
		return Optional.of(invokeTransaction(session -> {
			return new DefaultListSearchModel<>(session, getMapperClass(), parameters);
		}));
	}
}
