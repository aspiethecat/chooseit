package autist.application.action;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import autist.database.EntityMapper;
import autist.database.Entity;

import autist.application.handler.HandlerException;
import autist.application.Model;
import autist.application.model.DefaultFetchModel;
import autist.application.parameter.EntityParameters;

@SuppressWarnings("unused")
public abstract class DefaultFetchAction<
		ContextType extends GenericActionContext<ContextType>,
		EntityType extends Entity,
		EntityMapperType extends EntityMapper<EntityType>,
		ParametersType extends EntityParameters<ParametersType>>
		extends GenericAction<
				DefaultFetchAction<ContextType, EntityType, EntityMapperType, ParametersType>,
				ContextType, EntityType, EntityMapperType, ParametersType>
{
	@Override
	@NotNull
	@Contract("null -> fail; !null -> _")
	public Optional<Model> invoke(
			final @NotNull ParametersType query)
			throws HandlerException
	{
		if (!query.id().isPresent())
			return Optional.empty();

		final DefaultFetchModel<EntityType, EntityMapperType> model =
				invokeTransaction(session -> {
					return new DefaultFetchModel<>(session, getMapperClass(), query.id().get());
				});

		return (model.getItem() != null) ? Optional.of(model) : Optional.empty();
	}
}
