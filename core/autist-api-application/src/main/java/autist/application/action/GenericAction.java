package autist.application.action;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import autist.commons.reflection.ReflectionUtil;
import autist.database.Entity;
import autist.database.EntityMapper;

import autist.application.parameter.Parameters;

@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class GenericAction<
		HandlerType extends GenericAction<
				HandlerType, ContextType, EntityType, EntityMapperType, ParametersType>,
		ContextType extends GenericActionContext<ContextType>,
		EntityType extends Entity,
		EntityMapperType extends EntityMapper<EntityType>,
		ParametersType extends Parameters<ParametersType>>
		extends PersistenceAction<HandlerType, ContextType, ParametersType>
{
	@NotNull
	private final Class<EntityMapperType> m_mapperClass;

	@SuppressWarnings("unchecked")
	public GenericAction() {
		m_mapperClass = ReflectionUtil.getGenericParameterType(getClass(), GenericAction.class, 2);
	}

	@NotNull
	@Contract(pure = true)
	protected final Class<EntityMapperType> getMapperClass() {
		return m_mapperClass;
	}
}
