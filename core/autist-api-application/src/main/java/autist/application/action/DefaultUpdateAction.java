package autist.application.action;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import org.apache.ibatis.session.SqlSession;

import autist.database.EntityMapper;
import autist.database.Entity;

import autist.application.handler.HandlerException;
import autist.application.Model;
import autist.application.model.AbstractUpdateModel;
import autist.application.parameter.EntityParameters;
import autist.application.view.JsonView;

@SuppressWarnings("unused")
public abstract class DefaultUpdateAction<
		ContextType extends GenericActionContext<ContextType>,
		EntityType extends Entity,
		EntityMapperType extends EntityMapper<EntityType>,
		ParametersType extends EntityParameters<ParametersType>>
		extends GenericAction<
				DefaultUpdateAction<ContextType, EntityType, EntityMapperType, ParametersType>,
				ContextType, EntityType, EntityMapperType, ParametersType>
{
	@Override
	@NotNull
	@Contract("null -> fail; !null -> _")
	public final Optional<Model> invoke(
			final @NotNull ParametersType query)
			throws HandlerException
	{
		if (!query.id().isPresent())
			return Optional.empty();

		return Optional.of(new JsonView(invokeTransaction(
				session -> {
					final AbstractUpdateModel<EntityType, EntityMapperType, ParametersType> model = createModel(session, query);

					model.setStatus(DefaultFormAction.validate(model, query));
					model.check();

					if (model.getStatus())
						model.update();

					return model;
				})));
	}

	@NotNull
	@Contract("null, _ -> fail; _, null -> fail; !null, !null -> _")
	protected abstract AbstractUpdateModel<EntityType, EntityMapperType, ParametersType> createModel(
			final @NotNull SqlSession session,
			final @NotNull ParametersType query);
}
