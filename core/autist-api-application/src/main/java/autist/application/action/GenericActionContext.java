package autist.application.action;

import org.jetbrains.annotations.NotNull;

import autist.application.handler.HandlerEnvironment;

@SuppressWarnings({"WeakerAccess", "unused"})
public class GenericActionContext<ContextType extends GenericActionContext<ContextType>>
		extends PersistenceActionContext<ContextType> {
	public GenericActionContext(final @NotNull HandlerEnvironment parent) {
		super(parent);
	}
}
