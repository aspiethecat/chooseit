package autist.application.action;

import org.jetbrains.annotations.NotNull;

import autist.application.handler.HandlerEnvironment;

public class DefaultActionContext extends AbstractActionContext<DefaultActionContext> {
	public DefaultActionContext(final @NotNull HandlerEnvironment parent) {
		super(parent);
	}
}
