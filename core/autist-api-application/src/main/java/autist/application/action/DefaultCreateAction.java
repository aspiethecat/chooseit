package autist.application.action;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import org.apache.ibatis.session.SqlSession;

import autist.database.Entity;
import autist.database.EntityMapper;

import autist.application.Model;
import autist.application.handler.HandlerException;
import autist.application.model.AbstractCreateModel;
import autist.application.parameter.EntityParameters;
import autist.application.view.JsonView;

@SuppressWarnings("unused")
public abstract class DefaultCreateAction<
		ContextType extends GenericActionContext<ContextType>,
		EntityType extends Entity,
		EntityMapperType extends EntityMapper<EntityType>,
		ParametersType extends EntityParameters<ParametersType>>
		extends GenericAction<
				DefaultCreateAction<ContextType, EntityType, EntityMapperType, ParametersType>,
				ContextType, EntityType, EntityMapperType, ParametersType>
{
	@Override
	@NotNull
	@Contract("null -> fail; !null -> _")
	public final Optional<Model> invoke(
			final @NotNull ParametersType parameters)
			throws HandlerException
	{
		final AbstractCreateModel<EntityType, EntityMapperType, ParametersType> data =
				invokeTransaction(session -> {
					final AbstractCreateModel<
							EntityType, EntityMapperType, ParametersType> model =
							createModel(session, parameters);

					model.setStatus(DefaultFormAction.validate(model, parameters));
					model.check();

					if (model.getStatus())
						model.create();

					return model;
				});

		return parameters.id().map(id -> new JsonView(data));
	}

	@NotNull
	@Contract("null, _ -> fail; _, null -> fail; !null, !null -> _")
	protected abstract AbstractCreateModel<EntityType, EntityMapperType, ParametersType> createModel(
			final @NotNull SqlSession session,
			final @NotNull ParametersType query);
}
