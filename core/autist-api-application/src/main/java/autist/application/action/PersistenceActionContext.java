package autist.application.action;

import org.jetbrains.annotations.NotNull;

import org.apache.ibatis.session.SqlSession;

import autist.application.handler.HandlerEnvironment;

@SuppressWarnings({"WeakerAccess", "unused"})
public class PersistenceActionContext<
		ContextType extends PersistenceActionContext<ContextType>>
		extends AbstractActionContext<ContextType>
{
	public PersistenceActionContext(final @NotNull HandlerEnvironment parent) {
		super(parent);
	}

	@NotNull
	SqlSession openSession() {
		throw new UnsupportedOperationException();
	}
}
