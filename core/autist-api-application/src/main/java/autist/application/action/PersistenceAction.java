package autist.application.action;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;

import autist.application.handler.HandlerException;
import autist.application.parameter.Parameters;

@SuppressWarnings("unused")
public abstract class PersistenceAction<
		HandlerType extends PersistenceAction<HandlerType, ContextType, ParametersType>,
		ContextType extends PersistenceActionContext<ContextType>,
		ParametersType extends Parameters<ParametersType>>
		extends AbstractAction<HandlerType, ContextType, ParametersType>
{
	@NotNull
	private SqlSession openSession() {
		return context().openSession();
	}

	protected final void invokeTransaction(
			final @NotNull Consumer<SqlSession> action)
			throws HandlerException
	{
		Objects.requireNonNull(action);

		try (SqlSession session = openSession()) {
			action.accept(session);
			session.commit();
		} catch (final PersistenceException e) {
			throw new HandlerException(e.getMessage(), e);
		}
	}

	protected final <ResultType> ResultType invokeTransaction(
			final @NotNull Function<SqlSession, ResultType> action)
			throws HandlerException
	{
		Objects.requireNonNull(action);

		final ResultType result;
		try (SqlSession session = openSession()) {
			result = action.apply(session);
			session.commit();
		} catch (final PersistenceException e) {
			throw new HandlerException(e.getMessage(), e);
		}

		return result;
	}
}
