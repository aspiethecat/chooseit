package autist.application;

public enum ErrorType {
	INTERNAL,
	BAD_REQUEST,
	FORBIDDEN,
	NOT_FOUND,
}
