package autist.application.view.form;

import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import autist.application.Request;
import autist.application.Response;
import autist.application.handler.HandlerContext;
import autist.application.parameter.Parameters;
import autist.application.view.AbstractView;
import autist.application.view.ViewException;

@SuppressWarnings("unused")
public class FormView extends AbstractView {
	private static final Map<Class<? extends Parameters<?>>, FormDescriptor> m_queryMap =
			new ConcurrentHashMap<>();

	private final Class<? extends Parameters<?>> m_queryClass;

	public FormView(final Class<? extends Parameters<?>> queryClass) {
		m_queryClass = Objects.requireNonNull(queryClass);
	}

	@Override
	public final void render(
			final @NotNull HandlerContext<?> context,
			final @NotNull Request request,
			final @NotNull Response response)
			throws ViewException
	{
		super.render(context, request, response);

		final FormDescriptor descriptor =
				m_queryMap.computeIfAbsent(m_queryClass, FormDescriptor::new);
	}
}
