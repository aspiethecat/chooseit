package autist.application.view;

import org.jetbrains.annotations.NotNull;

import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.type.TypeFactory;

import autist.application.Request;
import autist.application.Response;
import autist.application.handler.HandlerContext;

@SuppressWarnings("unused")
public class JsonView extends AbstractView {
	@NotNull
	private final ObjectMapper m_mapper = new ObjectMapper();

	@NotNull
	private final Object m_model;

	@NotNull
	private final ObjectWriter m_writer;

	public JsonView(final @NotNull Object model) {
		Objects.requireNonNull(model, "model == null");

		m_model = model;
		m_writer = m_mapper.writer();
	}

	public <T extends V, V> JsonView(
			final @NotNull Collection<T> model,
			final @NotNull Class<V> viewType)
	{
		Objects.requireNonNull(model, "model == null");
		Objects.requireNonNull(viewType, "viewType == null");

		m_model = model;
		m_writer = m_mapper.writerFor(
				m_mapper.getTypeFactory().constructCollectionType(Collection.class, viewType));
	}

	public <T extends V, V> JsonView(
			final @NotNull Map<?, T> model,
			final @NotNull Class<V> viewType)
	{
		Objects.requireNonNull(model, "model == null");
		Objects.requireNonNull(viewType, "viewType == null");

		m_model = model;

		final TypeFactory factory = m_mapper.getTypeFactory();
		m_writer = m_mapper.writerFor(factory.constructMapType(
				Map.class, TypeFactory.unknownType(), factory.constructType(viewType)));
	}

	@Override
	public final void render(
			final @NotNull HandlerContext context,
			final @NotNull Request request,
			final @NotNull Response response)
			throws ViewException
	{
		super.render(context, request, response);

		final String content;
		try {
			content = m_writer.writeValueAsString(m_model);
		} catch (final JsonProcessingException e) {
			throw new ViewException("Marshalling model to JSON failed", e);
		}

		response.setContentType("application/json; charset=UTF-8");
		response.setContent(content, StandardCharsets.UTF_8);
	}
}
