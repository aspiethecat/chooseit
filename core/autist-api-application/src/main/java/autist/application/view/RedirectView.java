package autist.application.view;

import autist.application.Request;
import autist.application.Response;
import autist.application.handler.HandlerContext;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

@SuppressWarnings("unused")
public class RedirectView extends AbstractView {
	@NotNull
	private final String m_location;

	public RedirectView(final @NotNull String location) {
		Objects.requireNonNull(location, "location == null");
		m_location = location;
	}

	@Override
	public final void render(
			final @NotNull HandlerContext context,
			final @NotNull Request request,
			final @NotNull Response response)
			throws ViewException
	{
		super.render(context, request, response);

		URI uri;

		try {
			uri = new URI(m_location);
			if (uri.getScheme() == null) {
				uri = new URI(
					"http", request.getServerName(),
					uri.getPath(), uri.getQuery(), uri.getFragment()
				);
			}
		} catch (URISyntaxException e) {
			throw new ViewException(
					String.format("Invalid location: %s", m_location),
					e);
		}

		response.setContent(uri);
	}
}
