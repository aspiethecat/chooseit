package autist.application.view;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Objects;

import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import autist.application.Request;
import autist.application.Response;
import autist.application.handler.HandlerContext;

@SuppressWarnings("unused")
public class TemplateView extends AbstractView {
	private static final String DEFAULT_CONTENT_TYPE = "text/html";

	/** Template path */
	@NotNull
	private final String m_path;

	/** Template data model */
	@Nullable
	private final Object m_model;

	public TemplateView(final @NotNull String path) {
		this(path, null);
	}

	public TemplateView(
			final @NotNull String path,
			final @Nullable Object model)
	{
		Objects.requireNonNull(path, "path == null");

		m_path = path;
		m_model = model;
	}

	@Override
	public final void render(
			final @NotNull HandlerContext<?> context,
			final @NotNull Request request,
			final @NotNull Response response)
			throws ViewException
	{
		super.render(context, request, response);

		final ByteArrayOutputStream output = new ByteArrayOutputStream();
		final OutputStreamWriter writer = new OutputStreamWriter(output);

		final Template template;
		try {
			template = context
					.require(TemplateProvider.class)
					.getTemplate(m_path)
					.orElseThrow(() -> new ViewException(
							String.format("Template not found: %s", m_path)));

			final Environment environment =
					template.createProcessingEnvironment(m_model, writer, null);

			final ObjectWrapper wrapper = environment.getObjectWrapper();
			environment.setGlobalVariable("actionContext", wrapper.wrap(context));

			environment.process();
		} catch (final TemplateException | IOException e) {
			throw new ViewException("Can't render template", e);
		}

		final Object contentType = template.getCustomAttribute("content_type");

		response.setContentType((contentType != null) ?
				String.format("%s; charset=%s", contentType.toString(), template.getEncoding()) :
				DEFAULT_CONTENT_TYPE);
		response.setContent(output.toByteArray());
	}
}
