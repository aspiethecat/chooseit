package autist.application.view;

import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import autist.application.util.Cookie;
import autist.application.Request;
import autist.application.Response;
import autist.application.handler.HandlerContext;

public abstract class AbstractView implements View
{
	private Set<Cookie> m_cookies = new HashSet<>();

	public void setCookie(final @NotNull Cookie cookie) {
		Objects.requireNonNull(cookie, "cookie == null");
		m_cookies.add(cookie);
	}

	@Override
	public void render(
			final @NotNull HandlerContext<?> context,
			final @NotNull Request request,
			final @NotNull Response response)
			throws ViewException
	{
		Objects.requireNonNull(context, "context == null");
		Objects.requireNonNull(request, "request == null");
		Objects.requireNonNull(response, "response == null");

		response.setCookies(m_cookies);
	}
}
