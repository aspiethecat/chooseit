package autist.application.view.form;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.metadata.BeanDescriptor;

public class FormDescriptor {
	private static final ValidatorFactory m_validatorFactory =
			Validation.buildDefaultValidatorFactory();

	public FormDescriptor(final Class<?> queryClass) {
		final Validator validator = m_validatorFactory.getValidator();
		final BeanDescriptor beanDescriptor = validator.getConstraintsForClass(queryClass);

		if (!beanDescriptor.isBeanConstrained())
			return;

		beanDescriptor.getConstrainedProperties().forEach((property) -> {
		});
	}
}
