package autist.application.view;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings({"WeakerAccess", "unused"})
public class ViewException extends Exception {
	private static final long serialVersionUID = 1L;

	public ViewException() { }

	public ViewException(final @NotNull String message) {
		super(message);
	}

	public ViewException(final @NotNull String message, @NotNull Throwable cause) {
		super(message, cause);
	}

	public ViewException(final @NotNull Throwable cause) {
		super(cause);
	}
}
