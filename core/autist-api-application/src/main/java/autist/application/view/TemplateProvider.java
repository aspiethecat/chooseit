package autist.application.view;

import freemarker.template.Template;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Locale;
import java.util.Optional;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface TemplateProvider {
	@NotNull
	Optional<Template> getTemplate(final @NotNull String name)
			throws IOException;

	@NotNull
	Optional<Template> getTemplate(
			final @NotNull String name,
			final @NotNull Locale locale)
			throws IOException;
}
