package autist.application.view;

import autist.application.Model;
import autist.application.Request;
import autist.application.Response;
import autist.application.handler.HandlerContext;
import org.jetbrains.annotations.NotNull;

public interface View extends Model {
	void render(
			final @NotNull HandlerContext<?> context,
			final @NotNull Request request,
			final @NotNull Response response)
			throws ViewException;
}
