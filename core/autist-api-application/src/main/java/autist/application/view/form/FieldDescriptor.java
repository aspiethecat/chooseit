package autist.application.view.form;

import java.beans.PropertyDescriptor;

public class FieldDescriptor {
	/**
	 * Field name.
	 */
	private String m_name;

	/**
	 * Field description.
	 */
	private String m_description;

	/**
	 * Property descriptor.
	 */
	private PropertyDescriptor m_descriptor;

	public String getName() {
		return m_name;
	}

	public void setName(final String name) {
		m_name = name;
	}

	public String getDescription() {
		return m_description;
	}

	public void setDescription(final String description) {
		m_description = description;
	}

	public PropertyDescriptor getDescriptor() {
		return m_descriptor;
	}

	public void setDescriptor(final PropertyDescriptor descriptor) {
		m_descriptor = descriptor;
	}
}
