package autist.application.view;

import autist.application.Request;
import autist.application.Response;
import autist.application.handler.HandlerContext;
import org.jetbrains.annotations.NotNull;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

@SuppressWarnings("unused")
public class TextView extends AbstractView {
	private final String m_content;

	public TextView(final String content) {
		m_content = Objects.requireNonNull(content);
	}

	@Override
	public final void render(
			final @NotNull HandlerContext context,
			final @NotNull Request request,
			final @NotNull Response response)
			throws ViewException
	{
		super.render(context, request, response);

		response.setContentType("text/plain; charset=UTF-8");
		response.setContent(m_content, StandardCharsets.UTF_8);
	}
}
