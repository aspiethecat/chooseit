package autist.application.routing;

import autist.commons.util.KeySupplier;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface RouteTag<
		TagType extends Enum<TagType> & RouteTag<TagType>>
		extends KeySupplier<String>
{
}
