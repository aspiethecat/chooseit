package autist.application.routing;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;

import java.net.URI;

import autist.configuration.ConfigurationException;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface RoutingMapFactory {
	@NotNull
	<
			MapType extends RoutingMap<MapType, NodeType, ContextType, KeyType, TagType>,
			NodeType extends RoutingNode<NodeType, ContextType, KeyType, TagType>,
			ContextType extends RoutingContext<ContextType, KeyType, TagType>,
			KeyType extends Enum<KeyType> & RouteKey<KeyType>,
			TagType extends Enum<TagType> & RouteTag<TagType>>
	MapType create(
			final @NotNull Class<KeyType> keyClass,
			final @NotNull Class<TagType> tagClass,
			final @NotNull NodeType root)
			throws ConfigurationException;

	@NotNull <
			MapType extends RoutingMap<MapType, NodeType, ContextType, KeyType, TagType>,
			NodeType extends RoutingNode<NodeType, ContextType, KeyType, TagType>,
			ContextType extends RoutingContext<ContextType, KeyType, TagType>,
			KeyType extends Enum<KeyType> & RouteKey<KeyType>,
			TagType extends Enum<TagType> & RouteTag<TagType>>
	Optional<MapType> load(
			final @NotNull Class<KeyType> keyClass,
			final @NotNull Class<TagType> tagClass,
			final @NotNull URI uri)
			throws ConfigurationException;

	static Optional<RoutingMapFactory> getInstance()
			throws ServiceConfigurationError
	{
		return ServiceLoader.load(RoutingMapFactory.class).findFirst();
	}
}
