package autist.application.routing;

import autist.commons.util.KeySupplier;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface RouteKey<KeyType extends Enum<KeyType> & RouteKey<KeyType>>
		extends KeySupplier<String>
{
}
