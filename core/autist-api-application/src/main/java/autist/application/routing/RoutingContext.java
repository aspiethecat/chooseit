package autist.application.routing;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface RoutingContext<
		ContextType extends RoutingContext<ContextType, KeyType, TagType>,
		KeyType extends Enum<KeyType> & RouteKey<KeyType>,
		TagType extends Enum<TagType> & RouteTag<TagType>>
{
	@NotNull
	@Contract(pure = true)
	Class<KeyType> keyClass();

	@NotNull
	@Contract(pure = true)
	Optional<KeyType> resolveKey(final @NotNull String key);

	@NotNull
	@Contract(pure = true)
	Class<TagType> tagClass();

	@NotNull
	@Contract(pure = true)
	Optional<TagType> resolveTag(final @NotNull String tag);
}
