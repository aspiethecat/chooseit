package autist.application.routing;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import autist.commons.net.URN;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface RoutingNode<
		NodeType extends RoutingNode<NodeType, ContextType, KeyType, TagType>,
		ContextType extends RoutingContext<ContextType, KeyType, TagType>,
		KeyType extends Enum<KeyType> & RouteKey<KeyType>,
		TagType extends Enum<TagType> & RouteTag<TagType>>
		extends Iterable<NodeType>
{
	@NotNull
	@Contract(pure = true)
	ContextType context();

	@NotNull
	@Contract(pure = true)
	Optional<NodeType> parent();

	@NotNull
	@Contract(pure = true)
	String name();

	@NotNull
	@Contract(pure = true)
	URN path();

	@NotNull
	@Contract(pure = true)
	Optional<Pattern> pattern();

	@NotNull
	@Contract(pure = true)
	List<RouteParameter> parameters();

	@NotNull
	@Contract(pure = true)
	Optional<KeyType> key();

	@NotNull
	@Contract(pure = true)
	List<TagType> tags();

	@NotNull
	@Contract(pure = true)
	Optional<URN> target();

	@NotNull
	@Contract(pure = true)
	Optional<Class<?>> action();

	@NotNull
	@Contract(pure = true)
	Optional<URN> payload();

	@NotNull
	@Contract(pure = true)
	List<Class<?>> filters();

	@NotNull
	@Contract(pure = true)
	Optional<NodeType> findChild(final @NotNull String key);
}
