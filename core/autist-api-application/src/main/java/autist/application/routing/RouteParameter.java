package autist.application.routing;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface RouteParameter {
	@NotNull
	@Contract(pure = true)
	String name();

	@Contract(pure = true)
	int matchIndex();

	@NotNull
	@Contract(pure = true)
	Optional<String> defaultValue();
}
