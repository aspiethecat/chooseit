package autist.application.routing;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Optional;

import autist.commons.net.URN;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface RoutingMap<
		MapType extends RoutingMap<MapType, NodeType, ContextType, KeyType, TagType>,
		NodeType extends RoutingNode<NodeType, ContextType, KeyType, TagType>,
		ContextType extends RoutingContext<ContextType, KeyType, TagType>,
		KeyType extends Enum<KeyType> & RouteKey<KeyType>,
		TagType extends Enum<TagType> & RouteTag<TagType>>
		extends Iterable<NodeType>
{
	@NotNull
	@Contract(pure = true)
	ContextType context();

	@NotNull
	@Contract(pure = true)
	NodeType root();

	@NotNull
	@Contract(pure = true)
	Optional<NodeType> find(final @NotNull KeyType key);

	@NotNull
	@Contract(pure = true)
	Optional<NodeType> find(final @NotNull URN urn);

	@NotNull
	@Contract(pure = true)
	Collection<NodeType> findAll(final @NotNull TagType tag);
}
