package autist.application.model;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import org.apache.ibatis.session.SqlSession;

import autist.application.parameter.EntityParameters;
import autist.database.EntityMapper;
import autist.database.Entity;

@SuppressWarnings("unused")
public abstract class AbstractUpdateModel<
		EntityType extends Entity,
		MapperType extends EntityMapper<EntityType>,
		ParametersType extends EntityParameters<ParametersType>>
		extends AbstractStoreModel<EntityType, MapperType, ParametersType>
{
	public AbstractUpdateModel(
			final @NotNull SqlSession session,
			final @NotNull Class<MapperType> mapperClass,
			final @NotNull ParametersType query)
	{
		super(
				Objects.requireNonNull(session),
				Objects.requireNonNull(mapperClass),
				Objects.requireNonNull(query));
	}

	public final void update() {
		if (!isChecked())
			throw new IllegalStateException("Updating unchecked model not allowed");
		if (!getStatus())
			throw new IllegalStateException("Updating invalid model not allowed");

		update(getMapper(), getQuery());
	}

	protected abstract boolean update(
			final @NotNull MapperType mapper,
			final @NotNull ParametersType query);
}
