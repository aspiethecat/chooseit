package autist.application.model;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import org.apache.ibatis.session.SqlSession;

import autist.database.EntityMapper;
import autist.database.Entity;
import autist.application.parameter.EntityParameters;

@SuppressWarnings("unused")
public abstract class AbstractStoreModel<
		EntityType extends Entity,
		MapperType extends EntityMapper<EntityType>,
		ParametersType extends EntityParameters<ParametersType>>
		extends AbstractFormModel
{
	private final MapperType m_mapper;
	private final ParametersType m_query;

	private boolean m_checked = false;

	public AbstractStoreModel(
			final @NotNull SqlSession session,
			final @NotNull Class<MapperType> mapperClass,
			final @NotNull ParametersType query)
	{
		super(Objects.requireNonNull(session));

		m_mapper = session.getMapper(Objects.requireNonNull(mapperClass));
		m_query  = Objects.requireNonNull(query);
	}

	@Contract(pure = true)
	protected final MapperType getMapper() {
		return m_mapper;
	}

	protected final <X> X getMapper(@NotNull Class<X> mapperClass) {
		Objects.requireNonNull(mapperClass);
		return getSession().getMapper(mapperClass);
	}

	@Contract(pure = true)
	protected final ParametersType getQuery() {
		return m_query;
	}

	@Contract(pure = true)
	public final boolean isChecked() {
		return m_checked;
	}

	public final boolean check() {
		setStatus(getStatus() & checkQuery(m_mapper, m_query));
		m_checked = true;

		return getStatus();
	}

	protected boolean checkQuery(
			final @NotNull MapperType mapper,
			final @NotNull ParametersType query)
	{
		return true;
	}

	protected final boolean checkExists(
			final @NotNull String parameter,
			final @NotNull Class<? extends EntityMapper<?>> mapperClass,
			@Nullable final Long id)
	{
		Objects.requireNonNull(parameter);
		Objects.requireNonNull(mapperClass);

		if (id == null)
			return true;

		if (!getMapper(mapperClass).isExists(id)) {
			addError(parameter, getResourceString(String.format("%s.notFound", parameter)));
			return false;
		}

		return true;
	}
}
