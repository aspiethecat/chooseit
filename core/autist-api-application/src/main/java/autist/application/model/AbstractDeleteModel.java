package autist.application.model;

import autist.database.EntityMapper;
import autist.database.Entity;
import autist.application.Model;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

@SuppressWarnings("unused")
public abstract class AbstractDeleteModel<
		EntityType extends Entity,
		MapperType extends EntityMapper<EntityType>>
		implements Model
{
	private final boolean m_status;

	@Contract("null, _, _ -> fail; _, null, _ -> fail; _, _, null -> fail")
	public AbstractDeleteModel(
			final @NotNull SqlSession session,
			final @NotNull Class<MapperType> mapperClass,
			final @NotNull Long id)
	{
		Objects.requireNonNull(session);
		Objects.requireNonNull(mapperClass);
		Objects.requireNonNull(id);

		final MapperType mapper = session.getMapper(mapperClass);

		if (!mapper.isExists(id))
			m_status = false;
		else
			m_status = delete(mapper, id);
	}

	@Contract(pure = true)
	public final boolean getStatus() {
		return m_status;
	}

	@Contract("null, _ -> fail; _, null -> fail; !null, !null -> _")
	protected abstract boolean delete(
			final @NotNull MapperType mapper,
			final @NotNull Long id);
}
