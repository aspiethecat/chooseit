package autist.application.model;

import org.jetbrains.annotations.NotNull;

import org.apache.ibatis.session.SqlSession;

import autist.database.TreeEntityMapper;
import autist.database.TreeEntity;
import autist.application.parameter.TreeItemParameters;

@SuppressWarnings("unused")
public class DefaultTreeSearchModel<
		EntityType extends TreeEntity<EntityType, EntityType>,
		EntityMapperType extends TreeEntityMapper<EntityType, EntityType>,
		QueryType extends TreeItemParameters<QueryType, FieldType>,
		FieldType extends Enum<FieldType>>
		extends DefaultListModel<EntityType>
{
	public DefaultTreeSearchModel(
			final @NotNull SqlSession session,
			final @NotNull Class<EntityMapperType> mapperClass,
			final @NotNull QueryType query)
	{
		final EntityMapperType mapper = session.getMapper(mapperClass);

		setItems(query.parentId()
				.map(mapper::listChildren)
				.orElseGet(mapper::listRoots));
	}
}
