package autist.application.model;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;

import org.apache.ibatis.session.SqlSession;

import autist.application.Model;
import autist.commons.resource.ResourceLoader;

@SuppressWarnings("unused")
public class AbstractFormModel implements Model {
	@NotNull
	private final ResourceBundle m_resources = ResourceLoader.getResourceBundle(getClass());

	@NotNull
	private final Map<String, String> m_errors = new HashMap<>();

	@NotNull
	private final SqlSession m_session;

	private boolean m_status = true;

	@Nullable
	private String m_statusMessage = null;

	@Contract("null -> fail")
	protected AbstractFormModel(final @NotNull SqlSession session) {
		m_session = Objects.requireNonNull(session);
	}

	@NotNull
	@Contract(pure = true)
	protected final SqlSession getSession() {
		return m_session;
	}

	@NotNull
	@Contract("null -> fail; !null -> _")
	public final String getResourceString(final @NotNull String key) {
		return m_resources.getString(Objects.requireNonNull(key));
	}

	@Contract(pure = true)
	public final boolean getStatus() {
		return m_status;
	}

	public final void setStatus(final boolean status, @Nullable final String message) {
		setStatus(status);
		setStatusMessage(message);
	}

	public final void setStatus(final boolean status) {
		if (!m_status && status)
			throw new IllegalStateException("Model should be valid");

		m_status = status;
	}

	@Nullable
	@Contract(pure = true)
	public final String getStatusMessage() {
		return m_statusMessage;
	}

	public final void setStatusMessage(@Nullable final String statusMessage) {
		m_statusMessage = statusMessage;
	}

	@NotNull
	@Contract(pure = true)
	public final Map<String, String> getErrors() {
		return Collections.unmodifiableMap(m_errors);
	}

	public final void addError(final String property, final String message) {
		m_errors.put(
				Objects.requireNonNull(property),
				Objects.requireNonNull(message));
	}
}
