package autist.application.model;

import autist.application.parameter.CoreEntityParameters;
import autist.database.CoreEntityMapper;
import autist.database.CoreEntity;
import org.apache.ibatis.session.SqlSession;

import java.util.Objects;

@SuppressWarnings("unused")
public abstract class CoreEntityStoreModel<
		EntityType extends CoreEntity<?>,
		MapperType extends CoreEntityMapper<EntityType>,
		ParametersType extends CoreEntityParameters<ParametersType>>
		extends NamedEntityStoreModel<EntityType, MapperType, ParametersType>
{
	public CoreEntityStoreModel(
			final SqlSession session,
			final Class<MapperType> mapperClass,
			final ParametersType query)
	{
		super(session, mapperClass, query);
	}

	protected static <
			EntityType extends CoreEntity<?>,
			Q extends CoreEntityParameters<Q>>
	EntityType buildEntity(
			final EntityType entity,
			final Q query)
	{
		Objects.requireNonNull(entity);
		Objects.requireNonNull(query);

		DictionaryEntityStoreModel.buildEntity(entity, query);

		return entity;
	}
}
