package autist.application.model;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import org.apache.ibatis.session.SqlSession;

import autist.database.EntityMapper;
import autist.database.Entity;
import autist.application.parameter.EntityParameters;

@SuppressWarnings("unused")
public abstract class AbstractCreateModel<
		EntityType extends Entity,
		MapperType extends EntityMapper<EntityType>,
		ParametersType extends EntityParameters<ParametersType>>
		extends AbstractStoreModel<EntityType, MapperType, ParametersType>
{
	@Contract("null, _, _ -> fail; _, null, _ -> fail; _, _, null -> fail")
	public AbstractCreateModel(
			final @NotNull SqlSession session,
			final @NotNull Class<MapperType> mapperClass,
			final @NotNull ParametersType query)
	{
		super(
				Objects.requireNonNull(session),
				Objects.requireNonNull(mapperClass),
				Objects.requireNonNull(query));
	}

	public final void create() {
		if (!isChecked())
			throw new IllegalStateException("Creating unchecked model not allowed");
		if (!getStatus())
			throw new IllegalStateException("Creating invalid model not allowed");

		create(getMapper(), getQuery());
	}

	@Contract("null, _ -> fail; _, null -> fail; !null, !null -> _")
	protected abstract boolean create(
			final @NotNull MapperType mapper,
			final @NotNull ParametersType query);
}
