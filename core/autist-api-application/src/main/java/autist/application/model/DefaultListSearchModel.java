package autist.application.model;

import org.jetbrains.annotations.NotNull;
import org.apache.ibatis.session.SqlSession;

import autist.database.EntityMapper;
import autist.database.Entity;
import autist.application.parameter.ListItemParameters;

@SuppressWarnings("unused")
public class DefaultListSearchModel<
		EntityType extends Entity,
		EntityMapperType extends EntityMapper<EntityType>,
		QueryType extends ListItemParameters<QueryType, FieldType>,
		FieldType extends Enum<FieldType>>
		extends DefaultListModel<EntityType>
{
	public DefaultListSearchModel(
			final @NotNull SqlSession session,
			final @NotNull Class<EntityMapperType> mapperClass,
			final @NotNull QueryType query)
	{
		setItems(session.getMapper(mapperClass).listAll());
	}
}
