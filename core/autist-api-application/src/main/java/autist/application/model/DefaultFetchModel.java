package autist.application.model;

import autist.database.EntityMapper;
import autist.database.Entity;
import org.apache.ibatis.session.SqlSession;

import java.util.Objects;

@SuppressWarnings("unused")
public class DefaultFetchModel<
		EntityType extends Entity,
		EntityMapperType extends EntityMapper<EntityType>>
		extends DefaultItemModel<EntityType>
{
	public DefaultFetchModel(
			final SqlSession session,
			final Class<EntityMapperType> mapperClass,
			final Long id)
	{
		Objects.requireNonNull(session);
		Objects.requireNonNull(mapperClass);
		Objects.requireNonNull(id);

		setItem(session.getMapper(mapperClass).fetchById(id));
	}
}
