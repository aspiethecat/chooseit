package autist.application.model;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface DataSourceProvider {
	@NotNull
	SqlSession openSession();

	@NotNull
	SqlSession openSession(final @NotNull String dataSource);
}
