package autist.application.model;

import autist.database.DictionaryEntityMapper;
import autist.database.DictionaryEntity;
import autist.application.parameter.DictionaryEntityParameters;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

@SuppressWarnings("unused")
public abstract class DictionaryEntityStoreModel<
		T extends DictionaryEntity,
		M extends DictionaryEntityMapper<T>,
		Q extends DictionaryEntityParameters<Q>>
		extends NamedEntityStoreModel<T, M, Q>
{
	@Contract("null, _, _ -> fail; _, null, _ -> fail; _, _, null -> fail")
	public DictionaryEntityStoreModel(
			final @NotNull SqlSession session,
			final @NotNull Class<M> mapperClass,
			final @NotNull Q query)
	{
		super(session, mapperClass, query);
	}

	@Contract("null, _ -> fail; _, null -> fail; !null, !null -> param1")
	protected static <
			T extends DictionaryEntity,
			Q extends DictionaryEntityParameters<Q>>
	T buildEntity(
			final @NotNull T entity,
			final @NotNull Q query)
	{
		Objects.requireNonNull(entity);
		Objects.requireNonNull(query);

		NamedEntityStoreModel.buildEntity(entity, query);
		entity.setDescription(query.description().orElse(null));

		return entity;
	}
}
