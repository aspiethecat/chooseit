package autist.application.model;

import org.jetbrains.annotations.NotNull;
import autist.database.EntityMapper;
import autist.database.Entity;
import org.apache.ibatis.session.SqlSession;

@SuppressWarnings("unused")
public class DefaultDeleteModel<
		EntityType extends Entity,
		MapperType extends EntityMapper<EntityType>>
		extends AbstractDeleteModel<EntityType, MapperType>
{
	public DefaultDeleteModel(
			final @NotNull SqlSession session,
			final @NotNull Class<MapperType> mapperClass,
			final @NotNull Long id)
	{
		super(session, mapperClass, id);
	}

	@Override
	protected boolean delete(
			final @NotNull MapperType mapper,
			final @NotNull Long id) {
		mapper.deleteById(id);
		return true;
	}
}
