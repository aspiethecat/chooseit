package autist.application.model;

import autist.application.Model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@SuppressWarnings("unused")
public class DefaultJsonModel implements Model {
	private final boolean m_status;

	private final String m_statusMessage;

	private final Map<String, String> m_errors = new HashMap<>();

	public DefaultJsonModel(final boolean status) {
		m_status = status;
		m_statusMessage = null;
	}

	public DefaultJsonModel(
			final boolean status,
			final String statusMessage)
	{
		m_status = status;
		m_statusMessage = Objects.requireNonNull(statusMessage);
	}

	public DefaultJsonModel(
			final boolean status,
			final String statusMessage,
			final Map<String, String> errors) {
		m_status = status;
		m_statusMessage = Objects.requireNonNull(statusMessage);

		m_errors.putAll(Objects.requireNonNull(errors));
	}

	public final void addError(final String key, final String message) {
		m_errors.put(key, message);
	}

	public final boolean getStatus() {
		return m_status;
	}

	public final String getStatusMessage() {
		return m_statusMessage;
	}

	public final Map<String, String> getErrors() {
		return Collections.unmodifiableMap(m_errors);
	}
}
