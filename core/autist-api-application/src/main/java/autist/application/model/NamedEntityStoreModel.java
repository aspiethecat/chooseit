package autist.application.model;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import org.apache.ibatis.session.SqlSession;

import autist.database.NamedEntityMapper;
import autist.database.NamedEntity;
import autist.application.parameter.NamedEntityParameters;

@SuppressWarnings("unused")
public abstract class NamedEntityStoreModel<
		T extends NamedEntity,
		M extends NamedEntityMapper<T>,
		Q extends NamedEntityParameters<Q>>
		extends DefaultStoreModel<T, M, Q>
{
	public NamedEntityStoreModel(
			final @NotNull SqlSession session,
			final @NotNull Class<M> mapperClass,
			final @NotNull Q query)
	{
		super(session, mapperClass, query);
	}

	@Override
	@Contract("null, _ -> fail; _, null -> fail; !null, !null -> _")
	protected boolean checkEntity(
			final @NotNull M mapper,
			final @NotNull T data)
	{
		return checkEntity(mapper, data, true);
	}

	@Contract("null, _, _ -> fail; _, null, _ -> fail; !null, !null, _ -> _")
	protected boolean checkEntity(
			final @NotNull M mapper,
			final @NotNull T entity,
			final boolean checkUniqueName)
	{
		Objects.requireNonNull(mapper);
		Objects.requireNonNull(entity);

		if (entity.getName() != null) {
			if (checkUniqueName && !mapper.isNameUnique(entity)) {
				final String error = getResourceString("name.notUnique");

				setStatusMessage(error);
				addError("name", error);

				return false;
			}
		}

		return true;
	}

	@Contract("null, _ -> fail; _, null -> fail; !null, !null -> param1")
	protected static <
			T extends NamedEntity,
			Q extends NamedEntityParameters<Q>>
	T buildEntity(
			final @NotNull T entity,
			final @NotNull Q query)
	{
		Objects.requireNonNull(entity);
		Objects.requireNonNull(query);

		entity.setId(query.id().orElse(null));
		entity.setName(query.name().orElse(null));

		return entity;
	}
}
