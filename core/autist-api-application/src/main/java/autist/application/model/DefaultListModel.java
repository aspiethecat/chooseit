package autist.application.model;

import autist.application.Model;

import java.util.Collections;
import java.util.List;

@SuppressWarnings("unused")
public abstract class DefaultListModel<T> implements Model {
	private List<T> m_items = Collections.emptyList();

	public final List<T> getItems() {
		return m_items;
	}

	protected final void setItems(final List<T> items) {
		m_items = items;
	}
}
