package autist.application.model;

import java.util.Objects;

import org.apache.ibatis.session.SqlSession;

import autist.database.NamedEntityMapper;
import autist.database.FilterResult;
import autist.database.NamedEntity;

import autist.application.parameter.FilterParameters;

@SuppressWarnings("unused")
public class DefaultFilterModel<
		EntityType extends NamedEntity,
		MapperType extends NamedEntityMapper<EntityType>,
		ParametersType extends FilterParameters<?>>
		extends DefaultListModel<FilterResult>
{
	private static final int DEFAULT_LIMIT = 5;

	public DefaultFilterModel(
			final SqlSession session,
			final Class<MapperType> mapperClass,
			final ParametersType parameters)
	{
		Objects.requireNonNull(session);
		Objects.requireNonNull(mapperClass);
		Objects.requireNonNull(parameters);

		setItems(session
				.getMapper(mapperClass)
				.filter(parameters.filter().orElse(null), DEFAULT_LIMIT));
	}
}
