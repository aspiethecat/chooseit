package autist.application.model;

import org.jetbrains.annotations.NotNull;

import autist.database.EntityMapper;
import autist.database.Entity;
import autist.application.parameter.EntityParameters;
import org.apache.ibatis.session.SqlSession;

import java.util.Objects;

@SuppressWarnings("unused")
public abstract class DefaultStoreModel<
		T extends Entity,
		M extends EntityMapper<T>,
		Q extends EntityParameters<Q>>
		extends AbstractStoreModel<T, M, Q>
{
	public DefaultStoreModel(
			final @NotNull SqlSession session,
			final @NotNull Class<M> mapperClass,
			final @NotNull Q query)
	{
		super(
				Objects.requireNonNull(session),
				Objects.requireNonNull(mapperClass),
				Objects.requireNonNull(query));
	}

	public final void store() {
		if (!isChecked())
			throw new IllegalStateException("Storing unchecked model not allowed");
		if (!getStatus())
			throw new IllegalStateException("Storing invalid model not allowed");

		final M mapper = getMapper();
		final Q query  = getQuery();

		final T data = buildEntity(query);
		if (!checkEntity(mapper, data)) {
			setStatus(false);
			return;
		}

		if (query.id().isPresent())
			setStatus(update(mapper, data));
		else
			setStatus(create(mapper, data));
	}

	protected abstract T buildEntity(@NotNull Q query);

	protected boolean checkEntity(
			final @NotNull M mapper,
			final @NotNull T entity)
	{
		return true;
	}

	protected boolean update(
			final @NotNull M mapper,
			final @NotNull T data)
	{
		Objects.requireNonNull(mapper);
		Objects.requireNonNull(data);

		mapper.update(data);

		return true;
	}

	protected boolean create(
			final @NotNull M mapper,
			final @NotNull T data)
	{
		Objects.requireNonNull(mapper);
		Objects.requireNonNull(data);

		mapper.create(data);

		return true;
	}
}
