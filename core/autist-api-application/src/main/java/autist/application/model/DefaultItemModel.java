package autist.application.model;

import autist.application.Model;
import org.jetbrains.annotations.Contract;

@SuppressWarnings("unused")
public abstract class DefaultItemModel<T> implements Model {
	private T m_item = null;

	@Contract(pure = true)
	public final T getItem() {
		return m_item;
	}

	protected final void setItem(final T item) {
		m_item = item;
	}
}
