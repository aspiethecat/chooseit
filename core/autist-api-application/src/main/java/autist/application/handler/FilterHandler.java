package autist.application.handler;

import autist.application.parameter.Parameters;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface FilterHandler<
		HandlerType extends FilterHandler<HandlerType, ContextType, ParametersType>,
		ContextType extends HandlerContext<ContextType>,
		ParametersType extends Parameters<ParametersType>>
		extends Handler<HandlerType, ContextType, ParametersType>
{
}
