package autist.application.handler;

import autist.commons.context.ContextNode;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface HandlerContext<ContextType extends HandlerContext<ContextType>>
		extends ContextNode<ContextType, HandlerEnvironment>
{
}
