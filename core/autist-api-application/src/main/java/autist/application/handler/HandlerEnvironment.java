package autist.application.handler;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import autist.commons.context.ContextRoot;
import autist.application.EnvironmentType;
import autist.application.routing.RoutingMap;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface HandlerEnvironment extends ContextRoot<HandlerEnvironment> {
	@NotNull
	@Contract(pure = true)
	EnvironmentType type();

	@NotNull
	@Contract(pure = true)
	RoutingMap routingMap();
}
