package autist.application.handler;

@SuppressWarnings("unused")
public class HandlerException extends Exception {
	public HandlerException() { }

	public HandlerException(final String message) {
		super(message);
	}

	public HandlerException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public HandlerException(final Throwable cause) {
		super(cause);
	}
}
