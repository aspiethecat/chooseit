package autist.application.handler;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import autist.commons.context.ContextAware;

import autist.application.Model;
import autist.application.parameter.Parameters;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface Handler<
		HandlerType extends Handler<HandlerType, ContextType, ParametersType>,
		ContextType extends HandlerContext<ContextType>,
		ParametersType extends Parameters<ParametersType>>
		extends ContextAware<ContextType>
{
	@NotNull
	@Contract(pure = true)
	ContextType context();

	@NotNull
	Optional<? extends Model> invoke(
			final @NotNull ParametersType parameters)
			throws HandlerException;
}
