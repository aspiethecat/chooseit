package autist.application.handler;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import autist.application.Request;
import autist.application.parameter.Parameters;
import autist.application.view.AbstractView;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface HandlerInvocation<
		ActionType extends ActionHandler<ActionType, ActionContextType, ParametersType>,
		ActionContextType extends HandlerContext<ActionContextType>,
		ParametersType extends Parameters<ParametersType>>
{
	@NotNull
	@Contract(pure = true)
	Class<ActionType> actionClass();

	@NotNull
	@Contract(pure = true)
	Class<ActionContextType> actionContextClass();


	@NotNull
	Optional<AbstractView> invoke(
			final @NotNull Request request)
			throws HandlerException;
}
