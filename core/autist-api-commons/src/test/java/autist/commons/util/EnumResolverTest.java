package autist.commons.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("autist.util.Bounds")
public class EnumResolverTest {
	private static final String UNDEFINED = "UNDEFINED";

	private enum Target implements KeySupplier<String> {
		FOO("FOO"),
		BAR("BAR");

		@NotNull
		private final String m_key;

		Target(final @NotNull String key) {
			m_key = key;
		}

		@NotNull
		@Contract(pure = true)
		public String key() {
			return m_key;
		}
	}

	@Test
	void test() {
		assertThrows(NullPointerException.class, () -> new EnumResolver<String, Target>(null));

		final var resolver = new EnumResolver<>(Target.class);

		assertEquals(resolver.target(), Target.class);

		assertThrows(NullPointerException.class, () -> resolver.resolve(null));
		for (final var value : Target.values()) {
			final var result = resolver.resolve(value.key());

			assertTrue(result.isPresent());
			assertEquals(result.get(), value);
		}

		final var emptyResult = resolver.resolve(UNDEFINED);
		assertFalse(emptyResult.isPresent());
	}
}
