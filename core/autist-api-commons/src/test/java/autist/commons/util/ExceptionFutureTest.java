package autist.commons.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("autist.util.ExceptionFuture")
class ExceptionFutureTest {
	@Test
	void test() {
		final var exception = new Exception();
		final var exceptionFuture = new DeferredException<>(Exception.class);

		assertDoesNotThrow(exceptionFuture::throwIfPresent);

		exceptionFuture.bind(exception);
		assertThrows(Exception.class, exceptionFuture::throwIfPresent);
	}
}
