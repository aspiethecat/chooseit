package autist.commons.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("autist.util.Bounds")
class BoundsTest {
	private static final Integer LOWER = -1;
	private static final Integer UPPER = 1;

	@Test
	void testNone() {
		final var bounds = Bounds.none();

		assertFalse(bounds.hasLowerBound());
		assertFalse(bounds.lowerBound().isPresent());

		assertFalse(bounds.hasUpperBound());
		assertFalse(bounds.upperBound().isPresent());
	}

	@Test
	void testRange() {
		assertThrows(NullPointerException.class, () -> Bounds.range(null, null));
		assertThrows(NullPointerException.class, () -> Bounds.range(LOWER, null));
		assertThrows(NullPointerException.class, () -> Bounds.range(null, UPPER));
		assertThrows(IllegalArgumentException.class, () -> Bounds.range(UPPER, LOWER));

		final var bounds = Bounds.range(LOWER, UPPER);

		assertTrue(bounds.hasLowerBound());
		assertTrue(bounds.lowerBound().isPresent());
		assertEquals(bounds.lowerBound().get(), LOWER);

		assertTrue(bounds.hasUpperBound());
		assertTrue(bounds.upperBound().isPresent());
		assertEquals(bounds.upperBound().get(), UPPER);
	}

	@Test
	void testMin() {
		assertThrows(NullPointerException.class, () -> Bounds.min(null));

		final var bounds = Bounds.min(LOWER);

		assertTrue(bounds.hasLowerBound());
		assertTrue(bounds.lowerBound().isPresent());
		assertEquals(bounds.lowerBound().get(), LOWER);

		assertFalse(bounds.hasUpperBound());
		assertFalse(bounds.upperBound().isPresent());
	}

	@Test
	void testMax() {
		assertThrows(NullPointerException.class, () -> Bounds.max(null));

		final var bounds = Bounds.max(UPPER);

		assertFalse(bounds.hasLowerBound());
		assertFalse(bounds.lowerBound().isPresent());

		assertTrue(bounds.hasUpperBound());
		assertTrue(bounds.upperBound().isPresent());
		assertEquals(bounds.upperBound().get(), UPPER);
	}
}
