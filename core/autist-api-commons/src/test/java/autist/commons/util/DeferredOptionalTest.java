package autist.commons.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.NoSuchElementException;

@DisplayName("autist.util.ValueFuture")
class DeferredOptionalTest {
	@Test
	void test() {
		final var value = DeferredOptional.class;
		final var valueFuture = new DeferredOptional<Class>();

		assertTrue(valueFuture.isAbsent());
		assertFalse(valueFuture.isPresent());
		assertThrows(NoSuchElementException.class, valueFuture::get);

		assertThrows(NullPointerException.class, () -> valueFuture.bind(null));

		valueFuture.bind(value);
		assertFalse(valueFuture.isAbsent());
		assertTrue(valueFuture.isPresent());
		assertEquals(valueFuture.get(), value);

		assertThrows(IllegalStateException.class, () -> valueFuture.bind(value));
	}
}
