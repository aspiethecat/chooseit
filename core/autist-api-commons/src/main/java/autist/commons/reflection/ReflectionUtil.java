package autist.commons.reflection;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Stack;

import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

@SuppressWarnings({"WeakerAccess", "unused"})
public class ReflectionUtil {
	public static boolean isSubclassOf(
		final @NotNull Class<?> clazz,
		final @Nullable Class<?> parent)
	{
		Objects.requireNonNull(clazz, "clazz == null");

		if (parent == null)
			return true;

		return !clazz.equals(parent) && parent.isAssignableFrom(clazz);
	}

	public static List<Class<?>> findInterfaces(
			final @NotNull Class<?> clazz)
	{
		return findInterfaces(clazz, null);
	}

	public static List<Class<?>> findInterfaces(
			final @NotNull Class<?> clazz,
			final @Nullable Class<?> parent)
	{
		Objects.requireNonNull(clazz, "clazz == null");

		if ((parent != null) && !parent.isInterface())
			throw new IllegalArgumentException("!parent.isInterface()");

		final List<Class<?>> interfaces = new ArrayList<>();
		final Stack<Iterator<Class<?>>> stack = new Stack<>();

		if (clazz.isInterface() && isSubclassOf(clazz, parent))
			interfaces.add(clazz);

		var position = Arrays.asList(clazz.getInterfaces()).iterator();
		while (position.hasNext()) {
			final var item = position.next();
			final var parents = item.getInterfaces();

			if (isSubclassOf(item, parent)) {
				interfaces.add(item);

				if (parents.length > 0) {
					stack.push(position);
					position = Arrays.asList(parents).iterator();
					continue;
				}
			}

			while (!stack.isEmpty()) {
				if (!position.hasNext())
					position = stack.pop();
			}
		}

		return interfaces;
	}

	@SuppressWarnings("unchecked")
	public static Class getGenericParameterType(
			final @NotNull Class actualClass,
			final @NotNull Class genericClass,
			final int parameterIndex)
	{
		Objects.requireNonNull(actualClass, "actualClass == null");
		Objects.requireNonNull(genericClass, "genericClass == null");

		if (!genericClass.isAssignableFrom(actualClass.getSuperclass())) {
			throw new IllegalArgumentException("Class " + genericClass.getName() + " is not a superclass of "
					+ actualClass.getName() + ".");
		}

		Class<?> clazz = actualClass;

		final Stack<ParameterizedType> genericClasses = new Stack<>();
		while (true) {
			final Type genericSuperclass = clazz.getGenericSuperclass();
			final boolean isParameterizedType = genericSuperclass instanceof ParameterizedType;

			if (isParameterizedType) {
				genericClasses.push((ParameterizedType) genericSuperclass);
			} else {
				genericClasses.clear();
			}

			final Type rawType = isParameterizedType ?
					((ParameterizedType) genericSuperclass).getRawType() :
					genericSuperclass;

			if (!rawType.equals(genericClass)) {
				clazz = clazz.getSuperclass();
			} else {
				break;
			}
		}

		Type result = genericClasses.pop().getActualTypeArguments()[parameterIndex];

		while ((result instanceof TypeVariable) && !genericClasses.empty()) {
			final int actualArgumentIndex = getParameterTypeDeclarationIndex((TypeVariable<?>) result);
			final ParameterizedType type = genericClasses.pop();
			result = type.getActualTypeArguments()[actualArgumentIndex];
		}

		if (result instanceof TypeVariable) {
			throw new IllegalStateException("Unable to resolve type variable " + result + "."
					+ " Try to replace instances of parametrized class with its non-parameterized subtype.");
		}

		if (result instanceof ParameterizedType) {
			result = ((ParameterizedType) result).getRawType();
		}

		if (result == null) {
			throw new IllegalStateException("Unable to determine actual parameter type for "
					+ actualClass.getName() + ".");
		}

		if (!(result instanceof Class)) {
			throw new IllegalStateException("Actual parameter type for " + actualClass.getName() + " is not a Class.");
		}

		return (Class<?>) result;
	}

	private static int getParameterTypeDeclarationIndex(
			final @NotNull TypeVariable<?> typeVariable)
	{
		final GenericDeclaration genericDeclaration = typeVariable.getGenericDeclaration();

		final TypeVariable<?>[] typeVariables = genericDeclaration.getTypeParameters();
		Integer actualArgumentIndex = null;
		for (int i = 0; i < typeVariables.length; i++) {
			if (typeVariables[i].equals(typeVariable)) {
				actualArgumentIndex = i;
				break;
			}
		}
		if (actualArgumentIndex != null) {
			return actualArgumentIndex;
		} else {
			throw new IllegalStateException("Argument " + typeVariable.toString() + " is not found in "
					+ genericDeclaration.toString() + ".");
		}
	}
}
