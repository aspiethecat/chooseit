package autist.commons.reflection;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings({"WeakerAccess", "unused"})
public class InvocationException extends RuntimeException {
	public InvocationException() {
		super();
	}

	public InvocationException(final @NotNull String message) {
		super(message);
	}

	public InvocationException(final @NotNull String message, @NotNull Throwable cause) {
		super(message, cause);
	}

	public InvocationException(@NotNull Throwable cause) {
		super(cause);
	}
}
