package autist.commons.reflection;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.util.Objects;

@SuppressWarnings({"WeakerAccess", "unused"})
public class InvocationUtil {
	public static boolean isCompatible(
			final @NotNull Executable executable,
			final Class<?>[] argumentTypes)
	{
		Objects.requireNonNull(executable, "executable == null");
		Objects.requireNonNull(argumentTypes, "argumentTypes == null");

		if (executable.getParameterCount() != argumentTypes.length)
			return false;

		Class<?>[] declaredTypes = executable.getParameterTypes();

		for (int i = 0; i < argumentTypes.length; i++) {
			if (Objects.isNull(argumentTypes[i]))
				continue;

			if (declaredTypes[i].isAssignableFrom(argumentTypes[i]))
				continue;

			return false;
		}

		return true;
	}

	@Contract(value = "_ -> new", pure = true)
	public static Class<?>[] getArgumentTypes(
			final @Nullable Object... arguments)
	{
		if (arguments == null)
			return new Class[] { null };

		final Class[] types = new Class[arguments.length];
		for (int i = 0; i < arguments.length; i++) {
			if (arguments[i] == null)
				continue;

			types[i] = arguments[i].getClass();
		}

		return types;
	}

	@NotNull
	public static String getConstructorString(
			final @NotNull Constructor<?> constructor)
	{
		Objects.requireNonNull(constructor, "constructor == null");
		return getConstructorString(constructor.getDeclaringClass(), constructor.getParameterTypes());
	}

	@NotNull
	public static String getConstructorString(
			final @NotNull Class<?> clazz,
			final Class<?>[] argumentTypes)
	{
		Objects.requireNonNull(clazz, "clazz == null");
		Objects.requireNonNull(argumentTypes, "argumentTypes == null");

		final StringBuilder builder = new StringBuilder();

		builder.append(clazz.getName());
		builder.append('(');
		for (int i = 0; i < argumentTypes.length; i++) {
			if (i > 0)
				builder.append(", ");

			if (Objects.nonNull(argumentTypes[i]))
				builder.append(argumentTypes[i].getName());
			else
				builder.append('?');
		}
		builder.append(')');

		return builder.toString();
	}
}
