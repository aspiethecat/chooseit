package autist.commons.reflection;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
import java.util.Optional;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class ClassUtil {
	public static <ClassType> Optional<Constructor<ClassType>> getConstructor(
			final @NotNull Class<ClassType> clazz,
			final @Nullable Class<?>... argumentTypes)
			throws SecurityException
	{
		Objects.requireNonNull(clazz, "clazz == null");

		Constructor<ClassType> constructor = null;
		try {
			constructor = clazz.getConstructor(
					Objects.nonNull(argumentTypes) ? argumentTypes : new Class[] { null });
		} catch (final NoSuchMethodException ignore) { }

		return Optional.ofNullable(constructor);
	}

	@SuppressWarnings("unchecked")
	public static <ClassType> Optional<Constructor<ClassType>> findCompatibleConstructor(
			final @NotNull Class<ClassType> clazz,
			final @Nullable Class<?>... argumentTypes)
			throws SecurityException
	{
		Objects.requireNonNull(clazz, "clazz == null");

		for (final Constructor<?> constructor : clazz.getDeclaredConstructors()) {
			final boolean compatible = InvocationUtil.isCompatible(
					constructor,
					Objects.nonNull(argumentTypes) ? argumentTypes : new Class<?>[] { null });

			if (compatible)
				return Optional.of((Constructor<ClassType>) constructor);
		}

		return Optional.empty();
	}

	public static <T> T createInstance(
			final @NotNull Constructor<T> constructor,
			final @Nullable Object... arguments)
			throws InvocationException, SecurityException
	{
		Objects.requireNonNull(constructor, "constructor == null");

		final T instance;
		try {
			instance = constructor.newInstance(arguments);
		} catch (final SecurityException e) {
			throw e;
		} catch (final Throwable e) {
			final String constructorString = InvocationUtil.getConstructorString(constructor);

			final String message;
			if (e instanceof InvocationTargetException)
				message = "An exception in constructor";
			else if (e instanceof IllegalArgumentException)
				message = "Illegal constructor arguments";
			else if (e instanceof IllegalAccessException)
				message = "Constructor is not accessible";
			else if (e instanceof InstantiationException)
				message = "Class cannot be instantiated";
			else if (e instanceof ExceptionInInitializerError)
				message = "Class initialization failed";
			else
				throw new AssertionError(e);

			throw new InvocationException(String.format("%s: %s", message, constructorString), e);
		}

		return instance;
	}

	public static <T> T createInstance(
			final @NotNull Class<T> clazz,
			final @Nullable Object... arguments)
			throws NoSuchMethodError, SecurityException
	{
		Objects.requireNonNull(clazz, "clazz == null");

		final Class<?>[] argumentTypes = InvocationUtil.getArgumentTypes(arguments);
		final Constructor<T> constructor = findCompatibleConstructor(clazz, argumentTypes)
				.orElseThrow(() ->
					new NoSuchMethodError(String.format(
							"Constructor not found: %s",
							InvocationUtil.getConstructorString(clazz, argumentTypes))));

		return createInstance(constructor);
	}
}
