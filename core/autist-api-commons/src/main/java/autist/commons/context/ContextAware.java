package autist.commons.context;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface ContextAware<ContextType extends Context<ContextType>> {
	@NotNull
	@Contract(pure = true)
	ContextType context();
}
