package autist.commons.context;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;

/**
 * Generic context interface.
 *
 * @param <ContextType> context type
 * @see AbstractContext
 * @see ContextRoot
 * @see ContextNode
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public interface Context<ContextType extends Context<ContextType>> {
	@Contract(pure = true)
	boolean contains(
			final @NotNull Class<?> key);

	@NotNull
	@Contract(pure = true)
	<ValueType> Optional<ValueType> get(
			final @NotNull Class<ValueType> key);

	<ValueType> ValueType require(
			final @NotNull Class<ValueType> key)
			throws NoSuchElementException;

	void put(final @NotNull Object value);

	<ValueType> void put(
			final @NotNull Class<ValueType> key,
			final @NotNull ValueType value);

	@NotNull
	<ValueType> Optional<ValueType> remove(
			final @NotNull Class<ValueType> key);

	void invoke(final @NotNull Runnable task)
			throws Exception;

	@Nullable
	<ResultType> ResultType invoke(
			final @NotNull Callable<ResultType> task)
			throws Exception;

	@NotNull
	static <ContextType extends Context<ContextType>>
	Optional<? extends ContextType> getInstance(
			final @NotNull Class<ContextType> contextType)
	{
		Objects.requireNonNull(contextType, "contextType == null");
		return AbstractContext.getInstance(contextType);
	}

	@NotNull
	static <ContextType extends Context<ContextType>>
	ContextType requireInstance(
			final @NotNull Class<ContextType> contextType)
			throws NoSuchElementException
	{
		Objects.requireNonNull(contextType, "contextType == null");

		return Context.getInstance(contextType).orElseThrow(
				() -> new NoSuchElementException("Required context missing: " + contextType.getName()));
	}
}
