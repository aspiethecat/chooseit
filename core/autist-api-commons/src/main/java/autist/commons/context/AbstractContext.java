package autist.commons.context;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.IdentityHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

@SuppressWarnings({"WeakerAccess", "unused"})
abstract class AbstractContext<ContextType extends Context<ContextType>>
		implements Context<ContextType>
{
	/**
	 * Thread-local active context map.
	 */
	@NotNull
	private static ThreadLocal<Context<?>> s_threadContext =
			ThreadLocal.withInitial(() -> null);

	/**
	 * Context properties map.
	 *
	 * @see #contains(Class)
	 * @see #get(Class)
	 * @see #put(Object)
	 * @see #put(Class, Object)
	 * @see #remove(Class)
	 */
	@NotNull
	private final Map<Class<?>, Object> m_propertyMap = new IdentityHashMap<>(0);

	/**
	 * Returns {@code true} if this context or contains an association for the specified key.
	 *
	 * @param key key whose presence is to be tested
	 * @return {@code true} if this context or any of its parents contains an association for the specified key
	 * @see #get(Class)
	 * @see #put(Object)
	 * @see #put(Class, Object)
	 * @see #remove(Class)
	 */
	@Override
	@Contract(pure = true)
	public boolean contains(final @NotNull Class<?> key) {
		return m_propertyMap.containsKey(key);
	}

	/**
	 * Returns the value to which the specified key is associated in this context.
	 *
	 * @param key the key whose associated value is to be returned
	 * @param <ValueType> value type type
	 * @return the value to which the specified key is mapped
	 * @see #contains(Class)
	 * @see #put(Object)
	 * @see #put(Class, Object)
	 * @see #remove(Class)
	 */
	@Override
	@NotNull
	@SuppressWarnings("unchecked")
	public <ValueType> Optional<ValueType> get(
			final @NotNull Class<ValueType> key)
	{
		Objects.requireNonNull(key, "key == null");
		return Optional.ofNullable((ValueType) m_propertyMap.get(key));
	}

	/**
	 * Returns the value to which the specified key is associated in this context or
	 * throws {@link NoSuchElementException} if it was not found.
	 *
	 * @param key the key whose associated value is to be returned
	 * @param <ValueType> value type type
	 * @return the value to which the specified key is mapped
	 * @throws NoSuchElementException if the key was not found
	 * @see #contains(Class)
	 * @see #put(Object)
	 * @see #put(Class, Object)
	 * @see #remove(Class)
	 */
	@Override
	@NotNull
	public final <ValueType> ValueType require(
			final @NotNull Class<ValueType> key)
			throws NoSuchElementException
	{
		Objects.requireNonNull(key, "key == null");

		return get(key).orElseThrow(() -> new NoSuchElementException(
				String.format("Required context item missing: %s", key.getName())));
	}

	/**
	 * Associates the specified value with its default key in this context.
	 * Equivalent to {@code put(value.getClass(), value)}.
	 *
	 * @param value value to be associated with its default key
	 * @see #contains(Class)
	 * @see #get(Class)
	 * @see #put(Class, Object)
	 * @see #remove(Class)
	 */
	@Override
	public final void put(final @NotNull Object value) {
		Objects.requireNonNull(value, "value == null");
		m_propertyMap.put(value.getClass(), value);
	}

	/**
	 * Associates the specified value with the specified key in this context. If this context already contains
	 * association with specified key, replaces it with new value. This call doesn't modify parent contexts or
	 * interact with them in any way, so it can be used to override their associations.
	 *
	 * @param key key with which the specified value is to be associated
	 * @param value value to be associated with the specified key
	 * @param <ValueType> value type
	 * @see #contains(Class)
	 * @see #get(Class)
	 * @see #put(Object)
	 * @see #remove(Class)
	 */
	@Override
	public final <ValueType> void put(
			final @NotNull Class<ValueType> key,
			final @NotNull ValueType value)
	{
		m_propertyMap.put(
				Objects.requireNonNull(key, "key == null"),
				Objects.requireNonNull(value, "value == null"));
	}

	/**
	 * Removes the mapping for a key from this context if it is present. This call doesn't modify parent contexts or
	 * interact with them in any way, so it can be used clear parent context value overrides.
	 *
	 * @param key key whose association is to be removed from the context
	 * @param <ValueType> value type
	 * @return the previous value associated with {@code key}
	 * @see #contains(Class)
	 * @see #get(Class)
	 * @see #put(Object)
	 * @see #put(Class, Object)
	 */
	@Override
	@NotNull
	@SuppressWarnings("unchecked")
	public final <ValueType> Optional<ValueType> remove(
			final @NotNull Class<ValueType> key)
	{
		Objects.requireNonNull(key, "key == null");
		return Optional.ofNullable((ValueType) m_propertyMap.remove(key));
	}

	/**
	 * Executes callable task within the specified context.
	 *
	 * @param task task to be called
	 * @throws Exception if unable to execute task
	 */
	public void invoke(final @NotNull Runnable task) throws Exception {
		Objects.requireNonNull(task, "task == null");
		invoke(Executors.callable(task));
	}

	/**
	 * Executes callable task within the specified context.
	 *
	 * @param task task to be called
	 * @param <ResultType> task result type
	 * @return task result value
	 * @throws Exception if unable to execute task
	 */
	public <ResultType> ResultType invoke(
			final @NotNull Callable<ResultType> task)
			throws Exception
	{
		Objects.requireNonNull(task, "task == null");

		final Context<?> savedContext = s_threadContext.get();
		try {
			return task.call();
		} finally {
			s_threadContext.set(savedContext);
		}
	}

	/**
	 * Returns thread-local context of specified type.
	 *
	 * @param contextType type of the context is to be returned
	 * @return the context of specified type
	 */
	@NotNull
	@Contract(pure = true)
	@SuppressWarnings("unchecked")
	protected static <
			InstanceType extends Context<InstanceType>>
	Optional<InstanceType> getInstance(
			final @NotNull Class<InstanceType> contextType)
	{
		Objects.requireNonNull(contextType, "contextType == null");

		final InstanceType context = (InstanceType) s_threadContext.get();
		if (context == null)
			return Optional.empty();

		if (!contextType.isAssignableFrom(context.getClass()))
			return Optional.empty();

		return Optional.of(contextType.cast(context));
	}
}
