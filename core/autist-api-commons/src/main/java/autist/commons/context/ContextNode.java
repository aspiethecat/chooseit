package autist.commons.context;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/**
 * Context node interface.
 *
 * @param <ContextType> context type
 * @param <ParentType> parent context type
 * @see Context
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public interface ContextNode<
		ContextType extends ContextNode<ContextType, ParentType>,
		ParentType extends Context<ParentType>>
		extends Context<ContextType>
{
	@NotNull
	@Contract(pure = true)
	ParentType parent();
}
