package autist.commons.context;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;

/**
 * Base class for generic context implementations.
 *
 * @param <ParentType> parent context type
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class AbstractContextNode<
		ContextType extends ContextNode<ContextType, ParentType>,
		ParentType extends Context<ParentType>>
		extends AbstractContext<ContextType>
		implements ContextNode<ContextType, ParentType>
{
	/**
	 * The parent of this context.
	 *
	 * @see #AbstractContextNode(Context)
	 * @see #parent()
	 */
	@NotNull
	private final ParentType m_parent;

	/**
	 * Creates new child context with the specified parent.
	 *
	 * @param parent the parent for this context
	 * @see #parent()
	 */
	protected AbstractContextNode(final @NotNull ParentType parent) {
		m_parent = Objects.requireNonNull(parent, "parent == null");
	}

	/**
	 * Returns the parent of this context.
	 *
	 * @return parent context
	 * @see #AbstractContextNode(Context)
	 */
	@Override
	@NotNull
	@Contract(pure = true)
	public final ParentType parent() {
		return m_parent;
	}

	/**
	 * Returns {@code true} if this context or any of its parents contains an association for the
	 * specified key.
	 *
	 * @param key key whose presence is to be tested
	 * @return {@code true} if this context or any of its parents contains an association for the specified key
	 * @see #get(Class)
	 * @see #put(Object)
	 * @see #put(Class, Object)
	 * @see #remove(Class)
	 */
	@Override
	@Contract(pure = true)
	public final boolean contains(final @NotNull Class<?> key) {
		Objects.requireNonNull(key, "key == null");
		return super.contains(key) || m_parent.contains(key);
	}

	/**
	 * Returns the value to which the specified key is associated in this context or any of its parents.
	 *
	 * @param key the key whose associated value is to be returned
	 * @param <ValueType> value type type
	 * @return the value to which the specified key is mapped
	 * @see #contains(Class)
	 * @see #put(Object)
	 * @see #put(Class, Object)
	 * @see #remove(Class)
	 */
	@Override
	@NotNull
	@SuppressWarnings("unchecked")
	public <ValueType> Optional<ValueType> get(
			final @NotNull Class<ValueType> key)
	{
		Objects.requireNonNull(key, "key == null");
		return super.get(key).or(() -> m_parent.get(key));
	}
}
