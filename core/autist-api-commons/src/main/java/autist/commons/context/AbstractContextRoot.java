package autist.commons.context;

/**
 * Base class for root context implementations.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class AbstractContextRoot<ContextType extends ContextRoot<ContextType>>
		extends AbstractContext<ContextType>
		implements ContextRoot<ContextType>
{ }
