package autist.commons.context;

/**
 * Root context interface.
 *
 * @param <ContextType> context type
 * @see Context
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public interface ContextRoot<ContextType extends ContextRoot<ContextType>>
		extends Context<ContextType>
{
}
