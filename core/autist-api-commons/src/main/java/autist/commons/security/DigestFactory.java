package autist.commons.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class DigestFactory {
	/**
	 * Default digest algorithm.
	 */
	public static final String DIGEST_ALGORITHM = "SHA-256";

	/**
	 * Secure random number generator instance.
	 */
	private static final SecureRandom s_secureRandom = new SecureRandom();

	public static byte[] generateSecureRandom(final int length) {
		if (length <= 0)
			throw new IllegalArgumentException("Invalid random identifier length");

		final byte data[] = new byte[length];
		s_secureRandom.nextBytes(data);

		return data;
	}

	public static MessageDigest getDigestInstance() {
		final MessageDigest messageDigest;

		try {
			messageDigest = MessageDigest.getInstance(DIGEST_ALGORITHM);
		} catch (final NoSuchAlgorithmException e) {
			throw new AssertionError(
					String.format("Digest algorithm not found: %s", DIGEST_ALGORITHM), e);
		}

		return messageDigest;
	}
}
