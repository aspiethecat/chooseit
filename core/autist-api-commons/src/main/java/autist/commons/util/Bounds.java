package autist.commons.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.Optional;

@SuppressWarnings({"WeakerAccess", "unused"})
public class Bounds<ValueType extends Comparable<ValueType>> {
	@Nullable
	private final ValueType m_lowerBound;

	@Nullable
	private final ValueType m_upperBound;

	protected Bounds(
			@Nullable final ValueType lowerBound,
			@Nullable final ValueType upperBound)
	{
		m_lowerBound = lowerBound;
		m_upperBound = upperBound;
	}

	@Contract(pure = true)
	public final boolean hasLowerBound() {
		return (m_lowerBound != null);
	}

	@NotNull
	@Contract(pure = true)
	public final Optional<ValueType> lowerBound() {
		return Optional.ofNullable(m_lowerBound);
	}

	@Contract(pure = true)
	public final boolean hasUpperBound() {
		return (m_upperBound != null);
	}

	@NotNull
	@Contract(pure = true)
	public final Optional<ValueType> upperBound() {
		return Optional.ofNullable(m_upperBound);
	}

	@NotNull
	@Contract(" -> new")
	public static <ValueType extends Comparable<ValueType>> Bounds<ValueType> none() {
		return new Bounds<>(null, null);
	}

	@NotNull
	@Contract("_, _ -> new")
	public static <ValueType extends Comparable<ValueType>> Bounds<ValueType> range(
			final @NotNull ValueType lowerBound,
			final @NotNull ValueType upperBound)
	{
		Objects.requireNonNull(lowerBound, "lowerBound == null");
		Objects.requireNonNull(upperBound, "upperBound == null");

		if (upperBound.compareTo(lowerBound) < 0)
			throw new IllegalArgumentException("upperBound < lowerBound");

		return new Bounds<>(lowerBound, upperBound);
	}

	@NotNull
	@Contract("_ -> new")
	public static <ValueType extends Comparable<ValueType>> Bounds<ValueType> min(
			final @NotNull ValueType value)
	{
		Objects.requireNonNull(value, "value == null");
		return new Bounds<>(value, null);
	}

	@NotNull
	@Contract("_ -> new")
	public static <ValueType extends Comparable<ValueType>> Bounds<ValueType> max(
			final @NotNull ValueType value)
	{
		Objects.requireNonNull(value, "value == null");
		return new Bounds<>(null, value);
	}
}
