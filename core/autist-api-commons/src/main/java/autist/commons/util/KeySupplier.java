package autist.commons.util;

import org.jetbrains.annotations.NotNull;

@FunctionalInterface
@SuppressWarnings({"WeakerAccess", "unused"})
public interface KeySupplier<KeyType> {
	@NotNull
	KeyType key();
}
