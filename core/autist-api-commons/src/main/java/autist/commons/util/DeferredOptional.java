package autist.commons.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

@SuppressWarnings({"WeakerAccess", "unused"})
public class DeferredOptional<ValueType> {
	private ValueType m_value = null;

	@Contract(pure = true)
	public final boolean isAbsent() {
		return Objects.isNull(m_value);
	}

	@Contract(pure = true)
	public final boolean isPresent() {
		return Objects.nonNull(m_value);
	}

	public final void bind(final @NotNull ValueType value)
			throws IllegalStateException
	{
		Objects.requireNonNull(value, "value == null");

		if (Objects.nonNull(m_value))
			throw new IllegalStateException("Value already present");

		m_value = value;
	}

	public final void bindNullable(final @Nullable ValueType value)
			throws IllegalStateException
	{
		if (Objects.nonNull(m_value))
			throw new IllegalStateException("Value already present");

		if (Objects.nonNull(value))
			m_value = value;
	}

	public final boolean bindIfAbsent(
			final @NotNull Supplier<ValueType> supplier)
	{
		Objects.requireNonNull(supplier, "supplier == null");

		if (isPresent())
			return true;

		final var value = supplier.get();
		if (Objects.isNull(value))
			return false;

		bind(value);

		return true;
	}

	@NotNull
	@Contract(pure = true)
	public final ValueType get()
			throws NoSuchElementException
	{
		if (isAbsent())
			throw new NoSuchElementException("No value present");

		return m_value;
	}

	@Contract(pure = true)
	public final void ifPresent(
			final @NotNull Consumer<ValueType> action)
	{
		Objects.requireNonNull(action, "action == null");

		if (isPresent())
			action.accept(m_value);
	}


	@Contract(pure = true)
	public final void ifPresentOrElse(
			final @NotNull Consumer<ValueType> action,
			final @NotNull Runnable emptyAction)
	{
		Objects.requireNonNull(action, "action == null");
		Objects.requireNonNull(action, "emptyAction == null");

		if (isPresent())
			action.accept(m_value);
		else
			emptyAction.run();
	}

	public Optional<ValueType> filter(Predicate<? super ValueType> predicate) {
		Objects.requireNonNull(predicate, "predicate == null");

		if (isAbsent())
			return Optional.empty();

		return predicate.test(m_value) ? Optional.of(m_value) : Optional.empty();
	}

	@NotNull
	@Contract(pure = true)
	public final <ResultType> Optional<ResultType> map(
			final @NotNull Function<? super ValueType, ? extends ResultType> mapper)
	{
		Objects.requireNonNull(mapper, "mapper == null");

		if (isAbsent())
			return Optional.empty();

		return Optional.ofNullable(mapper.apply(m_value));
	}


	@NotNull
	@Contract(pure = true)
	@SuppressWarnings("unchecked")
	public <ResultType> Optional<ResultType> flatMap(
			final @NotNull Function<? super ValueType, ? extends Optional<? extends ResultType>> mapper)
	{
		Objects.requireNonNull(mapper, "mapper == null");

		if (isAbsent())
			return Optional.empty();

		return Objects.requireNonNull(
				(Optional<ResultType>) mapper.apply(m_value),
				"mapper.apply() == null");
	}

	@NotNull
	@Contract(pure = true)
	@SuppressWarnings("unchecked")
	public final Optional<ValueType> or(
			final @NotNull Supplier<? extends Optional<? extends ValueType>> supplier)
	{
		Objects.requireNonNull(supplier, "supplier == null");

		if (isPresent())
			return Optional.of(m_value);

		return Objects.requireNonNull(
				(Optional<ValueType>) supplier.get(),
				"supplier.get() == null");
	}

	@NotNull
	@Contract(pure = true)
	public Stream<ValueType> stream() {
		return Stream.ofNullable(m_value);
	}

	@Nullable
	@Contract(pure = true)
	public final ValueType orElse(final @Nullable ValueType value) {
		return isPresent() ? m_value : value;
	}

	@Nullable
	@Contract(pure = true)
	public final ValueType orElseGet(final @NotNull Supplier<ValueType> supplier) {
		Objects.requireNonNull(supplier, "supplier == null");
		return isPresent() ? m_value : supplier.get();
	}

	@Override
	public boolean equals(final @Nullable Object object) {
		if (object == this)
			return true;

		if (object instanceof DeferredOptional)
			return Objects.equals(m_value, ((DeferredOptional<?>) object).m_value);

		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(m_value);
	}

	@Override
	@NotNull
	public final String toString() {
		return String.format(
				"%s[%s]", getClass().getSimpleName(), isPresent() ? m_value : null);
	}
}
