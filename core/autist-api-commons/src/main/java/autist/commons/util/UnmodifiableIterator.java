package autist.commons.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.Objects;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class UnmodifiableIterator<T> implements Iterator<T> {
	@NotNull
	private final Iterator<T> m_iterator;

	public UnmodifiableIterator(final @NotNull Iterator<T> iterator) {
		m_iterator = Objects.requireNonNull(iterator, "iterator == null");
	}

	@Override
	@Contract(pure = true)
	public boolean hasNext() {
		return m_iterator.hasNext();
	}

	@Override
	@Contract(pure = true)
	public T next() {
		return m_iterator.next();
	}

	@Override
	@Contract(" -> fail")
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
