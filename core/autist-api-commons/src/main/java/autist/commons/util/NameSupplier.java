package autist.commons.util;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface NameSupplier {
	@NotNull
	String name();
}
