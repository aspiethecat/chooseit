package autist.commons.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

@SuppressWarnings({"WeakerAccess", "unused"})
public class DeferredException<ExceptionType extends Throwable>
		extends DeferredOptional<ExceptionType>
{
	@NotNull
	private final Class<ExceptionType> m_type;

	public DeferredException(final @NotNull Class<ExceptionType> type) {
		m_type = Objects.requireNonNull(type, "type == null");
	}

	@NotNull
	@Contract(pure = true)
	public final Class<ExceptionType> type() {
		return m_type;
	}

	public void wrap(
			final @NotNull Runnable action)
			throws RuntimeException
	{
		Objects.requireNonNull(action, "action == null");
		wrap(Executors.callable(action));
	}

	@Nullable
	@SuppressWarnings("unchecked")
	public <ResultType> ResultType wrap(
			final @NotNull Callable<ResultType> action)
			throws RuntimeException
	{
		Objects.requireNonNull(action, "action == null");

		try {
			 return action.call();
		} catch (final Exception e) {
			if (m_type.isAssignableFrom(e.getClass())) {
				bind((ExceptionType) e);
				return null;
			}

			throw new RuntimeException(e);
		}
	}

	public void throwIfPresent()
			throws ExceptionType
	{
		if (isPresent())
			throw get();
	}
}
