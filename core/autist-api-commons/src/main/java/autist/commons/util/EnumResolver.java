package autist.commons.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings({"WeakerAccess", "unused"})
public class EnumResolver<KeyType, EnumType extends Enum<EnumType> & KeySupplier<KeyType>> {
	private static final Map<Class<? extends Enum>, Map<?, ? extends Enum>> s_resolverCache =
			new ConcurrentHashMap<>();

	@NotNull
	private final Class<EnumType> m_target;

	@NotNull
	private final Map<KeyType, EnumType> m_resolver;

	@SuppressWarnings("unchecked")
	public EnumResolver(final @NotNull Class<EnumType> target) {
		Objects.requireNonNull(target, "target == null");

		m_target = target;
		m_resolver = (Map<KeyType, EnumType>) s_resolverCache.computeIfAbsent(
				target,
				arg -> {
					final var valueMap = new ConcurrentHashMap<KeyType, EnumType>();

					for (final var value : target.getEnumConstants())
						valueMap.put(value.key(), value);

					return valueMap;
				});
	}

	@NotNull
	@Contract(pure = true)
	public final Class<EnumType> target() {
		return m_target;
	}

	@NotNull
	@Contract(pure = true)
	public final Optional<EnumType> resolve(
			final @NotNull KeyType key)
	{
		Objects.requireNonNull(key, "key == null");
		return Optional.ofNullable(m_resolver.get(key));
	}
}
