package autist.commons.net;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringTokenizer;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.net.URLEncoder;

@SuppressWarnings({"WeakerAccess", "unused"})
public class URN implements Comparable<URN>, Iterable<String>, Serializable {
	public static final URN EMPTY = new URN();

	public static final String URI_SCHEME = "urn";

	public static final char PATH_SEPARATOR = '/';
	public static final char NAME_SEPARATOR = '.';
	public static final char CLASS_SEPARATOR = '.';

	@NotNull
	private final List<String> m_entries;

	@Nullable
	private String m_stringValue = null;

	@Nullable
	private Path m_pathValue = null;

	private URN() {
		m_entries = Collections.emptyList();
	}

	public URN(final @NotNull String str) {
		m_entries = extract(Objects.requireNonNull(str, "str == null"));
	}

	public URN(final @NotNull String[] entries) {
		m_entries = List.of(Objects.requireNonNull(entries, "entries == null"));
	}

	public URN(final @NotNull Class<?> clazz) {
		m_entries = extract(Objects.requireNonNull(clazz, "clazz == null"));
	}

	public URN(final @NotNull URI uri) {
		m_entries = extract(Objects.requireNonNull(uri, "uri == null"));
	}

	private URN(final @NotNull List<String> entries) {
		Objects.requireNonNull(entries, "entries == null");
		m_entries = Collections.unmodifiableList(entries);
	}

	@Contract(pure = true)
	public final boolean isEmpty() {
		return m_entries.isEmpty();
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public final Iterator<String> iterator() {
		return m_entries.iterator();
	}

	@NotNull
	@Contract(value = "-> new", pure = true)
	public final URI toURI() {
		final URI uri;

		try {
			uri = new URI(null, null, toString());
		} catch (final URISyntaxException e) {
			throw new AssertionError(e);
		}

		return uri;
	}

	@NotNull
	@Contract(value = "_ -> new", pure = true)
	public final URN append(final @NotNull URN urn) {
		Objects.requireNonNull(urn, "urn == null");

		final var entries = new ArrayList<String>();

		entries.addAll(m_entries);
		entries.addAll(urn.m_entries);

		return new URN(entries);
	}

	@NotNull
	@Contract(value = "_ -> new", pure = true)
	public final URN append(final @NotNull String str) {
		Objects.requireNonNull(str, "str == null");
		return append(new URN(str));
	}

	@NotNull
	@Contract(value = "-> new", pure = true)
	public final URN normalize() {
		final var entries = new ArrayList<String>();

		m_entries.forEach(entry -> {
			if (entry.equals("."))
				return;

			if (entry.equals("..")) {
				if (!entries.isEmpty())
					entries.remove(entries.size() - 1);
				return;
			}

			entries.add(entry);
		});

		return new URN(entries);
	}

	@NotNull
	@Contract(value = "-> new", pure = true)
	public final Path toPath() {
		// TODO: Escape pathname here
		if (m_pathValue == null) {
			m_pathValue = Paths.get(String.join(Character.toString(PATH_SEPARATOR), m_entries.toArray(new String[0])));
		}

		return m_pathValue;
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public final String toString() {
		if (m_stringValue == null) {
			final var builder = new StringBuilder();

			m_entries.forEach(entry -> builder
					.append('/')
					.append(URLEncoder.encode(entry, StandardCharsets.UTF_8)));
			m_stringValue = builder.toString();
		}

		return m_stringValue;
	}

	@Override
	public final int compareTo(final @NotNull URN urn) {
		Objects.requireNonNull(urn, "urn == null");
		return toString().compareTo(urn.toString());
	}

	@NotNull
	private static List<String> extract(
			final @NotNull Class<?> clazz)
	{
		final var entries = new ArrayList<String>();
		final var tokenizer = new StringTokenizer(clazz.getName(), ".");

		for (int i = 0; tokenizer.hasMoreTokens(); i++)
			entries.add(tokenizer.nextToken());

		return Collections.unmodifiableList(entries);
	}

	@NotNull
	private static List<String> extract(
			final @NotNull URI uri)
	{
		final var path = uri.getPath();
		if (path == null)
			throw new IllegalArgumentException("URI path undefined");

		return extract(path);
	}

	@NotNull
	private static List<String> extract(
			final @NotNull String str)
	{
		final var entries = new ArrayList<String>();
		final var tokenizer = new StringTokenizer(str, Character.toString(PATH_SEPARATOR));

		for (int i = 0; tokenizer.hasMoreTokens(); i++)
			entries.add(URLDecoder.decode(tokenizer.nextToken(), StandardCharsets.UTF_8));

		return Collections.unmodifiableList(entries);
	}

	@NotNull
	@Contract(value = "-> new", pure = true)
	public final Optional<URN> parent() {
		if (m_entries.size() <= 1)
			return Optional.empty();

		return Optional.of(new URN(m_entries.subList(0, m_entries.size() - 1)));
	}

	@Contract(pure = true)
	public final int componentCount() {
		return m_entries.size();
	}

	@NotNull
	@Contract(pure = true)
	public final Optional<String> lastComponent() {
		if (m_entries.isEmpty())
			return Optional.empty();

		return Optional.of(m_entries.get(m_entries.size() - 1));
	}

	@NotNull
	@Contract(pure = true)
	public Optional<String> extension() {
		return lastComponent().map(name -> {
			final var offset = name.lastIndexOf(NAME_SEPARATOR);
			if (offset < 0)
				return null;

			final String extension = name.substring(offset + 1);
			if (extension.isEmpty())
				return null;

			return extension;
		});
	}

	@NotNull
	@Contract(value = "_ -> new", pure = true)
	public final Optional<URN> relativize(
			final @NotNull URN basePath)
	{
		Objects.requireNonNull(basePath, "basePath == null");

		final var iterator = startsWithEx(basePath);
		if (iterator == null)
			return Optional.empty();

		final var entries = new ArrayList<String>();
		while (iterator.hasNext())
			entries.add(iterator.next());

		return Optional.of(new URN(entries));
	}

	@Contract(pure = true)
	public final boolean startsWith(final @NotNull URN basePath) {
		Objects.requireNonNull(basePath, "basePath == null");
        return startsWithEx(basePath) != null;
	}

	@Nullable
	@Contract(pure = true)
	private Iterator<String> startsWithEx(final @NotNull URN basePath) {
		var iterator = m_entries.iterator();
		for (var pathEntry : basePath.m_entries) {
			if (!iterator.hasNext())
				return null;
			if (!iterator.next().equals(pathEntry))
				return null;
		}

		return iterator;
	}
}
