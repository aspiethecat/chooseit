package autist.commons.net;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

@SuppressWarnings({"WeakerAccess", "unused"})
public class URNSyntaxException extends Exception {
	@NotNull
	private final String m_message;

	@NotNull
	private final String m_input;

	private final int m_index;

	public URNSyntaxException(
			final @NotNull String input,
			final @NotNull String reason)
	{
		this(input, reason, -1);
	}

	public URNSyntaxException(
			final @NotNull String input,
			final @NotNull String reason,
			int index)
	{
		super(Objects.requireNonNull(reason,"reason"));

		Objects.requireNonNull(input, "input");
		if (index < -1)
			throw new IllegalArgumentException("input");

		m_input = input;
		m_index = index;
		m_message = buildMessage();
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public String getMessage() {
		return m_message;
	}

	@NotNull
	@Contract(pure = true)
	public String getInput() {
		return m_input;
	}

	@NotNull
	@Contract(pure = true)
	public String getReason() {
		return super.getMessage();
	}

	@Contract(pure = true)
	public int getIndex() {
		return m_index;
	}

	@NotNull
	private String buildMessage() {
		final var builder = new StringBuilder();

		builder.append(getReason());

		if (m_index >= 0) {
			builder.append(" at index ");
			builder.append(m_index);
		}

		builder.append(": ");
		builder.append(m_input);

		return builder.toString();
	}
}
