package autist.commons.net;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@SuppressWarnings({"WeakerAccess", "unused"})
public class LocalURN extends AbstractURN<LocalURN> {
	private static final String NAMESPACE = "local";
	private static final char SEPARATOR = '/';

	public LocalURN(final @NotNull LocalURN urn) {
		this(Objects.requireNonNull(urn, "urn").getValue());
	}

	public LocalURN(final @Nullable String value) {
		super(NAMESPACE, value);
	}

	public LocalURN normalize() {
		throw new UnsupportedOperationException();
	}

	public LocalURN relativize(LocalURN baseURN) {
		throw new UnsupportedOperationException();
	}

	public boolean startsWith(LocalURN urn) {
		throw new UnsupportedOperationException();
	}

	public boolean endsWith(LocalURN urn) {
		throw new UnsupportedOperationException();
	}

	public LocalURN getParent() {
		throw new UnsupportedOperationException();
	}

	public String getName() {
		throw new UnsupportedOperationException();
	}

	public String getExtension() {
		throw new UnsupportedOperationException();
	}

	@Contract(value = "_ -> new", pure = true)
	public @NotNull Path toPath(final @NotNull String basePath) {
		Objects.requireNonNull(basePath, "basePath == null");
		return Paths.get(basePath, getValue());
	}

	@Contract(value = "_ -> new", pure = true)
	public @NotNull URI toURI(final @NotNull URI baseURI) throws URISyntaxException {
		Objects.requireNonNull(baseURI, "baseURI == null");

		if (baseURI.isOpaque())
			throw new IllegalArgumentException("baseURI is opaque");

		return new URI(
				baseURI.getScheme(),
				baseURI.getUserInfo(),
				baseURI.getHost(),
				baseURI.getPort(),
				buildPath(baseURI.getPath(), getValue()),
				null,
				null);
	}

	@Nullable
	@Contract(
			value = "null, null -> null; null, !null -> param2; !null, null -> param1; !null, !null -> !null",
			pure = true
	)
	private String buildPath(
			final @Nullable String parentPath,
			final @Nullable String childPath)
	{
		if (parentPath == null) {
			if (childPath == null)
				return null;

			return childPath;
		}

		if (childPath == null)
			return parentPath;

		return String.join(Character.toString(SEPARATOR), parentPath, childPath);
	}
}
