package autist.commons.net;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.net.SocketAddress;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.Objects;

@SuppressWarnings({"WeakerAccess", "unused"})
public class DomainSocketAddress extends SocketAddress {
	@NotNull
	private final Path m_path;

	public DomainSocketAddress(final @NotNull File file) throws InvalidPathException {
		Objects.requireNonNull(file, "file == null");
		m_path = file.toPath();
	}

	public DomainSocketAddress(final @NotNull Path path) {
		m_path = Objects.requireNonNull(path, "path == null");
	}

	@NotNull
	@Contract(pure = true)
	public final Path getPath() {
		return m_path;
	}
}
