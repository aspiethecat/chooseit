package autist.commons.net;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

@SuppressWarnings({"WeakerAccess", "unused"})
public class AbstractURN<T extends AbstractURN<T>> implements Comparable<T> {
	public static final String URI_SCHEME = "urn";

	@NotNull
	private final String m_namespace;

	@Nullable
	private final String m_value;

	@Nullable
	private String m_urn = null;

	public AbstractURN(final @NotNull String namespace) {
		this(namespace, null);
	}

	public AbstractURN(
			final @NotNull String namespace,
			final @Nullable String value)
	{
		m_namespace = Objects.requireNonNull(namespace, "namespace == null");

		if (!Objects.isNull(value) && !value.isEmpty())
			m_value = value;
		else
			m_value = null;
	}

	@NotNull
	@Contract(pure = true)
	public String getNamespace() {
		return m_namespace;
	}

	@Nullable
	@Contract(pure = true)
	public String getValue() {
		return m_value;
	}

	@Contract(pure = true)
	public boolean isEmpty() {
		return Objects.isNull(m_value);
	}

	@Override
	@NotNull
	@Contract(pure = true)
	public String toString() {
		if (m_urn == null) {
			final String value;
			if (m_value != null)
				value = URLEncoder.encode(m_value, StandardCharsets.UTF_8);
			else
				value = "";

			m_urn = String.format("%s:%s:%s", URI_SCHEME, m_namespace, value);
		}

		return m_urn;
	}

	@Override
	@Contract(pure = true)
	public int compareTo(final @NotNull T urn) {
		Objects.requireNonNull(urn, "urn == null");
		return toString().compareTo(urn.toString());
	}
}
