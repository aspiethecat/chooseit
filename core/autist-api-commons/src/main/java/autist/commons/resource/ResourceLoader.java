package autist.commons.resource;

import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.MissingResourceException;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class ResourceLoader {
	private static final Map<Class<?>, ResourceBundle>
			s_resourceCache = new ConcurrentHashMap<>();

	public static ResourceBundle getResourceBundle(
			final @NotNull Class<?> clazz)
			throws MissingResourceException
	{
		Objects.requireNonNull(clazz, "clazz == null");

		return s_resourceCache.computeIfAbsent(
				clazz,
				key -> ResourceBundle.getBundle(
						getResourceBundleClass(clazz).getName(),
						new XmlResourceBundleControl()));
	}

	private static Class<?> getResourceBundleClass(
			final @NotNull Class<?> clazz)
	{
		Class<?> bundleClass = clazz;

		while (bundleClass.isMemberClass())
			bundleClass = bundleClass.getEnclosingClass();

		return bundleClass;
	}
}
