package autist.commons.resource;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class XmlResourceBundleControl extends ResourceBundle.Control {
	private static String XML = "xml";

	@Override
	@NotNull
	public final List<String> getFormats(final @NotNull String baseName) {
		Objects.requireNonNull(baseName, "baseName == null");
		return Collections.singletonList(XML);
	}

	@Override
	@Nullable
	public final ResourceBundle newBundle(
			final @NotNull String baseName,
			final @NotNull Locale locale,
			final @NotNull String format,
			final @NotNull ClassLoader loader,
			final boolean reload)
			throws IOException
	{
		Objects.requireNonNull(baseName, "baseName == null");
		Objects.requireNonNull(locale, "locale == null");
		Objects.requireNonNull(format, "format == null");
		Objects.requireNonNull(loader, "loader == null");

		if (!format.equals(XML))
			return null;

		final String bundleName = toBundleName(baseName, locale);
		final String resourceName = toResourceName(bundleName, format);

		final URL url = loader.getResource(resourceName);
		if (url == null)
			return null;

		final URLConnection connection = url.openConnection();
		if (connection == null)
			return null;
		if (reload)
			connection.setUseCaches(false);

		final InputStream stream = connection.getInputStream();
		if (stream == null)
			return null;

		final XmlResourceBundle bundle;
		try (BufferedInputStream bufferedStream = new BufferedInputStream(stream)) {
			bundle = new XmlResourceBundle(bufferedStream);
		}

		return bundle;
	}
}
