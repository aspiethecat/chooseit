package autist.commons.resource;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Objects;
import java.util.Properties;
import java.util.ResourceBundle;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class XmlResourceBundle extends ResourceBundle {
	private final Properties m_properties;

	XmlResourceBundle(final @NotNull InputStream stream)
			throws IOException
	{
		Objects.requireNonNull(stream, "stream == null");

		m_properties = new Properties();
		m_properties.loadFromXML(stream);
	}

	@Override
	@Nullable
	protected final Object handleGetObject(final @NotNull String key) {
		Objects.requireNonNull(key, "key == null");
		return m_properties.getProperty(key);
	}

	@Override
	@NotNull
	public final Enumeration<String> getKeys() {
		return Collections.enumeration(m_properties.stringPropertyNames());
	}
}
