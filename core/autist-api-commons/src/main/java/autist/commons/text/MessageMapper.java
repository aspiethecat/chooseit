package autist.commons.text;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Locale;

@SuppressWarnings({"WeakerAccess", "unused"})
public class MessageMapper {
	@NotNull
	private final Locale m_locale;

	public MessageMapper() {
		this(Locale.getDefault());
	}

	public MessageMapper(final @NotNull Locale locale) {
		m_locale = locale;
	}

	@NotNull
	@Contract(pure = true)
	public final Locale locale() {
		return m_locale;
	}

	public <T> T getMapper(final Class<T> messageClass) {
		throw new UnsupportedOperationException();
	}
}
