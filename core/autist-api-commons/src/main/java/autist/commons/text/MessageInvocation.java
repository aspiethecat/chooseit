package autist.commons.text;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;

@SuppressWarnings({"WeakerAccess", "unused"})
public class MessageInvocation {
	private final Method m_method;
	private MessageFormat m_messageFormat;
	private final String[] m_parameterNames;

	public MessageInvocation(final @NotNull Method method) {
		m_method = method;

		m_parameterNames = new String[method.getParameterCount()];

		int parameterIndex = 0;
		for (final var parameter : method.getParameters())
			m_parameterNames[parameterIndex++] = parameter.getName();
	}

	public void invoke() {
	}
}
