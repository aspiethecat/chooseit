package autist.commons.text;

import org.jetbrains.annotations.NotNull;

import autist.commons.util.KeySupplier;

@SuppressWarnings({"WeakerAccess", "unused"})
public class MessageFormat<KeyType extends KeySupplier<String>> {
	public MessageFormat(final @NotNull String format) {
	}

	public String format() {
		throw new UnsupportedOperationException();
	}

	private class Field {
	}
}
