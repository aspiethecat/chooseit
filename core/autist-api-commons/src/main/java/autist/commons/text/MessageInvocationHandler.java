package autist.commons.text;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MessageInvocationHandler implements InvocationHandler {
	@Override
	public Object invoke(
			final @NotNull Object proxy,
			final @NotNull Method method,
			final @NotNull Object[] args)
			throws Throwable
	{
		return null;
	}
}
