package autist.journal;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

@SuppressWarnings({"WeakerAccess", "unused"})
public enum JournalType {
	SERVER("SRV", "autist.journal.server"),
	APPLICATION("APP", "autist.journal.application"),
	REQUEST("REQ", "autist.journal.request");

	@NotNull
	private final String m_key;

	@NotNull
	private final String m_loggerName;

	JournalType(
			final @NotNull String key,
			final @NotNull String loggerName)
	{
		m_key = key;
		m_loggerName = loggerName;
	}

	@NotNull
	@Contract(pure = true)
	public final String key() {
		return m_key;
	}

	@NotNull
	@Contract(pure = true)
	public final String loggerName() {
		return m_loggerName;
	}
}
