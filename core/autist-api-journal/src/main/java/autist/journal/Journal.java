package autist.journal;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class Journal {
	private static final @NotNull Map<JournalType, Journal> s_journalMap =
		new ConcurrentHashMap<>();

	private final @NotNull JournalType m_type;

	private final @Nullable JournalContext m_context;

	private final @NotNull Logger m_logger;

	public Journal(final @NotNull JournalType type) {
		this(type, null);
	}

	@Contract("null, _ -> fail")
	public Journal(
			final @NotNull JournalType type,
			final @Nullable JournalContext context)
	{
		Objects.requireNonNull(type, "type == null");

		m_type = type;
		m_context = context;

		m_logger = Logger.getLogger(type.loggerName());
	}

	@NotNull
	@Contract(pure = true)
	public JournalType type() {
		return m_type;
	}

	@NotNull
	@Contract(pure = true)
	public Optional<JournalContext> context() {
		return Optional.ofNullable(m_context);
	}

	public boolean isLoggable(final @NotNull Level level) {
		Objects.requireNonNull(level, "level == null");
		return m_logger.isLoggable(level);
	}

	public void log(
			final @NotNull Level level,
			final @NotNull String message)
	{
		Objects.requireNonNull(level, "level == null");
		Objects.requireNonNull(message, "message == null");

		m_logger.log(buildRecord(level, message));
	}

	public void log(
			final @NotNull Level level,
			final @NotNull String message,
			final @NotNull Throwable cause)
	{
		Objects.requireNonNull(level, "level == null");
		Objects.requireNonNull(message, "message == null");
		Objects.requireNonNull(cause, "cause == null");

		m_logger.log(buildRecord(level, message, cause));
	}

	public void log(final @NotNull LogRecord record) {
		Objects.requireNonNull(record, "level == null");
		m_logger.log(record);
	}

	@NotNull
	@Contract("_, _ -> new")
	private LogRecord buildRecord(
			final @NotNull Level level,
			final @NotNull String message)
	{
		return buildRecord(level, message, null);
	}

	@NotNull
	@Contract("_, _, _ -> new")
	private LogRecord buildRecord(
			final @NotNull Level level,
			final @Nullable String message,
			@Nullable Throwable cause)
	{
		JournalRecord record = new JournalRecord(m_type,  m_context, level, message);

		record.setLoggerName(m_logger.getName());
		record.setThrown(cause);

		return record;
	}

	public static @NotNull Journal get(final @NotNull JournalType type) {
		return  s_journalMap.computeIfAbsent(type, typeArg -> new Journal(type));
	}
}
