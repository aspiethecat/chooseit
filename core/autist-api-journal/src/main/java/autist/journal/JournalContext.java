package autist.journal;

import autist.commons.util.NameSupplier;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface JournalContext extends NameSupplier {
}
