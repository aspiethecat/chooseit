package autist.journal;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.LogRecord;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class JournalRecord extends LogRecord {
	private final @NotNull JournalType m_type;
	private final @Nullable JournalContext m_context;

	@Contract("null, _, _ -> fail")
	JournalRecord(
			final @NotNull JournalType type,
			final @NotNull Level level,
			final @Nullable String message)
	{
		this(type, null, level, message);
	}

	@Contract("null, _, null, _ -> fail")
	JournalRecord(
			final @NotNull JournalType type,
			final @Nullable JournalContext context,
			final @NotNull Level level,
			final @Nullable String message)
	{
		super(level, message);

		m_type = type;
		m_context = context;
	}

	@NotNull
	@Contract(pure = true)
	public JournalType type() {
		return m_type;
	}

	@NotNull
	@Contract(pure = true)
	public Optional<JournalContext> context() {
		return Optional.ofNullable(m_context);
	}
}
