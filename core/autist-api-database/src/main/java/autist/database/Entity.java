package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface Entity {
	@Nullable
	@Contract(pure = true)
	Long getId();

	void setId(@Nullable final Long id);
}
