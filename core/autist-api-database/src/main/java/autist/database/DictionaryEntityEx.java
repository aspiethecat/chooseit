package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings({"WeakerAccess", "unused"})
public class DictionaryEntityEx extends DictionaryEntity {
	/**
	 * Priority.
	 *
	 * @see #getPriority()
	 * @see #setPriority(Integer)
	 */
	@Nullable
	private Integer m_priority = null;

	@Nullable
	@Contract(pure = true)
	public Integer getPriority() {
		return m_priority;
	}

	public void setPriority(@Nullable final Integer priority) {
		m_priority = priority;
	}
}
