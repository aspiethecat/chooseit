package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
@SuppressWarnings({"WeakerAccess", "unused"})
public interface NamedEntityMapper<T extends NamedEntity>
		extends EntityMapper<T>
{
	@Contract("null -> fail; !null -> _")
	boolean isNameUnique(@NotNull @Param("entity") T entity);

	@NotNull
	@Contract("_, _ -> _")
	List<FilterResult> filter(
			@Nullable @Param("filter") String filter,
			@Param("limit") int limit);
}
