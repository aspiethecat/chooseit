package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
@SuppressWarnings({"WeakerAccess", "unused"})
public interface TreeEntityMapper<
		T extends TreeEntity<T, P>,
		P extends TreeEntity<P, ?>>
		extends ChildEntityMapper<T, P>
{
	@NotNull
	List<P> listRoots();

	@NotNull
	@Contract("null -> fail; !null -> _")
	List<T> listChildren(@NotNull @Param("id") Long id);
}
