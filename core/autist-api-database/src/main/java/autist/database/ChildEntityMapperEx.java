package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
@SuppressWarnings({"WeakerAccess", "unused"})
public interface ChildEntityMapperEx<
		T extends ChildEntityEx<T, P>,
		P extends Entity>
		extends EntityMapper<T>
{
	@NotNull
	@Contract("null -> fail; !null -> _")
	List<P> listParents(@NotNull @Param("id") final Long id);
}
