package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface OrderedEntity extends Entity {
	@Nullable
	@Contract(pure = true)
	Short getPriority();

	void setPriority(@Nullable final Short priority);
}
