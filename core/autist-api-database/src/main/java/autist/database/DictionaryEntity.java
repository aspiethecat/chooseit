package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings({"WeakerAccess", "unused"})
public class DictionaryEntity extends NamedEntity {
	/**
	 * Short description.
	 *
	 * @see #getDescription()
	 * @see #setDescription(String)
	 */
	@Nullable
	private String m_description = null;

	@Nullable
	@Contract(pure = true)
	public String getDescription() {
		return m_description;
	}

	public void setDescription(@Nullable final String description) {
		m_description = description;
	}
}
