package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings({"WeakerAccess", "unused"})
public class TreeDictionaryEntity<
		T extends TreeDictionaryEntity<T>>
		extends DictionaryEntity
		implements TreeEntity<T, T>
{
	/**
	 * Parent entity.
	 *
	 * @see #getParent()
	 * @see #setParent(T)
	 */
	@Nullable
	private T m_parent = null;

	/**
	 * Parent entity identifier.
	 *
	 * @see #getParentId()
	 * @see #setParentId(Long)
	 */
	@Nullable
	private Long m_parentId = null;

	/**
	 * Children entities count.
	 *
	 * @see #getChildrenCount()
	 * @see #setChildrenCount(Integer)
	 */
	@Nullable
	private Integer m_childrenCount = null;

	@Override
	@Nullable
	@Contract(pure = true)
	public T getParent() {
		return m_parent;
	}

	@Override
	public void setParent(@Nullable final T parent) {
		m_parent = parent;
	}

	@Override
	@Nullable
	@Contract(pure = true)
	public Long getParentId() {
		return m_parentId;
	}

	@Override
	public void setParentId(@Nullable final Long parentId) {
		m_parentId = parentId;
	}

	@Override
	@Nullable
	@Contract(pure = true)
	public Integer getChildrenCount() {
		return m_childrenCount;
	}

	@Override
	public void setChildrenCount(@Nullable final Integer childrenCount) {
		m_childrenCount = childrenCount;
	}
}
