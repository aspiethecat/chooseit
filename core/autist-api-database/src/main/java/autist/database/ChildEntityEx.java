package autist.database;

import org.jetbrains.annotations.Contract;

import java.util.List;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface ChildEntityEx <T extends Entity, P extends Entity> extends Entity {
	@Contract(pure = true)
	List<P> getParents();

	void setParents(final List<P> parents);

	@Contract(pure = true)
	List<Long> getParentIds();

	void setParentIds(final List<Long> parentIds);

	@Contract(pure = true)
	Long getParentCount();

	void setParentCount(final Long parentCount);
}
