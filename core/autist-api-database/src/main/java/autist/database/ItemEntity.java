package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class ItemEntity extends SimpleEntity {
	/**
	 * Parent entity identifier.
	 *
	 * @see #getParentId()
	 * @see #setParentId(Long)
	 */
	@Nullable
	private Long m_parentId = null;

	@Nullable
	@Contract(pure = true)
	public Long getParentId() {
		return m_parentId;
	}

	public void setParentId(@Nullable final Long parentId) {
		m_parentId = parentId;
	}
}
