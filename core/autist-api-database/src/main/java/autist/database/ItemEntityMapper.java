package autist.database;

import org.apache.ibatis.annotations.Mapper;

@Mapper
@SuppressWarnings({"WeakerAccess", "unused"})
public interface ItemEntityMapper<T extends ItemEntity> extends EntityMapper<T> {
}
