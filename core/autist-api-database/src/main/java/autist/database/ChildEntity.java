package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface ChildEntity<P extends Entity> extends Entity {
	@Nullable
	@Contract(pure = true)
	P getParent();

	void setParent(@Nullable final P parent);

	@Nullable
	@Contract(pure = true)
	Long getParentId();

	void setParentId(@Nullable final Long parentId);
}
