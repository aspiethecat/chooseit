package autist.database;

import org.apache.ibatis.annotations.Mapper;

@Mapper
@SuppressWarnings({"WeakerAccess", "unused"})
public interface DictionaryEntityMapper<T extends DictionaryEntity>
		extends NamedEntityMapper<T>
{
}
