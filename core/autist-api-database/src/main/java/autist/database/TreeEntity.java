package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface TreeEntity<
		T extends TreeEntity<T, P>,
		P extends TreeEntity<P, ?>>
		extends ChildEntity<P>
{
	@Nullable
	@Contract(pure = true)
	Integer getChildrenCount();

	void setChildrenCount(@Nullable final Integer childrenCount);
}
