package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings({"WeakerAccess", "unused"})
public class NamedEntity extends SimpleEntity {
	/**
	 * Name.
	 *
	 * @see #getName()
	 * @see #setName(String)
	 */
	@Nullable
	private String m_name = null;

	@Nullable
	@Contract(pure = true)
	public String getName() {
		return m_name;
	}

	public void setName(@Nullable final String name) {
		m_name = name;
	}
}
