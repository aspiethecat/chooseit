package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDateTime;

@SuppressWarnings({"WeakerAccess", "unused"})
public class CoreEntity<S> extends DictionaryEntity {
	/**
	 * Creation date and time (UTC).
	 *
	 * @see #getCreationTime()
	 * @see #setCreationTime(LocalDateTime)
	 */
	@Nullable
	private LocalDateTime m_creationTime = null;

	/**
	 * Last modification date and time (UTC).
	 *
	 * @see #getModificationTime()
	 * @see #setModificationTime(LocalDateTime)
	 */
	@Nullable
	private LocalDateTime m_modificationTime = null;

	/**
	 * Status.
	 *
	 * @see #getStatus()
	 * @see #setStatus(S)
	 */
	@Nullable
	private S m_status;

	@Nullable
	@Contract(pure = true)
	public LocalDateTime getCreationTime() {
		return m_creationTime;
	}

	public void setCreationTime(@Nullable final LocalDateTime creationTime) {
		m_creationTime = creationTime;
	}

	@Nullable
	@Contract(pure = true)
	public LocalDateTime getModificationTime() {
		return m_modificationTime;
	}

	public void setModificationTime(@Nullable final LocalDateTime modificationTime) {
		m_modificationTime = modificationTime;
	}

	@Nullable
	@Contract(pure = true)
	public S getStatus() {
		return m_status;
	}

	public void setStatus(@Nullable final S status) {
		m_status = status;
	}
}
