package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings({"WeakerAccess", "unused"})
public class SimpleEntity implements Entity {
	/**
	 * Unique identifier, generated automatically.
	 *
	 * @see #getId()
	 * @see #setId(Long)
	 */
	@Nullable
	private Long m_id = null;

	@Override
	@Nullable
	@Contract(pure = true)
	public Long getId() {
		return m_id;
	}

	@Override
	public void setId(@Nullable final Long id) {
		m_id = id;
	}
}
