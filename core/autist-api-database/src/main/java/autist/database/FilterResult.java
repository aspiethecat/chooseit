package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings({"WeakerAccess", "unused"})
public class FilterResult {
	@Nullable
	private Long m_id = null;

	@Nullable
	private Integer m_depth = 0;

	@Nullable
	private String m_name = null;

	@Nullable
	private Short m_priority = null;

	@Nullable
	@Contract(pure = true)
	public Long getId() {
		return m_id;
	}

	public void setId(@Nullable final Long id) {
		m_id = id;
	}

	@Nullable
	@Contract(pure = true)
	public Integer getDepth() {
		return m_depth;
	}

	public void setDepth(@Nullable final Integer depth) {
		m_depth = depth;
	}

	@Nullable
	@Contract(pure = true)
	public String getName() {
		return m_name;
	}

	public void setName(@Nullable final String name) {
		m_name = name;
	}

	@Nullable
	@Contract(pure = true)
	public Short getPriority() {
		return m_priority;
	}

	public void setPriority(@Nullable final Short priority) {
		m_priority = priority;
	}
}
