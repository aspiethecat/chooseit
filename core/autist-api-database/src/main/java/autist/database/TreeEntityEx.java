package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface TreeEntityEx<T extends TreeEntityEx<T>> extends ChildEntityEx<T, T>
{
	@Nullable
	@Contract(pure = true)
	Integer getChildrenCount();

	void setChildrenCount(@Nullable final Integer childrenCount);
}
