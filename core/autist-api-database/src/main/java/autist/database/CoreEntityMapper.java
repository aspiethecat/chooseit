package autist.database;

import org.apache.ibatis.annotations.Mapper;

@Mapper
@SuppressWarnings({"WeakerAccess", "unused"})
public interface CoreEntityMapper<T extends CoreEntity>
		extends DictionaryEntityMapper<T>
{
}
