package autist.database;

import org.apache.ibatis.annotations.Mapper;

@Mapper
@SuppressWarnings({"WeakerAccess", "unused"})
public interface TreeDictionaryEntityMapper<T extends TreeDictionaryEntity<T>>
		extends DictionaryEntityMapper<T>, TreeEntityMapper<T, T>
{
}
