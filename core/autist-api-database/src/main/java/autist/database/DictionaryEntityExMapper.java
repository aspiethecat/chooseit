package autist.database;

import org.apache.ibatis.annotations.Mapper;

@Mapper
@SuppressWarnings({"WeakerAccess", "unused"})
public interface DictionaryEntityExMapper<T extends DictionaryEntityEx>
		extends DictionaryEntityMapper<T>
{
}
