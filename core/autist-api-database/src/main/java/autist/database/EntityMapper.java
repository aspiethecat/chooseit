package autist.database;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
@SuppressWarnings({"WeakerAccess", "unused"})
public interface EntityMapper<T extends Entity> {
	@Contract("null -> fail; !null -> _")
	boolean isExists(@NotNull @Param("id") final Long id);

	@Contract("null -> fail")
	void create(@NotNull @Param("entity") final T entity);

	@Contract("null -> fail")
	void update(@NotNull @Param("entity") final T entity);

	@Nullable
	@Contract("null -> fail; !null -> _")
	T fetchById(@NotNull @Param("id") final Long id);

	@Nullable
	@Contract("null -> fail; !null -> _")
	T lockAndFetchById(@NotNull @Param("id") final Long id);

	@Contract("null -> fail")
	void deleteById(@NotNull @Param("id") final Long id);

	@NotNull
	List<T> listAll();
}
