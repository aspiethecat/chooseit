package autist.database;

import org.apache.ibatis.annotations.Mapper;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@Mapper
@SuppressWarnings({"WeakerAccess", "unused"})
public interface ChildEntityMapper<T extends ChildEntity<P>, P extends Entity>
		extends EntityMapper<T>
{
	@NotNull
	List<T> listOrphans();
}
