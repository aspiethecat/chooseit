package ru.chooseit.webapp.course;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;
import autist.mvc.action.ActionException;
import autist.mvc.Model;
import autist.mvc.action.PersistenceAction;

import autist.mvc.query.DefaultQuery;

@Route("index")
public class Index extends PersistenceAction<ActionContextImpl, DefaultQuery> {
	@Override
	@NotNull
	public final Optional<Model> invoke(final @NotNull DefaultQuery query) throws ActionException {
		return Optional.empty();
	}
}
