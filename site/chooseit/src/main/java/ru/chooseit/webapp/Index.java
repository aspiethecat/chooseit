package ru.chooseit.webapp;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;
import autist.mvc.action.ActionException;
import autist.mvc.action.AbstractAction;
import autist.mvc.view.AbstractView;

import autist.mvc.query.DefaultQuery;
import autist.mvc.view.TemplateView;

@Route("index")
public class Index extends AbstractAction<ActionContextImpl, DefaultQuery> {
	@Override
	@NotNull
	public final Optional<AbstractView> invoke(
			final @NotNull DefaultQuery query)
			throws ActionException
	{
		return Optional.of(new TemplateView("/webapp/index.ftlx"));
	}
}
