package ru.chooseit.webapp;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;
import autist.mvc.action.ActionException;
import autist.mvc.Model;
import autist.mvc.action.PersistenceAction;

import autist.mvc.query.DefaultQuery;

import ru.chooseit.dao.Profession;
import ru.chooseit.mapper.ProfessionMapper;

@SuppressWarnings("unused")
public class ProfessionController {
	@Route("index")
	public static class Index extends PersistenceAction<ActionContextImpl, DefaultQuery> {
		public static class Data implements Model {
			private final Map<Character, List<Profession>> m_professionMap = new HashMap<>(0);

			public Data(final SqlSession session) {
				final List<Profession> professionList =
						session.getMapper(ProfessionMapper.class).listAll();

				Character groupKey = null;
				List<Profession> groupData = null;

				for (final Profession item : professionList) {
					final Character key;
					try {
						key = item.getName().charAt(0);
					} catch (final StringIndexOutOfBoundsException e) {
						continue;
					}

					if (!key.equals(groupKey)) {
						groupKey  = key;
						groupData = new LinkedList<>();

						m_professionMap.put(key, groupData);
					}

					groupData.add(item);
				}
			}

			public final Map<Character, List<Profession>> getProfessionMap() {
				return m_professionMap;
			}
		}

		@Override
		@NotNull
		public final Optional<Model> invoke(final @NotNull DefaultQuery query) throws ActionException {
			return Optional.of(invokeTransaction(Data::new));
		}
	}
}

