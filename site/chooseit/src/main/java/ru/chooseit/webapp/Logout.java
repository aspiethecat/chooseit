package ru.chooseit.webapp;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;
import autist.mvc.action.ActionException;
import autist.mvc.action.AbstractAction;
import autist.mvc.view.AbstractView;

import autist.mvc.query.DefaultQuery;
import autist.mvc.view.RedirectView;

@Route("logout")
public final class Logout extends AbstractAction<ActionContextImpl, DefaultQuery> {
	@Override
	@NotNull
	public Optional<AbstractView> invoke(final @NotNull DefaultQuery query) throws ActionException {
		return Optional.of(new RedirectView("/"));
	}
}
