package ru.chooseit.admin.content;


import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.constraint.MaxDigits;
import autist.mvc.constraint.MinValue;
import autist.mvc.constraint.Required;

import ru.chooseit.dao.Constraints;
import ru.chooseit.dao.EducationalContent;

import ru.chooseit.mapper.EducationalContentMapper;

@SuppressWarnings("unused")
public class EducationalContentController {
	public static class StoreQuery<Q extends StoreQuery<Q>>
			extends PaidContentController.StoreQuery<Q>
	{
		/**
		 * Duration in academic hours (required).
		 *
		 * @see #getTotalHours()
		 * @see #setTotalHours(Short)
		 */
		@MaxDigits(
				integer = Constraints.TOTAL_HOURS_DIGITS,
				fraction = 0,
				message = "{totalHours.invalid}"
		)
		@MinValue(
				value = Constraints.TOTAL_HOURS_MIN,
				message = "{totalHours.invalid}"
		)
		@Required(message = "{totalHours.required}")
		private Short m_totalHours;

		/**
		 * Duration in weeks (optional).
		 *
		 * @see #getTotalWeeks()
		 * @see #setTotalWeeks(Short)
		 */
		@MaxDigits(
				integer = Constraints.TOTAL_WEEKS_DIGITS,
				fraction = 0,
				message = "{totalWeeks.invalid}"
		)
		@MinValue(
				value = Constraints.TOTAL_WEEKS_MIN,
				message = "{totalWeeks.invalid}"
		)
		private Short m_totalWeeks;

		@Nullable
		@Contract(pure = true)
		public final Short getTotalHours() {
			return m_totalHours;
		}

		public final void setTotalHours(@Nullable final Short totalHours) {
			m_totalHours = totalHours;
		}

		@Nullable
		@Contract(pure = true)
		public final Short getTotalWeeks() {
			return m_totalWeeks;
		}

		public final void setTotalWeeks(@Nullable final Short totalWeeks) {
			m_totalWeeks = totalWeeks;
		}
	}

	public abstract static class StoreModel<
			T extends EducationalContent<T>,
			M extends EducationalContentMapper<T>,
			Q extends StoreQuery<Q>>
			extends PaidContentController.StoreModel<T, M, Q>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull Class<M> mapperClass,
				final @NotNull Q query)
		{
			super(session, mapperClass, query);
		}

		@Contract("_, _ -> param1")
		protected static <
				T extends EducationalContent<T>,
				Q extends StoreQuery<Q>>
		T buildEntity(
				final @NotNull T entity,
				final @NotNull Q query)
		{
			Objects.requireNonNull(entity);
			Objects.requireNonNull(query);

			PaidContentController.StoreModel.buildEntity(entity, query);

			entity.setTotalHours(query.getTotalHours());
			entity.setTotalWeeks(query.getTotalWeeks());

			return entity;
		}
	}
}
