package ru.chooseit.admin.provider;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultDeleteAction;
import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultStoreAction;
import autist.mvc.action.DefaultTreeSearchAction;

import autist.mvc.model.DefaultStoreModel;

import autist.mvc.query.DefaultTreeQuery;
import autist.mvc.query.Parameter;

import autist.mvc.constraint.Required;

import ru.chooseit.dao.University;
import ru.chooseit.mapper.UniversityMapper;
import ru.chooseit.admin.common.FormContext;

@SuppressWarnings("unused")
public class UniversityController {
	public static class StoreQuery
			extends ProviderController.StoreQuery<StoreQuery>
	{
		/**
		 * Type identifier (required).
		 *
		 * @see #getType()
		 * @see #setType(Long)
		 */
		@Parameter(message = "{type.invalid}")
		@Required(message = "{type.required}")
		private Long m_type;

		/**
		 * Full name (required).
		 *
		 * @see #getFullName()
		 * @see #setFullName(String)
		 */
		@Required(message = "{fullName.required}")
		private String m_fullName;

		/**
		 * Military facility existence flag (optional, NULL means false).
		 *
		 * @see #getMilitary()
		 * @see #setMilitary(Boolean)
		 */
		private Boolean m_military;

		/**
		 * Residental facility existence flag (optional, NULL means false).
		 *
		 * @see #getResidental()
		 * @see #setResidental(Boolean)
		 */
		private Boolean m_residental;

		/**
		 * Grade (optional, NULL means zero).
		 *
		 * @see #getGrade()
		 * @see #setGrade(Short)
		 */
		private Short m_grade;

		@Nullable
		@Contract(pure = true)
		public final Long getType() {
			return m_type;
		}

		public final void setType(@Nullable final Long type) {
			m_type = type;
		}

		@Nullable
		@Contract(pure = true)
		public final String getFullName() {
			return m_fullName;
		}

		public final void setFullName(@Nullable final String fullName) {
			m_fullName = fullName;
		}

		@Nullable
		@Contract(pure = true)
		public final Boolean getMilitary() {
			return m_military;
		}

		public final void setMilitary(@Nullable final Boolean military) {
			m_military = military;
		}

		@Nullable
		@Contract(pure = true)
		public final Boolean getResidental() {
			return m_residental;
		}

		public final void setResidental(@Nullable final Boolean residental) {
			m_residental = residental;
		}

		@Nullable
		@Contract(pure = true)
		public final Short getGrade() {
			return m_grade;
		}

		public final void setGrade(@Nullable final Short grade) {
			m_grade = grade;
		}
	}

	public static class StoreModel
			extends ProviderController.StoreModel<University, University, UniversityMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, UniversityMapper.class, query);
		}

		@Override
		protected University buildEntity(final @NotNull StoreQuery query) {
			final University university =
					ProviderController.StoreModel.buildEntity(new University(), query);

			university.setTypeId(query.getType());
			university.setFullName(query.getFullName());

			university.setMilitary(
					query.getMilitary() != null ? query.getMilitary() : false);
			university.setResidental(
					query.getResidental() != null ? query.getResidental() : false);

			university.setGrade(query.getGrade());

			return university;
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, University, UniversityMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<University, UniversityMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, University, UniversityMapper> { }

	public static class SearchQuery
			extends DefaultTreeQuery<ProviderController.Field> { }

	@Route("search")
	public static class Search
			extends DefaultTreeSearchAction<ActionContextImpl, University, UniversityMapper, SearchQuery> { }

	@Route("delete")
	public static class Delete
			extends DefaultDeleteAction<ActionContextImpl, University, UniversityMapper> { }
}
