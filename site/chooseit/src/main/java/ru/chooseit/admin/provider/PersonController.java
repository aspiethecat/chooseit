package ru.chooseit.admin.provider;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultDeleteAction;
import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultListSearchAction;
import autist.mvc.action.DefaultStoreAction;

import autist.mvc.model.DefaultStoreModel;

import autist.mvc.query.DefaultTreeQuery;

import ru.chooseit.dao.GenericProvider;
import ru.chooseit.dao.Person;
import ru.chooseit.mapper.PersonMapper;
import ru.chooseit.admin.common.FormContext;

@SuppressWarnings("unused")
public class PersonController {
	public static class StoreQuery
			extends ProviderController.StoreQuery<StoreQuery>
	{
		/**
		 * Position (optional).
		 *
		 * @see #getPosition()
		 * @see #setPosition(String)
		 */
		private String m_position;

		@Nullable
		@Contract(pure = true)
		public final String getPosition() {
			return m_position;
		}

		public final void setPosition(@Nullable final String position) {
			m_position = position;
		}
	}

	public static class StoreModel
			extends ProviderController.StoreModel<Person, GenericProvider, PersonMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, PersonMapper.class, query);
		}

		@Override
		@Contract("_ -> new")
		protected Person buildEntity(final @NotNull StoreQuery query) {
			final Person person =
					ProviderController.StoreModel.buildEntity(new Person(), query);

			person.setPosition(query.getPosition());

			return person;
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, Person, PersonMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<Person, PersonMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, Person, PersonMapper> { }

	public static class SearchQuery
			extends DefaultTreeQuery<ProviderController.Field> { }

	@Route("search")
	public static class Search
			extends DefaultListSearchAction<ActionContextImpl, Person, PersonMapper, SearchQuery> { }

	@Route("delete")
	public static class Delete
			extends DefaultDeleteAction<ActionContextImpl, Person, PersonMapper> { }
}
