package ru.chooseit.admin.content;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.math.BigDecimal;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.query.Parameter;

import autist.mvc.constraint.MaxDigits;
import autist.mvc.constraint.MinValue;

import ru.chooseit.dao.Constraints;
import ru.chooseit.dao.PaidContent;

import ru.chooseit.mapper.CurrencyMapper;
import ru.chooseit.mapper.PaidContentMapper;

@SuppressWarnings("unused")
public class PaidContentController {
	public static class StoreQuery<Q extends StoreQuery<Q>>
			extends ContentController.StoreQuery<Q>
	{
		/**
		 * Price (optional, NULL means free course).
		 *
		 * @see #getPrice()
		 * @see #setPrice(BigDecimal)
		 */
		@MaxDigits(
				integer = Constraints.PRICE_DIGITS,
				fraction = Constraints.PRICE_PRECISION,
				message = "{price.invalid}"
		)
		@MinValue(
				value = Constraints.PRICE_MIN,
				message = "{price.negative}"
		)
		@Parameter(
				format = Constraints.PRICE_FORMAT,
				message = "{price.invalid}"
		)
		private BigDecimal m_price;

		/**
		 * Currency (required when price is set).
		 *
		 * @see #getCurrency()
		 * @see #setCurrency(Long)
		 */
		private Long m_currency;

		@Nullable
		@Contract(pure = true)
		public final BigDecimal getPrice() {
			return m_price;
		}

		public final void setPrice(@Nullable final BigDecimal price) {
			m_price = price;
		}

		@Nullable
		@Contract(pure = true)
		public final Long getCurrency() {
			return m_currency;
		}

		public final void setCurrency(@Nullable final Long currency) {
			m_currency = currency;
		}
	}

	public static abstract class StoreModel<
			T extends PaidContent<T>,
			M extends PaidContentMapper<T>,
			Q extends StoreQuery<Q>>
			extends ContentController.StoreModel<T, M, Q>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull Class<M> mapperClass,
				final @NotNull Q query)
		{
			super(session, mapperClass, query);
		}

		@Override
		protected boolean checkEntity(
				final @NotNull M mapper,
				final @NotNull T entity)
		{
			boolean status = super.checkEntity(mapper, entity, false);

			if (entity.getPrice() != null) {
				if (entity.getCurrencyId() == null) {
					addError("currency", getResourceString("currency.required"));
					status = false;
				} else {
					status &= checkExists("currency", CurrencyMapper.class, entity.getCurrencyId());
				}
			}

			return status;
		}

		@Contract("_, _ -> param1")
		protected static <
				T extends PaidContent<T>,
				Q extends StoreQuery<Q>>
		T buildEntity(
				final @NotNull T entity,
				final @NotNull Q query)
		{
			Objects.requireNonNull(entity);
			Objects.requireNonNull(query);

			ContentController.StoreModel.buildEntity(entity, query);

			entity.setPrice(query.getPrice());
			entity.setCurrencyId(query.getCurrency());

			return entity;
		}
	}
}
