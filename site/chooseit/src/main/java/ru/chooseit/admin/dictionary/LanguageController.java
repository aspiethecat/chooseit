package ru.chooseit.admin.dictionary;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultListSearchAction;
import autist.mvc.action.DefaultStoreAction;

import autist.mvc.model.DefaultStoreModel;
import autist.mvc.model.NamedEntityStoreModel;

import autist.mvc.query.DefaultListQuery;
import autist.mvc.query.NamedEntityStoreQuery;

import autist.mvc.constraint.MaxDigits;
import autist.mvc.constraint.MinValue;
import autist.mvc.constraint.Pattern;
import autist.mvc.constraint.Required;

import ru.chooseit.dao.Constraints;
import ru.chooseit.dao.Language;
import ru.chooseit.mapper.LanguageMapper;
import ru.chooseit.admin.common.FormContext;

@SuppressWarnings("unused")
public class LanguageController {
	public enum Field {
		NAME
	}

	public static class StoreQuery
			extends NamedEntityStoreQuery<StoreQuery>
	{
		/**
		 * ISO 639 2-letter alphabetic code (required).
		 *
		 * @see #getIsoAlpha2()
		 * @see #setIsoAlpha2(String)
		 */
		@Pattern(
			regexp = Language.ISO_ALPHA2_PATTERN,
			message = "{isoAlpha2.invalid}"
		)
		@Required(message = "{isoAlpha2.required}")
		private String m_isoAlpha2;

		/**
		 * ISO 639 3-letter alphabetic code (required).
		 *
		 * @see #getIsoAlpha3()
		 * @see #setIsoAlpha3(String)
		 */
		@Pattern(
			regexp = Language.ISO_ALPHA3_PATTERN,
			message = "{isoAlpha3.invalid}"
		)
		@Required(message = "{isoAlpha3.required}")
		private String m_isoAlpha3;

		/**
		 * Display priority.
		 *
		 * @see #getPriority()
		 * @see #setPriority(Short)
		 */
		@MaxDigits(
			integer = Constraints.PRIORITY_DIGITS,
			fraction = 0,
			message = "{priority.invalid}"
		)
		@MinValue(
			value = Constraints.PRIORITY_MIN,
			message = "{priority.invalid}"
		)
		private Short m_priority;

		@Nullable
		@Contract(pure = true)
		public final String getIsoAlpha2() {
			return m_isoAlpha2;
		}

		public final void setIsoAlpha2(@Nullable final String isoAlpha2) {
			m_isoAlpha2 = isoAlpha2;
		}

		@Nullable
		@Contract(pure = true)
		public final String getIsoAlpha3() {
			return m_isoAlpha3;
		}

		public final void setIsoAlpha3(@Nullable final String isoAlpha3) {
			m_isoAlpha3 = isoAlpha3;
		}

		@Nullable
		@Contract(pure = true)
		public final Short getPriority() {
			return m_priority;
		}

		public final void setPriority(@Nullable final Short priority) {
			m_priority = priority;
		}
	}

	public static class StoreModel
			extends NamedEntityStoreModel<Language, LanguageMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, LanguageMapper.class, query);
		}

		@Override
		protected boolean checkEntity(
				final @NotNull LanguageMapper mapper,
				final @NotNull Language entity)
		{
			boolean status = super.checkEntity(mapper, entity);

			if (entity.getIsoAlpha2() != null) {
				if (!mapper.isIsoAlpha2Unique(entity)) {
					addError("isoAlpha2", getResourceString("isoAlpha2.notUnique"));
					status = false;
				}
			}

			if (entity.getIsoAlpha3() != null) {
				if (!mapper.isIsoAlpha3Unique(entity)) {
					addError("isoAlpha3", getResourceString("isoAlpha3.notUnique"));
					status = false;
				}
			}

			if (!status)
				setStatusMessage(getResourceString("notUnique"));

			return status;
		}

		@Override
		@Contract("_ -> new")
		protected Language buildEntity(final @NotNull StoreQuery query) {
			final Language language = buildEntity(new Language(), query);

			if (query.getIsoAlpha2() != null)
				language.setIsoAlpha2(query.getIsoAlpha2().toUpperCase());

			if (query.getIsoAlpha3() != null)
				language.setIsoAlpha3(query.getIsoAlpha3().toUpperCase());

			language.setPriority(query.getPriority());

			return language;
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, Language, LanguageMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<Language, LanguageMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, Language, LanguageMapper> { }

	public static class SearchQuery
			extends DefaultListQuery<Field> { }

	@Route("search")
	public static class Search
			extends DefaultListSearchAction<ActionContextImpl, Language, LanguageMapper, SearchQuery> { }
}
