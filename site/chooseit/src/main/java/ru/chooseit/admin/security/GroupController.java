package ru.chooseit.admin.security;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultDeleteAction;
import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultListSearchAction;
import autist.mvc.action.DefaultStoreAction;

import autist.mvc.model.DefaultStoreModel;

import autist.mvc.query.DefaultTreeQuery;

import ru.chooseit.dao.Group;
import ru.chooseit.mapper.GroupMapper;
import ru.chooseit.admin.common.FormContext;
import ru.chooseit.admin.common.ProvidedEntityController;

@SuppressWarnings("unused")
public class GroupController {
	public enum Field {
		NAME
	}

	public static class StoreQuery
			extends ProvidedEntityController.StoreQuery<StoreQuery> { }

	public static class StoreModel
			extends ProvidedEntityController.StoreModel<Group, GroupMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, GroupMapper.class, query);
		}

		@Override
		protected boolean checkEntity(
				final @NotNull GroupMapper mapper,
				final @NotNull Group entity)
		{
			return super.checkEntity(mapper, entity, false);
		}

		@Override
		@Contract("_ -> new")
		protected Group buildEntity(final @NotNull StoreQuery query) {
			return buildEntity(new Group(), query);
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, Group, GroupMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<Group, GroupMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, Group, GroupMapper> { }

	public static class SearchQuery
			extends DefaultTreeQuery<Field> { }

	@Route("search")
	public static class Search
			extends DefaultListSearchAction<ActionContextImpl, Group, GroupMapper, SearchQuery> { }

	@Route("delete")
	public static class Delete
			extends DefaultDeleteAction<ActionContextImpl, Group, GroupMapper> { }
}
