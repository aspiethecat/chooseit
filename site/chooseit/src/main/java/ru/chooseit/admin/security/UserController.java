package ru.chooseit.admin.security;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.Optional;

import java.io.IOException;
import java.io.StringWriter;

import java.nio.charset.StandardCharsets;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.ibatis.session.SqlSession;
import freemarker.template.TemplateException;

import autist.journal.Journal;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;
import autist.mvc.action.ActionException;
import autist.mvc.Model;

import autist.mvc.action.DefaultDeleteAction;
import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultFormAction;
import autist.mvc.action.DefaultListSearchAction;
import autist.mvc.action.DefaultStoreAction;

import autist.mvc.model.AbstractUpdateModel;
import autist.mvc.model.DefaultStoreModel;

import autist.mvc.query.DefaultStoreQuery;
import autist.mvc.query.DefaultTreeQuery;
import autist.mvc.query.Parameter;

import autist.mvc.constraint.Required;

import ru.chooseit.dao.AccountStatus;
import ru.chooseit.dao.User;

import ru.chooseit.mapper.UserMapper;

import ru.chooseit.admin.common.FormContext;
import ru.chooseit.admin.common.ProvidedEntityController;

@SuppressWarnings("unused")
public class UserController {
	public enum Field {
		NAME,
		EMAIL
	}

	public static class StoreQuery
			extends ProvidedEntityController.StoreQuery<StoreQuery>
	{
		/**
		 * E-mail address (required).
		 *
		 * @see #getEmail()
		 * @see #setEmail(InternetAddress)
		 */
		@Parameter(message = "{email.invalid}")
		@Required(message = "{email.required}")
		private InternetAddress m_email;

		/**
		 * Family name (required).
		 *
		 * @see #getFamilyName()
		 * @see #setFamilyName(String)
		 */
		@Required(message = "{familyName.required}")
		private String m_familyName;

		/**
		 * Other names (required).
		 *
		 * @see #getOtherNames()
		 * @see #setOtherNames(String)
		 */
		@Required(message = "{otherNames.required}")
		private String m_otherNames;

		/**
		 * Position (optional).
		 *
		 * @see #getPosition()
		 * @see #setPosition(String)
		 */
		private String m_position;

		@Nullable
		@Contract(pure = true)
		public final InternetAddress getEmail() {
			return m_email;
		}

		public final void setEmail(@Nullable final InternetAddress email) {
			m_email = email;
		}

		@Nullable
		@Contract(pure = true)
		public final String getFamilyName() {
			return m_familyName;
		}

		public final void setFamilyName(@Nullable final String familyName) {
			m_familyName = familyName;
		}

		@Nullable
		@Contract(pure = true)
		public final String getOtherNames() {
			return m_otherNames;
		}

		public final void setOtherNames(@Nullable final String otherNames) {
			m_otherNames = otherNames;
		}

		@Nullable
		@Contract(pure = true)
		public final String getPosition() {
			return m_position;
		}

		public final void setPosition(@Nullable final String position) {
			m_position = position;
		}
	}

	public static final class StoreModel
			extends ProvidedEntityController.StoreModel<User, UserMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, UserMapper.class, query);
		}

		@Override
		protected boolean checkEntity(
				final @NotNull UserMapper mapper,
				final @NotNull User entity)
		{
			boolean status = super.checkEntity(mapper, entity, false);

			if (entity.getEmail() != null) {
				if (!mapper.isEmailUnique(entity)) {
					addError("email", getResourceString("email.notUnique"));
					status = false;
				}
			}

			if (!status)
				setStatusMessage(getResourceString("notUnique"));

			return status;
		}

		@Override
		@Contract("_ -> new")
		protected User buildEntity(final @NotNull StoreQuery query) {
			final User user =
					ProvidedEntityController.StoreModel.buildEntity(new User(), query);

			user.setEmail(query.getEmail());
			user.setFamilyName(query.getFamilyName());
			user.setOtherNames(query.getOtherNames());
			user.setPosition(query.getPosition());

			return user;
		}
	}

	@Route("store")
	public static final class Store
			extends DefaultStoreAction<ActionContextImpl, User, UserMapper, StoreQuery> {
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<User, UserMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	static abstract class UserUpdateModel<Q extends DefaultStoreQuery<Q>>
			extends AbstractUpdateModel<User, UserMapper, Q>
	{
		private User m_user;

		UserUpdateModel(
				final @NotNull SqlSession session,
				final @NotNull Q query)
		{
			super(Objects.requireNonNull(session), UserMapper.class, query);
		}

		@Nullable
		@Contract(pure = true)
		User getUser() {
			return m_user;
		}

		protected User prepare(
				final @NotNull UserMapper mapper,
				final @NotNull Q query)
		{
			Objects.requireNonNull(mapper);
			Objects.requireNonNull(query);

			if (query.getId() == null)
				return null;

			m_user = mapper.lockAndFetchById(query.getId());
			if (m_user == null) {
				setStatus(false, getResourceString("user.notFound"));
				return null;
			}

			return m_user;
		}
	}

	static abstract class UserUpdateAction<Q extends DefaultStoreQuery<Q>>
			extends DefaultFormAction<ActionContextImpl, Q>
	{
		protected void notify(
				final @NotNull String templateName,
				final @NotNull String subject,
				final @NotNull UserUpdateModel model)
		{
			final Journal logger = getLogger();

			final StringWriter writer = new StringWriter();
			try {
				getContext().getTemplate(templateName).process(model.getUser(), writer);
			} catch (final IOException | TemplateException e) {
				logger.severe(String.format(
						"Can't create e-mail message: %s", e.getMessage()));
			}

			getContext().getMailer().ifPresentOrElse(
					mailer-> {
						final MimeMessage message = mailer.createMessage();

						try {
							message.setRecipient(Message.RecipientType.TO, model.getUser().getEmail());
							message.setSubject(subject);
							message.setText(writer.toString(), StandardCharsets.UTF_8.name(), "html");

							mailer.sendMessage(message);
						} catch (final MessagingException e) {
							logger.severe(String.format(
									"Can't send e-mail message: %s", e.getMessage()));
						}
					},
					() -> {
						logger.severe("Mailer not configured");
					});
		}
	}

	public static final class SetPasswordQuery
			extends DefaultStoreQuery<SetPasswordQuery>
	{
		@Required(message = "{password.required}")
		private String m_password1;

		@Required(message = "{password.required}")
		private String m_password2;

		@Nullable
		@Contract(pure = true)
		public String getPassword1() {
			return m_password1;
		}

		public void setPassword1(@Nullable final String password1) {
			m_password1 = password1;
		}

		@Nullable
		@Contract(pure = true)
		public String getPassword2() {
			return m_password2;
		}

		public void setPassword2(@Nullable final String password2) {
			m_password2 = password2;
		}
	}

	public static final class SetPasswordModel
			extends UserUpdateModel<SetPasswordQuery>
	{
		SetPasswordModel(
				final @NotNull SqlSession session,
				final @NotNull SetPasswordQuery query)
		{
			super(Objects.requireNonNull(session), query);

			if (!Objects.equals(query.getPassword1(), query.getPassword2()))
				setStatus(false, getResourceString("password.mismatch"));
		}

		@Override
		protected boolean update(
				final @NotNull UserMapper mapper,
				final @NotNull SetPasswordQuery query)
		{
			Objects.requireNonNull(mapper);
			Objects.requireNonNull(query);

			final User user = super.prepare(mapper, query);
			if (user == null)
				return false;

			User.setPassword(user, query.getPassword1());
			mapper.updatePassword(user);

			return true;
		}
	}

	@Route("set-password")
	public static final class SetPassword
			extends UserUpdateAction<SetPasswordQuery>
	{
		@Override
		@NotNull
		public Optional<Model> invoke(final @NotNull SetPasswordQuery query) throws ActionException {
			Objects.requireNonNull(query);

			if (query.getId() == null)
				return Optional.empty();

			final SetPasswordModel model = invokeTransaction(session -> {
				return new SetPasswordModel(session, query);
			});

			if (model.getStatus())
				notify("mail/user-password-changed.ftlx", "Password changed", model);

			return Optional.of(model);
		}
	}

	public static final class ActivateQuery
			extends DefaultStoreQuery<ActivateQuery>
	{
	}

	public static final class ActivateModel
			extends UserUpdateModel<ActivateQuery>
	{
		private User m_user;

		ActivateModel(
				final SqlSession session,
				final ActivateQuery query)
		{
			super(Objects.requireNonNull(session), query);
		}

		@Override
		protected boolean update(
				final @NotNull UserMapper mapper,
				final @NotNull ActivateQuery query)
		{
			Objects.requireNonNull(mapper);
			Objects.requireNonNull(query);

			final User user = super.prepare(mapper, query);
			if (user == null)
				return false;
			if (user.getStatus() != AccountStatus.NEW)
				return false;

			mapper.activate(user);

			return true;
		}
	}

	@Route("activate")
	public static final class Activate
			extends UserUpdateAction<ActivateQuery>
	{
		@Override
		@NotNull
		public Optional<Model> invoke(final @NotNull ActivateQuery query) throws ActionException {
			Objects.requireNonNull(query);

			final ActivateModel model = invokeTransaction(session -> {
				return new ActivateModel(session, query);
			});

			if (model.getStatus())
				notify("mail/user-activated.ftlx", "Account activated", model);

			return Optional.of(model);
		}
	}

	public static final class BlockQuery
			extends DefaultStoreQuery<BlockQuery>
	{
	}

	public static final class BlockModel
			extends UserUpdateModel<BlockQuery>
	{
		BlockModel(
				final @NotNull SqlSession session,
				final @NotNull BlockQuery query)
		{
			super(Objects.requireNonNull(session), query);
		}

		@Override
		protected boolean update(
				final @NotNull UserMapper mapper,
				final @NotNull BlockQuery query)
		{
			Objects.requireNonNull(mapper);
			Objects.requireNonNull(query);

			final User user = super.prepare(mapper, query);
			if (user == null)
				return false;
			if (user.getStatus() == AccountStatus.BLOCKED)
				return false;

			mapper.block(user);

			return true;
		}
	}

	@Route("block")
	public static final class Block
			extends UserUpdateAction<BlockQuery>
	{
		@Override
		@NotNull
		public Optional<Model> invoke(final @NotNull BlockQuery query) throws ActionException {
			Objects.requireNonNull(query);

			final BlockModel model =  invokeTransaction(session -> {
				return new BlockModel(session, query);
			});

			if (model.getStatus())
				notify("mail/user-blocked.ftlx", "Account blocked", model);

			return Optional.of(model);
		}
	}

	@Route("fetch")
	public static final class Fetch
			extends DefaultFetchAction<FormContext, User, UserMapper> { }

	public static final class SearchQuery
			extends DefaultTreeQuery<Field> { }

	@Route("search")
	public static final class Search
			extends DefaultListSearchAction<ActionContextImpl, User, UserMapper, SearchQuery> { }

	@Route("delete")
	public static final class Delete
			extends DefaultDeleteAction<ActionContextImpl, User, UserMapper> { }
}
