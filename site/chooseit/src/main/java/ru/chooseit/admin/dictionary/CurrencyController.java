package ru.chooseit.admin.dictionary;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultListSearchAction;
import autist.mvc.action.DefaultStoreAction;

import autist.mvc.model.DefaultStoreModel;
import autist.mvc.model.NamedEntityStoreModel;

import autist.mvc.query.DefaultListQuery;
import autist.mvc.query.NamedEntityStoreQuery;

import autist.mvc.constraint.MaxDigits;
import autist.mvc.constraint.MinValue;
import autist.mvc.constraint.Pattern;
import autist.mvc.constraint.Required;

import ru.chooseit.dao.Constraints;
import ru.chooseit.dao.Currency;
import ru.chooseit.mapper.CurrencyMapper;
import ru.chooseit.admin.common.FormContext;

@SuppressWarnings("unused")
public class CurrencyController {
	public enum Field {
		NAME
	}

	@SuppressWarnings("unused")
	public static class StoreQuery
			extends NamedEntityStoreQuery<StoreQuery>
	{
		/**
		 * ISO 4217 alphabetic code (required).
		 *
		 * @see #getIsoAlpha()
		 * @see #setIsoAlpha(String)
		 */
		@Pattern(
			regexp = Currency.ISO_ALPHA_PATTERN,
			message = "{isoAlpha.invalid}"
		)
		@Required(message = "{isoAlpha.required}")
		private String m_isoAlpha;

		/**
		 * ISO 4217 numeric code (required).
		 *
		 * @see #getIsoNumeric()
		 * @see #setIsoNumeric(Short)
		 */
		@MaxDigits(
			integer = Currency.ISO_NUMERIC_DIGITS,
			fraction = 0,
			message = "{isoNumeric.invalid}"
		)
		@MinValue(
			value = Currency.ISO_NUMERIC_MIN,
			message = "{isoNumeric.invalid}"
		)
		@Required(message = "{isoNumeric.required}")
		private Short m_isoNumeric;

		/**
		 * Number of fractional digits (required).
		 *
		 * @see #getPrecision()
		 * @see #setPrecision(Short)
		 */
		@MinValue(
			value = Currency.PRECISION_MIN,
			message = "{precision.invalid}"
		)
		@Required(message = "{precision.required}")
		private Short m_precision;

		/**
		 * Icon name.
		 *
		 * @see #getIcon()
		 * @see #setIcon(String)
		 */
		private String m_icon;

		/**
		 * Display priority.
		 *
		 * @see #getPriority()
		 * @see #setPriority(Short)
		 */
		@MaxDigits(
			integer = Constraints.PRIORITY_DIGITS,
			fraction = 0,
			message = "{priority.invalid}"
		)
		@MinValue(
			value = Constraints.PRIORITY_MIN,
			message = "{priority.invalid}"
		)
		private Short m_priority;

		@Nullable
		@Contract(pure = true)
		public final String getIsoAlpha() {
			return m_isoAlpha;
		}

		public final void setIsoAlpha(@Nullable final String isoAlpha) {
			m_isoAlpha = isoAlpha;
		}

		@Nullable
		@Contract(pure = true)
		public final Short getIsoNumeric() {
			return m_isoNumeric;
		}

		public final void setIsoNumeric(@Nullable final Short isoNumeric) {
			m_isoNumeric = isoNumeric;
		}

		@Nullable
		@Contract(pure = true)
		public final Short getPrecision() {
			return m_precision;
		}

		public final void setPrecision(@Nullable final Short precision) {
			m_precision = precision;
		}

		@Nullable
		@Contract(pure = true)
		public final String getIcon() {
			return m_icon;
		}

		public final void setIcon(@Nullable final String icon) {
			m_icon = icon;
		}

		@Nullable
		@Contract(pure = true)
		public final Short getPriority() {
			return m_priority;
		}

		public final void setPriority(@Nullable final Short priority) {
			m_priority = priority;
		}
	}

	public static class StoreModel
			extends NamedEntityStoreModel<Currency, CurrencyMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, CurrencyMapper.class, query);
		}

		@Override
		protected boolean checkEntity(
				final @NotNull CurrencyMapper mapper,
				final @NotNull Currency entity)
		{
			boolean status = super.checkEntity(mapper, entity);

			if (entity.getIsoAlpha() != null) {
				if (!mapper.isIsoAlphaUnique(entity)) {
					addError("isoAlpha", getResourceString("isoAlpha.notUnique"));
					status = false;
				}
			}

			if (entity.getIsoNumeric() != null) {
				if (!mapper.isIsoNumericUnique(entity)) {
					addError("isoNumeric", getResourceString("isoNumeric.notUnique"));
					status = false;
				}
			}

			if (!status)
				setStatusMessage(getResourceString("notUnique"));

			return status;
		}

		@Override
		@Contract("_ -> new")
		protected Currency buildEntity(final @NotNull StoreQuery query) {
			final Currency currency = buildEntity(new Currency(), query);

			if (query.getIsoAlpha() != null)
				currency.setIsoAlpha(query.getIsoAlpha().toUpperCase());

			currency.setIsoNumeric(query.getIsoNumeric());
			currency.setPrecision(query.getPrecision());
			currency.setIcon(query.getIcon());
			currency.setPriority(query.getPriority());

			return currency;
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, Currency, CurrencyMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<Currency, CurrencyMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, Currency, CurrencyMapper> { }

	public static class SearchQuery
			extends DefaultListQuery<Field> { }

	@Route("search")
	public static class Search
			extends DefaultListSearchAction<ActionContextImpl, Currency, CurrencyMapper, SearchQuery> { }
}
