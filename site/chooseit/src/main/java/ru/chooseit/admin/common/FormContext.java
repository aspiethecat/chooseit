package ru.chooseit.admin.common;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.context.ActionContextImpl;

import ru.chooseit.dao.LocationType;
import ru.chooseit.dao.EventType;

import ru.chooseit.mapper.EventTypeMapper;

@SuppressWarnings("unused")
public class FormContext extends ActionContextImpl {
	@NotNull
	public final List<LocationType> getLocationTypes() {
		return Arrays.asList(LocationType.values());
	}

	@NotNull
	public final List<EventType> getEventTypes() {
		try (SqlSession session = getDataSourceSession()) {
			return session.getMapper(EventTypeMapper.class).listAll();
		}
	}
}
