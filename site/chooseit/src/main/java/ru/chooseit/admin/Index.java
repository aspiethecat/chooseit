package ru.chooseit.admin;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import autist.net.URN;

import autist.mvc.Route;
import autist.mvc.action.ActionException;
import autist.mvc.view.AbstractView;

import autist.mvc.query.DefaultQuery;
import autist.mvc.view.TemplateView;

@Route("index")
@SuppressWarnings("unused")
public class Index extends URIHandler {
	private static final URN DASHBOARD_URN = new URN("/admin/dashboard");

	public static class Data {
		private final String m_actionPath;

		public Data(final String actionPath) {
			m_actionPath = actionPath;
		}

		@Contract(pure = true)
		public final String getActionPath() {
			return m_actionPath;
		}
	}

	@Override
	@NotNull
	public Optional<AbstractView> invoke(final @NotNull DefaultQuery query) throws ActionException {
		var actionPath = query.getRequest().getURN();
		if (!isActionPath(actionPath))
			actionPath = DASHBOARD_URN;

		return Optional.of(new TemplateView(
				"admin/layout.ftlx",
				new Data(actionPath.toString())));
	}
}
