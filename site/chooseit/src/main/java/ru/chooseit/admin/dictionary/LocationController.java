package ru.chooseit.admin.dictionary;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultStoreAction;
import autist.mvc.action.DefaultTreeSearchAction;

import autist.mvc.model.DefaultStoreModel;
import autist.mvc.model.NamedEntityStoreModel;

import autist.mvc.query.DefaultTreeQuery;
import autist.mvc.query.NamedEntityStoreQuery;
import autist.mvc.query.Parameter;

import autist.mvc.constraint.MaxDigits;
import autist.mvc.constraint.MinValue;
import autist.mvc.constraint.Required;

import ru.chooseit.dao.Constraints;
import ru.chooseit.dao.Location;
import ru.chooseit.dao.LocationType;

import ru.chooseit.mapper.LocationMapper;

import ru.chooseit.admin.common.FormContext;

@SuppressWarnings("unused")
public class LocationController {
	public enum Field {
		NAME
	}

	public static class StoreQuery
			extends NamedEntityStoreQuery<StoreQuery>
	{
		/**
		 * Type (required).
		 *
		 * @see #getType()
		 * @see #setType(LocationType)
		 */
		@Required(message = "{type.required}")
		@Parameter(message = "{type.invalid}")
		private LocationType m_type;

		/**
		 * External identifier (optional).
		 *
		 * @see #getExternalId()
		 * @see #setExternalId(Long)
		 */
		@MaxDigits(
			integer = Location.EXTERNAL_ID_DIGITS,
			fraction = 0,
			message = "{externalId.invalid}"
		)
		@MinValue(
			value = Location.EXTERNAL_ID_MIN,
			message = "{externalId.invalid}"
		)
		@Parameter(
			format = Location.EXTERNAL_ID_FORMAT,
			message = "{externalId.invalid}"
		)
		private Long m_externalId;

		/**
		 * ZIP code (optional).
		 *
		 * @see #getZipCode()
		 * @see #setZipCode(Integer)
		 */
		@MaxDigits(
			integer = Constraints.ZIP_CODE_DIGITS,
			fraction = 0,
			message = "{zipCode.invalid}"
		)
		@MinValue(
			value = Constraints.ZIP_CODE_MIN,
			message = "{zipCode.invalid}"
		)
		@Parameter(message = "{zipCode.invalid}")
		private Integer m_zipCode;

		@Nullable
		@Contract(pure = true)
		public final LocationType getType() {
			return m_type;
		}

		public final void setType(@Nullable final LocationType type) {
			m_type = type;
		}

		@Nullable
		@Contract(pure = true)
		public final Long getExternalId() {
			return m_externalId;
		}

		public final void setExternalId(@Nullable final Long externalId) {
			m_externalId = externalId;
		}

		@Nullable
		@Contract(pure = true)
		public final Integer getZipCode() {
			return m_zipCode;
		}

		public final void setZipCode(@Nullable final Integer zipCode) {
			m_zipCode = zipCode;
		}
	}

	public static class StoreModel
			extends NamedEntityStoreModel<Location, LocationMapper, StoreQuery>
	{
		StoreModel(
				final SqlSession session,
				final StoreQuery query)
		{
			super(session, LocationMapper.class, query);
		}

		@Override
		@Contract("_ -> new")
		protected Location buildEntity(final @NotNull StoreQuery query) {
			final Location location =
					NamedEntityStoreModel.buildEntity(new Location(), query);

			location.setType(query.getType());
			location.setExternalId(query.getExternalId());
			location.setZipCode(query.getZipCode());

			return location;
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, Location, LocationMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<Location, LocationMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, Location, LocationMapper> { }

	public static class SearchQuery
			extends DefaultTreeQuery<Field> { }

	@Route("search")
	public static class Search
			extends DefaultTreeSearchAction<ActionContextImpl, Location, LocationMapper, SearchQuery> { }
}
