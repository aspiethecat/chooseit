package ru.chooseit.admin.dictionary;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultStoreAction;
import autist.mvc.action.DefaultTreeSearchAction;

import autist.mvc.model.DefaultStoreModel;
import autist.mvc.model.DictionaryEntityStoreModel;

import autist.mvc.query.DefaultTreeQuery;
import autist.mvc.query.DictionaryEntityStoreQuery;

import ru.chooseit.dao.Profession;
import ru.chooseit.mapper.ProfessionMapper;
import ru.chooseit.admin.common.FormContext;

@SuppressWarnings("unused")
public class ProfessionController {
	public enum Field {
		NAME
	}

	public static class StoreQuery
			extends DictionaryEntityStoreQuery<StoreQuery> { }

	public static class StoreModel
			extends DictionaryEntityStoreModel<Profession, ProfessionMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, ProfessionMapper.class, query);
		}

		@Override
		@Contract("_ -> new")
		protected Profession buildEntity(final @NotNull StoreQuery query) {
			return buildEntity(new Profession(), query);
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, Profession, ProfessionMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<Profession, ProfessionMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, Profession, ProfessionMapper> { }

	public static class SearchQuery
			extends DefaultTreeQuery<Field> { }

	@Route("search")
	public static class Search
			extends DefaultTreeSearchAction<ActionContextImpl, Profession, ProfessionMapper, SearchQuery> { }
}
