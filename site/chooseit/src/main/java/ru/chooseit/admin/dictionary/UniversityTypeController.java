package ru.chooseit.admin.dictionary;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultListSearchAction;
import autist.mvc.action.DefaultStoreAction;

import autist.mvc.model.DefaultStoreModel;
import autist.mvc.model.DictionaryEntityStoreModel;

import autist.mvc.query.DefaultListQuery;
import autist.mvc.query.DictionaryEntityStoreQuery;

import ru.chooseit.dao.UniversityType;
import ru.chooseit.mapper.UniversityTypeMapper;
import ru.chooseit.admin.common.FormContext;

@SuppressWarnings("unused")
public class UniversityTypeController {
	public enum Field {
		NAME
	}

	public static class StoreQuery
			extends DictionaryEntityStoreQuery<StoreQuery> { }

	public static class StoreModel
			extends DictionaryEntityStoreModel<UniversityType, UniversityTypeMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, UniversityTypeMapper.class, query);
		}

		@Override
		@Contract("_ -> new")
		protected UniversityType buildEntity(final @NotNull StoreQuery query) {
			return buildEntity(new UniversityType(), query);
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, UniversityType, UniversityTypeMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<
				UniversityType,
				UniversityTypeMapper,
				StoreQuery>
		createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, UniversityType, UniversityTypeMapper> { }

	public static class SearchQuery
			extends DefaultListQuery<Field> { }

	@Route("search")
	public static class Search
			extends DefaultListSearchAction<ActionContextImpl, UniversityType, UniversityTypeMapper, SearchQuery> { }
}
