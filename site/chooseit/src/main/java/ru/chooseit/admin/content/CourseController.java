package ru.chooseit.admin.content;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultDeleteAction;
import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultListSearchAction;
import autist.mvc.action.DefaultStoreAction;

import autist.mvc.model.DefaultStoreModel;

import autist.mvc.query.DefaultTreeQuery;

import ru.chooseit.dao.Course;
import ru.chooseit.mapper.CourseMapper;
import ru.chooseit.admin.common.FormContext;

@SuppressWarnings("unused")
public class CourseController {
	public static class StoreQuery
			extends EducationalContentController.StoreQuery<StoreQuery>
	{
		/**
		 * Internal certificate issue flag (optional, NULL means false).
		 *
		 * @see #getIntCertificate()
		 * @see #setIntCertificate(Boolean)
		 */
		private Boolean m_intCertificate;

		/**
		 * External certificate issue flag (optional, NULL means false).
		 *
		 * @see #getExtCertificate()
		 * @see #setExtCertificate(Boolean)
		 */
		private Boolean m_extCertificate;

		@Nullable
		@Contract(pure = true)
		public final Boolean getIntCertificate() {
			return m_intCertificate;
		}

		public final void setIntCertificate(@Nullable final Boolean intCertificate) {
			m_intCertificate = intCertificate;
		}

		@Nullable
		@Contract(pure = true)
		public final Boolean getExtCertificate() {
			return m_extCertificate;
		}

		public final void setExtCertificate(@Nullable final Boolean extCertificate) {
			m_extCertificate = extCertificate;
		}
	}

	public static class StoreModel
			extends EducationalContentController.StoreModel<Course, CourseMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, CourseMapper.class, query);
		}

		@Override
		protected Course buildEntity(final @NotNull StoreQuery query) {
			final Course course =
					EducationalContentController.StoreModel.buildEntity(new Course(), query);

			course.setIntCertificate(
					query.getIntCertificate() != null ? query.getIntCertificate() : false);
			course.setExtCertificate(
					query.getExtCertificate() != null ? query.getExtCertificate() : false);

			return course;
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, Course, CourseMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<Course, CourseMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, Course, CourseMapper> { }

	public static class SearchQuery
			extends DefaultTreeQuery<ContentController.Field> { }

	@Route("search")
	public static class Search
			extends DefaultListSearchAction<ActionContextImpl, Course, CourseMapper, SearchQuery> { }

	@Route("delete")
	public static class Delete
			extends DefaultDeleteAction<ActionContextImpl, Course, CourseMapper> { }
}
