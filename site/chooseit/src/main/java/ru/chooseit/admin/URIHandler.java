package ru.chooseit.admin;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import autist.net.URN;

import autist.config.ConfigurationException;

import autist.mvc.context.ActionContextImpl;
import autist.mvc.action.AbstractAction;
import autist.mvc.action.ActionNode;
import autist.mvc.Application;
import autist.mvc.query.DefaultQuery;

import ru.chooseit.ActionNodeType;

public abstract class URIHandler extends AbstractAction<ActionContextImpl, DefaultQuery> {
	private static final Collection<String> s_systemNodeNames =
			Arrays.asList(
				"admin-login",
				"admin-logout"
			);

	private ActionNode m_baseNode = null;
	private Set<URN> m_systemNodeUris = new HashSet<>();

	@Override
	protected final void configure() throws ConfigurationException {
		final Application application = getContext().getApplication();

		final String baseNodeId = ActionNodeType.ADMIN_ROOT.getId();

		m_baseNode = application
				.getAction(baseNodeId)
				.orElseThrow(() -> new ConfigurationException(
						String.format("Base node not found: %s", baseNodeId)));

		for (final String nodeId : s_systemNodeNames) {
			final ActionNode node = application
					.getAction(nodeId)
					.orElseThrow(() -> new ConfigurationException(
							String.format("System node not found: %s", nodeId)));
			m_systemNodeUris.add(node.getURN());
		}
	}

	@Contract(pure = true)
	protected final ActionNode getBaseNode() {
		return m_baseNode;
	}

	final boolean isActionPath(final @NotNull URN urn) {
		final var baseURN = m_baseNode.getURN();
		final var relativeURN = urn.relativize(baseURN);

		if (relativeURN != null) {
			if (relativeURN.isEmpty())
				return false;

			return !m_systemNodeUris.contains(urn);
		}

		return false;
	}
}
