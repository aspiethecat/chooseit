package ru.chooseit.admin.dictionary;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultTreeSearchAction;
import autist.mvc.action.DefaultStoreAction;

import autist.mvc.query.DefaultTreeQuery;
import autist.mvc.query.DictionaryEntityStoreQuery;

import autist.mvc.model.DefaultStoreModel;
import autist.mvc.model.DictionaryEntityStoreModel;

import ru.chooseit.dao.Category;
import ru.chooseit.mapper.CategoryMapper;
import ru.chooseit.admin.common.FormContext;

@SuppressWarnings("unused")
public class CategoryController {
	public enum Field {
		NAME
	}

	public static class StoreQuery
			extends DictionaryEntityStoreQuery<StoreQuery> { }

	public static class StoreModel
			extends DictionaryEntityStoreModel<Category, CategoryMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, CategoryMapper.class, query);
		}

		@Override
		@Contract("_ -> new")
		protected Category buildEntity(final @NotNull StoreQuery query) {
			return buildEntity(new Category(), query);
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, Category, CategoryMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<Category, CategoryMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, Category, CategoryMapper> { }

	public static class SearchQuery
			extends DefaultTreeQuery<Field> { }

	@Route("search")
	public static class Search
			extends DefaultTreeSearchAction<ActionContextImpl, Category, CategoryMapper, SearchQuery> { }
}
