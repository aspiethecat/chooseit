package ru.chooseit.admin.common;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import autist.mvc.action.AbstractAction;
import autist.mvc.Model;

import autist.mvc.model.DefaultModel;
import autist.mvc.query.DefaultQuery;

public class FormAction extends AbstractAction<FormContext, DefaultQuery> {
	@Override
	@NotNull
	public Optional<Model> invoke(final @NotNull DefaultQuery query) {
		return Optional.of(new DefaultModel());
	}
}
