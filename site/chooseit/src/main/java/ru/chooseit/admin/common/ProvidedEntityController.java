package ru.chooseit.admin.common;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.model.CoreEntityStoreModel;
import autist.mvc.query.CoreEntityStoreQuery;

import ru.chooseit.dao.ProvidedEntity;
import ru.chooseit.mapper.GenericProviderMapper;
import ru.chooseit.mapper.ProvidedEntityMapper;

public abstract class ProvidedEntityController {
	public static class StoreQuery<Q extends StoreQuery<Q>>
			extends CoreEntityStoreQuery<Q>
	{
		/**
		 * Provider identifier (required by default).
		 *
		 * @see #getProvider()
		 * @see #setProvider(Long)
		 */
		private Long m_provider;

		@Nullable
		public final Long getProvider() {
			return m_provider;
		}

		public final void setProvider(@Nullable final Long provider) {
			m_provider = provider;
		}
	}

	public abstract static class StoreModel<
			T extends ProvidedEntity,
			M extends ProvidedEntityMapper<T>,
			Q extends StoreQuery<Q>>
			extends CoreEntityStoreModel<T, M, Q>
	{
		@Contract("null, _, _ -> fail; _, null, _ -> fail; _, _, null -> fail")
		public StoreModel(
				final @NotNull SqlSession session,
				final @NotNull Class<M> mapperClass,
				final @NotNull Q query)
		{
			super(session, mapperClass, query);
		}

		@Override
		@Contract("null, _ -> fail; _, null -> fail; !null, !null -> _")
		protected boolean checkEntity(
				final @NotNull M mapper,
				final @NotNull T entity)
		{
			return checkEntity(mapper, entity, true);
		}

		@Override
		@Contract("null, _, _ -> fail; _, null, _ -> fail; !null, !null, _ -> _")
		protected boolean checkEntity(
				final @NotNull M mapper,
				final @NotNull T entity,
				final boolean requireProvider)
		{
			boolean status = super.checkEntity(mapper, entity, true);

			if (entity.getProviderId() == null) {
				if (requireProvider) {
					addError("provider", getResourceString("provider.required"));
					status = false;
				}
			} else {
				status &= checkExists(
						"provider",
						GenericProviderMapper.class,
						entity.getProviderId());
			}

			return status;
		}

		@Contract("_, _ -> param1")
		protected static <
				T extends ProvidedEntity,
				Q extends StoreQuery<Q>>
		T buildEntity(
				final @NotNull T entity,
				final @NotNull Q query)
		{
			Objects.requireNonNull(entity);
			Objects.requireNonNull(query);

			CoreEntityStoreModel.buildEntity(entity,query);

			entity.setProviderId(query.getProvider());

			return entity;
		}
	}
}
