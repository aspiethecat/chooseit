package ru.chooseit.admin.content;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.net.URI;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.query.Parameter;

import ru.chooseit.dao.Content;
import ru.chooseit.mapper.ContentMapper;
import ru.chooseit.admin.common.ProvidedEntityController;

@SuppressWarnings("unused")
public class ContentController {
	public enum Field {
		NAME
	}

	public static class StoreQuery<Q extends StoreQuery<Q>>
			extends ProvidedEntityController.StoreQuery<Q>
	{
		/**
		 * Provider name (required when provider identifier is not set).
		 *
		 * @see #getProviderName()
		 * @see #setProviderName(String)
		 */
		private String m_providerName;

		/**
		 * Provider web-site URL (optional).
		 *
		 * @see #getProviderUrl()
		 * @see #setProviderUrl(URI)
		 */
		@Parameter(message = "{providerUrl.invalid}")
		private URI m_providerUrl;

		/**
		 * Related URL (optional).
		 *
		 * @see #getUrl()
		 * @see #setUrl(URI)
		 */
		@Parameter(message = "{url.invalid}")
		private URI m_url;

		@Nullable
		@Contract(pure = true)
		public final String getProviderName() {
			return m_providerName;
		}

		public final void setProviderName(@Nullable final String providerName) {
			m_providerName = providerName;
		}

		@Nullable
		@Contract(pure = true)
		public final URI getProviderUrl() {
			return m_providerUrl;
		}

		public final void setProviderUrl(@Nullable final URI providerUrl) {
			m_providerUrl = providerUrl;
		}

		@Nullable
		@Contract(pure = true)
		public final URI getUrl() {
			return m_url;
		}

		public final void setUrl(@Nullable final URI url) {
			m_url = url;
		}
	}

	public static abstract class StoreModel<
			T extends Content<T>,
			M extends ContentMapper<T>,
			Q extends StoreQuery<Q>>
			extends ProvidedEntityController.StoreModel<T, M, Q>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull Class<M> mapperClass,
				final @NotNull Q query)
		{
			super(session, mapperClass, query);
		}

		@Override
		protected boolean checkEntity(
				final @NotNull M mapper,
				final @NotNull T entity)
		{
			boolean status = super.checkEntity(mapper, entity, false);

			if (entity.getProviderId() == null) {
				if (entity.getProviderName() == null) {
					addError("providerName", getResourceString("providerName.required"));
					status = false;
				}
			}

			return status;
		}

		@Contract("_, _ -> param1")
		protected static <
				T extends Content<T>,
				Q extends StoreQuery<Q>>
		T buildEntity(
				final @NotNull T entity,
				final @NotNull Q query)
		{
			Objects.requireNonNull(entity);
			Objects.requireNonNull(query);

			ProvidedEntityController.StoreModel.buildEntity(entity, query);

			entity.setProviderName(query.getProviderName());
			entity.setProviderUrl(query.getProviderUrl());
			entity.setUrl(query.getUrl());

			return entity;
		}
	}
}
