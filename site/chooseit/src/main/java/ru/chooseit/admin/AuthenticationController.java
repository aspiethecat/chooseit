package ru.chooseit.admin;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Base64;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentSkipListMap;

import java.net.InetAddress;

import java.security.MessageDigest;

import org.apache.ibatis.session.SqlSession;

import autist.util.DigestUtil;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;
import autist.mvc.action.ActionException;
import autist.mvc.Cookie;
import autist.mvc.Model;
import autist.mvc.action.PersistenceAction;
import autist.mvc.query.Query;
import autist.mvc.RequestMethod;

import autist.mvc.action.DefaultFormAction;
import autist.mvc.model.AbstractFormModel;
import autist.mvc.query.DefaultQuery;
import autist.mvc.view.JsonView;

import autist.mvc.constraint.Required;

import ru.chooseit.dao.AccountStatus;
import ru.chooseit.dao.AuthSession;
import ru.chooseit.dao.AuthToken;
import ru.chooseit.dao.User;

import ru.chooseit.mapper.AuthSessionMapper;
import ru.chooseit.mapper.AuthTokenMapper;
import ru.chooseit.mapper.UserMapper;

public class AuthenticationController {
	/**
	 * Authentication session cookie name.
	 */
	private static final String SESSION_COOKIE = "session";

	/**
	 * Authentication session cache size.
	 */
	private static final int SESSION_CACHE_SIZE = 4096;

	/**
	 * Authentication token cookie name.
	 */
	private static final String TOKEN_COOKIE = "token";

	private static final Base64.Encoder s_base64Encoder = Base64.getEncoder();
	private static final Map<String, String> s_sessionMap = new ConcurrentSkipListMap<>();

	public static final class SessionQuery extends Query<SessionQuery> {
	}

	public static final class SessionFilter {
	}

	public static final class LoginQuery extends Query<LoginQuery> {
		/**
		 * User name.
		 *
		 * @see #getUserName()
		 * @see #setUserName(String)
		 */
		@Required(message = "{userName.required}")
		private String m_userName;

		/**
		 * Password.
		 *
		 * @see #getPassword()
		 * @see #setPassword(String)
		 */
		@Required(message = "{password.required}")
		private String m_password;

		/**
		 * Remember authentication flag.
		 *
		 * @see #getRemember()
		 * @see #setRemember(Boolean)
		 */
		private Boolean m_remember;

		@Nullable
		@Contract(pure = true)
		public String getUserName() {
			return m_userName;
		}

		public void setUserName(@Nullable final String userName) {
			m_userName = userName;
		}

		@Nullable
		@Contract(pure = true)
		public String getPassword() {
			return m_password;
		}

		public void setPassword(@Nullable final String password) {
			m_password = password;
		}

		@Nullable
		@Contract(pure = true)
		public Boolean getRemember() {
			return m_remember;
		}

		public void setRemember(@Nullable final Boolean remember) {
			m_remember = remember;
		}
	}

	public static final class LoginModel extends AbstractFormModel {
		private User m_user = null;
		private String m_tokenKey = null;
		private String m_sessionKey = null;

		LoginModel(final SqlSession session) {
			super(Objects.requireNonNull(session));
		}

		@Contract(pure = true)
		boolean hasToken() {
			return m_tokenKey != null;
		}

		@Nullable
		@Contract(pure = true)
		String getTokenKey() {
			return m_tokenKey;
		}

		@Nullable
		@Contract(pure = true)
		String getSessionKey() {
			return m_sessionKey;
		}

		void authenticate(final LoginQuery query) {
			Objects.requireNonNull(query);

			if (!findUser(query.getUserName()))
				return;
			if (!checkUserPassword(query.getPassword()))
				return;
			if (!checkUserStatus())
				return;

			final InetAddress ipAddress =
					query.getRequest().getClientAddress().getAddress();

			if ((query.getRemember() != null) && query.getRemember())
				createAuthToken(ipAddress);
			createAuthSession(ipAddress);
		}

		private void createAuthSession(final InetAddress ipAddress) {
			Objects.requireNonNull(ipAddress);

			final byte[] selector = DigestUtil.generateSecureRandom(AuthSession.SELECTOR_LENGTH);
			m_sessionKey = s_base64Encoder.encodeToString(selector);

			final AuthSession authSession = new AuthSession();

			authSession.setExtendedId(selector);
			authSession.setUserId(m_user.getId());
			authSession.setExpirationTime(AuthSession.DEFAULT_EXPIRATION_TIME);
			authSession.setIpAddress(ipAddress);

			getSession().getMapper(AuthSessionMapper.class).create(authSession);
		}

		private void createAuthToken(final InetAddress ipAddress) {
			Objects.requireNonNull(ipAddress);

			final byte[] selector = DigestUtil.generateSecureRandom(AuthToken.SELECTOR_LENGTH);
			final byte[] validator = DigestUtil.generateSecureRandom(AuthToken.VALIDATOR_LENGTH);

			m_tokenKey = String.format(
					"%s:%s",
					s_base64Encoder.encodeToString(selector),
					s_base64Encoder.encodeToString(validator));

			final AuthToken authToken = new AuthToken();
			final MessageDigest validatorDigest = DigestUtil.getDigestInstance();

			validatorDigest.update(validator);

			authToken.setExtendedId(selector);
			authToken.setValidatorDigest(validatorDigest.digest());
			authToken.setUserId(m_user.getId());
			authToken.setExpirationTime(AuthToken.DEFAULT_EXPIRATION_TIME);
			authToken.setIpAddress(ipAddress);

			getSession().getMapper(AuthTokenMapper.class).create(authToken);
		}

		private boolean checkUserStatus() {
			if (m_user.getStatus() == AccountStatus.ACTIVE)
				return true;

			setStatus(false);

			switch (m_user.getStatus()) {
				case NEW:
					setStatusMessage(getResourceString("user.inactive"));
					break;

				case BLOCKED:
					setStatusMessage(getResourceString("user.blocked"));
					break;

				default:
					throw new AssertionError(String.format(
							"Unsupported user status: %s", m_user.getStatus().toString()));
			}

			return false;
		}

		private boolean checkUserPassword(final String password) {
			Objects.requireNonNull(password);

			boolean status = User.checkPassword(m_user, password);
			if (!status)
				setStatus(false, getResourceString("password.invalid"));

			return status;
		}

		private boolean findUser(final String userName)
		{
			Objects.requireNonNull(userName);

			final UserMapper userMapper = getSession().getMapper(UserMapper.class);

			if (userName.indexOf('@') < 0)
				m_user = userMapper.fetchByName(userName);
			else
				m_user = userMapper.fetchByEmail(userName);

			if (m_user == null) {
				setStatus(false, getResourceString("user.notFound"));
				return false;
			}

			return true;
		}
	}

	@Route("login")
	public static final class Login extends DefaultFormAction<ActionContextImpl, LoginQuery> {
		@Override
		@NotNull
		public Optional<Model> invoke(final @NotNull LoginQuery query) throws ActionException {
			Objects.requireNonNull(query);

			if (query.getRequest().getMethod() != RequestMethod.POST)
				return Optional.of(new Model() {});

			final LoginModel model = invokeTransaction(
					session -> {
						final LoginModel result = new LoginModel(session);

						result.setStatus(validate(result, query));
						if (result.getStatus())
							result.authenticate(query);

						return result;
					});

			final JsonView view = new JsonView(model);

			if (model.getStatus()) {
				view.setCookie(createSessionCookie(query, model.getSessionKey()));
				if (model.hasToken())
					view.setCookie(createTokenCookie(query, model.getTokenKey()));
			}

			return Optional.of(view);
		}

		private Cookie createSessionCookie(
				final LoginQuery query,
				final String sessionKey)
		{
			Objects.requireNonNull(query);
			Objects.requireNonNull(sessionKey);

			return createAuthCookie(
					query, SESSION_COOKIE, sessionKey, AuthSession.DEFAULT_EXPIRATION_TIME);
		}

		private Cookie createTokenCookie(
				final LoginQuery query,
				final String tokenKey)
		{
			Objects.requireNonNull(query);
			Objects.requireNonNull(tokenKey);

			return createAuthCookie(
					query, TOKEN_COOKIE, tokenKey, AuthToken.DEFAULT_EXPIRATION_TIME);
		}

		private Cookie createAuthCookie(
				final LoginQuery query,
				final String name,
				final String value,
				final long expirationTime)
		{
			Objects.requireNonNull(query);
			Objects.requireNonNull(name);
			Objects.requireNonNull(value);

			final Cookie cookie = new Cookie(name, value);

			cookie.setDomain(query.getRequest().getServerName());
			cookie.setPath("/");
			cookie.setMaxAge(expirationTime);

			return cookie;
		}
	}

	public static final class LogoutModel {
	}

	@Route("logout")
	public static final class Logout extends PersistenceAction<ActionContextImpl, DefaultQuery> {
		@Override
		@NotNull
		public Optional<Model> invoke(final @NotNull DefaultQuery query) throws ActionException {
			return Optional.empty();
		}
	}
}
