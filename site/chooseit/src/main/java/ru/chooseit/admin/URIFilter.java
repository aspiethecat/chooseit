package ru.chooseit.admin;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import java.net.URI;
import java.net.URISyntaxException;

import autist.net.URN;

import autist.mvc.action.ActionException;
import autist.mvc.Request;
import autist.mvc.view.AbstractView;

import autist.mvc.query.DefaultQuery;

public class URIFilter extends URIHandler {
	@Override
	@NotNull
	public Optional<AbstractView> invoke(final @NotNull DefaultQuery query) throws ActionException {
		final Request request = query.getRequest();

		if (!isActionPath(request.getURN()))
			return Optional.empty();

		do {
			final String referer = request.getHeader("Referer");
			if (referer == null)
				break;

			final URI refererUri;
			try {
				refererUri = new URI(referer);
			} catch (URISyntaxException e) {
				break;
			}

			// TODO: Check domain here
			final URN refererPath = new URN(refererUri);
			if (!refererPath.startsWith(getBaseNode().getURN()))
				break;

			return Optional.empty();
		} while (false);

		return getContext().getApplication().invokeAction(getBaseNode(), request);
	}
}
