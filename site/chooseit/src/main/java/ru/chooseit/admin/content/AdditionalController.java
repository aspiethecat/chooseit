package ru.chooseit.admin.content;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultDeleteAction;
import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultListSearchAction;
import autist.mvc.action.DefaultStoreAction;

import autist.mvc.model.DefaultStoreModel;

import autist.mvc.query.DefaultTreeQuery;

import ru.chooseit.dao.Additional;
import ru.chooseit.mapper.AdditionalMapper;
import ru.chooseit.admin.common.FormContext;

@SuppressWarnings("unused")
public class AdditionalController {
	public static class StoreQuery
			extends ContentController.StoreQuery<StoreQuery> { }

	public static class StoreModel
			extends ContentController.StoreModel<Additional, AdditionalMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, AdditionalMapper.class, query);
		}

		@Override
		protected Additional buildEntity(final @NotNull StoreQuery query) {
			return buildEntity(new Additional(), query);
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, Additional, AdditionalMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<Additional, AdditionalMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, Additional, AdditionalMapper> { }

	public static class SearchQuery
			extends DefaultTreeQuery<ContentController.Field> { }

	@Route("search")
	public static class Search
			extends DefaultListSearchAction<ActionContextImpl, Additional, AdditionalMapper, SearchQuery> { }

	@Route("delete")
	public static class Delete
			extends DefaultDeleteAction<ActionContextImpl, Additional, AdditionalMapper> { }
}
