package ru.chooseit.admin.content;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultDeleteAction;
import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultListSearchAction;
import autist.mvc.action.DefaultStoreAction;

import autist.mvc.model.DefaultStoreModel;

import autist.mvc.query.DefaultTreeQuery;

import ru.chooseit.dao.Article;
import ru.chooseit.mapper.ArticleMapper;
import ru.chooseit.admin.common.FormContext;

@SuppressWarnings("unused")
public class ArticleController {
	public static class StoreQuery
			extends ContentController.StoreQuery<StoreQuery>
	{
		/**
		 * HTML content (optional).
		 *
		 * @see #getContent()
		 * @see #setContent(String)
		 */
		@Nullable
		private String m_content;

		@Nullable
		@Contract(pure = true)
		public final String getContent() {
			return m_content;
		}

		public final void setContent(@Nullable final String content) {
			m_content = content;
		}
	}

	public static class StoreModel
			extends ContentController.StoreModel<Article, ArticleMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, ArticleMapper.class, query);
		}

		@Override
		protected Article buildEntity(final @NotNull StoreQuery query) {
			final Article article =
					buildEntity(new Article(), query);

			article.setContent(query.getContent());

			return article;
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, Article, ArticleMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<Article, ArticleMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, Article, ArticleMapper> { }

	public static class SearchQuery
			extends DefaultTreeQuery<ContentController.Field> { }

	@Route("search")
	public static class Search
			extends DefaultListSearchAction<ActionContextImpl, Article, ArticleMapper, SearchQuery> { }

	@Route("delete")
	public static class Delete
			extends DefaultDeleteAction<ActionContextImpl, Article, ArticleMapper> { }
}
