package ru.chooseit.admin.content;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultDeleteAction;
import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultListSearchAction;
import autist.mvc.action.DefaultStoreAction;

import autist.mvc.model.DefaultStoreModel;

import autist.mvc.query.DefaultTreeQuery;
import autist.mvc.query.Parameter;

import autist.mvc.constraint.Required;

import ru.chooseit.dao.Constraints;
import ru.chooseit.dao.Preliminary;
import ru.chooseit.mapper.PreliminaryMapper;
import ru.chooseit.admin.common.FormContext;

@SuppressWarnings("unused")
public class PreliminaryController {
	public static class StoreQuery
			extends EducationalContentController.StoreQuery<StoreQuery>
	{
		/**
		 * Start date (required).
		 *
		 * @see #getStartDate()
		 * @see #setStartDate(LocalDate)
		 */
		@Parameter(
				format = Constraints.DATE_FORMAT,
				message = "{startDate.invalid}"
		)
		@Required(message = "{startDate.required}")
		private LocalDate m_startDate;

		@Nullable
		@Contract(pure = true)
		public final LocalDate getStartDate() {
			return m_startDate;
		}

		public final void setStartDate(@Nullable final LocalDate startDate) {
			m_startDate = startDate;
		}
	}

	public static class StoreModel
			extends EducationalContentController.StoreModel<Preliminary, PreliminaryMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, PreliminaryMapper.class, query);
		}

		@Contract("_ -> new")
		protected Preliminary buildEntity(final @NotNull StoreQuery query) {
			final Preliminary preliminary =
					EducationalContentController.StoreModel.buildEntity(new Preliminary(), query);

			preliminary.setStartDate(query.getStartDate());

			return preliminary;
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, Preliminary, PreliminaryMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<Preliminary, PreliminaryMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, Preliminary, PreliminaryMapper> { }

	public static class SearchQuery
			extends DefaultTreeQuery<ContentController.Field> { }

	@Route("search")
	public static class Search
			extends DefaultListSearchAction<ActionContextImpl, Preliminary, PreliminaryMapper, SearchQuery> { }

	@Route("delete")
	public static class Delete
			extends DefaultDeleteAction<ActionContextImpl, Preliminary, PreliminaryMapper> { }
}
