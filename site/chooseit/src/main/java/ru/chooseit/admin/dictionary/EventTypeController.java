package ru.chooseit.admin.dictionary;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultListSearchAction;
import autist.mvc.action.DefaultStoreAction;

import autist.mvc.model.DefaultStoreModel;
import autist.mvc.model.DictionaryEntityStoreModel;

import autist.mvc.query.DefaultListQuery;
import autist.mvc.query.DictionaryEntityStoreQuery;

import ru.chooseit.dao.EventType;
import ru.chooseit.mapper.EventTypeMapper;
import ru.chooseit.admin.common.FormContext;

@SuppressWarnings("unused")
public class EventTypeController {
	public enum Field {
		NAME
	}

	public static class StoreQuery
			extends DictionaryEntityStoreQuery<StoreQuery> { }

	public static class StoreModel
			extends DictionaryEntityStoreModel<EventType, EventTypeMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, EventTypeMapper.class, query);
		}

		@Override
		@Contract("_ -> new")
		protected EventType buildEntity(final @NotNull StoreQuery query) {
			return buildEntity(new EventType(), query);
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, EventType, EventTypeMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<EventType, EventTypeMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, EventType, EventTypeMapper> { }

	public static class SearchQuery
			extends DefaultListQuery<Field> { }

	@Route("search")
	public static class Search
			extends DefaultListSearchAction<ActionContextImpl, EventType, EventTypeMapper, SearchQuery> { }
}
