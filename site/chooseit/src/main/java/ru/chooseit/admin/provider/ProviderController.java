package ru.chooseit.admin.provider;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.net.URI;

import javax.mail.internet.InternetAddress;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.model.CoreEntityStoreModel;
import autist.mvc.query.CoreEntityStoreQuery;
import autist.mvc.query.Parameter;

import autist.mvc.constraint.MaxDigits;
import autist.mvc.constraint.MinValue;

import ru.chooseit.dao.Constraints;
import ru.chooseit.dao.Provider;
import ru.chooseit.mapper.ProviderMapper;

@SuppressWarnings("unused")
public class ProviderController {
	public enum Field {
		NAME
	}

	public static class StoreQuery<Q extends StoreQuery<Q>>
			extends CoreEntityStoreQuery<Q>
	{
		/**
		 * Parent identifier (optional).
		 *
		 * @see #getParent()
		 * @see #setParent(Long)
		 */
		private Long m_parent;

		/**
		 * Web-site URL (optional).
		 *
		 * @see #getSiteUrl()
		 * @see #setSiteUrl(URI)
		 */
		@Parameter(message = "url.invalid")
		private URI m_siteUrl;

		/**
		 * Video presentation URL (optional).
		 *
		 * @see #getVideoUrl()
		 * @see #setVideoUrl(URI)
		 */
		@Parameter(message = "url.invalid")
		private URI m_videoUrl;

		/**
		 * Location identifier (optional).
		 *
		 * @see #getLocation()
		 * @see #setLocation(Long)
		 */
		private Long m_location;

		/**
		 * Primary e-mail address (optional).
		 *
		 * @see #getPrimaryEmail()
		 * @see #setPrimaryEmail(InternetAddress)
		 */
		@Parameter(message = "{primaryEmail.invalid}")
		private InternetAddress m_primaryEmail;

		/**
		 * Primary phone number (optional).
		 *
		 * @see #getPrimaryPhoneNumber()
		 * @see #setPrimaryPhoneNumber(Long)
		 */
		@Parameter(
			format = Constraints.PHONE_NUMBER_FORMAT,
			message = "{primaryPhoneNumber.invalid}"
		)
		private Long m_primaryPhoneNumber;

		/**
		 * Primary phone extension number (optional).
		 *
		 * @see #getPrimaryPhoneNumberExt()
		 * @see #setPrimaryPhoneNumberExt(Short)
		 */
		private Short m_primaryPhoneNumberExt;

		/**
		 * Street address (optional).
		 *
		 * @see #getAddress()
		 * @see #setAddress(String)
		 */
		private String m_address;

		/**
		 * ZIP code (optional).
		 *
		 * @see #getZipCode()
		 * @see #setZipCode(Integer)
		 */
		@MaxDigits(
			integer = Constraints.ZIP_CODE_DIGITS,
			fraction = 0,
			message = "{zipCode.invalid}"
		)
		@MinValue(
			value = Constraints.ZIP_CODE_MIN,
			message = "{zipCode.invalid}"
		)
		@Parameter(message = "{zipCode.invalid}")
		private Integer m_zipCode;

		@Nullable
		@Contract(pure = true)
		public final Long getParent() {
			return m_parent;
		}

		public final void setParent(@Nullable final Long parent) {
			m_parent = parent;
		}

		@Nullable
		@Contract(pure = true)
		public final URI getSiteUrl() {
			return m_siteUrl;
		}

		public final void setSiteUrl(@Nullable final URI siteUrl) {
			m_siteUrl = siteUrl;
		}

		@Nullable
		@Contract(pure = true)
		public final URI getVideoUrl() {
			return m_videoUrl;
		}

		public final void setVideoUrl(@Nullable final URI videoUrl) {
			m_videoUrl = videoUrl;
		}

		@Contract(pure = true)
		@Nullable
		public final Long getLocation() {
			return m_location;
		}

		public final void setLocation(@Nullable final Long location) {
			m_location = location;
		}

		@Nullable
		@Contract(pure = true)
		public final InternetAddress getPrimaryEmail() {
			return m_primaryEmail;
		}

		public final void setPrimaryEmail(@Nullable final InternetAddress primaryEmail) {
			m_primaryEmail = primaryEmail;
		}

		@Nullable
		@Contract(pure = true)
		public final Long getPrimaryPhoneNumber() {
			return m_primaryPhoneNumber;
		}

		public final void setPrimaryPhoneNumber(@Nullable final Long primaryPhoneNumber) {
			m_primaryPhoneNumber = primaryPhoneNumber;
		}

		@Nullable
		@Contract(pure = true)
		public final Short getPrimaryPhoneNumberExt() {
			return m_primaryPhoneNumberExt;
		}

		public final void setPrimaryPhoneNumberExt(@Nullable final Short primaryPhoneNumberExt) {
			m_primaryPhoneNumberExt = primaryPhoneNumberExt;
		}

		@Nullable
		@Contract(pure = true)
		public final String getAddress() {
			return m_address;
		}

		public final void setAddress(@Nullable final String address) {
			m_address = address;
		}

		@Nullable
		@Contract(pure = true)
		public final Integer getZipCode() {
			return m_zipCode;
		}

		public final void setZipCode(@Nullable final Integer zipCode) {
			m_zipCode = zipCode;
		}
	}

	public static abstract class StoreModel<
			T extends Provider<T, P>,
			P extends Provider<P, ?>,
			M extends ProviderMapper<T, P>,
			Q extends StoreQuery<Q>>
			extends CoreEntityStoreModel<T, M, Q>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull Class<M> mapperClass,
				final @NotNull Q query)
		{
			super(session, mapperClass, query);
		}

		@Contract("_, _ -> param1")
		protected static <
				T extends Provider<T, P>,
				P extends Provider<P, ?>,
				Q extends StoreQuery<Q>>
		T buildEntity(
				final @NotNull T entity,
				final @NotNull Q query)
		{
			Objects.requireNonNull(entity);
			Objects.requireNonNull(query);

			CoreEntityStoreModel.buildEntity(entity, query);

			entity.setParentId(query.getParent());
			entity.setSiteUrl(query.getSiteUrl());
			entity.setVideoUrl(query.getVideoUrl());
			entity.setLocationId(query.getLocation());

			if (query.getPrimaryEmail() != null)
				entity.setPrimaryEmail(query.getPrimaryEmail().getAddress());

			entity.setPrimaryPhoneNumber(query.getPrimaryPhoneNumber());
			entity.setPrimaryPhoneNumberExt(query.getPrimaryPhoneNumberExt());
			entity.setAddress(query.getAddress());
			entity.setZipCode(query.getZipCode());

			return entity;
		}
	}
}
