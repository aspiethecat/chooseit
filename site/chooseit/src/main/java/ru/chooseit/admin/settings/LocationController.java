package ru.chooseit.admin.settings;

import autist.mvc.Route;
import autist.mvc.action.DefaultFetchAction;

import ru.chooseit.admin.common.FormContext;
import ru.chooseit.dao.Location;
import ru.chooseit.mapper.LocationMapper;

public class LocationController {
	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, Location, LocationMapper> { }
}
