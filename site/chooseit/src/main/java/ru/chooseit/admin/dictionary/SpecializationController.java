package ru.chooseit.admin.dictionary;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultStoreAction;
import autist.mvc.action.DefaultTreeSearchAction;

import autist.mvc.model.DefaultStoreModel;
import autist.mvc.model.DictionaryEntityStoreModel;

import autist.mvc.query.DefaultTreeQuery;
import autist.mvc.query.DictionaryEntityStoreQuery;
import autist.mvc.query.Parameter;

import autist.mvc.constraint.MaxDigits;
import autist.mvc.constraint.MinValue;

import ru.chooseit.dao.Specialization;
import ru.chooseit.mapper.SpecializationMapper;

@SuppressWarnings("unused")
public class SpecializationController {
	public enum Field {
		NAME
	}

	public static class StoreQuery
			extends DictionaryEntityStoreQuery<StoreQuery>
	{
		/**
		 * External identifier (optional).
		 *
		 * @see #getExternalId()
		 * @see #setExternalId(Integer)
		 */
		@MaxDigits(
			integer = Specialization.EXTERNAL_ID_DIGITS,
			fraction = 0,
			message = "{externalId.invalid}"
		)
		@MinValue(
			value = Specialization.EXTERNAL_ID_MIN,
			message = "{externalId.invalid}"
		)
		@Parameter(
			format = Specialization.EXTERNAL_ID_FORMAT,
			message = "{externalId.invalid}"
		)
		private Integer m_externalId = null;

		/**
		 * International education area code (optional).
		 *
		 * @see #getIntlAreaCode()
		 * @see #setIntlAreaCode(Short)
		 */
		@MaxDigits(
			integer = Specialization.INTL_AREA_CODE_DIGITS,
			fraction = 0,
			message = "{intlAreaCode.invalid}"
		)
		@MinValue(
			value = Specialization.INTL_AREA_CODE_MIN,
			message = "{intlAreaCode.invalid}"
		)
		private Short m_intlAreaCode = null;

		/**
		 * International education course code (optional).
		 *
		 * @see #getIntlCourseCode()
		 * @see #setIntlCourseCode(Short)
		 */
		@MaxDigits(
			integer = Specialization.INTL_COURSE_CODE_DIGITS,
			fraction = 0,
			message = "{intlCourseCode.invalid}"
		)
		@MinValue(
			value = Specialization.INTL_COURSE_CODE_MIN,
			message = "{intlCourseCode.invalid}"
		)
		private Short m_intlCourseCode = null;

		@Nullable
		@Contract(pure = true)
		public final Integer getExternalId() {
			return m_externalId;
		}

		public final void setExternalId(@Nullable final Integer externalId) {
			m_externalId = externalId;
		}

		@Nullable
		@Contract(pure = true)
		public final Short getIntlAreaCode() {
			return m_intlAreaCode;
		}

		public final void setIntlAreaCode(@Nullable final Short intlAreaCode) {
			m_intlAreaCode = intlAreaCode;
		}

		@Nullable
		@Contract(pure = true)
		public final Short getIntlCourseCode() {
			return m_intlCourseCode;
		}

		public final void setIntlCourseCode(@Nullable final Short intlCourseCode) {
			m_intlCourseCode = intlCourseCode;
		}
	}

	public static class StoreModel
			extends DictionaryEntityStoreModel<Specialization, SpecializationMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, SpecializationMapper.class, query);
		}

		@Override
		protected boolean checkEntity(
				final @NotNull SpecializationMapper mapper,
				final @NotNull Specialization entity)
		{
			boolean status = super.checkEntity(mapper, entity);

			if (entity.getExternalId() != null) {
				if (!mapper.isExternalIdUnique(entity)) {
					addError("externalId", getResourceString("externalId.notUnique"));
					status = false;
				}
			}

			if (!status)
				setStatusMessage(getResourceString("notUnique"));

			return status;
		}

		@Override
		@Contract("_ -> new")
		protected Specialization buildEntity(final @NotNull StoreQuery query) {
			final Specialization specialization =
					DictionaryEntityStoreModel.buildEntity(new Specialization(), query);

			specialization.setExternalId(query.getExternalId());
			specialization.setIntlAreaCode(query.getIntlAreaCode());
			specialization.setIntlCourseCode(query.getIntlCourseCode());

			return specialization;
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, Specialization, SpecializationMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		public final DefaultStoreModel<Specialization, SpecializationMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<ActionContextImpl, Specialization, SpecializationMapper> { }

	public static class SearchQuery
			extends DefaultTreeQuery<Field> { }

	@Route("search")
	public static class Search
			extends DefaultTreeSearchAction<ActionContextImpl, Specialization, SpecializationMapper, SearchQuery> { }
}
