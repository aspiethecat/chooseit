package ru.chooseit.admin.provider;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultDeleteAction;
import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultStoreAction;
import autist.mvc.action.DefaultTreeSearchAction;

import autist.mvc.model.DefaultStoreModel;

import autist.mvc.query.DefaultTreeQuery;

import ru.chooseit.dao.Employer;
import ru.chooseit.mapper.EmployerMapper;
import ru.chooseit.admin.common.FormContext;

@SuppressWarnings("unused")
public class EmployerController {
	public static class StoreQuery
			extends ProviderController.StoreQuery<StoreQuery> { }

	public static class StoreModel
			extends ProviderController.StoreModel<Employer, Employer, EmployerMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, EmployerMapper.class, query);
		}

		@Override
		@Contract("_ -> new")
		protected Employer buildEntity(final @NotNull StoreQuery query) {
			return buildEntity(new Employer(), query);
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, Employer, EmployerMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<Employer, EmployerMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, Employer, EmployerMapper> { }

	public static class SearchQuery
			extends DefaultTreeQuery<ProviderController.Field> { }

	@Route("search")
	public static class Search
			extends DefaultTreeSearchAction<ActionContextImpl, Employer, EmployerMapper, SearchQuery> { }

	@Route("delete")
	public static class Delete
			extends DefaultDeleteAction<ActionContextImpl, Employer, EmployerMapper> { }
}
