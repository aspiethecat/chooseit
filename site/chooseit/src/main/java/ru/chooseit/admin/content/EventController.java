package ru.chooseit.admin.content;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;
import java.time.LocalTime;

import org.apache.ibatis.session.SqlSession;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.action.DefaultDeleteAction;
import autist.mvc.action.DefaultFetchAction;
import autist.mvc.action.DefaultListSearchAction;
import autist.mvc.action.DefaultStoreAction;

import autist.mvc.model.DefaultStoreModel;

import autist.mvc.query.DefaultTreeQuery;
import autist.mvc.query.Parameter;

import autist.mvc.constraint.Required;

import ru.chooseit.dao.Constraints;
import ru.chooseit.dao.Event;

import ru.chooseit.mapper.EventMapper;
import ru.chooseit.mapper.EventTypeMapper;
import ru.chooseit.mapper.LocationMapper;

import ru.chooseit.admin.common.FormContext;

@SuppressWarnings("unused")
public class EventController {
	public static class StoreQuery
			extends PaidContentController.StoreQuery<StoreQuery>
	{
		/**
		 * Type identifier (required).
		 *
		 * @see #getType()
		 * @see #setType(Long)
		 */
		@Required(message = "{type.required}")
		private Long m_type;

		/**
		 * Location identifier (optional, NULL means online event).
		 *
		 * @see #getLocation()
		 * @see #setLocation(Long)
		 */
		private Long m_location;

		/**
		 * Start date (required).
		 *
		 * @see #getStartDate()
		 * @see #setStartDate(LocalDate)
		 */
		@Parameter(
			format = Constraints.DATE_FORMAT,
			message = "{startDate.invalid}"
		)
		@Required(message = "{startDate.required}")
		private LocalDate m_startDate;

		/**
		 * Start time (required).
		 *
		 * @see #getStartTime()
		 * @see #setStartTime(LocalTime)
		 */
		@Parameter(
			format = Constraints.TIME_FORMAT,
			message = "{startTime.invalid}"
		)
		@Required(message = "{startTime.required}")
		private LocalTime m_startTime;

		/**
		 * End date (optional, NULL means one-day event).
		 *
		 * @see #getEndDate()
		 * @see #setEndDate(LocalDate)
		 */
		@Parameter(
			format = Constraints.DATE_FORMAT,
			message = "{endDate.invalid}"
		)
		private LocalDate m_endDate;

		/**
		 * Online access flag.
		 *
		 * @see #getOnline()
		 * @see #setOnline(Boolean)
		 */
		private Boolean m_online;

		@Nullable
		@Contract(pure = true)
		public final Long getType() {
			return m_type;
		}

		public final void setType(@Nullable final Long type) {
			m_type = type;
		}

		@Nullable
		@Contract(pure = true)
		public final Long getLocation() {
			return m_location;
		}

		public final void setLocation(@Nullable final Long location) {
			m_location = location;
		}

		@Nullable
		@Contract(pure = true)
		public final LocalDate getStartDate() {
			return m_startDate;
		}

		public final void setStartDate(@Nullable final LocalDate startDate) {
			m_startDate = startDate;
		}

		@Nullable
		@Contract(pure = true)
		public final LocalTime getStartTime() {
			return m_startTime;
		}

		public final void setStartTime(@Nullable final LocalTime startTime) {
			m_startTime = startTime;
		}

		@Nullable
		@Contract(pure = true)
		public final LocalDate getEndDate() {
			return m_endDate;
		}

		public final void setEndDate(@Nullable final LocalDate endDate) {
			m_endDate = endDate;
		}

		@Nullable
		@Contract(pure = true)
		public final Boolean getOnline() {
			return m_online;
		}

		public final void setOnline(@Nullable final Boolean online) {
			m_online = online;
		}
	}

	public static class StoreModel
			extends PaidContentController.StoreModel<Event, EventMapper, StoreQuery>
	{
		StoreModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			super(session, EventMapper.class, query);
		}

		@Override
		protected boolean checkEntity(
				final @NotNull EventMapper mapper,
				final @NotNull Event entity)
		{
			boolean status = super.checkEntity(mapper, entity, false);

			status &= checkExists("type", EventTypeMapper.class, entity.getTypeId());
			status &= checkExists("location", LocationMapper.class, entity.getLocationId());

			if ((entity.getStartDate() != null) && (entity.getEndDate() != null)) {
				if (entity.getEndDate().isBefore(entity.getStartDate())) {
					addError("endDate", getResourceString("endDate.tooEarly"));
					status = false;
				}
			}

			return status;
		}

		@Override
		protected Event buildEntity(final @NotNull StoreQuery query) {
			final Event event =
					PaidContentController.StoreModel.buildEntity(new Event(), query);

			event.setTypeId(query.getType());
			event.setLocationId(query.getLocation());
			event.setStartDate(query.getStartDate());
			event.setStartTime(query.getStartTime());
			event.setEndDate(query.getEndDate());
			event.setOnline(query.getOnline() != null ? query.getOnline() : false);

			return event;
		}
	}

	@Route("store")
	public static class Store
			extends DefaultStoreAction<ActionContextImpl, Event, EventMapper, StoreQuery>
	{
		@Override
		@NotNull
		@Contract("_, _ -> new")
		protected final DefaultStoreModel<Event, EventMapper, StoreQuery> createModel(
				final @NotNull SqlSession session,
				final @NotNull StoreQuery query)
		{
			return new StoreModel(session, query);
		}
	}

	@Route("fetch")
	public static class Fetch
			extends DefaultFetchAction<FormContext, Event, EventMapper> { }

	public static class SearchQuery
			extends DefaultTreeQuery<ContentController.Field> { }

	@Route("search")
	public static class Search
			extends DefaultListSearchAction<ActionContextImpl, Event, EventMapper, SearchQuery> { }

	@Route("delete")
	public static class Delete
			extends DefaultDeleteAction<ActionContextImpl, Event, EventMapper> { }
}
