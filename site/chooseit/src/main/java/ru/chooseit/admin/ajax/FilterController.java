package ru.chooseit.admin.ajax;

import autist.mvc.Route;
import autist.mvc.context.ActionContextImpl;

import autist.mvc.query.DefaultFilterQuery;
import autist.mvc.action.DefaultFilterAction;

import ru.chooseit.dao.Currency;
import ru.chooseit.dao.GenericProvider;
import ru.chooseit.dao.Language;
import ru.chooseit.dao.Location;
import ru.chooseit.dao.Profession;
import ru.chooseit.dao.Specialization;
import ru.chooseit.dao.UniversityType;

import ru.chooseit.mapper.CurrencyMapper;
import ru.chooseit.mapper.GenericProviderMapper;
import ru.chooseit.mapper.LanguageMapper;
import ru.chooseit.mapper.LocationMapper;
import ru.chooseit.mapper.ProfessionMapper;
import ru.chooseit.mapper.SpecializationMapper;
import ru.chooseit.mapper.UniversityTypeMapper;

@SuppressWarnings("unused")
public class FilterController {
	@Route("language-list")
	public static class LanguageList
			extends DefaultFilterAction<ActionContextImpl, Language, LanguageMapper, DefaultFilterQuery> { }

	@Route("currency-list")
	public static class CurrencyList
			extends DefaultFilterAction<ActionContextImpl, Currency, CurrencyMapper, DefaultFilterQuery> { }

	@Route("location-list")
	public static class LocationList
			extends DefaultFilterAction<ActionContextImpl, Location, LocationMapper, DefaultFilterQuery> { }

	@Route("university-type-list")
	public static class UniversityTypeList
			extends DefaultFilterAction<ActionContextImpl, UniversityType, UniversityTypeMapper, DefaultFilterQuery> { }

	@Route("specialization-list")
	public static class SpecializationList
			extends DefaultFilterAction<ActionContextImpl, Specialization, SpecializationMapper, DefaultFilterQuery> { }

	@Route("profession-list")
	public static class ProfessionList
			extends DefaultFilterAction<ActionContextImpl, Profession, ProfessionMapper, DefaultFilterQuery> { }

	@Route("provider-list")
	public static class ProviderList
			extends DefaultFilterAction<ActionContextImpl, GenericProvider, GenericProviderMapper, DefaultFilterQuery> { }
}
