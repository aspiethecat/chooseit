package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import ru.chooseit.dao.University;

@Mapper
public interface UniversityMapper
		extends ProviderMapper<University, University>
{
}
