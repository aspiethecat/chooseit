package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import ru.chooseit.dao.AuthSession;

@Mapper
public interface AuthSessionMapper extends AuthEntityMapper<AuthSession> {
}
