package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;

import autist.data.mapper.NamedEntityMapper;
import autist.data.mapper.TreeEntityMapper;

import ru.chooseit.dao.Location;

@Mapper
public interface LocationMapper
		extends NamedEntityMapper<Location>, TreeEntityMapper<Location, Location>
{
}
