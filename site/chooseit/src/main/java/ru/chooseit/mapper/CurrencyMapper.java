package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import autist.data.mapper.NamedEntityMapper;
import ru.chooseit.dao.Currency;

@Mapper
public interface CurrencyMapper extends NamedEntityMapper<Currency> {
	boolean isIsoAlphaUnique(@Param("entity") Currency entity);
	boolean isIsoNumericUnique(@Param("entity") Currency entity);
}
