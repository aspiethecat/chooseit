package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import ru.chooseit.dao.GenericContent;

@Mapper
public interface GenericContentMapper
		extends ContentMapper<GenericContent>
{
}
