package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import ru.chooseit.dao.Additional;

@Mapper
public interface AdditionalMapper
		extends ContentMapper<Additional>
{
}
