package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;

import ru.chooseit.dao.GenericProvider;
import ru.chooseit.dao.Person;

@Mapper
public interface PersonMapper
		extends ProviderMapper<Person, GenericProvider>
{
}
