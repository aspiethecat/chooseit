package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import ru.chooseit.dao.PaidContent;

@Mapper
public interface PaidContentMapper<T extends PaidContent<T>>
		extends ContentMapper<T>
{
}
