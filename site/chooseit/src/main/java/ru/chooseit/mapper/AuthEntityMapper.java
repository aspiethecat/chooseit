package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import ru.chooseit.dao.AuthEntity;
import autist.data.mapper.EntityMapper;

@Mapper
public interface AuthEntityMapper<T extends AuthEntity> extends EntityMapper<T> {
	T fetchByExtendedId(@Param("extendedId") byte[] extendedId);
}
