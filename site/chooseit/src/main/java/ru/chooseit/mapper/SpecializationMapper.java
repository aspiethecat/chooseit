package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import autist.data.mapper.TreeDictionaryEntityMapper;
import ru.chooseit.dao.Specialization;

@Mapper
public interface SpecializationMapper
		extends TreeDictionaryEntityMapper<Specialization>
{
	boolean isExternalIdUnique(@Param("entity") Specialization entity);
}
