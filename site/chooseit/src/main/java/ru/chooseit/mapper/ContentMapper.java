package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import ru.chooseit.dao.Content;

@Mapper
public interface ContentMapper<T extends Content<T>>
		extends ProvidedEntityMapper<T>
{
}
