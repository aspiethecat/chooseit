package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import ru.chooseit.dao.Preliminary;

@Mapper
public interface PreliminaryMapper
		extends EducationalContentMapper<Preliminary>
{
}
