package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import ru.chooseit.dao.GenericProvider;

@Mapper
public interface GenericProviderMapper
		extends ProviderMapper<GenericProvider, GenericProvider>
{
}
