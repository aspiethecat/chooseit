package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;

import autist.data.mapper.NamedEntityMapper;
import ru.chooseit.dao.Tag;

@Mapper
public interface TagMapper extends NamedEntityMapper<Tag> {
}
