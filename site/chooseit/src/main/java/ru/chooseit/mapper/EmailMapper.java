package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;

import autist.data.mapper.ItemEntityMapper;
import ru.chooseit.dao.Email;

@Mapper
public interface EmailMapper
		extends ItemEntityMapper<Email>
{
}
