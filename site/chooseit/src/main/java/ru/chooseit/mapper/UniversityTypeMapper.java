package ru.chooseit.mapper;

import autist.data.mapper.DictionaryEntityMapper;
import ru.chooseit.dao.UniversityType;

public interface UniversityTypeMapper
		extends DictionaryEntityMapper<UniversityType>
{
}
