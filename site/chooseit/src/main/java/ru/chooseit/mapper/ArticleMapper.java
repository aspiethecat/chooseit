package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;

import autist.data.mapper.ChildEntityMapperEx;
import ru.chooseit.dao.Article;
import ru.chooseit.dao.Category;

@Mapper
public interface ArticleMapper
		extends ContentMapper<Article>, ChildEntityMapperEx<Article, Category>
{
}
