package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import ru.chooseit.dao.AuthToken;

@Mapper
public interface AuthTokenMapper extends AuthEntityMapper<AuthToken> {
}
