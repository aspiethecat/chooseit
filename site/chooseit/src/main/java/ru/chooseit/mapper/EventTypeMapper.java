package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;

import autist.data.mapper.DictionaryEntityMapper;
import ru.chooseit.dao.EventType;

@Mapper
public interface EventTypeMapper extends DictionaryEntityMapper<EventType> {
}
