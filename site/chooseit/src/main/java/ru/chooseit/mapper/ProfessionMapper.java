package ru.chooseit.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import autist.data.mapper.TreeDictionaryEntityMapper;
import ru.chooseit.dao.Profession;
import ru.chooseit.dao.Specialization;


@Mapper
public interface ProfessionMapper
		extends TreeDictionaryEntityMapper<Profession>
{
	List<Specialization> listSpecializations(Long id);
}
