package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import ru.chooseit.dao.Employer;

@Mapper
public interface EmployerMapper extends ProviderMapper<Employer, Employer> {
}
