package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;

import autist.data.mapper.ChildEntityMapper;
import autist.data.mapper.CoreEntityMapper;

import ru.chooseit.dao.Media;

@Mapper
public interface MediaMapper<P extends Media<P>>
		extends CoreEntityMapper<Media<P>>, ChildEntityMapper<Media<P>, P>
{
}
