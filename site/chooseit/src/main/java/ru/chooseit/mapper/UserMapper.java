package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import ru.chooseit.dao.User;

@Mapper
public interface UserMapper
		extends ProvidedEntityMapper<User>
{
	boolean isEmailUnique(@Param("entity") User entity);

	User fetchByName(@Param("name") String name);
	User fetchByEmail(@Param("email") String email);

	void updatePassword(@Param("entity") User entity);

	void activate(@Param("entity") User entity);
	void block(@Param("entity") User entity);
}
