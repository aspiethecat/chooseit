package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;

import autist.data.mapper.DictionaryEntityMapper;
import ru.chooseit.dao.MimeType;

@Mapper
public interface MimeTypeMapper
		extends DictionaryEntityMapper<MimeType>
{
}
