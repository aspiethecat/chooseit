package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;

import autist.data.mapper.TreeDictionaryEntityMapper;
import ru.chooseit.dao.Category;

@Mapper
public interface CategoryMapper
		extends TreeDictionaryEntityMapper<Category>
{
}
