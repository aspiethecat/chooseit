package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import ru.chooseit.dao.Group;

@Mapper
public interface GroupMapper extends ProvidedEntityMapper<Group> {
}
