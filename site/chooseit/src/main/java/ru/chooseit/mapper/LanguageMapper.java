package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import autist.data.mapper.NamedEntityMapper;
import ru.chooseit.dao.Language;

@Mapper
public interface LanguageMapper
		extends NamedEntityMapper<Language>
{
	boolean isIsoAlpha2Unique(@Param("entity") Language entity);
	boolean isIsoAlpha3Unique(@Param("entity") Language entity);
}
