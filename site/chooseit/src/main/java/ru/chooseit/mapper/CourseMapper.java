package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import ru.chooseit.dao.Course;

@Mapper
public interface CourseMapper
		extends EducationalContentMapper<Course>
{
}
