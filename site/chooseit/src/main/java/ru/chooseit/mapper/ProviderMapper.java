package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;

import autist.data.mapper.CoreEntityMapper;
import autist.data.mapper.TreeEntityMapper;

import ru.chooseit.dao.Provider;

@Mapper
public interface ProviderMapper<
		T extends Provider<T, P>,
		P extends Provider<P, ?>>
		extends CoreEntityMapper<T>, TreeEntityMapper<T, P>
{
}
