package ru.chooseit.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import autist.data.mapper.CoreEntityMapper;
import ru.chooseit.dao.ProvidedEntity;

@Mapper
public interface ProvidedEntityMapper<T extends ProvidedEntity>
		extends CoreEntityMapper<T>
{
	List<T> listByProvider(@Param("providerId") Long providerId);
}
