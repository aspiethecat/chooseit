package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import ru.chooseit.dao.EducationalContent;

@Mapper
public interface EducationalContentMapper<T extends EducationalContent<T>>
		extends PaidContentMapper<T>
{
}
