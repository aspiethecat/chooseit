package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;
import ru.chooseit.dao.Event;

@Mapper
public interface EventMapper
		extends PaidContentMapper<Event>
{
}
