package ru.chooseit.mapper;

import org.apache.ibatis.annotations.Mapper;

import autist.data.mapper.ItemEntityMapper;
import ru.chooseit.dao.Phone;

@Mapper
public interface PhoneMapper
		extends ItemEntityMapper<Phone>
{
}
