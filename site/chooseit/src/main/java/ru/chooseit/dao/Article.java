package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import autist.data.model.ChildEntityEx;

@SuppressWarnings("unused")
public class Article
		extends Content<Article>
		implements ChildEntityEx<Article, Category>
{
	/**
	 * Categories.
	 *
	 * @see #getParents()
	 * @see #setParents(List)
	 */
	private List<Category> m_parents;

	/**
	 * Category identifiers.
	 *
	 * @see #getParentIds()
	 * @see #setParentIds(List)
	 */
	private List<Long> m_parentIds;

	/**
	 * Category count.
	 *
	 * @see #getParentCount()
	 * @see #setParentCount(Long)
	 */
	private Long m_parentCount;

	/**
	 * Content (HTML).
	 *
	 * @see #getContent()
	 * @see #setContent(String)
	 */
	private String m_content;

	@Override
	@Nullable
	@Contract(pure = true)
	public List<Category> getParents() {
		return m_parents;
	}

	@Override
	public void setParents(@Nullable final List<Category> parents) {
		m_parents = parents;
	}

	@Override
	@Nullable
	@Contract(pure = true)
	public List<Long> getParentIds() {
		return m_parentIds;
	}

	@Override
	public void setParentIds(@Nullable final List<Long> parentIds) {
		m_parentIds = parentIds;
	}

	@Override
	@Nullable
	@Contract(pure = true)
	public Long getParentCount() {
		return m_parentCount;
	}

	@Override
	public void setParentCount(@Nullable final Long parentCount) {
		m_parentCount = parentCount;
	}

	@Nullable
	@Contract(pure = true)
	public String getContent() {
		return m_content;
	}

	public void setContent(@Nullable final String content) {
		m_content = content;
	}
}
