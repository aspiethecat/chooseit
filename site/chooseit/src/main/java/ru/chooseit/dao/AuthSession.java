package ru.chooseit.dao;

@SuppressWarnings("unused")
public class AuthSession extends AuthEntity {
	/**
	 * Default expiration time in seconds.
	 */
	public static final long DEFAULT_EXPIRATION_TIME = 86400L;

	/**
	 * Selector length in bytes.
	 */
	public static final int SELECTOR_LENGTH = 32;
}
