package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import autist.data.model.TreeDictionaryEntity;

@SuppressWarnings("unused")
public class Specialization extends TreeDictionaryEntity<Specialization> {
	/**
	 * External identifier digits count.
	 */
	public static final int EXTERNAL_ID_DIGITS = 10;

	/**
	 * Minimal external identifier value.
	 */
	public static final long EXTERNAL_ID_MIN = 1000000;

	/**
	 * External identifier format string.
	 */
	public static final String EXTERNAL_ID_FORMAT =
			"0,00,00,00;; groupingSeparator='.' decimalSeparator=','";

	/**
	 * International area code digits count.
	 */
	public static final int INTL_AREA_CODE_DIGITS = 4;

	/**
	 * Minimal international area code value.
	 */
	public static final long INTL_AREA_CODE_MIN = 0;

	/**
	 * International course code digits count.
	 */
	public static final int INTL_COURSE_CODE_DIGITS = 3;

	/**
	 * Minimal international course code value.
	 */
	public static final long INTL_COURSE_CODE_MIN = 0;

	/**
	 * External identifier.
	 *
	 * @see #getExternalId()
	 * @see #setExternalId(Integer)
	 */
	private Integer m_externalId;

	/**
	 * International education area code.
	 *
	 * @see #getIntlAreaCode()
	 * @see #setIntlAreaCode(Short)
	 */
	private Short m_intlAreaCode;

	/**
	 * International education course code.
	 *
	 * @see #getIntlCourseCode()
	 * @see #setIntlCourseCode(Short)
	 */
	private Short m_intlCourseCode;

	@Nullable
	@Contract(pure = true)
	public Integer getExternalId() {
		return m_externalId;
	}

	public void setExternalId(@Nullable final Integer externalId) {
		m_externalId = externalId;
	}

	@Nullable
	@Contract(pure = true)
	public Short getIntlAreaCode() {
		return m_intlAreaCode;
	}

	public void setIntlAreaCode(@Nullable final Short intlAreaCode) {
		m_intlAreaCode = intlAreaCode;
	}

	@Nullable
	@Contract(pure = true)
	public Short getIntlCourseCode() {
		return m_intlCourseCode;
	}

	public void setIntlCourseCode(@Nullable final Short intlCourseCode) {
		m_intlCourseCode = intlCourseCode;
	}
}
