package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Objects;

import java.nio.charset.StandardCharsets;

import java.security.MessageDigest;

import javax.mail.internet.InternetAddress;

import autist.util.DigestUtil;

@SuppressWarnings("unused")
public class User extends ProvidedEntity<AccountStatus> {
	/**
	 * Password salt length in bytes.
	 */
	public static final int SALT_LENGTH = 16;

	/**
	 * E-mail address.
	 *
	 * @see #getEmail()
	 * @see #setEmail(InternetAddress)
	 */
	private InternetAddress m_email;

	/**
	 * Password hashsum (Base64).
	 *
	 * @see #getPasswordDigest()
	 * @see #setPasswordDigest(byte[])
	 */
	private byte[] m_passwordDigest;

	/**
	 * Password salt (Base64).
	 *
	 * @see #getPasswordSalt()
	 * @see #setPasswordSalt(byte[])
	 */
	private byte[] m_passwordSalt;

	/**
	 * Family name.
	 *
	 * @see #getFamilyName()
	 * @see #setFamilyName(String)
	 */
	private String m_familyName;

	/**
	 * Other names.
	 *
	 * @see #getOtherNames()
	 * @see #setOtherNames(String)
	 */
	private String m_otherNames;

	/**
	 * Position.
	 *
	 * @see #getPosition()
	 * @see #setPosition(String)
	 */
	private String m_position;

	@Nullable
	@Contract(pure = true)
	public InternetAddress getEmail() {
		return m_email;
	}

	public void setEmail(@Nullable final InternetAddress email) {
		m_email = email;
	}

	@Nullable
	@Contract(pure = true)
	public byte[] getPasswordDigest() {
		return m_passwordDigest;
	}

	public void setPasswordDigest(@Nullable final byte[] passwordDigest) {
		m_passwordDigest = passwordDigest;
	}

	@Nullable
	@Contract(pure = true)
	public byte[] getPasswordSalt() {
		return m_passwordSalt;
	}

	public void setPasswordSalt(@Nullable final byte[] passwordSalt) {
		m_passwordSalt = passwordSalt;
	}

	@Nullable
	@Contract(pure = true)
	public String getFamilyName() {
		return m_familyName;
	}

	public void setFamilyName(@Nullable final String familyName) {
		m_familyName = familyName;
	}

	@Nullable
	@Contract(pure = true)
	public String getOtherNames() {
		return m_otherNames;
	}

	public void setOtherNames(@Nullable final String otherNames) {
		m_otherNames = otherNames;
	}

	@Nullable
	@Contract(pure = true)
	public String getPosition() {
		return m_position;
	}

	public void setPosition(@Nullable final String position) {
		m_position = position;
	}

	public static void setPassword(
			final @NotNull User user,
			final @NotNull String password)
	{
		Objects.requireNonNull(user);
		Objects.requireNonNull(password);

		final byte[] salt = DigestUtil.generateSecureRandom(SALT_LENGTH);

		user.setPasswordDigest(computePasswordDigest(password, salt));
		user.setPasswordSalt(salt);
	}

	public static boolean checkPassword(
			final @NotNull User user,
			final @NotNull String password)
	{
		Objects.requireNonNull(user);
		Objects.requireNonNull(password);

		if (user.getPasswordDigest() == null)
			return false;
		if (user.getPasswordSalt() == null)
			return false;

		final byte[] passwordDigest = computePasswordDigest(
				password, user.getPasswordSalt());

		return Arrays.equals(passwordDigest, user.getPasswordDigest());
	}

	private static byte[] computePasswordDigest(
			final @NotNull String password,
			final @NotNull byte[] salt)
	{
		Objects.requireNonNull(password);
		Objects.requireNonNull(salt);

		final MessageDigest digest = DigestUtil.getDigestInstance();

		digest.update(password.getBytes(StandardCharsets.UTF_8));

		final byte[] rawDigest = digest.digest();

		digest.reset();
		digest.update(rawDigest);
		digest.update(salt);

		return digest.digest();
	}
}
