package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import autist.data.model.ChildEntity;
import autist.data.model.CoreEntity;

@SuppressWarnings("unused")
public class Media<P extends CoreEntity>
		extends CoreEntity<Void>
		implements ChildEntity<P>
{
	/**
	 * Parent entity.
	 *
	 * @see #getParent()
	 * @see #setParent(P)
	 */
	private P m_parent;

	/**
	 * Parent entity identifier.
	 *
	 * @see #getParentId()
	 * @see #setParentId(Long)
	 */
	private Long m_parentId;

	/**
	 * Type.
	 * @see #getType()
	 * @see #setType(MediaType)
	 */
	private MediaType m_type;

	/**
	 * MIME type.
	 *
	 * @see #getMimeType()
	 * @see #setMimeType(MimeType)
	 */
	private MimeType m_mimeType;

	/**
	 * MIME type identifier.
	 *
	 * @see #getMimeTypeId()
	 * @see #setMimeTypeId(Long)
	 */
	private Long m_mimeTypeId;

	/**
	 * Size in bytes.
	 *
	 * @see #getSize()
	 * @see #setSize(Long)
	 */
	private Long m_size;

	@Override
	@Nullable
	@Contract(pure = true)
	public P getParent() {
		return m_parent;
	}

	@Override
	public void setParent(@Nullable final P parent) {
		m_parent = parent;
	}

	@Override
	@Nullable
	@Contract(pure = true)
	public Long getParentId() {
		return m_parentId;
	}

	@Override
	public void setParentId(@Nullable final Long parentId) {
		m_parentId = parentId;
	}

	@Nullable
	@Contract(pure = true)
	public MediaType getType() {
		return m_type;
	}

	public void setType(@Nullable final MediaType type) {
		m_type = type;
	}

	@Nullable
	@Contract(pure = true)
	public MimeType getMimeType() {
		return m_mimeType;
	}

	public void setMimeType(@Nullable final MimeType mimeType) {
		m_mimeType = mimeType;
	}

	@Nullable
	@Contract(pure = true)
	public Long getMimeTypeId() {
		return m_mimeTypeId;
	}

	public void setMimeTypeId(@Nullable final Long mimeTypeId) {
		m_mimeTypeId = mimeTypeId;
	}

	@Nullable
	@Contract(pure = true)
	public Long getSize() {
		return m_size;
	}

	public void setSize(@Nullable final Long size) {
		m_size = size;
	}
}
