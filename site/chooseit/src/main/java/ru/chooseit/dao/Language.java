package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import autist.data.model.NamedEntity;
import autist.data.model.OrderedEntity;

@SuppressWarnings("unused")
public class Language extends NamedEntity implements OrderedEntity {
	/**
	 * ISO 639 2-letter alphabetic code pattern.
	 */
	public static final String ISO_ALPHA2_PATTERN = "^[a-zA-Z]{2}$";

	/**
	 * ISO 639 3-letter alphabetic code pattern.
	 */
	public static final String ISO_ALPHA3_PATTERN = "^[a-zA-Z]{3}$";

	/**
	 * ISO 639 2-letter alphabetic code.
	 *
	 * @see #getIsoAlpha2()
	 * @see #setIsoAlpha2(String)
	 */
	private String m_isoAlpha2;

	/**
	 * ISO 639 3-letter alphabetic code.
	 *
	 * @see #getIsoAlpha3()
	 * @see #setIsoAlpha3(String)
	 */
	private String m_isoAlpha3;

	/**
	 * Display priority.
	 *
	 * @see #getPriority()
	 * @see #setPriority(Short)
	 */
	private Short m_priority;

	@Nullable
	@Contract(pure = true)
	public String getIsoAlpha2() {
		return m_isoAlpha2;
	}

	public void setIsoAlpha2(@Nullable final String isoAlpha2) {
		m_isoAlpha2 = isoAlpha2;
	}

	@Nullable
	@Contract(pure = true)
	public String getIsoAlpha3() {
		return m_isoAlpha3;
	}

	public void setIsoAlpha3(@Nullable final String isoAlpha3) {
		m_isoAlpha3 = isoAlpha3;
	}

	@Override
	@Nullable
	@Contract(pure = true)
	public Short getPriority() {
		return m_priority;
	}

	@Override
	public void setPriority(@Nullable final Short priority) {
		m_priority = priority;
	}
}
