package ru.chooseit.dao;

import org.jetbrains.annotations.NotNull;

import java.util.ResourceBundle;
import autist.util.ResourceUtil;

public enum MediaType {
	AVATAR,
	DOCUMENT;

	private static final ResourceBundle resourceBundle =
			ResourceUtil.getResourceBundle(MediaType.class);

	@Override
	@NotNull
	public String toString() {
		return resourceBundle.getString(name());
	}
}
