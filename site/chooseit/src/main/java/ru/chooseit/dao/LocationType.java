package ru.chooseit.dao;

import org.jetbrains.annotations.NotNull;

import java.util.ResourceBundle;
import autist.util.ResourceUtil;

public enum LocationType {
	COUNTRY,
	STATE,
	REPUBLIC,
	DISTRICT,
	AREA,
	REGION,
	CITY,
	STREET;

	private static final ResourceBundle resourceBundle =
			ResourceUtil.getResourceBundle(LocationType.class);

	@Override
	@NotNull
	public String toString() {
		return resourceBundle.getString(name());
	}
}
