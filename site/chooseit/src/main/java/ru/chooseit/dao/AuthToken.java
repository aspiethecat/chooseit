package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

public class AuthToken extends AuthEntity {
	/**
	 * Default expiration time in seconds.
	 */
	public static final long DEFAULT_EXPIRATION_TIME = 86400L * 365;

	/**
	 * Selector length in bytes.
	 */
	public static final int SELECTOR_LENGTH = 32;

	/**
	 * Validator length in bytes.
	 */
	public static final int VALIDATOR_LENGTH = 32;

	/**
	 * Validator digest (SHA-256).
	 *
	 * @see #getValidatorDigest()
	 * @see #setValidatorDigest(byte[])
	 */
	private byte[] m_validatorDigest;

	@Nullable
	@Contract(pure = true)
	public byte[] getValidatorDigest() {
		return m_validatorDigest;
	}

	public void setValidatorDigest(@Nullable final byte[] validatorDigest) {
		m_validatorDigest = validatorDigest;
	}
}
