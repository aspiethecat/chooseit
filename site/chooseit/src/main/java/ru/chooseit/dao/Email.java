package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import autist.data.model.ItemEntity;

@SuppressWarnings("unused")
public class Email extends ItemEntity {
	/**
	 * E-mail address.
	 *
	 * @see #getEmail()
	 * @see #setEmail(String)
	 */
	private String m_email;

	/**
	 * Short description.
	 *
	 * @see #getDescription()
	 * @see #setDescription(String)
	 */
	private String m_description;

	@Nullable
	@Contract(pure = true)
	public String getEmail() {
		return m_email;
	}

	public void setEmail(@Nullable final String email) {
		m_email = email;
	}

	@Nullable
	@Contract(pure = true)
	public String getDescription() {
		return m_description;
	}

	public void setDescription(@Nullable final String description) {
		m_description = description;
	}
}
