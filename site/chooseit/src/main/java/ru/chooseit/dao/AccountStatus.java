package ru.chooseit.dao;

import org.jetbrains.annotations.NotNull;

import java.util.ResourceBundle;
import autist.util.ResourceUtil;

public enum AccountStatus {
	NEW,
	ACTIVE,
	BLOCKED,
	DELETED;

	private static final ResourceBundle resourceBundle =
			ResourceUtil.getResourceBundle(AccountStatus.class);

	@NotNull
	@Override
	public String toString() {
		return resourceBundle.getString(name());
	}
}
