package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings("unused")
public class EducationalContent<T extends EducationalContent<T>>
		extends PaidContent<T>
{
	/**
	 * Duration (academic hours).
	 *
	 * @see #getTotalHours()
	 * @see #setTotalHours(Short)
	 */
	private Short m_totalHours;

	/**
	 * Duration (weeks).
	 *
	 * @see #getTotalWeeks()
	 * @see #setTotalWeeks(Short)
	 */
	private Short m_totalWeeks;

	@Nullable
	@Contract(pure = true)
	public Short getTotalHours() {
		return m_totalHours;
	}

	public void setTotalHours(@Nullable final Short totalHours) {
		m_totalHours = totalHours;
	}

	@Nullable
	@Contract(pure = true)
	public Short getTotalWeeks() {
		return m_totalWeeks;
	}

	public void setTotalWeeks(@Nullable final Short totalWeeks) {
		m_totalWeeks = totalWeeks;
	}
}
