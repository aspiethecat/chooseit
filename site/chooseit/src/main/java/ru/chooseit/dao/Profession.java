package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import autist.data.model.TreeDictionaryEntity;

@SuppressWarnings("unused")
public class Profession extends TreeDictionaryEntity<Profession> {
	/**
	 * Assigned specializations.
	 *
	 * @see #getSpecializations()
	 * @see #setSpecializations(List)
	 */
	private List<Specialization> m_specializations = new ArrayList<>();

	@Nullable
	@Contract(pure = true)
	public List<Specialization> getSpecializations() {
		return m_specializations;
	}

	public void setSpecializations(@Nullable final List<Specialization> specializations) {
		m_specializations = specializations;
	}
}
