package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@SuppressWarnings("unused")
public class University extends Provider<University, University> {
	/**
	 * Type.
	 *
	 * @see #getType()
	 * @see #setType(UniversityType)
	 */
	private UniversityType m_type;

	/**
	 * Type identifier.
	 *
	 * @see #getTypeId()
	 * @see #setTypeId(Long)
	 */
	private Long m_typeId;

	/**
	 * Full name.
	 *
	 * @see #getFullName()
	 * @see #setFullName(String)
	 */
	private String m_fullName;

	/**
	 * Military facility existence flag.
	 *
	 * @see #getMilitary()
	 * @see #setMilitary(Boolean)
	 */
	private Boolean m_military;

	/**
	 * Residental facility existence flag.
	 *
	 * @see #getResidental()
	 * @see #setResidental(Boolean)
	 */
	private Boolean m_residental;

	/**
	 * Grade.
	 *
	 * @see #getGrade()
	 * @see #setGrade(Short)
	 */
	private Short m_grade;

	/**
	 * Teaching languages.
	 *
	 * @see #getLanguages()
	 * @see #setLanguages(List)
	 */
	private List<Language> m_languages;

	/**
	 * Specializations.
	 *
	 * @see #getSpecializations()
	 * @see #setSpecializations(List)
	 */
	private List<Specialization> m_specializations;

	@Nullable
	@Contract(pure = true)
	public UniversityType getType() {
		return m_type;
	}

	public void setType(@Nullable final UniversityType type) {
		m_type = type;
	}

	@Nullable
	@Contract(pure = true)
	public Long getTypeId() {
		return m_typeId;
	}

	public void setTypeId(@Nullable final Long typeId) {
		m_typeId = typeId;
	}

	@Nullable
	@Contract(pure = true)
	public String getFullName() {
		return m_fullName;
	}

	public void setFullName(@Nullable final String fullName) {
		m_fullName = fullName;
	}

	@Nullable
	@Contract(pure = true)
	public Boolean getMilitary() {
		return m_military;
	}

	public void setMilitary(@Nullable final Boolean military) {
		m_military = military;
	}

	@Nullable
	@Contract(pure = true)
	public Boolean getResidental() {
		return m_residental;
	}

	public void setResidental(@Nullable final Boolean residental) {
		m_residental = residental;
	}

	@Nullable
	@Contract(pure = true)
	public Short getGrade() {
		return m_grade;
	}

	public void setGrade(@Nullable final Short grade) {
		m_grade = grade;
	}

	@Nullable
	@Contract(pure = true)
	public List<Language> getLanguages() {
		return m_languages;
	}

	public void setLanguages(@Nullable final List<Language> languages) {
		m_languages = languages;
	}

	@Nullable
	@Contract(pure = true)
	public List<Specialization> getSpecializations() {
		return m_specializations;
	}

	public void setSpecializations(@Nullable final List<Specialization> specializations) {
		m_specializations = specializations;
	}
}
