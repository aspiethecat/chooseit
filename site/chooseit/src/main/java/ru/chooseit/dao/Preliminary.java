package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;

@SuppressWarnings("unused")
public class Preliminary extends EducationalContent<Preliminary> {
	/**
	 * Start date.
	 *
	 * @see #getStartDate()
	 * @see #setStartDate(LocalDate)
	 */
	private LocalDate m_startDate;

	@Nullable
	@Contract(pure = true)
	public LocalDate getStartDate() {
		return m_startDate;
	}

	public void setStartDate(@Nullable final LocalDate startDate) {
		m_startDate = startDate;
	}
}
