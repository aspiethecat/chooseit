package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.net.URI;
import java.util.List;

import autist.data.model.CoreEntity;
import autist.data.model.TreeEntity;

@SuppressWarnings("unused")
public class Provider<
		T extends Provider<T, P>,
		P extends Provider<P, ?>>
		extends CoreEntity<AccountStatus>
		implements TreeEntity<T, P>
{
	/**
	 * Parent provider.
	 *
	 * @see #getParent()
	 * @see #setParent(Provider)
	 */
	private P m_parent;

	/**
	 * Parent provider identifier.
	 *
	 * @see #getParentId()
	 * @see #setParentId(Long)
	 */
	private Long m_parentId;

	/**
	 * Children count.
	 *
	 * @see #getChildrenCount()
	 * @see #setChildrenCount(Integer)
	 */
	private Integer m_childrenCount;

	/**
	 * Web-site URL.
	 *
	 * @see #getSiteUrl()
	 * @see #setSiteUrl(URI)
	 */
	private URI m_siteUrl;

	/**
	 * Video presentation URL.
	 *
	 * @see #getVideoUrl()
	 * @see #setVideoUrl(URI)
	 */
	private URI m_videoUrl;

	/**
	 * Location.
	 *
	 * @see #getLocation()
	 * @see #setLocation(Location)
	 */
	private Location m_location;

	/**
	 * Location identifier.
	 *
	 * @see #getLocationId()
	 * @see #setLocationId(Long)
	 */
	private Long m_locationId;

	/**
	 * Primary e-mail address.
	 *
	 * @see #getPrimaryEmail()
	 * @see #setPrimaryEmail(String)
	 */
	private String m_primaryEmail;

	/**
	 * Primary phone number.
	 *
	 * @see #getPrimaryPhoneNumber()
	 * @see #setPrimaryPhoneNumber(Long)
	 */
	private Long m_primaryPhoneNumber;

	/**
	 * Primary phone extension number.
	 *
	 * @see #getPrimaryPhoneNumberExt()
	 * @see #setPrimaryPhoneNumberExt(Short)
	 */
	private Short m_primaryPhoneNumberExt;

	/**
	 * Street address.
	 *
	 * @see #getAddress()
	 * @see #setAddress(String)
	 */
	private String m_address;

	/**
	 * ZIP code.
	 *
	 * @see #getZipCode()
	 * @see #setZipCode(Integer)
	 */
	private Integer m_zipCode;

	/**
	 * Phone numbers.
	 *
	 * @see #getPhones()
	 * @see #setPhones(List)
	 */
	private List<Phone> m_phones;

	/**
	 * E-mail addresses.
	 *
	 * @see #getEmails()
	 * @see #setEmails(List)
	 */
	private List<Email> m_emails;

	/**
	 * Related tags.
	 *
	 * @see #getTags()
	 * @see #setTags(List)
	 */
	private List<Tag> m_tags;

	/**
	 * Rating score.
	 *
	 * @see #getRating()
	 * @see #setRating(Float)
	 */
	private Float m_rating;

	@Override
	@Nullable
	@Contract(pure = true)
	public P getParent() {
		return m_parent;
	}

	@Override
	public void setParent(@Nullable final P parent) {
		m_parent = parent;
	}

	@Override
	@Nullable
	@Contract(pure = true)
	public Long getParentId() {
		return m_parentId;
	}

	@Override
	public void setParentId(@Nullable final Long parentId) {
		m_parentId = parentId;
	}

	@Override
	@Nullable
	@Contract(pure = true)
	public Integer getChildrenCount() {
		return m_childrenCount;
	}

	@Override
	public void setChildrenCount(@Nullable final Integer childrenCount) {
		m_childrenCount = childrenCount;
	}

	@Nullable
	@Contract(pure = true)
	public URI getSiteUrl() {
		return m_siteUrl;
	}

	public void setSiteUrl(@Nullable final URI siteUrl) {
		m_siteUrl = siteUrl;
	}

	@Nullable
	@Contract(pure = true)
	public URI getVideoUrl() {
		return m_videoUrl;
	}

	public void setVideoUrl(@Nullable final URI videoUrl) {
		m_videoUrl = videoUrl;
	}

	@Nullable
	@Contract(pure = true)
	public Location getLocation() {
		return m_location;
	}

	public void setLocation(@Nullable final Location location) {
		m_location = location;
	}

	@Nullable
	@Contract(pure = true)
	public Long getLocationId() {
		return m_locationId;
	}

	public void setLocationId(@Nullable final Long locationId) {
		m_locationId = locationId;
	}

	@Nullable
	@Contract(pure = true)
	public String getPrimaryEmail() {
		return m_primaryEmail;
	}

	public void setPrimaryEmail(@Nullable final String primaryEmail) {
		m_primaryEmail = primaryEmail;
	}

	@Nullable
	@Contract(pure = true)
	public Long getPrimaryPhoneNumber() {
		return m_primaryPhoneNumber;
	}

	public void setPrimaryPhoneNumber(@Nullable final Long primaryPhoneNumber) {
		m_primaryPhoneNumber = primaryPhoneNumber;
	}

	@Nullable
	@Contract(pure = true)
	public Short getPrimaryPhoneNumberExt() {
		return m_primaryPhoneNumberExt;
	}

	public void setPrimaryPhoneNumberExt(@Nullable final Short primaryPhoneNumberExt) {
		m_primaryPhoneNumberExt = primaryPhoneNumberExt;
	}

	@Nullable
	@Contract(pure = true)
	public String getAddress() {
		return m_address;
	}

	public void setAddress(@Nullable final String address) {
		m_address = address;
	}

	@Nullable
	@Contract(pure = true)
	public Integer getZipCode() {
		return m_zipCode;
	}

	public void setZipCode(@Nullable final Integer zipCode) {
		m_zipCode = zipCode;
	}

	@Nullable
	@Contract(pure = true)
	public List<Phone> getPhones() {
		return m_phones;
	}

	public void setPhones(@Nullable final List<Phone> phones) {
		m_phones = phones;
	}

	@Nullable
	@Contract(pure = true)
	public List<Email> getEmails() {
		return m_emails;
	}

	public void setEmails(@Nullable final List<Email> emails) {
		m_emails = emails;
	}

	@Nullable
	@Contract(pure = true)
	public List<Tag> getTags() {
		return m_tags;
	}

	public void setTags(@Nullable final List<Tag> tags) {
		m_tags = tags;
	}

	@Nullable
	@Contract(pure = true)
	public Float getRating() {
		return m_rating;
	}

	public void setRating(@Nullable final Float rating) {
		m_rating = rating;
	}
}
