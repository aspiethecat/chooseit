package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import autist.data.model.NamedEntity;
import autist.data.model.OrderedEntity;

@SuppressWarnings("unused")
public class Currency extends NamedEntity implements OrderedEntity {
	/**
	 * ISO 4217 alphabetic code pattern.
	 */
	public static final String ISO_ALPHA_PATTERN = "^[a-zA-Z]{3}$";

	/**
	 * ISO 4217 numeric code digits count.
	 */
	public static final int ISO_NUMERIC_DIGITS = 3;

	/**
	 * Minimal ISO 4217 numeric code value.
	 */
	public static final long ISO_NUMERIC_MIN = 0;

	/**
	 * Minimal number of fractional digits.
	 */
	public static final long PRECISION_MIN = 0;

	/**
	 * ISO 4217 alphabetic code.
	 *
	 * @see #getIsoAlpha()
	 * @see #setIsoAlpha(String)
	 */
	private String m_isoAlpha;

	/**
	 * ISO 4217 numeric code.
	 *
	 * @see #getIsoNumeric()
	 * @see #setIsoNumeric(Short)
	 */
	private Short m_isoNumeric;

	/**
	 * Number of fractional digits.
	 *
	 * @see #getPrecision()
	 * @see #setPrecision(Short)
	 */
	private Short m_precision;

	/**
	 * Display priority.
	 *
	 * @see #getPriority()
	 * @see #setPriority(Short)
	 */
	private Short m_priority;

	/**
	 * Icon name.
	 *
	 * @see #getIcon()
	 * @see #setIcon(String)
	 */
	private String m_icon;

	@Nullable
	@Contract(pure = true)
	public String getIsoAlpha() {
		return m_isoAlpha;
	}

	public void setIsoAlpha(@Nullable final String isoAlpha) {
		m_isoAlpha = isoAlpha;
	}

	@Nullable
	@Contract(pure = true)
	public Short getIsoNumeric() {
		return m_isoNumeric;
	}

	public void setIsoNumeric(@Nullable final Short isoNumeric) {
		m_isoNumeric = isoNumeric;
	}

	@Nullable
	@Contract(pure = true)
	public Short getPrecision() {
		return m_precision;
	}

	public void setPrecision(@Nullable final Short precision) {
		m_precision = precision;
	}

	@Nullable
	@Contract(pure = true)
	public String getIcon() {
		return m_icon;
	}

	public void setIcon(@Nullable final String icon) {
		m_icon = icon;
	}

	@Override
	@Nullable
	@Contract(pure = true)
	public Short getPriority() {
		return m_priority;
	}

	@Override
	public void setPriority(@Nullable final Short priority) {
		m_priority = priority;
	}
}
