package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings("unused")
public class Course extends EducationalContent<Course> {
	/**
	 * Internal certificate issue flag.
	 *
	 * @see #getIntCertificate()
	 * @see #setIntCertificate(Boolean)
	 */
	private Boolean m_intCertificate;

	/**
	 * External certificate issue flag.
	 *
	 * @see #getExtCertificate()
	 * @see #setExtCertificate(Boolean)
	 */
	private Boolean m_extCertificate;

	@Nullable
	@Contract(pure = true)
	public Boolean getIntCertificate() {
		return m_intCertificate;
	}

	public void setIntCertificate(@Nullable final Boolean intCertificate) {
		m_intCertificate = intCertificate;
	}

	@Nullable
	@Contract(pure = true)
	public Boolean getExtCertificate() {
		return m_extCertificate;
	}

	public void setExtCertificate(@Nullable final Boolean extCertificate) {
		m_extCertificate = extCertificate;
	}
}
