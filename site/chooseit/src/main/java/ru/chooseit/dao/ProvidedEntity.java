package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import autist.data.model.CoreEntity;

@SuppressWarnings("unused")
public class ProvidedEntity<S> extends CoreEntity<S> {
	/**
	 * Provider.
	 *
	 * @see #getProvider()
	 * @see #setProvider(GenericProvider)
	 */
	private GenericProvider m_provider;

	/**
	 * Provider identifier.
	 *
	 * @see #getProviderId()
	 * @see #setProviderId(Long)
	 */
	private Long m_providerId;

	@Nullable
	@Contract(pure = true)
	public GenericProvider getProvider() {
		return m_provider;
	}

	public void setProvider(@Nullable final GenericProvider provider) {
		m_provider = provider;
	}

	@Nullable
	@Contract(pure = true)
	public Long getProviderId() {
		return m_providerId;
	}

	public void setProviderId(@Nullable final Long providerId) {
		m_providerId = providerId;
	}
}
