package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;
import java.time.LocalTime;

@SuppressWarnings("unused")
public class Event extends PaidContent<Event> {
	/**
	 * Type.
	 *
	 * @see #getType()
	 * @see #setType(EventType)
	 */
	private EventType m_type;

	/**
	 * Type identifier.
	 *
	 * @see #getTypeId()
	 * @see #setTypeId(Long)
	 */
	private Long m_typeId;

	/**
	 * Location.
	 *
	 * @see #getLocation()
	 * @see #setLocation(Location)
	 */
	private Location m_location;

	/**
	 * Location identifier.
	 *
	 * @see #getLocationId()
	 * @see #setLocationId(Long)
	 */
	private Long m_locationId;

	/**
	 * Start date.
	 *
	 * @see #getStartDate()
	 * @see #setStartDate(LocalDate)
	 */
	private LocalDate m_startDate;

	/**
	 * Start time.
	 *
	 * @see #getStartTime()
	 * @see #setStartTime(LocalTime)
	 */
	private LocalTime m_startTime;

	/**
	 * End date.
	 *
	 * @see #getEndDate()
	 * @see #setEndDate(LocalDate)
	 */
	private LocalDate m_endDate;

	/**
	 * Online access flag.
	 *
	 * @see #getOnline()
	 * @see #setOnline(Boolean)
	 */
	private Boolean m_online;

	@Nullable
	@Contract(pure = true)
	public EventType getType() {
		return m_type;
	}

	public void setType(@Nullable final EventType type) {
		m_type = type;
	}

	@Nullable
	@Contract(pure = true)
	public Long getTypeId() {
		return m_typeId;
	}

	public void setTypeId(@Nullable final Long typeId) {
		m_typeId = typeId;
	}

	@Nullable
	@Contract(pure = true)
	public Location getLocation() {
		return m_location;
	}

	public void setLocation(@Nullable final Location location) {
		m_location = location;
	}

	@Nullable
	@Contract(pure = true)
	public Long getLocationId() {
		return m_locationId;
	}

	public void setLocationId(@Nullable final Long locationId) {
		m_locationId = locationId;
	}

	@Nullable
	@Contract(pure = true)
	public LocalDate getStartDate() {
		return m_startDate;
	}

	public void setStartDate(@Nullable final LocalDate startDate) {
		m_startDate = startDate;
	}

	@Nullable
	@Contract(pure = true)
	public LocalTime getStartTime() {
		return m_startTime;
	}

	public void setStartTime(@Nullable final LocalTime startTime) {
		m_startTime = startTime;
	}

	@Nullable
	@Contract(pure = true)
	public LocalDate getEndDate() {
		return m_endDate;
	}

	public void setEndDate(@Nullable final LocalDate endDate) {
		m_endDate = endDate;
	}

	@Nullable
	@Contract(pure = true)
	public Boolean getOnline() {
		return m_online;
	}

	public void setOnline(@Nullable final Boolean online) {
		m_online = online;
	}
}
