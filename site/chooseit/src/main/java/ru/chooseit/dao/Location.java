package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import autist.data.model.NamedEntity;
import autist.data.model.TreeEntity;

@SuppressWarnings("unused")
public class Location
		extends NamedEntity
		implements TreeEntity<Location, Location>
{
	/**
	 * External identifier digits count.
	 */
	public static final int EXTERNAL_ID_DIGITS = 13;

	/**
	 * Minimal external identifier value.
	 */
	public static final long EXTERNAL_ID_MIN = 100000000000L;

	/**
	 * External identifier format string.
	 */
	public static final String EXTERNAL_ID_FORMAT =
			"00,000,000,000,00;; groupingSeparator='.' decimalSeparator=','";

	/**
	 * Parent location.
	 *
	 * @see #getParent()
	 * @see #setParent(Location)
	 */
	private Location m_parent;

	/**
	 * Parent location identifier.
	 *
	 * @see #getParentId()
	 * @see #setParentId(Long)
	 */
	private Long m_parentId;

	/**
	 * Children count.
	 *
	 * @see #getChildrenCount()
	 * @see #setChildrenCount(Integer)
	 */
	private Integer m_childrenCount;

	/**
	 * Type.
	 *
	 * @see #getType()
	 * @see #setType(LocationType)
	 */
	private LocationType m_type;

	/**
	 * External reference identifier.
	 *
	 * @see #getExternalId()
	 * @see #setExternalId(Long)
	 */
	private Long m_externalId;

	/**
	 * ZIP code.
	 *
	 * @see #getZipCode()
	 * @see #setZipCode(Integer)
	 */
	private Integer m_zipCode;

	@Override
	@Nullable
	@Contract(pure = true)
	public Location getParent() {
		return m_parent;
	}

	@Override
	public void setParent(@Nullable final Location parent) {
		m_parent = parent;
	}

	@Override
	@Nullable
	@Contract(pure = true)
	public Long getParentId() {
		return m_parentId;
	}

	@Override
	public void setParentId(@Nullable final Long parentId) {
		m_parentId = parentId;
	}

	@Override
	@Nullable
	@Contract(pure = true)
	public Integer getChildrenCount() {
		return m_childrenCount;
	}

	@Override
	public void setChildrenCount(@Nullable final Integer childrenCount) {
		m_childrenCount = childrenCount;
	}

	@Nullable
	@Contract(pure = true)
	public LocationType getType() {
		return m_type;
	}

	public void setType(@Nullable final LocationType type) {
		m_type = type;
	}

	@Nullable
	@Contract(pure = true)
	public Long getExternalId() {
		return m_externalId;
	}

	public void setExternalId(@Nullable final Long externalId) {
		m_externalId = externalId;
	}

	@Nullable
	@Contract(pure = true)
	public Integer getZipCode() {
		return m_zipCode;
	}

	public void setZipCode(@Nullable final Integer zipCode) {
		m_zipCode = zipCode;
	}
}
