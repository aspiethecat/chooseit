package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import autist.data.model.ItemEntity;

@SuppressWarnings("unused")
public class Phone extends ItemEntity {
	/**
	 * Phone number.
	 *
	 * @see #getNumber()
	 * @see #setNumber(Long)
	 */
	private Long m_number;

	/**
	 * Phone number extension.
	 *
	 * @see #getNumberExt()
	 * @see #setNumberExt(Short)
	 */
	private Short m_numberExt;

	/**
	 * Short description.
	 *
	 * @see #getDescription()
	 * @see #setDescription(String)
	 */
	private String m_description;

	@Nullable
	@Contract(pure = true)
	public Long getNumber() {
		return m_number;
	}

	public void setNumber(@Nullable final Long number) {
		m_number = number;
	}

	@Nullable
	@Contract(pure = true)
	public Short getNumberExt() {
		return m_numberExt;
	}

	public void setNumberExt(@Nullable final Short numberExt) {
		m_numberExt = numberExt;
	}

	@Nullable
	@Contract(pure = true)
	public String getDescription() {
		return m_description;
	}

	public void setDescription(@Nullable final String description) {
		m_description = description;
	}
}
