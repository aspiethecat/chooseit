package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.net.URI;

@SuppressWarnings("unused")
public class Content<T extends Content<T>>
		extends ProvidedEntity<ContentStatus>
{
	/**
	 * Provider name.
	 *
	 * @see #getProviderName()
	 * @see #setProviderName(String)
	 */
	private String m_providerName;

	/**
	 * Provider web-site URL.
	 *
	 * @see #getProviderUrl()
	 * @see #setProviderUrl(URI)
	 */
	private URI m_providerUrl;

	/**
	 * URL.
	 *
	 * @see #getUrl()
	 * @see #setUrl(URI)
	 */
	private URI m_url;

	/**
	 * Related professions.
	 *
	 * @see #getProfessions()
	 * @see #setProfessions(List)
	 */
	private List<Profession> m_professions;

	/**
	 * Tags.
	 *
	 * @see #getTags()
	 * @see #setTags(List)
	 */
	private List<Tag> m_tags;

	@Nullable
	@Contract(pure = true)
	public String getProviderName() {
		return m_providerName;
	}

	public void setProviderName(@Nullable final String providerName) {
		m_providerName = providerName;
	}

	@Nullable
	@Contract(pure = true)
	public URI getProviderUrl() {
		return m_providerUrl;
	}

	public void setProviderUrl(@Nullable final URI providerUrl) {
		m_providerUrl = providerUrl;
	}

	@Nullable
	@Contract(pure = true)
	public URI getUrl() {
		return m_url;
	}

	public void setUrl(@Nullable final URI url) {
		m_url = url;
	}

	@Nullable
	@Contract(pure = true)
	public List<Profession> getProfessions() {
		return m_professions;
	}

	public void setProfessions(@Nullable List<Profession> professions) {
		m_professions = professions;
	}

	@Nullable
	@Contract(pure = true)
	public List<Tag> getTags() {
		return m_tags;
	}

	public void setTags(@Nullable final List<Tag> tags) {
		m_tags = tags;
	}
}
