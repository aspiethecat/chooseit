package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;

@SuppressWarnings("unused")
public class PaidContent<T extends PaidContent<T>>
		extends Content<T>
{
	/**
	 * Price.
	 *
	 * @see #getPrice()
	 * @see #setPrice(BigDecimal)
	 */
	private BigDecimal m_price;

	/**
	 * Currency.
	 *
	 * @see #getCurrency()
	 * @see #setCurrency(Currency)
	 */
	private Currency m_currency;

	/**
	 * Currency identifier.
	 *
	 * @see #getCurrencyId()
	 * @see #setCurrencyId(Long)
	 */
	private Long m_currencyId;

	@Nullable
	@Contract(pure = true)
	public BigDecimal getPrice() {
		return m_price;
	}

	public void setPrice(@Nullable final BigDecimal price) {
		m_price = price;
	}

	@Nullable
	@Contract(pure = true)
	public Currency getCurrency() {
		return m_currency;
	}

	public void setCurrency(@Nullable final Currency currency) {
		m_currency = currency;
	}

	@Nullable
	@Contract(pure = true)
	public Long getCurrencyId() {
		return m_currencyId;
	}

	public void setCurrencyId(@Nullable final Long currencyId) {
		m_currencyId = currencyId;
	}
}
