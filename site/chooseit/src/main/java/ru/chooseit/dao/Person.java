package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings("unused")
public class Person extends Provider<Person, GenericProvider> {
	/**
	 * Position.
	 *
	 * @see #getPosition()
	 * @see #setPosition(String)
	 */
	private String m_position;

	@Nullable
	@Contract(pure = true)
	public String getPosition() {
		return m_position;
	}

	public void setPosition(@Nullable final String position) {
		m_position = position;
	}
}
