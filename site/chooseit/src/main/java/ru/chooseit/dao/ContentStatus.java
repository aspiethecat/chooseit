package ru.chooseit.dao;

import org.jetbrains.annotations.NotNull;

import java.util.ResourceBundle;
import autist.util.ResourceUtil;

public enum ContentStatus {
	DRAFT,
	NEW,
	PUBLISHED,
	BLOCKED,
	DELETED;

	private static final ResourceBundle resourceBundle =
			ResourceUtil.getResourceBundle(ContentStatus.class);

	@Override
	@NotNull
	public String toString() {
		return resourceBundle.getString(name());
	}
}
