package ru.chooseit.dao;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.net.InetAddress;
import java.time.LocalDateTime;

import autist.data.model.SimpleEntity;

public class AuthEntity extends SimpleEntity {
	/**
	 * Extended identifier.
	 *
	 * @see #getExtendedId()
	 * @see #setExtendedId(byte[])
	 */
	private byte[] m_extendedId;

	/**
	 * Creation date and time (UTC).
	 *
	 * @see #getCreationTime()
	 * @see #setCreationTime(LocalDateTime)
	 */
	private LocalDateTime m_creationTime = null;

	/**
	 * Expiration timeout in seconds.
	 *
	 * @see #getExpirationTime()
	 * @see #setExpirationTime(Long)
	 */
	private Long m_expirationTime = null;

	/**
	 * User identifier.
	 *
	 * @see #getUserId()
	 * @see #setUserId(Long)
	 */
	private Long m_userId;

	/**
	 * User.
	 *
	 * @see #getUser()
	 * @see #setUser(User)
	 */
	private User m_user;

	/**
	 * IP address.
	 *
	 * @see #getIpAddress()
	 * @see #setIpAddress(InetAddress)
	 */
	private InetAddress m_ipAddress;

	@Nullable
	@Contract(pure = true)
	public byte[] getExtendedId() {
		return m_extendedId;
	}

	public void setExtendedId(@Nullable final byte[] extendedId) {
		m_extendedId = extendedId;
	}

	@Nullable
	@Contract(pure = true)
	public LocalDateTime getCreationTime() {
		return m_creationTime;
	}

	public void setCreationTime(@Nullable final LocalDateTime creationTime) {
		m_creationTime = creationTime;
	}

	@Nullable
	@Contract(pure = true)
	public Long getExpirationTime() {
		return m_expirationTime;
	}

	public void setExpirationTime(@Nullable final Long expirationTime) {
		m_expirationTime = expirationTime;
	}

	@Nullable
	@Contract(pure = true)
	public Long getUserId() {
		return m_userId;
	}

	public void setUserId(@Nullable final Long userId) {
		m_userId = userId;
	}

	@Nullable
	@Contract(pure = true)
	public User getUser() {
		return m_user;
	}

	public void setUser(@Nullable final User user) {
		m_user = user;
	}

	@Nullable
	@Contract(pure = true)
	public InetAddress getIpAddress() {
		return m_ipAddress;
	}

	public void setIpAddress(@Nullable final InetAddress ipAddress) {
		m_ipAddress = ipAddress;
	}
}
