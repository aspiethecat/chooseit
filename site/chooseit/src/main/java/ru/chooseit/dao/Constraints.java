package ru.chooseit.dao;

@SuppressWarnings("unused")
public class Constraints {
	/**
	 * Date format string.
	 */
	public static final String DATE_FORMAT = "dd.MM.yyyy";

	/**
	 * Time format string.
	 */
	public static final String TIME_FORMAT = "HH:mm";

	/**
	 * Price integer digits count.
	 */
	public static final int PRICE_DIGITS = 12;

	/**
	 * Price fractional digits count.
	 */
	public static final int PRICE_PRECISION = 2;

	/**
	 * Minimum price value.
	 */
	public static final long PRICE_MIN = 0;

	/**
	 * Price format string.
	 */
	public static final String PRICE_FORMAT =
			"000,000,000,000.00;; groupingSeparator=',' decimalSeparator='.'";

	/**
	 * Phone number format.
	 */
	public static final String PHONE_NUMBER_FORMAT =
			"000,000,00,00;; groupingSeparator='-' decimalSeparator=','";

	/**
	 * ZIP code digits count.
	 */
	public static final int ZIP_CODE_DIGITS = 6;

	/**
	 * Minimum ZIP code value.
	 */
	public static final long ZIP_CODE_MIN = 0;

	/**
	 * Duration digits count (academic hours).
	 */
	public static final int TOTAL_HOURS_DIGITS = 4;

	/**
	 * Minimal duration value (academic hours).
	 */
	public static final long TOTAL_HOURS_MIN = 1;

	/**
	 * Duration digits count (weeks).
	 */
	public static final int TOTAL_WEEKS_DIGITS = 3;

	/**
	 * Minimal duration value (weeks).
	 */
	public static final long TOTAL_WEEKS_MIN = 1;

	/**
	 * Display priority digits count.
	 */
	public static final int PRIORITY_DIGITS = 5;

	/**
	 * Minimal display priority value.
	 */
	public static final long PRIORITY_MIN = 0;
}
