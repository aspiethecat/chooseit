package ru.chooseit;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public enum ActionNodeType {
	WEBAPP_ROOT("webapp-root"),
	ADMIN_ROOT("admin-root");

	private final String m_id;

	ActionNodeType(final @NotNull String id) {
		m_id = Objects.requireNonNull(id);
	}

	@NotNull
	@Contract(pure = true)
	public final String getId() {
		return m_id;
	}
}
