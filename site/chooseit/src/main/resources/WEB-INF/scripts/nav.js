$(document).ready(function(){
	$(".nav-button, .menu-overlay").click(function() {
		$(".nav-button").toggleClass("cross");
		$("header").toggleClass("opened-menu");
		$('.site').toggleClass("move")
	});
	mobileMenu();
	$(window).resize(function(){
		mobileMenu();
	});
	$('.popup-bg').on('click', function(){
		$(this).parent('.popup').toggleClass('showmodal');
	});
	$(document).keyup(function(e) {
		if (e.keyCode == 27 && $('.showmodal').length) {
			$('.showmodal').toggleClass('showmodal');
		}
	});
});