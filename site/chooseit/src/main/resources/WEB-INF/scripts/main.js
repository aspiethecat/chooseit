$(document).ready(function(){
	$('#add-more-vids').click(function(){
		for (var i = 0; i < 4; i++) {
			$('.video-wrapper').eq(0).clone().appendTo('.video-grid');
		}
	});
	$('#add-more-unis').click(function(){
		var last_numb = $('#university .scrollbar ul li:last-child .number').text();
		var last_numb = parseInt(last_numb);
		last_numb += 1;
		for (var i = 0; i < 5; i++, last_numb++) {
			var clone = $('#university .scrollbar ul li:last-child');
			$('#university .scrollbar ul li:last-child').clone().insertAfter(clone);
			$('#university .scrollbar ul li:last-child').children('.number').text(last_numb);
		}
	});

	if ( ! Modernizr.objectfit ) {
		$('.video-container').objectfitFix();
	}
});