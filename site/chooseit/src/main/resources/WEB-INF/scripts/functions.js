$.fn.isInViewport = function() {
	var elementTop = $(this).offset().top;
	var elementBottom = elementTop + $(this).outerHeight();

	var viewportTop = $(window).scrollTop();
	var viewportBottom = viewportTop + $(window).height();

	return elementBottom > viewportTop && elementTop < viewportBottom;
};
$.fn.objectfitFix = function() {
	$(this).each(function () {
		var container = $(this),
			imgUrl = container.find('img').prop('src');
		if (imgUrl) {
			return container
				.css('backgroundImage', 'url(' + imgUrl + ')')
				.addClass('compat-object-fit');
		}  
	});
};
$.fn.openPopup = function() {
	if($(this).length) {
		$(this).toggleClass('showmodal');
		return false;
	}
}
function unCheck() {
	$('#master input[type=checkbox]').removeAttr('checked');
	$('#unselect-all').fadeOut();
}
function whichTransitionEvent(){
	var t;
	var el = document.createElement('fakeelement');
	var transitions = {
		'transition':'transitionend',
		'OTransition':'oTransitionEnd',
		'MozTransition':'transitionend',
		'WebkitTransition':'webkitTransitionEnd'
	}

	for(t in transitions){
		if( el.style[t] !== undefined ){
				return transitions[t];
		}
	}
}
function mobileMenu() {
	if ($(window).width() >= 1024) {
		if ($("header").hasClass("opened-menu")) {
			$("header").removeClass("opened-menu");
			$(".nav-button").removeClass("cross");
		}
	}
}
function popularH() {
	setTimeout(function() {
		var secH = $('#home #sectors').height();
		$('#home #popular').css({'padding-top': secH+'px'});
	}, 300);
}
$('input, select, textarea').on('focus',function() {
    var curScroll = $(window).scrollTop();
    $('html, body').animate({scrollTop:curScroll},1);
});