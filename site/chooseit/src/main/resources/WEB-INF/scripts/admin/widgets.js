var TComponent = (function() {
	var DATA_KEY = "component";

	function TComponent(typeClass, element) {
		if (this instanceof TComponent) {
			this.init(element);
			element.data(DATA_KEY, this);
			return this;
		}

		return new typeClass(element);
	}

	TComponent.prototype.init = function(element) {
		this.element = element;
	};

	TComponent.prototype.on = function(eventName, selector, handler) {
		if (typeof selector === 'function') {
			handler = selector;
			selector = undefined;
		}

		$(this.element).on(eventName, selector, $.proxy(handler, this));
	};

	TComponent.prototype.off = function(eventName, selector) {
		$(this.element).off(eventName, selector);
	};

	$.fn.component = function () {
		return this.data(DATA_KEY);
	};

	return TComponent;
})();

var TPage = (function() {
	function TPage(element) {
		return TComponent.call(this, TPage, element);
	}

	TPage.prototype = Object.create(TComponent.prototype);
	TPage.prototype.constructor = TPage;

	TPage.prototype.showMessage = function() {
	};

	return TPage;
})();

var TForm = (function() {
	var FIELD_TYPE_MAP = [];

	function TForm(element) {
		return TComponent.call(this, TForm, element);
	}

	TForm.prototype = Object.create(TComponent.prototype);
	TForm.prototype.constructor = TForm;

	TForm.registerFieldType = function(typeClass, selector) {
		FIELD_TYPE_MAP[selector] = typeClass;
		return typeClass;
	};

	TForm.prototype.init = function(element) {
		Object.getPrototypeOf(TForm.prototype).init(element);

		FIELD_TYPE_MAP.forEach(function(typeClass, selector) {
			$(selector, this.element).each(function() {
				typeClass(this);
			});
		});
	};

	return TForm;
})();

var TField = (function() {
	function TField(typeClass, element) {
		return TComponent.call(this, typeClass, element);
	}

	TField.prototype = Object.create(TComponent.prototype);
	TField.prototype.constructor = TField;

	TField.prototype.init = function (element, inputElement) {
		Object.getPrototypeOf(TField.prototype).init(element);

		this.inputElement = inputElement;
		this.initialValue = inputElement.val();
	};

	TField.prototype.isRequired = function() {
		return this.inputElement.attr('required') === 'required';
	};

	TField.prototype.isValid = function() {
		return this.valid;
	};

	TField.prototype.setValid = function() {
	};

	return TField;
})();

var TTextField = (function() {
	function TTextField(element) {
		return TField.call(this, TTextField, element);
	}

	TTextField.prototype = Object.create(TField.prototype);
	TTextField.prototype.constructor = TTextField;

	TTextField.prototype.init = function (element) {
		Object.getPrototypeOf(TTextField.prototype).init(element, element.children("input"));

		this.field = element.children("textarea");
		this.on("focus", this.inputElement, this.focusHandler);
	};

	TTextField.prototype.focusHandler = function() {

	};

	TTextField.prototype.inputHandler = function() {

	};

	TTextField.prototype.blurHandler = function() {
		this.off("blur", this.inputElement);
		this.off("input", this.inputElement);
	};

	return TForm.registerFieldType(TTextField, ".field-text");
})();

var TTextArea = (function() {
	function TTextArea(element) {
		return TField.call(this, TTextArea, element);
	}

	TTextArea.prototype = Object.create(TField.prototype);
	TTextArea.prototype.constructor = TTextArea;

	TTextArea.prototype.init = function(element) {
		Object.getPrototypeOf(TTextArea.prototype).init(element, element.children("input"));

		this.field = element.children("textarea");
		this.field.innerHeight(this.field.prop("scrollHeight"));

		this.on("input", this.inputElement, this.inputHandler);
	};

	TTextArea.prototype.inputHandler = function() {
		this.field.innerHeight(0);
		this.field.innerHeight(this.field.prop("scrollHeight"));
	};

	return TForm.registerFieldType(TTextArea, ".field-textarea");
})();

var TSelectField = (function() {
	function TSelectField(element) {
		return TField.call(this, TSelectField, element);
	}

	TSelectField.prototype = Object.create(TField.prototype);
	TSelectField.prototype.constructor = TSelectField;

	TSelectField.prototype.focusHandler = function() {
	};

	TSelectField.prototype.keyDownHandler = function() {
	};

	TSelectField.prototype.inputHandler = function() {
	};

	TSelectField.prototype.mouseDownHandler = function() {
	};

	TSelectField.prototype.blurHandler = function() {
	};

	return TForm.registerFieldType(TSelectField, ".field-select");
})();
