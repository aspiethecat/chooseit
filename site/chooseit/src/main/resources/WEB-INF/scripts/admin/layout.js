(function() {
	var globSplitRegExp = /\s+/g;
	var globWildcardRegExp = /[*?\\]/g;
	var globWildcardMap = {
		'*': '\\S*',
		'?': '\\S',
		'(': '\\(',
		')': '\\)',
		'\\': '\\\\'
	};

	var compileGlob = function (glob) {
		var globList = glob.trim().split(globSplitRegExp);

		var pattern = "";
		for (var i = 0; i < globList.length; i++) {
			var item = globList[i].replace(globWildcardRegExp, function(match) {
				return globWildcardMap[match];
			});
			if (i > 0)
				pattern += '|';
			pattern += '(' + item + ')';
		}

		return new RegExp('(^|\\s?)((' + pattern + ')($|\\s+))+', 'g');
	};

	$.fn.hasClassEx = function (glob) {
		var result = false;
		if (typeof glob === "string") {
			var regExp = compileGlob(glob);

			for (var i = 0; !result && (i < this.length); i++)
				result = result || regExp.test(this[i].className);
		}

		return result;
	};

	$.fn.removeClassEx = function (glob) {
		if (typeof glob === "string") {
			var regExp = compileGlob(glob);

			for (var i = 0; i < this.length; i++)
				this[i].className = this[i].className.replace(regExp, ' ').trim();
		}

		return this;
	};

	$.fn.switchClassEx = function (removeGlob, addClassName) {
		if ((typeof removeGlob === "string") && (typeof addClassName === "string")) {
			var regExp = compileGlob(removeGlob);
			for (var i = 0; i < this.length; i++) {
				this[i].className = this[i].className.replace(regExp, ' ').trim() + ' ' + addClassName;
			}
		}

		return this;
	};

	$.icon = function(iconName) {
		return $("<i></i>").attr({
			"class": "fa fa-" + iconName,
			"aria-hidden": "true"
		});
	};

	$.fn.icon = function (iconName) {
		for (var i = 0; i < this.length; i++) {
			if (/\bfa\b/.test(this[i].className))
				this.switchClassEx("fa-*", "fa-" + iconName);
		}

		return this;
	};
})();

function pushHistoryState(url) {
	history.pushState({ url: url }, null, url);
}

function replaceHistoryState(url) {
	history.replaceState({ url : url }, null, url);
}

function isMenuSectionVisible(target) {
	return target.hasClass("open");
}

function setMenuSectionVisible(target, visible) {
	var icon  = $("i.fa", target.children("a"));

	if (visible)
		target.addClass("open");
	else
		target.removeClass("open");

	icon.removeClass().addClass(visible ? 'fa fa-folder-open-o' : 'fa fa-folder-o');
}

function toggleMenuSection(target) {
	setMenuSectionVisible(target, !isMenuSectionVisible(target));
}

function showMenuSection(target) {
	setMenuSectionVisible(target, true);
}

function hideMenuSection(target) {
	setMenuSectionVisible(target, false);
}

function findMenuItem(url) {
	return $("#main-menu li li").has("a.action[href='" + url + "']");
}

function setActiveMenuItem(target) {
	$("#main-menu li.active").removeClass("active");
	target.addClass("active");
}

function getMenuSection(item) {
	return item.parent().closest("li");
}

function setupPageContent(target, handler) {
	if (!handler)
		return null;

	return handler();
}

function loadPageContent(target, url, handler) {
	target.load(url, function(response, status) {
		if (status !== "success") {
			alert("Failed to load page");
			return;
		}

		setupPageContent(target, handler);
	});

	return false;
}

function setupPage(url, pushState) {
	if (pushState)
		pushHistoryState(url);
	else
		replaceHistoryState(document.location.href);

	var menuItem = findMenuItem(url);

	setActiveMenuItem(menuItem);
	showMenuSection(getMenuSection(menuItem));
}

function loadPage(url, pushState) {
	return loadPageContent($("#page-container"), url, setupPage(url, pushState));
}

function loadStartPage() {
	return loadPage($("#page-container").data("action"), false);
}

function loadPreviousPage(state) {
	if (state === null)
		return null;

	return loadPage(state.url, false);
}

function source(x) {
	return x.prop("tagName") + "#" + x.prop("id") + "[class=" + x.attr("class") + "]";
}

window.Splitter = {
	rootElement     : null,
	splitterElement : null,
	ownerElement    : null,
	baseOffset      : 0,
	baseWidth       : 0,

	init : function(root) {
		Splitter.rootElement = root;

		root.on("mouseover", ".splitter, .splitter *", Splitter.onMouseOver);
		root.on("mousemove", ".splitter, .splitter *", Splitter.onMouseMove);
		root.on("mousedown", ".splitter, .splitter *", Splitter.onMouseDown);
	},

	updateHandlePosition : function(event) {
		var splitter = Splitter.splitterElement || $(event.target).closest(".splitter");

		if (!splitter)
			return;

		var offset   = splitter.offset();
		var position = event.pageY - offset.top;

		if (position < 0 || position > splitter.height())
			return;

		splitter.children("i").css("top", position + 'px');

		return false;
	},

	onMouseOver : function(event) {
		return Splitter.updateHandlePosition(event);
	},

	onMouseDown : function(event) {
		Splitter.splitterElement = $(event.target).closest(".splitter");
		Splitter.ownerElement    = Splitter.splitterElement.parent();
		Splitter.baseOffset      = event.pageX;
		Splitter.baseWidth       = Splitter.ownerElement.width();

		Splitter.rootElement.on("mouseup", Splitter.onMouseUp);
		Splitter.rootElement.on("mousemove", Splitter.onMouseMove);

		return false;
	},

	onMouseUp : function(event) {
		Splitter.rootElement.off("mouseup");
		Splitter.rootElement.off("mousemove");

		Splitter.splitterElement = null;
		Splitter.ownerElement    = null;
		Splitter.baseOffset      = 0;
		Splitter.baseWidth       = 0;

		return false;
	},

	onMouseMove : function(event) {
		Splitter.updateHandlePosition(event);

		if (Splitter.splitterElement !== null) {
			var width = Math.round(Splitter.baseWidth + event.pageX - Splitter.baseOffset);

			Splitter.ownerElement.css("width", width + 'px');
			Splitter.ownerElement.css("min-width", 'inherit');
			Splitter.ownerElement.css("max-width", 'inherit');
		}
	}
};

window.Navigator = {
	rootElement : null,
	actionMap   : { },

	init : function(root) {
		Navigator.rootElement = root;

		root.on("click", "a.action", Navigator.onAction);

		root.on("click", "a.handle", Navigator.onHandleClick);
		root.on("mouseover", "a.handle > i", Navigator.onHandleMouseOver);
		root.on("mouseleave", "a.handle > i", Navigator.onHandleMouseLeave);
	},

	bind : function(prefix, actions) {
		Navigator.actionMap = { };

		for (index = 0; index < actions.length; index++) {
			Navigator.actionMap[actions[index]] = prefix + "/" + actions[index];
		}

		loadPageContent(
			$("#page-nav-content", Navigator.rootElement),
			Navigator.actionMap['search']);
	},

	getItem : function(element) {
		return element.closest("li");
	},

	onItemCreate : function(event) {
		if (!Navigator.actionMap['new'])
			return false;

		loadPageContent(
			$("#page-body", Navigator.rootElement),
			Navigator.actionMap['new']);
	},

	onItemOpen : function(event) {
		var item = Navigator.getItem($(event.target));

		if (!Navigator.actionMap['fetch'])
			return false;

		loadPageContent(
			$("#page-body", Navigator.rootElement),
			Navigator.actionMap['fetch'] + "?id=" + item.data("id"));

		return false;
	},

	onItemSave : function(event) {
		if (!Navigator.actionMap['store'])
			return false;

		var action = $(this);
		var form =  $("#" + $(this).data("form"));

		action.toggleClass("disabled", true);
		Form.editable(form, false);

		$.ajax({
			type: 'POST',
			url: Navigator.actionMap['store'],
			data: form.serialize(),
			processData: false,
			dataType: 'json',
			complete: Navigator.onItemSaveCompleted,
			success: Navigator.onItemSaveSuccess
		}).fail(function(xhr, status, error) {
			Page.showMessage("error", "Ошибка при сохранении изменений: " + error);
		}).done(function(data, status) {
			if (data.status === true) {
				Page.showMessage("success", "Изменения успешно сохранены");
				return;
			}

			Page.showMessage(
				"error",
				data.statusMessage ? data.statusMessage : "Форма содержит ошибки");

			Form.bindErrors(form, data.errors);
		}).always(function() {
			action.toggleClass("disabled", false);
			Form.editable(form, true);
		});

		return false;
	},

	onItemClose : function(event) {
		var content = $("<div>").attr({
			"id": "page-body-content",
			"class": "page-content"
		});

		$("#page-body").empty();
		$("#page-body").append(content);

		return false;
	},

	onAction : function(event) {
		if ($(this).hasClass("disabled"))
			return false;

		var action = $(this).data("action");

		switch (action) {
			case "create":
				return Navigator.onItemCreate(event);

			case "open":
				return Navigator.onItemOpen(event);

			case "save":
				return Navigator.onItemSave.call($(this), event);

			case "close":
				return Navigator.onItemClose(event);

			case "hide":
				$(this).closest("a.action").parent().addClass("hide");
				break;

			case "filter":
				$('#page-nav-filters').toggleClass('open');
				break;

			case "section":
				return Navigator.onSectionClick.call($(this), event);

			case "toolbar":
				return Navigator.onToolbarClick.call($(this), event);
		}

		return false;
	},

	updateHandleIcon : function(event) {
		var handle = $(event.target);
		var item   = Navigator.getItem(handle);

		if (item.data("type") === "node") {
			handle.removeClass("fa-plus-square fa-plus-square-o fa-minus-square fa-minus-square-o");

			if (item.hasClass("open")) {
				handle.addClass((event.type === "mouseover") ? "fa-minus-square" : "fa-minus-square-o");
			} else {
				handle.addClass((event.type === "mouseover") ? "fa-plus-square" : "fa-plus-square-o");
			}
		} else {
			handle.removeClass((event.type === "mouseover") ? "fa-square-o" : "fa-square");
			handle.addClass((event.type === "mouseover") ? "fa-square" : "fa-square-o");
		}

		return false;
	},

	onHandleMouseOver : function(event) {
		return Navigator.updateHandleIcon(event);
	},

	onHandleMouseLeave : function(event) {
		return Navigator.updateHandleIcon(event);
	},

	onHandleClick : function(event) {
		var handle = $(this);
		var item   = Navigator.getItem(handle);

		if (item.data("type") !== "node")
			return false;

		var state   = item.data("state");
		var content = item.children("ul");
		var icon    = handle.children("i");

		if (state === "open") {
			content.empty();
			icon.removeClass("fa-minus-square fa-minus-square-o");
			icon.addClass("fa-plus-square");
		} else {
			icon.removeClass("fa-plus-square fa-plus-square-o");
			icon.addClass("fa-minus-square");
			loadPageContent(
				content,
				Navigator.actionMap['search'] + "?parentId=" + item.data("id"));
		}

		item.toggleClass("open");

		return false;
	},

	onSectionClick : function(event) {
		var handle  = $(this);
		var icon    = handle.children("i");
		var section = $("#" + handle.data("id"));

		if (section.hasClass("open")) {
			icon.removeClass("fa-chevron-circle-down");
			icon.addClass("fa-chevron-circle-right");
		} else {
			icon.removeClass("fa-chevron-circle-right");
			icon.addClass("fa-chevron-circle-down");
		}

		section.toggleClass("open");

		return false;
	},

	onToolbarClick : function(event) {
		var handle  = $(this);
		var toolbar = $("#" + handle.data("id"));

		toolbar.toggleClass("open");

		return false;
	}
};

var UIObject = {
	property : function(name, args) {
		if ((args !== undefined) && (args.length > 0)) {
			if (args[0] !== null)
				this.data(name, args[0]);
			else
				this.removeData(name);
		}
		return this.data(name);
	},

	id : function() { return this.attr("id"); }
};

var Page = {
	__proto__ : UIObject,

	id : function() { return this.attr("id"); },

	showMessage : function(type, message) {
		var icon;

		if (type === "error")
			icon = "warning";
		else if (type === "success")
			icon = "check";
		else
			icon = "info";

		$("#page-body-info").removeClass("info success error").addClass(type);
		$("#page-body-info > i").icon(icon);
		$("#page-body-info > span").text(message);

		$("#page-body-info").removeClass("hide");
	}
};

var Form = {
	__proto__ : UIObject,

	init : function(root) {
	},

	handle : function(obj) {
		return $.extend(obj, Form);
	},

	editable : function(obj, newState) {
		var currentState = obj.data("editable");

		if (typeof currentState !== "boolean") {
			currentState = true;
			obj.data("editable", currentState);
		}

		if (newState === undefined)
			return currentState;

		if (typeof newState !== "boolean")
			newState = !currentState;

		if (newState !== currentState) {
			obj.find("input, textarea").each(function() {
				var readonly;

				if (newState) {
					readonly = $(this).data("readonly");

					if (readonly !== undefined)
						$(this).attr("readonly", readonly);
					else
						$(this).removeAttr("readonly");

					$(this).removeData("readonly");

					if ($(this).is(":checkbox, :radio"))
						$(this).off("click");
				} else {
					readonly = $(this).attr("readonly");

					if (readonly === "readonly")
						$(this).data("readonly", readonly);
					else
						$(this).removeData("readonly");

					$(this).attr("readonly", "readonly");

					if ($(this).is(":checkbox, :radio")) {
						$(this).on("click", function () {
							return false;
						});
					}
				}
			});

			obj.data("editable", newState);
		}

		return obj;
	},

	bindErrors : function(form, errors) {
		for (var key in errors) {
			var input = form.find("input[name='" + key + "']");

			input.toggleClass("error", true);
			Field.handle(input).valid(false, errors[key]);
		}
	}
};

var Field = {
	__proto__ : UIObject,

	handle : function(obj) {
		return $.extend(obj, Field);
	},

	required : function() {
		return (this.attr("required") === "required");
	},

	valid : function(state, msg) {
		var nextSibling = this.next();

		var field;
		if (nextSibling.hasClass("field-body"))
			field = nextSibling.children("input");
		else
			field = this;

		if (state !== undefined) {
			var parent = this.parent();

			var error;
			if (parent.hasClass("field-body"))
				error = parent.nextAll(".error-text");
			else
				error = this.nextAll(".error-text");

			error.empty();
			field.removeClass("valid error");

			if (state === false) {
				field.addClass("error");

				if ((typeof msg === "string") && (msg.length > 0)) {
					error
						.append($.icon("exclamation-triangle"))
						.append(msg);
				}
			} else {
				if (state === true)
					field.addClass("valid");
			}

			return this;
		}

		if (field.hasClass("valid"))
			return true;
		if (field.hasClass("error"))
			return false;

		return null;
	},

	invalid : function() {
		return this.valid() === false;
	},

	initialValue : function(value) { return this.property("initialValue", arguments); }
};

var TextField = {
	__proto__ : Field,

	handle : function(obj) {
		return $.extend(obj, TextField);
	},

	init : function(root) {
		root.on("focus", "input.text", TextField.focusHandler);
	},

	focusHandler : function(event) {
		var field = TextField.handle($(event.target));

		if (field.initialValue() === undefined)
			field.initialValue(field.val());

		field.on("blur", TextField.blurHandler);
		field.on("input", TextField.inputHandler);

		if (field.valid() !== true)
			field.inputHandler(event);
	},

	inputHandler : function(event) {
		var field = TextField.handle($(event.target));

		var empty   = (field.val() === "");
		var changed = (field.val() !== field.initialValue());

		if (empty && field.required())
			field.valid(false);
		else if (changed)
			field.valid(field[0].checkValidity());
		else
			field.valid(null);
	},

	blurHandler : function(event) {
		var field = TextField.handle($(event.target));

		field.off("blur");
		field.off("input");
	}
};

var TextArea = {
	__proto__ : Field,

	handle : function(obj) {
		return $.extend(obj, TextArea);
	},

	init : function(root) {
		root.on("input", "textarea", TextArea.inputHandler);
	},

	setup : function() {
		var obj = $(this);
		obj.innerHeight(obj.prop("scrollHeight"));
	},

	inputHandler : function(event) {
		var field = TextArea.handle($(event.target));

		field.innerHeight(0);
		field.innerHeight(field.prop("scrollHeight"));
	}
};

var SelectField = {
	__proto__ : Field,

	target : function() { return $("#" + this.data("target")); },
	list   : function() { return $("#" + this.data("list")); },

	supplier : function() { return this.property("supplier"); },

	lastValidId    : function(value) { return this.property("lastValidId", arguments); },
	lastValidValue : function(value) { return this.property("lastValidValue", arguments); },

	bindList : function() {
		var list   = this.list();
		var offset = this.offset();

		list.data("field", this.id());
		list.css({
			"top":       offset.top + this.outerHeight() - 1,
			"left":      offset.left,
			"min-width": this.parent().outerWidth()
		});
	},

	bindListEvents : function(state) {
		var target = this.list().children();

		if (state)
			target.on("mousedown", SelectField.mouseDownHandler);
		else
			target.off("mousedown");
	},

	init : function(root) {
		root.on("focus", "input.select", SelectField.focusHandler);
		root.on("click", "input.select + .action", SelectField.toggleListHandler);
	},

	update : function(selectedOption) {
		var target = this.target();
		var found  = selectedOption && (selectedOption.length > 0);

		if (found) {
			if (!selectedOption.data("id"))
				found = false;
		}

		if (found) {
			this.val(selectedOption.text());
			target.val(selectedOption.data("id"));
		} else {
			target.val("");
		}

		var empty    = (this.val() === "");
		var changed  = (this.val() !== this.initialValue());

		if (empty ? this.required() : changed && !found)
			this.valid(false);
		else if (changed)
			this.valid(true);
		else
			this.valid(null);

		if (!this.invalid()) {
			this.lastValidId(this.target().val());
			this.lastValidValue(this.val());
		}
	},

	handle : function(obj) {
		return $.extend(obj, SelectField);
	},

	toggleListHandler : function(event) {
		var field = SelectField.handle(
			$(event.target).closest(".action").prev("input.select"));

		if (!field.is(":focus")) {
			field.focus();
			field.list().show();
		} else {
			field.blur();
		}

		return false;
	},

	focusHandler : function(event, forced) {
		var field = SelectField.handle($(event.target));

		if (forced || !Form.editable(field.closest("form")))
			return false;

		if (field.initialValue() === undefined)
			field.initialValue(field.val());

		if (!field.hasClass("error")) {
			field.lastValidId(field.target().val());
			field.lastValidValue(field.val());
		} else {
			field.lastValidId(null);
			field.lastValidValue(null);
		}

		field.bindList();

		field.on("blur",    SelectField.blurHandler);
		field.on("keydown", SelectField.keyDownHandler);

		if (field.supplier()) {
			field.on("input", SelectField.inputHandler);

			if (!field.valid())
				field.inputHandler(event);
		} else {
			field.bindListEvents(true);

			if (!field.valid())
				field.list().show();
		}

		return false;
	},

	keyDownHandler : function(event) {
		var field  = SelectField.handle($(event.target));
		var list   = field.list();
		var option = list.children(".selected");

		if (!option.length)
			option = null;

		if (event.keyCode === 13 /* ENTER */) {
			if (option) {
				field.update(option);
				option.removeClass("selected");
				option = null;
			}

			if (!field.hasClass("error"))
				list.hide();
		} else if (event.keyCode === 38 /* UP */) {
			if (option) {
				option.removeClass("selected");
				option = option.prev();
			} else if (!field.valid() || !field.supplier()) {
				option = list.children().last();
			}
		} else if (event.keyCode === 40 /* DOWN */) {
			if (option) {
				option.removeClass("selected");
				option = option.next();
			} else if (!field.valid() || !field.supplier()) {
				option = list.children().first();
			}
		} else if (event.keyCode === 27 /* ESC */) {
			if (!field.valid() && field.lastValidId() !== null) {
				field.target().val(field.lastValidId());
				field.val(field.lastValidValue());

				field.removeClass("error");
				if (field.val() !== field.initialValue())
					field.addClass("valid");
			}

			option = null;
			list.hide();
		} else {
			return;
		}

		if (option && option.length && !option.hasClass("error")) {
			option.addClass("selected");
			list.show();
		}

		return false;
	},

	inputHandler : function(event) {
		var field = SelectField.handle($(event.target));

		var value  = field.val();
		var list   = field.list();

		$.getJSON(field.supplier(), { filter: value }, function(result) {
			var options = [];
			var found   = null;

			for (var index = 0; index < result.length; index++) {
				var item = $("<div />", {
					data: {
						id: result[index].id,
						depth: result[index].depth
					}
				}).css(
					"padding-left", result[index].depth ? 0.5 + result[index].depth * 1.5 + "rem" : "0.5rem"
				).append(result[index].name);

				if (value === result[index].name)
					found = item;

				options.push(item);
			}

			list.empty();

			if (options.length > 0) {
				list.append(options);
				field.bindListEvents(true);
			} else {
				list.append($("<div />", { "class": "error" }).append("Ничего не найдено"));
			}

			field.update(found);
			list.toggle(!found || options.length > 1);
		});

		return false;
	},

	mouseDownHandler : function(event) {
		var option = $(event.target);

		if (option.hasClass("error"))
			return false;

		var list  = option.parent();
		var field = SelectField.handle($("#" + list.data("field")));

		field.update(option);
		field.trigger("focus", true);

		list.hide();

		return false;
	},

	blurHandler : function(event) {
		var field = SelectField.handle($(event.target));
		var list  = field.list();

		field.off("blur");
		field.off("keydown");
		field.off("input");

		list.hide();

		if (field.supplier())
			list.empty();
		else
			field.bindListEvents(false);

		return false;
	}
};

function loginError(message) {
	$("#login-error > span").text(message);
	$("#login-error").removeClass("hide");
}

function login() {
	var action = $(this);
	var form   = $("#login");

	action.toggleClass("disabled", true);
	Form.editable(form, false);

	$.ajax({
		type: 'POST',
		url: form.attr("action"),
		data: form.serialize(),
		processData: false,
		dataType: 'json',
	}).fail(function(xhr, status, error) {
		loginError("Ошибка: " + error);
	}).done(function(data, status) {
		if (data.status === true)
			return window.location.replace('/admin/dashboard');

		loginError(data.statusMessage ? data.statusMessage : "Форма содержит ошибки");
		Form.bindErrors(form, data.errors);
	}).always(function() {
		action.toggleClass("disabled", false);
		Form.editable(form, true);
	});

	return false;
}

function setupLayout() {
	var uiRoot = $("#wrapper");

	Splitter.init(uiRoot);
	Navigator.init(uiRoot);
	TextField.init(uiRoot);
	TextArea.init(uiRoot);
	SelectField.init(uiRoot);

	if ($("body").attr("id") !== "login-container") {
		$("#main-menu a.action-group").click(function() {
			toggleMenuSection($(this).closest("li"));
		});
		$("#main-menu a.action").click(function() {
			setActiveMenuItem($(this).closest("li"));
			return loadPage($(this).attr("href"), true);
		});

		$("#wrapper").on("submit", "form", function () {
			return false;
		});

		$(window).on('popstate', function (event) {
			return loadPreviousPage(event.originalEvent.state);
		});

		return loadStartPage();
	}

	return false;
}

$(document).ready(setupLayout);
