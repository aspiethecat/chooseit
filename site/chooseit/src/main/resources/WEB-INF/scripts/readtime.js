$(document).ready(function(){
	$.fn.readTime = function() {
		var etaTarget = $('.eta'),
			container = $(this),
			txt = container.text(),
			charCount = txt.length;
			wordCount = txt.trim().replace(/[\s]+/g, " ").split(" ").length;
			wpsAverage = 3.75;


		setTimeout(function(){
			$('#go-next-btn').show();
		}, ((wordCount/wpsAverage)*1000));

		etaTarget.html( "Предполагаемое время чтения "+((wordCount/3.75) / 60).toFixed(2) + " минут");
	};
});