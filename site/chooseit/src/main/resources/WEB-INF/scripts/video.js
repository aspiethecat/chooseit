/*
 * Made by Tiago Valente on 12/09/2017 13:00
 * updated on 13:30
 * v0.2
 */
$(document).ready(function(){
	$.fn.calculateVid = function() {
		var parent = $(this),
			video = parent.find('video');

		video.on('timeupdate', function() {
			var current = video[0].currentTime
				time = parseInt(current,10),
				duration = video[0].duration,
				position = 100 * current / duration;

			if (position >= 95) {
				$('#go-next-btn').show();
				$('.right-side-c h2').text(video.duration);
			};
		});
	};
	$.fn.customVideoSkin = function() {
		/*
		 * Variables
		 */
		var parent = $(this),
			video = parent.find('video'),
			fullscreenbtn = parent.find('.fullscreen'),
			countdown,
			timeDrag = false,
			volumeDrag = false,
			updateTime = function(x) {
				var progress = parent.find('.progressbar'),
					maxduration = video[0].duration,
					position = x - progress.offset().left,
					percentage = 100 * position / progress.width();

				if(percentage > 100) {
					percentage = 100;
				}
				if(percentage < 0) {
					percentage = 0;
				}
				parent.find('.timebar').css('width', percentage+'%');
				video[0].currentTime = maxduration * percentage / 100;
			},
			updateVolume = function(x) {
				var volume = parent.find('.volumebar'),
					position = x - volume.offset().left,
					percentage = 100 * position / volume.width();

				if(percentage > 100) {
					percentage = 100;
				}
				if(percentage < 0) {
					percentage = 0;
				}
				parent.find('.volume').css('width', percentage+'%');
				video[0].volume = percentage / 100;
				if (parent.find('.volume').width() == 0) {
					parent.find('.muted .fa-volume-up').hide();
					parent.find('.muted .fa-volume-off').show();
				} else {
					parent.find('.muted .fa-volume-off').hide();
					parent.find('.muted .fa-volume-up').show();
				}
			},
			startBuffer = function() {
				var maxduration = video[0].duration,
					currentBuffer = video[0].buffered.end(0),
					percentage = 100 * currentBuffer / maxduration;

				parent.find('.bufferBar').css('width', percentage+'%');
				if(currentBuffer < maxduration) {
					setTimeout(startBuffer, 500);
				}
			};
		/*
		 * Starts the timeout of checking how much of the video has been loaded
		 * used timeout because chrome is buggy with progress event
		 */
		setTimeout(startBuffer, 500);


		/*
		 * All click events
		 * - play & pause button
		 * - mute button
		 * - volume control
		 */
		parent.find('.play-btn, video').click(function(){
			if (video[0].paused) { 
				video[0].play();
				parent.find('.play-btn i').toggle();
			} else {
				video[0].pause();
				parent.find('.play-btn i').toggle();
			}
			return false;
		});

		parent.find('.muted').click(function() {
			video[0].muted = !video[0].muted;
			parent.find('.muted i').toggle();
			if (parent.find('.volume').width() == 0) {
				parent.find('.volume').css('width', (video[0].volume * 100)+'%');
			} else {
				parent.find('.volume').css('width', '0%');
			}
			return false;
		});



		/*
		 * All mousedown events
		 * - progress bar drag
		 */
		parent.find('.progressbar').mousedown(function(e) {
			timeDrag = true;
			updateTime(e.pageX);
		});
		parent.find('.volumebar').mousedown(function(e) {
			volumeDrag = true;
			updateVolume(e.pageX);
		});


		/*
		 * All mousemove events
		 * - progress bar drag event listener
		 */
		$(document).mousemove(function(e) {
			if(timeDrag) { updateTime(e.pageX); }
		});
		$(document).mousemove(function(e) {
			if(volumeDrag) { updateVolume(e.pageX); }
		});


		/*
		 * All mouseup events
		 * - progress bar end of drag event listener
		 */
		$(document).mouseup(function(e) {
			if(timeDrag) {
				timeDrag = false;
				updateTime(e.pageX);
			}
		});
		$(document).mouseup(function(e) {
			if(volumeDrag) {
				volumeDrag = false;
				updateVolume(e.pageX);
			}
		});


		/*
		 * All video event listeners
		 * - show duration in a mins and secs format
		 * - show current playback time in a mins and secs format
		 */
		video.on('loadedmetadata', function() {
			var duration = video[0].duration,
				time = parseInt(duration,10);
			time = time < 0 ? 0 : time;

			var mins = Math.floor(time / 60),
				secs = time % 60;

			mins = mins < 10 ? "0"+mins : mins;
			secs = secs < 10 ? "0"+secs : secs;

			parent.find('.duration').text(mins+':'+secs);
		});

		video.on('timeupdate', function() {
			var current = video[0].currentTime
				time = parseInt(current,10),
				duration = video[0].duration,
				percentage = 100 * current / duration;

			time = time < 0 ? 0 : time;

			var mins = Math.floor(time / 60),
				secs = time % 60;

			mins = mins < 10 ? "0"+mins : mins;
			secs = secs < 10 ? "0"+secs : secs;

			parent.find('.current').text(mins+':'+secs);
			parent.find('.timebar').css('width', percentage+'%');
		});

		video.on('playing', function() {
			parent.find("video").mouseenter(function() {
				clearTimeout(countdown);
				parent.find('.large').fadeIn();
			})
			parent.find("video").mouseleave(function() {
				countdown = setTimeout(function() {
					parent.find('.large').fadeOut();
				}, 800);
			});
		});

		$('.fullscreen').click(function() {
			fullscreen();
		});
		function fullscreen() {
			if (video.requestFullscreen) {
				video.requestFullscreen();
			} else if (video.mozRequestFullScreen) {
				video.mozRequestFullScreen();
			} else if (video.webkitRequestFullscreen) {
				video.webkitRequestFullscreen();
			} else if (video.msRequestFullscreen) { 
				video.msRequestFullscreen();
			}
		}
	};
});