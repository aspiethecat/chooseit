new Application({
	rootElement: '#wrapper',

	rootPath: '/admin/',
	componentPath: '/admin/components',
	pagePath: '/admin/pages',

	preload: [
		'/component.js',

		'/controls/spacer.js',
		'/controls/label.js',
		'/controls/icon.js',
		'/controls/description.js',
		'/controls/action.js',
		'/controls/action-list.js',
		'/controls/header.js',
		'/controls/scrollable.js',
		'/controls/panel.js',
		'/controls/drawer.js',

		'/forms/form.js',
		'/forms/hidden.js',
		'/forms/field.js',
		'/forms/field-wrapper.js',
		'/forms/input.js',
		'/forms/checkbox.js',
		'/forms/select.js',

		'/views/layout.vue',
		'/views/page.vue'
	],

	routing: {
		mode: 'history',
		linkActiveClass: 'active',
		linkExactActiveClass: 'exact-active'
	},

	routingMap: {
		'/': {
			name: 'dashboard',
			component: '/dashboard.vue',
			meta: {
				label: 'Рабочий стол',
				icon: 'home'
			}
		},

		'/content': {
			name: 'content',
			component: '/content/index.vue',
			meta: {
				label: 'Аналитика',
				icon: 'books'
			},

			children: {
				'articles': {
					component: '/content/articles.vue',
					meta: {
						label: 'Статьи',
						icon: 'newspaper'
					}
				},

				'comments': {
					component: '/content/comments.vue',
					meta: {
						label: 'Комментарии',
						icon: 'comments'
					}
				},

				'categories': {
					component: '/content/categories.vue',
					meta: {
						label: 'Рубрикатор',
						icon: 'sitemap'
					}
				},
			}
		},

		'/events': {
			name: 'events',
			component: '/events/index.vue',
			meta: {
				label: 'События',
				icon: 'calendar-alt'
			}
		},

		'/education': {
			name: 'education',
			component: '/education/index.vue',
			meta: {
				label: 'Образование',
				icon: 'graduation-cap'
			},

			children: {
				'courses': {
					component: '/education/courses/index.vue',
					meta: {
						label: 'Курсы',
						icon: 'chalkboard'
					},

					children: {
						'new': {
							component: '/education/courses/details.vue',
							meta: {
								label: 'Новый курс',
								icon: 'chalkboard'
							}
						}
					}
				},

				'preliminary': {
					component: '/education/preliminary/index.vue',
					meta: {
						label: 'Довузовская подготовка',
						icon: 'bolt'
					},

					children: {
						'new': {
							component: '/education/preliminary/details.vue',
							meta: {
								label: 'Новая довузовская подготовка',
								icon: 'bolt'
							}
						}
					}
				},

				'additional': {
					component: '/education/additional/index.vue',
					meta: {
						label: 'Дополнительное образование',
						icon: 'award'
					},

					children: {
						'new': {
							component: '/education/additional/details.vue',
							meta: {
								label: 'Новое дополнительное образование',
								icon: 'award'
							}
						}
					}
				},

				'universities': {
					component: '/education/universities/index.vue',
					meta: {
						label: 'ВУЗы',
						icon: 'university'
					},

					children: {
						'new': {
							component: '/education/universities/details.vue',
							meta: {
								label: 'Новый ВУЗ',
								icon: 'bolt'
							}
						}
					}
				},

				'employers': {
					component: '/education/employers/index.vue',
					meta: {
						label: 'Компании',
						icon: 'building'
					},

					children: {
						'new': {
							component: '/education/employers/details.vue',
							meta: {
								label: 'Новая компания',
								icon: 'building'
							}
						}
					}
				},

				'persons': {
					component: '/education/persons/index.vue',
					meta: {
						label: 'Люди',
						icon: 'user-graduate'
					},

					children: {
						'new': {
							component: '/education/persons/details.vue',
							meta: {
								label: 'Новый человек',
								icon: 'user-graduate'
							}
						}
					}
				},

				'professions': {
					component: '/education/professions/index.vue',
					meta: {
						label: 'Профессии',
						icon: 'briefcase'
					},

					children: {
						'new': {
							component: '/education/professions/details.vue',
							meta: {
								label: 'Новая профессия',
								icon: 'briefcase'
							}
						}
					}
				},

				'specializations': {
					component: '/education/specializations/index.vue',
					meta: {
						label: 'Специализации',
						icon: 'graduation-cap'
					},

					children: {
						'new': {
							component: '/education/specializations/details.vue',
							meta: {
								label: 'Новая специализация',
								icon: 'graduation-cap'
							}
						}
					}
				}
			}
		},

		'/advertising': {
			name: 'advertising',
			component: '/advertising/index.vue',
			meta: {
				label: 'Реклама',
				icon: 'bullhorn'
			}
		},

		'/users': {
			name: 'users',
			component: '/users/index.vue',
			meta: {
				label: 'Пользователи',
				icon: 'users'
			}
		},

		'/settings': {
			name: 'settings',
			component: '/settings/index.vue',
			meta: {
				label: 'Настройки',
				icon: 'wrench'
			},

			children: {
				'performance': {
					component: '/settings/performance.vue',
					meta: {
						label: 'Производительность',
						icon: 'stopwatch',
						description: 'Настройка параметров производительности'
					}
				},

				'security': {
					component: '/settings/security.vue',
					meta: {
						label: 'Безопасность',
						icon: 'key',
						description: 'Настройка параметров подсистемы аутентификации и авторизации'
					}
				},

				'currencies': {
					component: '/settings/currencies/index.vue',
					meta: {
						label: 'Валюты',
						icon: 'usd-circle',
						description: 'Редактирование справочника валют'
					},
					children: {
						'new': {
							component: '/settings/currencies/details.vue',
							meta: {
								label: 'Новая валюта',
								icon: 'usd-circle'
							}
						}
					}
				},

				'locations': {
					component: '/settings/locations/index.vue',
					meta: {
						label: 'География',
						icon: 'atlas',
						description: 'Редактирование справочника стран и населённых пунктов'
					},
					children: {
						'new': {
							component: '/settings/locations/details.vue',
							meta: {
								label: 'Новое место',
								icon: 'atlas'
							}
						}
					}
				},

				'languages': {
					component: '/settings/languages/index.vue',
					meta: {
						label: 'Языки',
						icon: 'language',
						description: 'Редактирование справочника языков преподавания'
					},
					children: {
						'new': {
							component: '/settings/languages/details.vue',
							meta: {
								label: 'Новый язык',
								icon: 'language'
							}
						}
					}
				}
			}
		},

		'/statistics': {
			component: '/statistics.vue',
			meta: {
				label: 'Статистика',
				icon: 'analytics'
			}
		},

		'/profile': {
			component: '/profile.vue',
			meta: {
				icon: 'user-circle'
			}
		},

		'/preferences': {
			component: '/preferences.vue',
			meta: {
				icon: 'cog'
			}
		},

		'/help': {
			component: '/help.vue',
			meta: {
				icon: 'question-circle'
			}
		}
	},

	data: {
		title: 'Выбираю IT',
		user: {
			name: 'Alice'
		}
	}
});
