module.exports = {
	name: 'v-panel',

	require: [
		'v-component'
	],

	extends: 'v-component',

	mixins: [
		Mixin.Label,
		Mixin.Icon,
		Mixin.Collapsible
	],

	props: {
		/** Collapsible panel flag */
		collapsible: {
			type: Boolean,
			required: false,
			default: true
		},
	},

	render(createElement) {
		let options = {
			class: ['control', 'panel']
		};

		let content = [
			createElement('v-header', {
				props: {
					icon: this._icon,
					label: this._label
				}
			}),

			this.$slots.default
		];

		return createElement('div', options, content);
	}
};
