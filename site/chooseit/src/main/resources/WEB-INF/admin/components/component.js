module.exports = {
	name: 'v-component',

	computed: {
		$id() {
			return this.id || (this.$options._componentTag + '-' + this._uid);
		},

		$archetype() {
			return {};
		}
	}
};
