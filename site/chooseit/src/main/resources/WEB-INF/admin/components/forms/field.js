module.exports = {
	name: 'v-field',

	require: [
		'v-component'
	],

	extends: 'v-component',

	props: {
		/** Field name */
		name: {
			type: String,
			required: true
		},

		/** Required field flag */
		required: {
			type: Boolean,
			required: false,
			default: false
		},

		/** Read-only field flag */
		readOnly: {
			type: Boolean,
			required: false,
			default: false
		},

		/** Disabled field flag */
		disabled: {
			type: Boolean,
			required: false,
			default: false
		},

		/** Field value */
		value: {
			type: String,
			required: false
		}
	}
};
