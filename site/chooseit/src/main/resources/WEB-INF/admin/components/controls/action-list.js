let Objects = require('Objects');
let Mixin = require('Mixin');

module.exports = {
	name: 'v-action-list',

	require: [
		'v-component',
		'v-action',
		'v-separator'
	],

	extends: 'v-component',

	mixins: [
		Mixin.ItemList
	],

	props: {
		layout: {
			type: Object,
			required: false
		}
	},

	render(createElement) {
		let options = {
			class: ['control', 'action-list', 'layout'],
			attrs: {
				draggable: false
			}
		};

		let props = {};
		if (Objects.isNonNull(this.layout))
			props.layout = this.layout;

		let content = [];
		for (let item of this.items) {
			if (!Objects.isEmpty(item)) {
				content.push(createElement('v-action', {
					props: Object.assign({}, props, item)
				}));
			} else {
				content.push(createElement('v-spacer'));
			}
		}

		return createElement('div', options, content);
	}
};
