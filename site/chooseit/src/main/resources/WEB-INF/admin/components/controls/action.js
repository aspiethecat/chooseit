let Objects = require('Objects');
let Mixin = require('Mixin');

module.exports = {
	name: 'v-action',

	require: [
		'v-component',
		'v-icon'
	],

	extends: 'v-component',

	mixins: [
		Mixin.Label,
		Mixin.Hint,
		Mixin.Icon,
		Mixin.Description
	],

	props: {
		/** Link URL */
		link: {
			type: String,
			required: false
		},

		/** Event identifier */
		event: {
			type: String,
			required: false
		},

		/** Parameters */
		params: {
			type: Object,
			required: false
		},

		/** Disabled action flag */
		disabled: {
			type: Boolean,
			required: false,
			default: false
		},

		/** Layout */
		layout: {
			type: Object,
			required: false
		}
	},

	defaultLayout: {
		icon: true,
		label: true,
		hint: true,
		description: false
	},

	computed: {
		$archetype() {
			if (Objects.isNull(this.link))
				return {};

			return this.$router.resolve(this.link).route.meta;
		}
	},

	methods: {
		_onClick() {
			if (Objects.isNonNull(this.event))
				this.$emit(this.event, this.params);
		},

		_enabled(key) {
			return ((this.layout != null) && (this.layout[key] === true)) || (this.$options.defaultLayout[key] === true);
		}
	},

	render(createElement) {
		let options = {
			class: [ 'control', 'action' ],
			attrs: {
				draggable: false
			}
		};

		if (this._enabled('hint'))
			options.attrs.title = this._hint;

		let element;
		if (this.link != null) {
			element = 'router-link';
			options.props = {
				to: {
					path: this.link,
					params: this.params
				}
			};
		} else {
			element = 'a';
			options.attrs.href = '#';
			options.on = {
				click: this._onClick
			};
		}

		let content = [];

		if (this._enabled('icon')) {
			let icon = this._icon;
			if (Objects.isNonNull(icon)) {
				content.push(createElement('v-icon', {
					class: 'icon',
					props: icon
				}));
			}
		}

		if (this._enabled('label')) {
			let label = this._label;
			if (Objects.isNonNull(label)) {
				content.push(createElement('v-label', {
					class: 'label',
					props: {
						text: label
					}
				}));
			}
		}

		if (this._enabled('description')) {
			let description = this._description;
			if (Objects.isNonNull(description)) {
				content.push(createElement('v-description', {
					class: 'description',
					props: {
						text: description
					}
				}));
			}
		}

		return createElement(element, options, content);
	}
};
