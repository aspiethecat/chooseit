let Objects = require('Objects');
let Mixin   = require('Mixin');

module.exports = {
	name: 'v-select',

	require: [
		'v-field'
	],

	extends: 'v-field',

	mixins: [
		Mixin.Label
	],

	props: {
		/** Placeholder text */
		placeholder: {
			type: String,
			required: false
		},

		/** Field value identifier */
		valueId: {
			type: String,
			required: false
		}
	},

	render(createElement) {
		let content = [];

		if (Objects.isNonNull(this._label)) {
			content.push(createElement('label', {
				class: {
					required: this.required
				},
				attrs: {
					for: this.$id
				}
			}, [ this._label + ':' ]));
		}

		content.push(
			createElement('div', { class: 'input-wrapper' }, [
				createElement('input', {
					attrs: {
						id: this.$id,
						name: this.name,
						type: 'text',
						placeholder: this.placeholder
					}
				})
			]),
			createElement('span', { class: 'error' }));

		return createElement('v-field-wrapper', {}, content);
	}
};
