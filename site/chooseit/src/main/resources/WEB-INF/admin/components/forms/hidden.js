module.exports = {
	name: 'v-hidden',

	require: [
		'v-component'
	],

	extends: 'v-component',

	props: {
		/** Field name */
		name: {
			type: String,
			required: true
		},

		/** Field value */
		value: {
			type: String,
			required: false
		}
	},

	render(createElement) {
		return createElement('input', {
			attrs: {
				type: 'hidden',
				name: this.name,
				value: this.value
			}
		});
	}
};
