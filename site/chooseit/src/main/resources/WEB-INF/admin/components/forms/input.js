let Objects = require('Objects');
let Mixin   = require('Mixin');
let Cleave  = require('Cleave');

module.exports = {
	name: 'v-input',

	require: [
		'v-field'
	],

	extends: 'v-field',

	mixins: [
		Mixin.Label
	],

	props: {
		/** Data type */
		type: {
			type: String,
			required: false,
			default: 'text'
		},

		/** Maximum length */
		maxLength: {
			type: Number,
			required: false
		},

		/** Data pattern */
		pattern: {
			type: String,
			required: false
		},

		/** Placeholder text */
		placeholder: {
			type: String,
			required: false
		},

		/** Validation constraints */
		constraints: {
			type: Object,
			required: false
		}
	},

	archetypes: {
		/** Phone number field */
		phone: {
			pattern: '^\\d{3}-\\d{3}-\\d{2}-\\d{2}$',
			placeholder: '###-###-##-##',
			maxLength: 13,

			constraints: {
				blocks: [3, 3, 2, 2],
				delimiter: '-',
				numericOnly: true
			}
		},

		/** E-mail address field */
		email: {
			placeholder: 'name@example.com'
		},

		/** URL field */
		url: {
			placeholder: 'http://example.com/'
		},

		/** Date field */
		date: {
			pattern: '^\\d{2}\\.\\d{2}\\.\\d{4}$',
			placeholder: 'DD.MM.YYYY',
			maxLength: 10,

			constraints: {
				date: true,
				datePattern: ["d", "m", "Y"],
				delimiter: '.'
			}
		},

		/** Time field */
		time: {
			pattern: '^\\d{2}:\\d{2}$',
			placeholder: "HH:MM",
			maxLength: 5,

			constraints: {
				blocks: [2, 2],
				delimiter: ':',
				numericOnly: true
			}
		},

		/** Money field */
		money: {
			pattern: '^\\d{1,3}(,\\d{3})*(\\.\\d{1,2})?$',
			placeholder: '0.00',
			maxLength: 18,

			constraints: {
				numeral: true,
				numeralThousandsGroupStyle: 'thousand',
				numeralPositiveOnly: true,
				numeralIntegerScale: 12,
				numeralDecimalScale: 2,
				numeralDecimalMark: '.',
				delimiter: ',',
				stripLeadingZeroes: true
			}
		},

		/** ZIP code field */
		zip: {
			pattern: '^\\d{5,6}$',
			placeholder: '######',
			maxLength: 6,

			constraints: {
				numeral: true,
				numeralIntegerScale: 6,
				numeralDecimalScale: 0,
				numeralPositiveOnly: true,
				delimiter: '',
				stripLeadingZeroes: false
			}
		}
	},

	data() {
		return {
			_validator: null
		};
	},

	computed: {
		$archetype() {
			return this.$options.archetypes[this.type] || {};
		},

		_pattern() {
			return Objects.coalesce(this.pattern, this.$archetype.pattern);
		},

		_placeholder() {
			return Objects.coalesce(this.placeholder, this.$archetype.placeholder);
		},

		_maxLength() {
			return Objects.coalesce(this.maxLength, this.$archetype.maxLength);
		},

		_constraints() {
			return Objects.merge(this.constraints, this.$archetype.constraints);
		}
	},

	mounted() {
		if (Objects.isNull(this._constraints))
			return;

		this.$data._validator = new Cleave('#' + this.$id, this._constraints);
	},

	render(createElement) {
		let options = {
			class: 'control',
			attrs: {
				id: this.$id,
				name: this.name,
				type: this.type,
				placeholder: this._placeholder,
				pattern: this._pattern
			}
		};

		if (Objects.isNonNull(this._maxLength))
			options.attrs.maxlength = this._maxLength;

		if (this.required)
			options.attrs.required = 'required';
		if (this.readOnly)
			options.attrs.readonly = 'readonly';
		if (this.disabled)
			options.attrs.disabled = 'disabled';

		let content = [];

		if (Objects.isNonNull(this._label)) {
			content.push(createElement('label', {
				class: {
					required: this.required
				},
				attrs: {
					for: this.$id
				}
			}, [ this._label + ':' ]));
		}

		content.push(
			createElement('input', options),
			createElement('span', { class: 'error' })
		);

		return createElement('v-field-wrapper', {}, content);
	}
};
