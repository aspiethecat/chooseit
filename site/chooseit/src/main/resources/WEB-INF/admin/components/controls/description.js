module.exports = {
	name: 'v-description',

	props: {
		/** Description text */
		text: {
			type: String,
			required: true
		}
	},

	render(createElement) {
		return createElement('span', {}, [
			createElement('span', {}, this.text)
		]);
	}
};
