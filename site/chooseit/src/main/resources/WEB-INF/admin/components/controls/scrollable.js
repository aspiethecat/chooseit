module.exports = {
	name: 'v-scrollable',

	props: {
		/** Scrolling policy */
		policy: {
			type: String,
			required: false,
			default: 'vertical',
			validator: function(value) {
				return module.exports.props.policy.mapping.hasOwnProperty(value);
			},
			mapping: {
				horizontal: 'horizontal',
				vertical: 'vertical',
				both: null
			}
		}
	},

	computed: {
		_class: function() {
			return this.$options.props.policy.mapping[this.policy];
		}
	},

	render(createElement) {
		return createElement('div', {
			class: ['control', 'scrollable', 'layout', this._class]
		}, [
			this.$slots.default
		])
	}
};
