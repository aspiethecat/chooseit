let Mixin = require('Mixin');

module.exports = {
	name: 'v-drawer',

	require: [
		'v-component'
	],

	extends: 'v-component',

	mixins: [
		Mixin.Label,
		Mixin.Icon,
		Mixin.ItemList,
		Mixin.Collapsible
	],

	props: {
		/** Icon identifier */
		icon: {
			default: 'bars'
		},

		/** Collapsed view flag */
		collapsed: {
			default: true
		}
	},

	render(createElement) {
		let options = {
			class: ['control', 'drawer', this._collapsible_class]
		};

		let content = [
			createElement('v-action', {
				class: 'header',
				props: {
					event: 'toggle',
					icon: this._icon,
					label: this._label
				},
				on: {
					toggle: this._collapsible_onToggle
				}
			}),

			createElement('v-action-list', {
				class: 'content',
				props: {
					items: this.items
				}
			})
		];

		return createElement('div', options, content);
	}
};
