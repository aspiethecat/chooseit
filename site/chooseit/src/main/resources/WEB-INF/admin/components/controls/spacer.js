module.exports = {
	name: 'v-spacer',

	functional: true,

	render(createElement) {
		return createElement('div', { class: 'spacer' });
	}
};
