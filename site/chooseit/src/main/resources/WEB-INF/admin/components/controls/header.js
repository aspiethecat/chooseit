let Objects = require('Objects');
let Mixin = require('Mixin');

module.exports = {
	name: 'v-header',

	require: [
		'v-component'
	],

	extends: 'v-component',

	mixins: [
		Mixin.Label,
		Mixin.Icon,
		Mixin.ActionList
	],

	render(createElement) {
		let options = {
			class: ['control', 'header']
		};

		let content = [];

		let icon = this._icon;
		if (Objects.isNonNull(icon)) {
			content.push(createElement('v-icon', {
				class: 'icon',
				props: icon
			}));
		}

		let label = this._label;
		if (Objects.isNonNull(label)) {
			content.push(createElement('v-label', {
				class: 'label',
				props: {
					text: label
				}
			}));
		}

		let actions = this.actions;
		if (Objects.isNonNull(actions) && (actions.length > 0)) {
			content.push(createElement('v-action-list', {
				props: {
					items: actions
				}
			}));
		}

		return createElement('div', options, content);
	}
};
