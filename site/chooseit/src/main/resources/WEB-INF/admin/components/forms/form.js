module.exports = {
	name: 'v-form',

	require: [
		'v-component'
	],

	extends: 'v-component',

	props: {
		/** Form identifier (unique) */
		id: {
			type: String,
			required: false
		},
		'fetchUrl': {
			type: String
		},

		'storeUrl': {
			type: String
		}
	},

	render(createElement) {
		return createElement('form', {
			attrs: {
				id: this.$id
			}
		}, [
			this.$slots.default
		]);
	}
};
