module.exports = {
	name: 'v-field-wrapper',

	render(createElement) {
		return createElement('div', {
			class: 'field'
		}, [
			this.$slots.default
		])
	}
};
