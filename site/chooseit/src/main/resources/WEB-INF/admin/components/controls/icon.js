let Objects = require('Objects');

module.exports = {
	name: 'v-icon',

	props: {
		/** Icon identifier or descriptor */
		icon: {
			type: [String, Object],
			required: true
		},

		/** Fixed-width icon flag */
		fixedWidth: {
			type: Boolean,
			required: false,
			default: false,
			classMap: {
				true: 'fa-fw'
			}
		},

		/** Inverted icon flag */
		inverse: {
			type: Boolean,
			required: false,
			default: false,
			classMap: {
				true: 'fa-inverse'
			}
		},

		/** Icon size */
		size: {
			type: String,
			required: false,
			validator: function(value) {
				return module.exports.props.size.classMap.hasOwnProperty(value);
			},
			classMap: {
				'xs': 'fa-xs',
				'sm': 'fa-sm',
				'lg': 'fa-lg',
				'2x': 'fa-2x',
				'3x': 'fa-3x',
				'4x': 'fa-4x',
				'5x': 'fa-5x',
				'6x': 'fa-6x',
				'7x': 'fa-7x',
				'8x': 'fa-8x',
				'9x': 'fa-9x',
				'10x': 'fa-10x'
			}
		},

		/** Rotation amount */
		rotate: {
			type: Number,
			required: false,
			validator: function(value) {
				return module.exports.props.rotate.classMap.hasOwnProperty(value);
			},
			classMap: {
				0: null,
				90: 'fa-rotate-90',
				180: 'fa-rotate-180',
				270: 'fa-rotate-270'
			}
		},

		/** Mirroring direction */
		mirror: {
			type: String,
			required: false,
			validator: function(value) {
				return module.exports.props.mirror.classMap.hasOwnProperty(value);
			},
			classMap: {
				'horizontal': 'fa-flip-horizontal',
				'vertical':   'fa-flip-vertical'
			}
		},

		/** Icon style */
		weight: {
			type: String,
			required: false,
			default: 'light',
			validator: function(value) {
				return module.exports.props.weight.classMap.hasOwnProperty(value);
			},
			classMap: {
				'light':   'fal',
				'regular': 'far',
				'bold':    'fas'
			}
		}
	},

	render(createElement) {
		let classList = [ 'fa-' + this.icon ];

		for (const [name, spec] of Object.entries(this.$options.props)) {
			if (spec.classMap == null)
				continue;

			let value = this[name];
			let className = null;

			if (Objects.isNonNull(value)) {
				className = spec.classMap[value];
			} else if (Objects.isBoolean(spec.type) && Objects.isObject(spec.classMap)) {
				className = spec.classMap[false];
			}

			if (className == null)
				continue;

			classList.push(className);
		}

		return createElement('span', { class: classList });
	}
};
