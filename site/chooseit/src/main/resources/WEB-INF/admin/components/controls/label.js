module.exports = {
	name: 'v-label',

	props: {
		/** Label text */
		text: {
			type: String,
			required: true
		}
	},

	render(createElement) {
		return createElement('span', {}, this.text);
	}
};
