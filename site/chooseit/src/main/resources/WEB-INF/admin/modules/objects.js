(function umd(root, factory) {
	Object.assign(root, factory());
})(this, function factory() {
	'use strict';

	function Objects() { }

	Object.assign(Objects, {
		simpleTypes: [
			'Boolean',
			'Number',
			'String',
			'Function',
			'Symbol'
		],

		isNull(value) {
			return ((value === undefined) || (value === null));
		},

		isNonNull(value) {
			return ((value !== undefined) && (value !== null));
		},

		isBoolean(value) {
			return (typeof(value) === 'boolean');
		},

		isString(value) {
			return (typeof(value) === 'string');
		},

		isObject(value) {
			return ((value !== null) && (typeof(value) === 'object'));
		},

		isObjectObject(value) {
			return (Objects.isObject(value) && (Object.prototype.toString.call(value) === '[object Object]'));
		},

		isPlainObject(value) {
			if (Objects.isObjectObject(value) === false)
				return false;

			let ctor = value.constructor;
			if (typeof(ctor) !== 'function')
				return false;

			return Objects.isObjectObject(ctor.prototype);
		},

		isEmpty(value) {
			return (Object.entries(value).length === 0);
		},

		requireNonNull(value, message) {
			if (Objects.isNull(value))
				throw new TypeError(message);

			return value;
		},

		requireNonNullElse(value, defaultValue) {
			return Objects.isNonNull(value) ? value : defaultValue;
		},

		requireType(value, type, message) {
			if (Objects.isNull(value))
				return null;

			let typeId = type.name;

			let valid;
			if (Objects.simpleTypes.indexOf(typeId) >= 0) {
				valid = (typeof(value) === typeId.toLowerCase());
			} else if (typeId === 'Object') {
				valid = Objects.isPlainObject(value);
			} else if (typeId === 'Array') {
				valid = Array.isArray(value);
			} else {
				valid = (value instanceof type);
			}

			if (!valid)
				throw new Error(message);

			return value;
		},

		coalesce() {
			for (let value of arguments) {
				if (value !== undefined)
					return value;
			}

			return undefined;
		},

		merge() {
			let result = {};

			for (let props of arguments) {
				if (Objects.isNull(props))
					continue;

				for (const [key, value] of Object.entries(props)) {
					if (value === undefined)
						continue;
					if (result[key] !== undefined)
						continue;

					result[key] = value;
				}
			}

			return !Objects.isEmpty(result) ? result : undefined;
		}
	});

	return {
		Objects: Objects
	};
});
