(function umd(root, factory) {
	Object.assign(root, factory());
})(this, function factory() {
	'use strict';

	let Objects = require('Objects');

	function Mixin() { }

	Object.assign(Mixin, {
		buildProps(key, value) {
			if (Objects.isNull(value))
				return undefined;

			let props;

			if (Objects.isString(value)) {
				props = {};
				props[key] = value;
			} else if (Objects.isObject(value)) {
				props = value;
			} else {
				throw new TypeError('Invalid ' + key + ' value descriptor: ' + value);
			}

			return props;
		},

		Label: {
			props: {
				/** Label text */
				label: {
					type: String,
					required: false
				}
			},

			computed: {
				_label() {
					return Objects.coalesce(this.label, this.$archetype.label);
				}
			}
		},

		Hint: {
			props: {
				/** Hint text */
				hint: {
					type: String,
					required: false
				}
			},

			computed: {
				_hint() {
					return Objects.coalesce(this.hint, this.$archetype.hint);
				}
			}
		},

		Icon: {
			props: {
				/** Icon identifier */
				icon: {
					type: [String, Object],
					required: false
				}
			},

			computed: {
				_icon() {
					return Objects.merge(
						Mixin.buildProps('icon', this.icon),
						Mixin.buildProps('icon', this.$archetype.icon));
				}
			}
		},

		Description: {
			props: {
				/** Description text */
				description: {
					type: String,
					required: false
				}
			},

			computed: {
				_description() {
					return Objects.coalesce(this.description, this.$archetype.description);
				}
			}
		},

		ItemList: {
			props: {
				/** Items list */
				items: {
					type: [Array, Function],
					required: false,
					default: function() {
						return [];
					}
				}
			}
		},

		ActionList: {
			props: {
				/** Actions list */
				actions: {
					type: Array,
					required: false,
					default: function () {
						return [];
					}
				}
			}
		},

		Collapsible: {
			props: {
				/** Collapsed view flag */
				collapsed: {
					type: Boolean,
					required: false,
					default: false
				}
			},

			data: function() {
				return {
					_collapsible_collapsed: this.collapsed
				}
			},

			computed: {
				_collapsed() {
					return this.$data._collapsible_collapsed;
				},

				_collapsible_class() {
					return {
						collapsed: this._collapsed
					};
				}
			},

			methods: {
				_collapsible_onToggle() {
					this.$data._collapsible_collapsed = !this.$data._collapsible_collapsed;
				}
			}
		}
	});

	return {
		Mixin: Mixin
	};
});
