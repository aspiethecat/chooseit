(function umd(root, factory) {
	Object.assign(root, factory());
})(this, function factory() {
	'use strict';

	function httpRequest(uri) {
		return new Promise((resolve, reject) => {
			let request = new XMLHttpRequest();

			request.open('GET', uri);
			request.onreadystatechange = () => {
				if (request.readyState !== 4)
					return;

				if ((request.status < 200) || (request.status >= 300)) {
					return reject('HTTP request failed: ' + uri + ': ' + request.statusText);
				}

				resolve(new Resource(
					request.responseText,
					request.getResponseHeader('Content-Type')));
			};

			request.send(null);
		});
	}

	function Resource(content, contentType) {
		this.content     = content;
		this.contentType = contentType;
	}

	function Container(component, uri = null, content = null) {
		this.component = component;
		this.uri       = uri;
		this.content   = content;
	}

	Object.assign(Container, {
		prototype: {
			normalize() {
				if (this.uri == null)
					return Promise.resolve();

				return httpRequest(this.uri)
					.then((resource) => {
						if (resource.content != null)
							this.setContent(resource.content);
					});
			},

			getHead() {
				return document.head || document.getElementsByTagName('head')[0];
			}
		}
	});

	function ScriptContainer(component, uri, content) {
		Container.call(this, component, uri, content);

		this.module = {
			exports: {}
		};
	}

	Object.assign(ScriptContainer, {
		prototype: {
			__proto__: Container.prototype,

			componentHandler(uri) {
				return this.component.application.loadComponent(uri);
			},

			compile() {
				let context = {
					module: this.module,
					exports: this.module.exports,
					require: window.require
				};

				let ctorArgs = [];
				let bindArgs = [];
				for (let [name, value] of Object.entries(context)) {
					ctorArgs.push(name);
					bindArgs.push(value);
				}
				ctorArgs.push(this.content);

				try {
					Function
						.apply(null, ctorArgs)
						.apply(this.module, bindArgs);
				} catch (error) {
					return Promise.reject(error);
				}

				return Promise.resolve();
			}
		}
	});

	function TemplateContainer(component, uri, content) {
		Container.call(this, component, uri, content);
	}

	Object.assign(TemplateContainer, {
		prototype: {
			__proto__: Container.prototype,

			compile() {
				return Promise.resolve();
			}
		}
	});

	function StyleContainer(component, uri, content) {
		Container.call(this, component, uri, content);
	}

	Object.assign(StyleContainer, {
		scopeStyles(element, scopeId) {
			function process() {
				let sheet = element.sheet;
				let rules = sheet.cssRules;

				for (let i = 0; i < rules.length; i++) {
					let rule = rules[i];
					if (rule.type !== 1)
						continue;

					let scopedSelectors = [];

					rule.selectorText.split(/\s*,\s*/).forEach(
						selector => {
							scopedSelectors.push(scopeId + ' ' + selector);
							let segments = selector.match(/([^ :]+)(.+)?/);
							scopedSelectors.push(segments[1] + scopeId + (segments[2] || ''));
						});

					let scopedRule = scopedSelectors.join(',') + rule.cssText.substr(rule.selectorText.length);

					sheet.deleteRule(i);
					sheet.insertRule(scopedRule, i);
				}
			}

			try {
				// firefox may fail sheet.cssRules with InvalidAccessError
				process();
			} catch (ex) {
				if ((ex instanceof DOMException) && (ex.code === DOMException.INVALID_ACCESS_ERR)) {
					element.sheet.disabled = true;

					let onStyleLoaded = () => {
						element.removeEventListener('load', onStyleLoaded);

						// firefox need this timeout otherwise we have to use document.importNode(style, true)
						setTimeout(() => {
							process();
							element.sheet.disabled = false;
						});
					};
					element.addEventListener('load', onStyleLoaded);

					return;
				}

				throw ex;
			}
		},

		prototype: {
			__proto__: Container.prototype,

			compile() {
				return new Promise((resolve) => {
					let element;

					element = document.createElement('style');
					element.textContent = this.content;

					this.getHead().appendChild(element);

					return resolve();
				});
			}
		}
	});

	function Component(application, uri) {
		this.application = application;
		this.uri         = uri;
		this.baseURI     = uri.substr(0, uri.lastIndexOf('/') + 1);

		this.name     = null;
		this.template = null;
		this.script   = null;
		this.styles   = [];
		this.scopeId  = Component.allocateScopeId();
		this.instance = null;
	}

	Object.assign(Component, {
		lastScopeId: 0,

		allocateScopeId() {
			Component.lastScopeId++;
			return 'data-s-' + Component.lastScopeId.toString(36);
		},

		prototype: {
			load() {
				return httpRequest(this.uri)
					.then((resource) => {
						if (resource.contentType === 'application/javascript')
							return this.parseScript(resource.content);
						else
							return this.parseEnvelope(resource.content);
					})
					.then(() => this.fixup())
					.then(() => this.register())
					.then(() => {
						return this.instance;
					});
			},

			parseScript(content) {
				this.script = new ScriptContainer(this, null, content);

				return this.script.compile();
			},

			parseEnvelope(content) {
				let envelope;

				envelope = document.implementation.createHTMLDocument();
				envelope.body.innerHTML = content;

				let queue = [];
				for (let element = envelope.body.firstChild; element; element = element.nextSibling) {
					if (element.nodeType !== Element.ELEMENT_NODE)
						continue;

					let uri = null;
					if (element.hasAttribute('src'))
						uri = element.getAttribute('src');

					let factory;
					let content;
					switch (element.nodeName) {
						case 'TEMPLATE':
							content = element.innerHTML;
							factory = (uri, content) => {
								this.template = new TemplateContainer(this, uri, content);
								return this.template;
							};
							break;

						case 'SCRIPT':
							content = element.textContent;
							factory = (uri, content) => {
								this.script = new ScriptContainer(this, uri, content);
								return this.script;
							};
							break;

						case 'STYLE':
							if (element.hasAttribute('scoped')) {
								element.removeAttribute('scoped');
								StyleContainer.scopeStyles(element, '[' + this.scopeId + ']');
							}

							content = element.textContent;
							factory = (uri, content) => {
								let container = new StyleContainer(this, uri, content);
								this.styles.push(container);
								return container;
							};
							break;
					}

					queue.push(factory(uri, content));
				}

				let promises = [];
				queue.map((container) => {
					promises.push(
						container.normalize().then(() => container.compile()));
				});

				return Promise.all(promises);
			},

			fixup() {
				this.exports = {};

				if (this.script != null)
					this.exports = this.script.module.exports;

				if (this.template != null)
					this.exports.template = this.template.content;

				if (this.exports.name != null) {
					if (this.name == null)
						this.name = this.exports.name;
				} else {
					if (this.name == null)
						this.name = this.uri.match(/(.*?)([^/]+?)\/?(\.vue)?(\?.*|#.*|$)/)[2];

					this.exports.name = this.name;
				}

				if (this.exports.extends != null) {
					if (typeof(this.exports.extends) === 'string')
						this.exports.extends = Vue.component(this.exports.extends);
				}
			},

			register() {
				this.instance = Vue.component(this.name, this.exports);
			}
		}
	});

	return {
		Component: Component
	};
});
