(function umd(root, factory) {
	Object.assign(root, factory());
})(this, function factory() {
	'use strict';

	let Objects   = require('Objects');
	let Component = require('Component');
	let Vue       = require('Vue');
	let VueRouter = require('VueRouter');

	function logError(error, uri, description) {
		let message = error;

		if (uri != null)
			message += ': ' + uri;
		if (description != null)
			message += ': ' + description;

		console.error(message);
	}

	function Application(options = {}) {
		this.preloadMap    = {};
		this.componentMap  = {};
		this.routingMap    = null;
		this.router        = null;

		this.configure(options)
			.then(() => this.preloadComponents(options.preload))
			.then(() => this.compileRoutingMap(options.routingMap))
			.then(() => this.createRouter(options.routing))
			.then(() => this.createInstance(options.data))
			.catch((error) => logError(error));
	}

	Object.assign(Application, {
		prototype: {
			loadModule(moduleName) {
				return window[moduleName];
			},

			loadComponentEx(prefix, path) {
				let uri;

				if (prefix != null)
					uri = prefix + path;

				let component = this.findComponent(path);
				if (component != null)
					return Promise.resolve(component);

				return new Component(this, uri)
					.load()
					.then((component) => {
						this.componentMap[path] = component;
						return component;
					});
			},

			loadComponent(path) {
				return this.loadComponentEx(this.componentPath, path);
			},

			loadPage(path) {
				return this.loadComponentEx(this.pagePath, path);
			},

			findComponent(idref) {
				if (typeof(idref) === 'object')
					return idref;

				if (idref.startsWith('/'))
					return this.componentMap[idref];

				return Vue.component(idref);
			},

			configure(options = {}) {
				return new Promise((resolve, reject) => {
					try {
						Objects.requireType(options, Object, 'Options should be an object');

						this.rootElement = Objects.requireNonNull(
							options.rootElement,
							'Root element not specified');

						this.rootPath = Objects.requireType(
							Objects.requireNonNullElse(options.rootPath, '/'),
							String,
							'Root path should be a string');
						this.componentPath = Objects.requireType(
							Objects.requireNonNullElse(options.componentPath, '/'),
							String,
							'Component path should be a string');
						this.pagePath = Objects.requireType(
							Objects.requireNonNullElse(options.pagePath, '/'),
							String,
							'Page path should be a string');
					} catch (error) {
						return reject(error);
					}

					if (!this.rootPath.endsWith('/'))
						this.rootPath += '/';

					resolve();
				});
			},

			preloadComponents(preload) {
				if (Objects.isNull(preload))
					return Promise.resolve();

				console.group('Preloading components...');

				try {
					Objects.requireType(preload, Array, 'Preload components list should be an array');
				} catch (error) {
					return Promise.reject(error);
				}

				let promises = [];
				try {
					for (const name of preload) {
						console.info('Loading ' + name + '...');
						promises.push(this.loadComponent(name));
					}
				} catch (error) {
					console.groupEnd();
					return Promise.reject(error);
				}

				return Promise
					.all(promises)
					.then((components) => {
						for (const instance of components)
							this.preloadMap[instance.options.name] = instance;
					})
					.finally(() => {
						console.groupEnd();
					});
			},

			compileRoute(path, descriptor) {
				let route = {
					path: path,
					children: undefined
				};

				if (descriptor.component != null) {
					if (typeof(descriptor.component) === 'string') {
						let loader = (function () {
							return this.loadPage(descriptor.component);
						}).bind(this);

						route.component = (resolve, reject) => {
							loader().then(
								(component) => resolve(component),
								(reason) => reject(reason));
						};
					} else {
						route.component = descriptor.component;
					}
				}

				for (let [key, value] of Object.entries(descriptor)) {
					if (route.hasOwnProperty(key))
						continue;
					route[key] = value;
				}

				return route;
			},

			compileRoutingNode(prefix, node) {
				let routes = [];

				if (!prefix.endsWith('/'))
					prefix += '/';

				for (let [path, descriptor] of Object.entries(node)) {
					let fullPath = prefix;
					if (path.startsWith('/'))
						fullPath += path.substr(1);
					else
						fullPath += path;

					console.info('Compiling ' + fullPath + '...');

					let route = this.compileRoute(path, descriptor);

					if (descriptor.children != null)
						route.children = this.compileRoutingNode(fullPath, descriptor.children);

					routes.push(route);
				}

				return routes;
			},

			compileRoutingMap(routingMap) {
				if (Objects.isNull(routingMap))
					return Promise.resolve();

				console.group('Compiling routing map...');

				try {
					Objects.requireType(routingMap, Object, 'Routing map should be an object');
				} catch (error) {
					return Promise.reject(error);
				}

				return new Promise((resolve) => {
					this.routingMap = this.compileRoutingNode('/', routingMap);
					resolve();
				}).finally(() => {
					console.groupEnd();
				});
			},

			createRouter(options = {}) {
				if (Objects.isNull(this.routingMap))
					return Promise.resolve();

				console.info('Creating router instance...');

				try {
					Objects.requireType(options, Object, 'Routing options should be an object');
				} catch (error) {
					return Promise.reject(error);
				}

				this.router = new VueRouter(Object.assign({
					base: this.rootPath,
					routes: this.routingMap
				}, options));

				return Promise.resolve();
			},

			createInstance(data) {
				console.info('Creating application instance...');

				let options = {
					el:         this.rootElement,
					components: this.preloadMap,
					data:       data
				};

				if (this.router != null)
					options.router = this.router;

				this.instance = new Vue(options);
			}
		}
	});

	return {
		Application: Application
	};
});
