# User interface internals

## Software dependencies

 * Vue.js (version `2.5.17`) &mdash; core framework
 * Vue Router (version `3.0.1`) &mdash; routing
 * Font Awesome Pro (version `5.4.1-pro`) &mdash; icon rendering
 * Cleave.js (version `1.4.4`) &mdash; user input validation
 * FlexBox Grid (version `6.3.1`) &mdash; grid layout

## Modules (`/modules/`)

 * `Component` (`component.js`)
 * `Application` (`application.js`)

## Components

### Hierarchy

#### Controls (`/components/controls/`)

 * `v-spacer` (`spacer.js`) &mdash; spacer control
 * `v-label` (`label.js`) &mdash; label control
 * `v-icon` (`icon.js`) &mdash; icon control
 * `v-action` (`action.js`) &mdash; action control
 * `v-action-list` (`action-list.js`) &mdash; action list control
 * `v-header` (`header.js`) &mdash; header control
 * `v-drawer` (`drawer.js`) &mdash; drawer control
 * `v-panel` (`panel.js`) &mdash; panel control
 * `v-scrollable` (`scrollable.vue`) &mdash; scrollable pane

#### Forms (`/components/forms/`)

 * `v-form` (`form.js`) &mdash; form control
 * `v-hidden` (`hidden.js`) &mdash; hidden field control
 * `v-field` (`field.js`) &mdash; generic field control (*internal*)
   * `v-input` (`input.js`) &mdash; input control
   * `v-checkbox` (`checkbox.js`) &mdash; checkbox control
   * `v-select` (`select.js`) &mdash; select control

#### Views (`/components/views/`)

 * `v-layout` (`layout.vue`) &mdash; layout view
 * `v-page` (`page.vue`) &mdash; page view
