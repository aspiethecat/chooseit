<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper
	PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
	"http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="ru.chooseit.mapper.PersonMapper">
	<resultMap
			id="defaultMap"
			type="Person"
			extends="ru.chooseit.mapper.ProviderMapper.defaultMap">
		<result property="position" column="position" />

		<association
				property="parent"
				column="parent_id"
				columnPrefix="parent_"
				resultMap="ru.chooseit.mapper.GenericProviderMapper.defaultMap" />
	</resultMap>

	<select id="isNameUnique" resultType="boolean">
		<![CDATA[
			SELECT
				COUNT(*) = 0
			FROM
				persons AS p
			WHERE
				(p.name = #{entity.name}) AND (p.status <> 'DELETED')
		]]>
		<if test="entity.parentId != null">
			<![CDATA[
				AND (p.parent_id = #{entity.parentId})
			]]>
		</if>
		<if test="entity.parentId == null">
			<![CDATA[
				AND (p.parent_id IS NULL)
			]]>
		</if>
		<if test="entity.id != null">
			<![CDATA[
				AND (p.id <> #{entity.id})
			]]>
		</if>
	</select>

	<insert id="create">
		<![CDATA[
			INSERT INTO persons (
				parent_id,
				name,
				site_url,
				video_url,
				location_id,
				email,
				phone,
				phone_ext,
				address,
				zip_code,
				description,
				position
			) VALUES (
				#{entity.parentId},
				#{entity.name},
				#{entity.siteUrl},
				#{entity.videoUrl},
				#{entity.locationId},
				#{entity.primaryEmail},
				#{entity.primaryPhoneNumber},
				#{entity.primaryPhoneNumberExt},
				#{entity.address},
				#{entity.zipCode},
				#{entity.description},
				#{entity.position}
			)
		]]>
	</insert>

	<update id="update">
		<![CDATA[
			UPDATE
				persons
			SET
				parent_id   = #{entity.parentId},
				name        = #{entity.name},
				site_url    = #{entity.siteUrl},
				video_url   = #{entity.videoUrl},
				location_id = #{entity.locationId},
				email       = #{entity.primaryEmail},
				phone       = #{entity.primaryPhoneNumber},
				phone_ext   = #{entity.primaryPhoneNumberExt},
				address     = #{entity.address},
				zip_code    = #{entity.zipCode},
				description = #{entity.description},
				position    = #{entity.position}
			WHERE
				(id = #{entity.id}) AND (status <> 'DELETED')
		]]>
	</update>

	<select id="fetchById" resultMap="defaultMap">
		<![CDATA[
			SELECT
				p.id          AS id,
				pp.id         AS parent_id,
				pp.name       AS parent_name,
				p.children    AS children,
				p.name        AS name,
				p.site_url    AS site_url,
				p.video_url   AS video_url,
				l.id          AS location_id,
				l.name        AS location_name,
				p.email       AS email,
				p.phone       AS phone,
				p.phone_ext   AS phone_ext,
				p.address     AS address,
				p.zip_code    AS zip_code,
				p.description AS description,
				p.position    AS position
			FROM
				persons AS p
				LEFT JOIN locations AS l ON l.id = p.location_id
				LEFT JOIN providers AS pp ON pp.id = p.parent_id
			WHERE
				(p.id = #{id}) AND (p.status <> 'DELETED')
		]]>
	</select>

	<select id="listAll" resultMap="defaultMap">
		<![CDATA[
			SELECT
				p.id        AS id,
				p.parent_id AS parent_id,
				pp.name     AS parent_name,
				p.name      AS name,
				p.position  AS position
			FROM
				persons AS p
				LEFT JOIN providers AS pp ON pp.id = p.id
			WHERE
				p.status <> 'DELETED'
			ORDER BY
				p.name
		]]>
	</select>

	<select id="listRoots" resultMap="defaultMap">
		<![CDATA[
			SELECT DISTINCT
				pp.id    AS id,
				COUNT(*) AS children,
				pp.name  AS name
			FROM
				providers AS pp
				JOIN persons AS p ON p.id = pp.id
			WHERE
				(pp.oid <> 'persons'::regclass::oid) AND
				(pp.status <> 'DELETED')
			GROUP BY
				pp.id, pp.name
			ORDER BY
				pp.name
		]]>
	</select>

	<select id="listOrphans" resultMap="defaultMap">
		<![CDATA[
			SELECT
				p.id       AS id,
				p.name     AS name,
				p.position AS position
			FROM
				persons AS p
			WHERE
				(p.parent_id IS NULL) AND (p.status <> 'DELETED')
			ORDER BY
				p.name
		]]>
	</select>

	<select id="listChildren" resultMap="defaultMap">
		<![CDATA[
			SELECT
				p.id        AS id,
				pp.id       AS parent_id,
				pp.name     AS parent_name,
				pp.site_url AS parent_url,
				p.name      AS name,
				p.position  AS position
			FROM
				persons AS p
				LEFT JOIN providers AS pp ON pp.id = p.id
			WHERE
				(p.parent_id = #{id}) AND (p.status <> 'DELETED')
			ORDER BY
				p.name
		]]>
	</select>

	<update id="deleteById">
		<![CDATA[
			UPDATE
				persons
			SET
				status = 'DELETED'
			WHERE
				id = #{id}
		]]>
	</update>
</mapper>
