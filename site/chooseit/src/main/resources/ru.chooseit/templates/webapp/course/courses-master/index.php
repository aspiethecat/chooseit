<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Content-Language" content="ru">
		<meta charset="utf-8">
		<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
		<title>Educational Portal - HTML Mockup</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="../style.css">

		<script src="/scripts/modernizr-2.8.3.min.js"></script>

	</head>
	<body id="courses-master" class="desktop">
		<div class="site-wrapper"><div class="site">
			<header>
				<div class="menu-overlay" style="opacity:0;visibility:hidden;"></div>
				<?php include '../header.php'; ?>
			</header>

			<main class="content">
				<!-- Start of breadcrumbs -->
				<div class="container"><div class="wrapper">
					<ul class="breadcrumbs">
						<li><a href="../">Главная Портала</a></li>
						<li><a href="#">Онлайн курсы</a></li>
						<li><span>Мастер подбора курса</span></li>
					</ul>
				</div></div>
				<!-- End of breadcrumbs -->
				<!-- Start of Master Course Search -->
				<div class="container"><div class="wrapper" style="position: relative;">
					<div id="master" class="popup">
						<a class="popup-bg"></a>
						<div class="container modal"><div class="wrapper master">
							<a onclick="$('#master').openPopup();" class="close">x</a>
							<div class="col-12 choise">
								<a class="fa-circle-thin xl" href="#" onclick="$('#master').openPopup();">Выберите область знаний</a>
								<span id="unselect-all"><a onclick="unCheck();">Сбросить выбор</a></span>
							</div>
							<div class="col-12">
								<div class="col-4">
									<div class="custom-checkbox">
										<input type="checkbox" checked id="checbox-1"><label for="checbox-1">[@fa "circle-thin" /] <b>Веб-разработка</b> <span>(104)</span></label>
									</div>
									<div class="custom-checkbox">
										<input type="checkbox" id="checbox-2"><label for="checbox-2">[@fa "circle-thin" /] <b>Системное администрирование</b> <span>(104)</span></label>
									</div>
									<div class="custom-checkbox">
										<input type="checkbox" id="checbox-3"><label for="checbox-3">[@fa "circle-thin" /] <b>Программирование</b> <span>(104)</span></label>
									</div>
									<div class="custom-checkbox">
										<input type="checkbox" id="checbox-4"><label for="checbox-4">[@fa "circle-thin" /] <b>Аналитика баз данных</b> <span>(104)</span></label>
									</div>
								</div>
								<div class="col-4">
									<div class="custom-checkbox">
										<input type="checkbox" id="checbox-5"><label for="checbox-5">[@fa "circle-thin" /] <b>Менеджмент в ИТ</b> <span>(104)</span></label>
									</div>
									<div class="custom-checkbox">
										<input type="checkbox" id="checbox-6"><label for="checbox-6">[@fa "circle-thin" /] <b>Системное администрирование</b> <span>(104)</span></label>
									</div>
									<div class="custom-checkbox">
										<input type="checkbox" id="checbox-7"><label for="checbox-7">[@fa "circle-thin" /] <b>Дизайн</b> <span>(104)</span></label>
									</div>
									<div class="custom-checkbox">
										<input type="checkbox" id="checbox-8"><label for="checbox-8">[@fa "circle-thin" /] <b>Системное администрирование</b> <span>(104)</span></label>
									</div>
								</div>
								<div class="col-4">
									<div class="custom-checkbox">
										<input type="checkbox" id="checbox-9"><label for="checbox-9">[@fa "circle-thin" /] <b>Программирование</b> <span>(104)</span></label>
									</div>
									<div class="custom-checkbox">
										<input type="checkbox" id="checbox-10"><label for="checbox-10">[@fa "circle-thin" /] <b>Системное администрирование</b> <span>(104)</span></label>
									</div>
									<div class="custom-checkbox">
										<input type="checkbox" id="checbox-11"><label for="checbox-11">[@fa "circle-thin" /] <b>Аналитика баз данных</b> <span>(104)</span></label>
									</div>
									<div class="custom-checkbox">
										<input type="checkbox" id="checbox-12"><label for="checbox-12">[@fa "circle-thin" /] <b>Системное администрирование</b> <span>(104)</span></label>
									</div>
								</div>
							</div>
							<button>Показать курсы</button>
						</div></div>
					</div>
					<div class="left-side-c">
						<div class="col-6">
							<div class="col-12">
								<h2>Мастер подбора курса</h2>
								<a class="fa-circle-thin xl" onclick="$('#master').openPopup();">Выберите область знаний</a>
							</div>
							<div class="col-5">
								<h4>Стоимость</h4>
								<input id="slider-1" class="range-slider" data-step="50" data-min="0" data-max="34000" data-type="double">
								<input class="slider-textbox-1" id="slider-1-min" value="0" type="number">
								<input class="slider-textbox-1" id="slider-1-max" value="34000" type="number">
							</div>
							<div class="col-2">&nbsp;</div>
							<div class="col-5">
								<h4 class="accent-bg">Длительность</h4>
								<input id="slider-2" class="range-slider" data-step="2" data-min="0" data-max="350" data-type="double">
								<input class="slider-textbox-2" id="slider-2-min" value="0" type="number">
								<input class="slider-textbox-2" id="slider-2-max" value="350" type="number">
							</div>
							<div class="col-12 checkbox-container">
								<div class="custom-checkbox">
									<input type="checkbox" checked id="uni-certificate"><label for="uni-certificate">С сертификатом ВУЗа</label> 
								</div>
								<div class="custom-checkbox">
									<input id="free-only" type="checkbox"><label for="free-only">Только бесплатные</label>
								</div>
							</div>
						</div>
						<div class="right-side-c">
							<h5>Найдено<br>курсов:</h5>
							<h2>550</h2>
						</div>
					</div>
				</div></div>
				<!-- End of Master Course Search -->
				<div class="container"><div class="wrapper">
					<!-- Start of order by -->
					<div class="col-12 sort-by">
						<label>Сгруппировать по: </label>
						<select multiple="multiple" class="normal">
							<option selected value="1">Новизне</option>
							<option value="2">Example</option>
							<option value="3">Example</option>
							<option value="4">Example</option>
						</select>
						<a href="#">Цене</a>
						<a href="#">Участникам</a>
					</div>
					<!-- End of order by -->
					<!-- Start of video grid -->
					<div class="video-grid">
						<a href="#">
							<div class="video-wrapper">
								<div class="video-container">
									<img src="/images/video-poster.jpg" alt="video poster">
									<div class="top-bar">
										<h6 class="icon category">Веб-разработка </h6>
										<h6 class="price check-circle">Оплачено</h6>
									</div>
									<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
									<div class="bottom-bar">
										<small class="certificate">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
										<h3 class="time-icon"> 120</h3>
									</div>
								</div>
								<a href="#" class="video-title"><h5>
									Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
								</h5></a>
								<h6 class="icon">НГУ</h6>
							</div>
						</a>
						<a href="#">
							<div class="video-wrapper">
								<div class="video-container">
									<img src="/images/video-poster.jpg" alt="video poster">
									<div class="top-bar">
										<h6 class="icon category">Веб-разработка </h6>
										<h6 class="price ruble">38 000</h6>
									</div>
									<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
									<div class="bottom-bar">
										<small class="certificate hide">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
										<h3 class="time-icon"> 120</h3>
									</div>
								</div>
								<a href="#" class="video-title"><h5>
									Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
								</h5></a>
								<h6>Лаборатория Касперского</h6>
							</div>
						</a>
						<a href="#">
							<div class="video-wrapper">
								<div class="video-container">
									<img src="/images/video-poster.jpg" alt="video poster">
									<div class="top-bar">
										<h6 class="icon category">Веб-разработка </h6>
										<h6 class="price ruble">38 000</h6>
									</div>
									<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
									<div class="bottom-bar">
										<small class="certificate hide">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
										<h3 class="time-icon"> 120</h3>
									</div>
								</div>
								<a href="#" class="video-title"><h5>
									Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
								</h5></a>
								<img src="/images/kaspersky-logo.png" alt="Kaspersky logo">
							</div>
						</a>
						<a href="#">
							<div class="video-wrapper">
								<div class="video-container">
									<img src="/images/video-poster.jpg" alt="video poster">
									<div class="top-bar">
										<h6 class="icon category">Веб-разработка </h6>
										<h6 class="price ruble">38 000</h6>
									</div>
									<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
									<div class="bottom-bar">
										<small class="certificate hide">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
										<h3 class="time-icon"> 120</h3>
									</div>
								</div>
								<a href="#" class="video-title"><h5>
									Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
								</h5></a>
								<h6 class="icon">НГУ</h6>
							</div>
						</a>
						<a href="#">
							<div class="video-wrapper">
								<div class="video-container">
									<img src="/images/video-poster.jpg" alt="video poster">
									<div class="top-bar">
										<h6 class="icon category">Веб-разработка </h6>
										<h6 class="price ruble">38 000</h6>
									</div>
									<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
									<div class="bottom-bar">
										<small class="certificate blue">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
										<h3 class="time-icon"> 120</h3>
									</div>
								</div>
								<a href="#" class="video-title"><h5>
									Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
								</h5></a>
								<h6 class="icon">НГУ</h6>
							</div>
						</a>
						<a href="#">
							<div class="video-wrapper">
								<div class="video-container">
									<img src="/images/video-poster.jpg" alt="video poster">
									<div class="top-bar">
										<h6 class="icon category">Веб-разработка </h6>
										<h6 class="price ruble">38 000</h6>
									</div>
									<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
									<div class="bottom-bar">
										<small class="certificate hide">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
										<h3 class="time-icon"> 120</h3>
									</div>
								</div>
								<a href="#" class="video-title"><h5>
									Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
								</h5></a>
								<h6 class="icon">НГУ</h6>
							</div>
						</a>
						<a href="#">
							<div class="video-wrapper">
								<div class="video-container">
									<img src="/images/video-poster.jpg" alt="video poster">
									<div class="top-bar">
										<h6 class="icon category">Веб-разработка </h6>
										<h6 class="price ruble">38 000</h6>
									</div>
									<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
									<div class="bottom-bar">
										<small class="certificate hide">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
										<h3 class="time-icon"> 120</h3>
									</div>
								</div>
								<a href="#" class="video-title"><h5>
									Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
								</h5></a>
								<h6 class="icon">НГУ</h6>
							</div>
						</a>
						<a href="#">
							<div class="video-wrapper">
								<div class="video-container">
									<img src="/images/video-poster.jpg" alt="video poster">
									<div class="top-bar">
										<h6 class="icon category">Веб-разработка </h6>
										<h6 class="price ruble">38 000</h6>
									</div>
									<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
									<div class="bottom-bar">
										<small class="certificate hide">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
										<h3 class="time-icon"> 120</h3>
									</div>
								</div>
								<a href="#" class="video-title"><h5>
									Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
								</h5></a>
								<h6 class="icon">НГУ</h6>
							</div>
						</a>
						<a href="#">
							<div class="video-wrapper">
								<div class="video-container">
									<img src="/images/video-poster.jpg" alt="video poster">
									<div class="top-bar">
										<h6 class="icon category">Веб-разработка </h6>
										<h6 class="price ruble">38 000</h6>
									</div>
									<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
									<div class="bottom-bar">
										<small class="certificate blue">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
										<h3 class="time-icon"> 120</h3>
									</div>
								</div>
								<a href="#" class="video-title"><h5>
									Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
								</h5></a>
								<h6 class="icon">НГУ</h6>
							</div>
						</a>
						<a href="#">
							<div class="video-wrapper">
								<div class="video-container">
									<img src="/images/video-poster.jpg" alt="video poster">
									<div class="top-bar">
										<h6 class="icon category">Веб-разработка </h6>
										<h6 class="price ruble">38 000</h6>
									</div>
									<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
									<div class="bottom-bar">
										<small class="certificate hide">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
										<h3 class="time-icon"> 120</h3>
									</div>
								</div>
								<a href="#" class="video-title"><h5>
									Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
								</h5></a>
								<h6 class="icon">НГУ</h6>
							</div>
						</a>
						<a href="#">
							<div class="video-wrapper">
								<div class="video-container">
									<img src="/images/video-poster.jpg" alt="video poster">
									<div class="top-bar">
										<h6 class="icon category">Веб-разработка </h6>
										<h6 class="price ruble">38 000</h6>
									</div>
									<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
									<div class="bottom-bar">
										<small class="certificate hide">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
										<h3 class="time-icon"> 120</h3>
									</div>
								</div>
								<a href="#" class="video-title"><h5>
									Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
								</h5></a>
								<h6 class="icon">НГУ</h6>
							</div>
						</a>
						<a href="#">
							<div class="video-wrapper">
								<div class="video-container">
									<img src="/images/video-poster.jpg" alt="video poster">
									<div class="top-bar">
										<h6 class="icon category">Веб-разработка </h6>
										<h6 class="price ruble">38 000</h6>
									</div>
									<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
									<div class="bottom-bar">
										<small class="certificate hide">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
										<h3 class="time-icon"> 120</h3>
									</div>
								</div>
								<a href="#" class="video-title"><h5>
									Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
								</h5></a>
								<h6 class="icon">НГУ</h6>
							</div>
						</a>
					</div>
					<a href="#" class="load-more fa fa-circle-thin"></a>
				</div></div>
					<!-- End of video grid -->
				</div></div>

			</main>
			<?php include '../footer.php'; ?>
			<script src="/scripts/ion.rangeSlider.min.js"></script>
			<script type="text/javascript" src="/scripts/jquery.sumoselect.min.js"></script>
		</div></div>
		<script type="text/javascript">
		(function() { var s = document.createElement("script"); s.type = "text/javascript"; s.async = true; s.src = '//api.usersnap.com/load/2a3946d2-eed6-47f0-b743-1142bfdc00f4.js';
		var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); })();
		</script>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".range-slider").ionRangeSlider({
					force_edges: true
				});
				$('#slider-1').on("change", function () {
					var $this = $(this),
					from = $this.data("from"),
					to = $this.data("to");

					$('#slider-1-min').val(from);
					$('#slider-1-max').val(to);
				});
				$('#slider-2').on("change", function () {
					var $this = $(this),
					from = $this.data("from"),
					to = $this.data("to");

					$('#slider-2-min').val(from);
					$('#slider-2-max').val(to);
				});
				var slider_1 = $('#slider-1').data("ionRangeSlider"),
					slider_2 = $('#slider-2').data("ionRangeSlider");

				$('.slider-textbox-1').each(function() {
					var elem = $(this);
					elem.data('oldVal', elem.val());
					elem.bind("propertychange change focusout", function(event){
						if (elem.data('oldVal') != elem.val()) {
							elem.data('oldVal', elem.val());
							slider_1.update({
								from: $('#slider-1-min').val(),
								to: $('#slider-1-max').val()
							});
					  }
					});
				 });
				$('.slider-textbox-2').each(function() {
					var elem = $(this);
					elem.data('oldVal', elem.val());
					elem.bind("propertychange change focusout", function(event){
						if (elem.data('oldVal') != elem.val()) {
							elem.data('oldVal', elem.val());
							slider_2.update({
								from: $('#slider-2-min').val(),
								to: $('#slider-2-max').val()
							});
					  }
					});
				});
				
				var checkbox = $('#master input[type=checkbox]');
				$(checkbox).change(function(){
					if ($(checkbox).not(':checked').length == $(checkbox).length) {
						$('#unselect-all').fadeOut();
					} else {
						$('#unselect-all').fadeIn();
					}
				});
				$('select.normal').SumoSelect({ csvDispCount: 2, selectAll: true });
				$('select.search').SumoSelect({ csvDispCount: 2, selectAll: true, search: true, searchText:'Enter here.' });
			});
		</script>
	</body>
</html>
