<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Content-Language" content="ru">
		<meta charset="utf-8">
		<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
		<title>Educational Portal - HTML Mockup</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="../style.css">
	
		<script src="/scripts/modernizr-2.8.3.min.js"></script>

	</head>
	<body id="professions-page" class="sorted-alpha desktop">
		<div class="site-wrapper"><div class="site">
			<header>
				<div class="menu-overlay" style="opacity:0;visibility:hidden;"></div>
				<?php include '../header.php'; ?>
			</header>

			<main class="content">
				<!-- Start of breadcrumbs -->
				<div class="container"><div class="wrapper">
					<ul class="breadcrumbs">
						<li><a href="../">Главная Портала</a></li>
					</ul>
				</div></div>
				<!-- End of breadcrumbs -->

				<!-- Start of Professions list -->
				<div id="professions" class="container"><div class="wrapper">
					<h2>Каталог IT-профессий</h2>
					<div class="col-12">
						<div class="col-6 sort-by"><p><a href="#">По категориям</a> <a href="#">Популярные</a> <span>По алфавиту</span></p></div>
						<div class="col-6 search"><input class="search-bar" placeholder="Поиск по названию" type="text">[@fa "circle-o" /]</div>
					</div>
					<div class="col-3">
						<h3>3</h3>
						<ul>
							<a href="#"><li>3D-дизайнер</li></a>
							<a href="#"><li>3D-моделлер</li></a>
							<a href="#"><li>3D-визуализатор</li></a>
						</ul>
						<h3>A</h3>
						<ul>
							<a href="#"><li>Архитектор ПО</li></a>
							<a href="#"><li>Аналитик</li></a>
							<a href="#"><li>Архитектор ПО</li></a>
							<a href="#"><li>Аналитик</li></a>
							<a href="#"><li>Архитектор ПО</li></a>
							<a href="#"><li>Аналитик</li></a>
						</ul>
						<h3>B</h3>
						<ul>
							<a href="#"><li>Веб-дизайнер</li></a>
							<a href="#"><li>Веб-разработчик</li></a>
							<a href="#"><li>Веб-аналитик</li></a>
							<a href="#"><li>Веб-дизайнер</li></a>
							<a href="#"><li>Веб-разработчик</li></a>
							<a href="#"><li>Веб-аналитик</li></a>
						</ul>
						<h3>C</h3>
						<ul>
							<a href="#"><li>Системная аналитик</li></a>
							<a href="#"><li>Веб-аналитик</li></a>
							<a href="#"><li>Системная аналитик</li></a>
							<a href="#"><li>Веб-аналитик</li></a>
							<a href="#"><li>Системный аналитик</li></a>
							<a href="#"><li>Веб-аналитик</li></a>
						</ul>
						<h3>A</h3>
						<ul>
							<a href="#"><li>Архитектор ПО</li></a>
							<a href="#"><li>Архитектор ПО</li></a>
						</ul>
					</div>
					<div class="col-3">
						<h3>П</h3>
						<ul>
							<a href="#"><li>Программист</li></a>
							<a href="#"><li>Программист тестировщик</li></a>
							<a href="#"><li>Программист технический</li></a>
							<a href="#"><li>Тестировщик</li></a>
						</ul>
						<h3>Р</h3>
						<ul>
							<a href="#"><li>Архитектор ПО</li></a>
							<a href="#"><li>Программист</li></a>
							<a href="#"><li>Тестировщик</li></a>
						</ul>
						<h3>M</h3>
						<ul>
							<a href="#"><li>Менеджер продуктов</li></a>
							<a href="#"><li>Менеджер проектов</li></a>
							<a href="#"><li>IT-Директор</li></a>
						</ul>
						<h3>A</h3>
						<ul>
							<a href="#"><li>Администратор БД</li></a>
							<a href="#"><li>Системный администратор</li></a>
							<a href="#"><li>Администратор БД</li></a>
							<a href="#"><li>Администратор БД</li></a>
						</ul>
						<h3>C</h3>
						<ul>
							<a href="#"><li>Системная аналитик</li></a>
							<a href="#"><li>Веб-аналитик</li></a>
							<a href="#"><li>Системная аналитик</li></a>
							<a href="#"><li>Веб-аналитик</li></a>
							<a href="#"><li>Системный аналитик</li></a>
							<a href="#"><li>Веб-аналитик</li></a>
						</ul>
					</div>
					<div class="col-3">
						<h3>T</h3>
						<ul>
							<a href="#"><li>Тестировщик</li></a>
							<a href="#"><li>Технический писатель</li></a>
							<a href="#"><li>Тестировщик</li></a>
							<a href="#"><li>Технический писатель</li></a>
						</ul>
						<h3>P</h3>
						<ul>
							<a href="#"><li>Руководитель проекта</li></a>
							<a href="#"><li>Разработчик</li></a>
							<a href="#"><li>Разработчик</li></a>
							<a href="#"><li>Разработчик</li></a>
						</ul>
						<h3>Д</h3>
						<ul>
							<a href="#"><li>UX-дизайнер</li></a>
							<a href="#"><li>Дизайнер интерфейсов</li></a>
							<a href="#"><li>Дизайнер</li></a>
							<a href="#"><li>Дизайнер интерфейсов</li></a>
							<a href="#"><li>Дизайнер</li></a>
							<a href="#"><li>Дизайнер</li></a>
						</ul>
						<h3>М</h3>
						<ul>
							<a href="#"><li>Менеджер продуктов</li></a>
							<a href="#"><li>Менеджер проектов</li></a>
							<a href="#"><li>IT-Директор</li></a>
						</ul>
						<h3>B</h3>
						<ul>
							<a href="#"><li>Веб-аналитик</li></a>
							<a href="#"><li>Веб-аналитик</li></a>
						</ul>
					</div>
					<div class="col-3">
						<h3>П</h3>
						<ul>
							<a href="#"><li>Программист</li></a>
							<a href="#"><li>Программист тестировщик</li></a>
							<a href="#"><li>Программист технический</li></a>
							<a href="#"><li>Тестировщик</li></a>
						</ul>
						<h3>P</h3>
						<ul>
							<a href="#"><li>Архитектор ПО</li></a>
							<a href="#"><li>Программист</li></a>
							<a href="#"><li>Тестировщик</li></a>
						</ul>
						<h3>M</h3>
						<ul>
							<a href="#"><li>Менеджер продуктов</li></a>
							<a href="#"><li>Менеджер проектов</li></a>
							<a href="#"><li>IT-Директор</li></a>
						</ul>
						<h3>A</h3>
						<ul>
							<a href="#"><li>Администратор БД</li></a>
							<a href="#"><li>Системный администратор</li></a>
							<a href="#"><li>Администратор БД</li></a>
							<a href="#"><li>Администратор БД</li></a>
						</ul>
						<h3>C</h3>
						<ul>
							<a href="#"><li>Системная аналитик</li></a>
							<a href="#"><li>Веб-аналитик</li></a>
							<a href="#"><li>Системная аналитик</li></a>
							<a href="#"><li>Веб-аналитик</li></a>
							<a href="#"><li>Системный аналитик</li></a>
							<a href="#"><li>Веб-аналитик</li></a>
						</ul>
					</div>
				</div></div>
				<!-- End of Professions list -->
				<!-- Start of Articles list -->
				<div id="articles" class="container">
					<div class="wrapper">
						<h2 class="accent">Специальные партнеры портала</h2>
						<div class="partners-wrapper">
							<div class="partner">
								<div class="logo"><img src="/images/lanit-logo.png" alt="logo"></div>
								<h5>Образовательная академия ЛАНИТ</h5>
							</div>
							<div class="partner">
								<div class="logo"><img src="/images/ispran-logo.png" alt="logo"></div>
								<h5>Институт системного программирования РАН</h5>
							</div>
							<div class="partner">
								<div class="logo"><img src="/images/ispran-logo.png" alt="logo"></div>
								<h5>Образовательная академия ЛАНИТ</h5>
							</div>
							<div class="partner">
								<div class="logo"><img src="/images/lanit-logo.png" alt="logo"></div>
								<h5>Институт системного программирования РАН</h5>
							</div>
						</div>
					</div>
				</div>
				<!-- End of Articles list -->	
			</main>
			<?php include '../footer.php'; ?>
		</div></div>
		<script type="text/javascript">
		(function() { var s = document.createElement("script"); s.type = "text/javascript"; s.async = true; s.src = '//api.usersnap.com/load/2a3946d2-eed6-47f0-b743-1142bfdc00f4.js';
		var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); })();
		</script>
	</body>
</html>
