<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Content-Language" content="ru">
        <meta charset="utf-8">
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
        <title>Educational Portal - HTML Mockup</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="/style.css">

		<script type="text/javascript" src="/scripts/modernizr-2.8.3.min.js"></script>

	</head>
	<body id="course" class="course-body-text desktop">
		<div class="site-wrapper"><div class="site">
			<header>
				<div class="menu-overlay" style="opacity:0;visibility:hidden;"></div>
				<?php include '../header.php'; ?>
			</header>

			<main class="content">
				<!-- Start of breadcrumbs & progressbar -->
				<div class="container"><div class="wrapper">
					<ul class="breadcrumbs">
						<li><a href="/">Главная Портала</a></li>
						<li><a href="/courses-all/">Онлайн курсы</a></li>
						<li><a href="#">Веб-разработка</a></li>
						<li><a href="#">JAVA</a></li>
						<li><a href="#">Полный специальный курс по разработке высоконагруженных проектов на...</a></li>
					</ul>
					<div class="progress-bar">
						<span class="progress-value" style="width: 65%;">65%</span>
						<span class="invert">65%</span>
					</div>
				</div></div>
				<!-- End of breadcrumbs & progressbar -->
				<!-- Start of sidebar & main body -->
				<div class="container"><div class="wrapper">
					<div class="left-sidebar">
						<div class="scrollbar">
							<ul class="remove-bullets">
								<a href="#"><li>Обзор WebSphere MQ</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Работа с объектами WebSphere MQ</li></a>
								<a href="#"><li>Использование вызовов MQCONN, MQOPEN, MQCLOSE И M</li></a>
								<a href="#"><li>Использование вызова MQPUT</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Использование вызова MQPUT для создания с</li></a>
								<a href="#"><li>Открытие очередей, MQMD, свойства сообщений</li></a>
								<a href="#"><li>Использование вызовов MQGET и MQPUT1</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Работа с сообщениями</li></a>
								<a href="#"><li>Контроль получения сообщений</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Создание динамических очередей и управле</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Получение и настройка свойств сообщений</li></a>
								<a class="active" href="#"><li>Безопасность MQI</li></a>
								<a href="#"><li>Использование MQINQ и MQSET</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Работа с атрибутами очередей: MQINQ и MQSET</li></a>
								<a href="#"><li>Поддержка транзакций и триггеринг</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Транзакции: RESPOND и MQTMCGET</li></a>
								<a href="#"><li>Группировка и сегментация сообщений</li></a>
								<a href="#"><li>Асинхронное получение сообщений</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Асинхронное получение сообщений в WebSp</li></a>
								<a href="#"><li><strong class="accent">Сертификационный экзамен</strong></li></a>
								<a href="#"><li>Обзор WebSphere MQ</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Работа с объектами WebSphere MQ</li></a>
								<a href="#"><li>Использование вызовов MQCONN, MQOPEN, MQCLOSE И M</li></a>
								<a href="#"><li>Использование вызова MQPUT</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Использование вызова MQPUT для создания с</li></a>
								<a href="#"><li>Открытие очередей, MQMD, свойства сообщений</li></a>
								<a href="#"><li>Использование вызовов MQGET и MQPUT1</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Работа с сообщениями</li></a>
								<a href="#"><li>Контроль получения сообщений</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Создание динамических очередей и управле</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Получение и настройка свойств сообщений</li></a>
								<a href="#"><li>Безопасность MQI</li></a>
								<a href="#"><li>Использование MQINQ и MQSET</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Работа с атрибутами очередей: MQINQ и MQSET</li></a>
								<a href="#"><li>Поддержка транзакций и триггеринг</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Транзакции: RESPOND и MQTMCGET</li></a>
								<a href="#"><li>Группировка и сегментация сообщений</li></a>
								<a href="#"><li>Асинхронное получение сообщений</li></a>
								<a href="#"><li><strong class="accent">Упражнение</strong> Асинхронное получение сообщений в WebSp</li></a>
								<a href="#"><li><strong class="accent">Сертификационный экзамен</strong></li></a>
							</ul>
						</div>
					</div>
					<div class="right-side-c main-area" id="exam">
						<h2 data-content="12.">Безопасность MQI</h2>

						<p class="eta"></p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						 
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						 
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						 
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<button id="go-next-btn">следующий</button>
					</div>
				</div></div>
				<!-- End of sidebar & main body -->
			</main>
				<?php include '../footer.php'; ?>
				<script src="/scripts/readtime.min.js" />
		</div></div>
		<script type="text/javascript">
		(function() { var s = document.createElement("script"); s.type = "text/javascript"; s.async = true; s.src = '//api.usersnap.com/load/2a3946d2-eed6-47f0-b743-1142bfdc00f4.js';
		var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); })();
		</script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.scrollbar').animate({
					scrollTop: $("#course .left-sidebar a.active").position().top
				}, 1000);
				$('#exam').readTime();
				$(window).scroll(function() {
					var hT = $('.left-sidebar').position().top;
					if(window.pageYOffset>hT) {
						$('.left-sidebar').addClass("follow");
					} else {
						$('.left-sidebar').removeClass("follow");
					}
				});
			});
		</script>
	</body>
</html>
