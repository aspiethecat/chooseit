<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Content-Language" content="ru">
        <meta charset="utf-8">
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
        <title>Educational Portal - HTML Mockup</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="/style.css">

		<script type="text/javascript" src="/scripts/modernizr-2.8.3.min.js"></script>

	</head>
	<body class="desktop">
		<div class="site-wrapper"><div class="site">
			<main class="content">
				<div id="single-popup-profession" class="popup">
				<div class="modal">
				<div class="container"><div class="wrapper">
					<div class="description">
						<div class="scrollbar">
							<h2>Описание профессии PHP-Программист</h2>
							<p>PHP-Программист – специалист, который занимается разработкой веб-приложений.</p>
							<h5>Основная деятельность php-программиста  <i>связана с выполнением работ на языке программирования PHP, который интенсивно применяется для разработки веб-приложений. В настоящее время он поддерживается подавляющим большинством хостинг-провайдеров и является одним из лидеров среди языков программирования по разработке и внедрению программного обеспечения.</i></h5>
							<h5>К основным обязанностям php-программиста <i>относятся:</i></h5>
						     <ul>
						     	<li>[@fa "circle" /]определение вместе с непосредственным руководителем целей и задач проекта и обеспечение их своевременного и качественного исполнения;</li>
						     	<li>[@fa "circle" /]выбор среды разработки, необходимого программного обеспечения;</li>
						     	<li>[@fa "circle" /]разработка и сопровождение сайтов и приложений;</li>
						     	<li>[@fa "circle" /]разработка концепции развития веб-сервера;</li>
						     	<li>[@fa "circle" /]создание и модификация программных модулей;</li>
						     	<li>[@fa "circle" /]администрирование веб-сервера (при необходимости).</li>
						     </ul>
						     	<h5>Требования к индивидуальным особенностям специалиста</h5>
								<p>Специалист в области php-программирования должен обладать такими личностными качествами, как аккуратность, внимательность, целеустремленность, умение самостоятельно принимать решения, ответственность, терпеливость, настойчивость, склонность к интеллектуальным видам деятельности, независимость (наличие собственного мнения).</p>

 
								<h5>К профессионально важным качествам php-программиста <i>относятся:</i></h5>
								<ul>
									<li>[@fa "circle" /]гибкость и динамичность мышления;</li>
									<li>[@fa "circle" /]аналитические способности;</li>
									<li>[@fa "circle" /]хорошая память;</li>
									<li>[@fa "circle" /]способность грамотно выражать свои мысли;</li>
									<li>[@fa "circle" /]математические способности;</li>
									<li>[@fa "circle" /]высокий уровень развития технических способностей;</li>
									<li>[@fa "circle" /]развитое воображение.</li>
								</ul>
						</div>
						<div class="special right-side-c">
						     <h2>Специальности НГУ для профессии PHP-Программист</h2>
						     <ol>
						     	<li><h5>«Математическое обеспечение и администрирование информационных систем»  <i>— 150 тыс. в год</i></h5></li>
						     	<li><h5>ВПО «Программное обеспечение вычислительной техники и автоматизированных систем» <i>— 180 тыс. в год</i></h5></li>
						     	<li><h5>«Прикладная математика и информатика» <i>— 100 тыс. в год</i></h5></li>
						     </ol>
						     </div>
					</div>
				</div></div>
				</div></div>
				<!-- End of sidebar & main body -->
			</main>
		</div></div>
		<script type="text/javascript">
		(function() { var s = document.createElement("script"); s.type = "text/javascript"; s.async = true; s.src = '//api.usersnap.com/load/2a3946d2-eed6-47f0-b743-1142bfdc00f4.js';
		var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); })();
		</script>
		<script type="text/javascript">
			$('.scrollbar').animate({
		        scrollTop: $("#course .left-sidebar a.active").position().top
		    }, 2000);
		</script>
	</body>
</html>
