<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Content-Language" content="ru">
        <meta charset="utf-8">
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
        <title>Educational Portal - HTML Mockup</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="/style.css">

		<script type="text/javascript" src="/scripts/modernizr-2.8.3.min.js"></script>

	</head>
	<body class="desktop">
		<div class="site-wrapper"><div class="site">
			<main class="content">
				<div id="master" class="popup" style="opacity:1; visibility: visible;">
					<div class="container modal" style="opacity:1;"><div class="wrapper master">
						<a href="#" class="close">x</a>
						<div class="col-12 choise">
							<a class="fa-circle-thin xl" href="#" onclick="$('#master').openPopup();">Выберите область знаний</a>
							<span><a href="#">Сбросить выбор</a></span>
						</div>
						<div class="col-4">
							<div class="custom-checkbox">
								<input type="checkbox" checked id="checbox-1"><label for="checbox-1">[@fa "circle-thin" /] <b>Веб-разработка</b> <span>(104)</span></label>
							</div>
							<div class="custom-checkbox">
								<input type="checkbox" id="checbox-2"><label for="checbox-2">[@fa "circle-thin" /] <b>Системное администрирование</b> <span>(104)</span></label>
							</div>
							<div class="custom-checkbox">
								<input type="checkbox" id="checbox-3"><label for="checbox-3">[@fa "circle-thin" /] <b>Программирование</b> <span>(104)</span></label>
							</div>
							<div class="custom-checkbox">
								<input type="checkbox" id="checbox-4"><label for="checbox-4">[@fa "circle-thin" /] <b>Аналитика баз данных</b> <span>(104)</span></label>
							</div>
						</div>
						<div class="col-4">
							<div class="custom-checkbox">
								<input type="checkbox" id="checbox-5"><label for="checbox-5">[@fa "circle-thin" /] <b>Менеджмент в ИТ</b> <span>(104)</span></label>
							</div>
							<div class="custom-checkbox">
								<input type="checkbox" id="checbox-6"><label for="checbox-6">[@fa "circle-thin" /] <b>Системное администрирование</b> <span>(104)</span></label>
							</div>
							<div class="custom-checkbox">
								<input type="checkbox" id="checbox-7"><label for="checbox-7">[@fa "circle-thin" /] <b>Дизайн</b> <span>(104)</span></label>
							</div>
							<div class="custom-checkbox">
								<input type="checkbox" id="checbox-8"><label for="checbox-8">[@fa "circle-thin" /] <b>Системное администрирование</b> <span>(104)</span></label>
							</div>
						</div>
						<div class="col-4">
							<div class="custom-checkbox">
								<input type="checkbox" id="checbox-9"><label for="checbox-9">[@fa "circle-thin" /] <b>Программирование</b> <span>(104)</span></label>
							</div>
							<div class="custom-checkbox">
								<input type="checkbox" id="checbox-10"><label for="checbox-10">[@fa "circle-thin" /] <b>Системное администрирование</b> <span>(104)</span></label>
							</div>
							<div class="custom-checkbox">
								<input type="checkbox" id="checbox-11"><label for="checbox-11">[@fa "circle-thin" /] <b>Аналитика баз данных</b> <span>(104)</span></label>
							</div>
							<div class="custom-checkbox">
								<input type="checkbox" id="checbox-12"><label for="checbox-12">[@fa "circle-thin" /] <b>Системное администрирование</b> <span>(104)</span></label>
							</div>
						</div>
						<button>Показать курсы</button>
					</div></div>
				</div>
			</main>
		</div></div>
		<script type="text/javascript">
		(function() { var s = document.createElement("script"); s.type = "text/javascript"; s.async = true; s.src = '//api.usersnap.com/load/2a3946d2-eed6-47f0-b743-1142bfdc00f4.js';
		var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); })();
		</script>
	</body>
</html>
