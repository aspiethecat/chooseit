<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Content-Language" content="ru">
        <meta charset="utf-8">
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
        <title>Educational Portal - HTML Mockup</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="../style.css">
	
		<script src="/scripts/modernizr-2.8.3.min.js"></script>

	</head>
	<body id="analytics-category" class="desktop">
		<div class="site-wrapper"><div class="site">
			<header>
				<div class="menu-overlay" style="opacity:0;visibility:hidden;"></div>
				<?php include '../header.php'; ?>
			</header>

			<main class="content">
				<!-- Start of breadcrumbs -->
				<div class="container"><div class="wrapper">
					<ul class="breadcrumbs">
						<li><a href="/">Главная Портала</a></li>
						<li><span>Отраслевая аналитика</span></li>
					</ul>
				</div></div>
				<!-- End of breadcrumbs -->
				<!-- Start of analytics -->
				<div id="dev-ana" class="container">
					<div class="wrapper left-side-c">
						<h2>Отраслевая аналитика</h2>
						<div class="categories-analytics">
							
								<h5><a href="/analytics-category/">Обзор новых профессий</a></h5>
								<h5><a href="/analytics-category/">Аналитика зарплат</a></h5>
								<h5><a href="/analytics-category/">Востребованность профессий</a></h5>
								<h5><a href="/analytics-category/" class="current">Тренды в ИТ-образовании</a></h5>
								<h5><a href="/analytics-category/">Истории успеха</a></h5>
								<h5><a href="/analytics-category/">Обмен опытом</a></h5>
							
						</div>
						<div class="col-6 analytics">
							<div class="col-12 highlight">
							<h4>Тренды:</h4>
							<a href="/analytics-single/"><h5>Обзор наиболее востребованных ваканский в ИТ-отрасли за Май 2017</h5></a>
							<p>Контрпример соответствует интеграл по поверхности, что и требовалось доказать. Начало координат усиливает вектор. Интерполяция расточительно концентрирует экспериментальный разрыв функции, при этом, вместо 13 можно взять любую другую константу. Ряд Тейлора, в первом приближении, обуславливает параллельный многочлен.В общем, математический анализ транслирует предел функции.</p>
							<div class="meta">
								<span>14 мая 2017</span>
								<span class="icon">16 189</span>
								<span class="icon">81</span>
							</div>
						</div>
						<div class="col-7">
							<h4><a href="/analytics-category/">Профессии</a></h4>
							<a href="/analytics-single/">Чем на самом деле занимается системный администратор?</a>
							<p>Высшая арифметика по-прежнему востребована. Интеграл по бесконечной области расточительно оправдывает разрыв функции. Полином решительно раскручивает </p>
							<div class="meta">
								<span>14 мая 2017</span>
								<span class="icon">16 189</span>
								<span class="icon">81</span>
							</div>
						</div>
						<div class="col-5">
							<h4><a href="/analytics-category/">Профессии</a></h4>
							<a href="/analytics-single/">Чем на самом деле занимается</a>
							<p>Высшая арифметика по-прежнему востребована. Интеграл по бесконечной области расточительно оправдывает разрыв</p>
							<div class="meta">
								<span>14 мая 2017</span>
								<span class="icon">16 189</span>
								<span class="icon">81</span>
							</div>
						</div>
						<div class="col-7">
							<h4><a href="/analytics-category/">Профессии</a></h4>
							<a href="/analytics-single/">Чем на самом деле занимается системный администратор?</a>
							<p>Высшая арифметика по-прежнему востребована. Интеграл по бесконечной области расточительно оправдывает разрыв функции. Полином решительно раскручивает</p>
							<div class="meta">
								<span>14 мая 2017</span>
								<span class="icon">16 189</span>
								<span class="icon">81</span>
							</div>
						</div>
						<div class="col-5">
							<h4><a href="/analytics-category/">Профессии</a></h4>
							<a href="/analytics-single/">Чем на самом деле занимается</a>
							<p>Высшая арифметика по-прежнему востребована. Интеграл по бесконечной области расточительно оправдывает разрыв</p>
							<div class="meta">
								<span>14 мая 2017</span>
								<span class="icon">16 189</span>
								<span class="icon">81</span>
							</div>
						</div>
						</div>
						<div class="col-6 development">
							<div class="col-12 analytics-list">
							<h4><a href="/analytics-category/">Профессии</a></h4>
							<a href="/analytics-single/">Обзор наиболее востребованных ваканский в ИТ-отрасли за Май 2017</a>
							<p>Контрпример соответствует интеграл по поверхности, что и требовалось доказать. Начало координат усиливает вектор. Интерполяция расточительно концентрирует экспериментальный разрыв функции, при этом, вместо 13 можно взять любую другую константу. Ряд Тейлора, в первом приближении, обуславливает .</p>
							<div class="meta">
								<span>14 мая 2017</span>
								<span class="icon">16 189</span>
								<span class="icon">81</span>
							</div>
						</div>
						<div class="col-12 analytics-list">
							<h4><a href="/analytics-category/">Профессии</a></h4>
							<a href="/analytics-single/">Обзор наиболее востребованных ваканский в ИТ-отрасли за Май 2017</a>
							<p>Контрпример соответствует интеграл по поверхности, что и требовалось доказать. Начало координат усиливает вектор. Интерполяция расточительно концентрирует экспериментальный разрыв функции, при этом, вместо 13 можно взять любую другую константу. Ряд Тейлора, в первом приближении, обуславливает</p>
							<div class="meta">
								<span>14 мая 2017</span>
								<span class="icon">16 189</span>
								<span class="icon">81</span>
							</div>
						</div>
						<div class="col-12 analytics-list">
							<h4><a href="/analytics-category/">Профессии</a></h4>
							<a href="/analytics-single/">Обзор наиболее востребованных ваканский в ИТ-отрасли за Май 2017</a>
							<p>Контрпример соответствует интеграл по поверхности, что и требовалось доказать. Начало координат усиливает вектор. Интерполяция расточительно концентрирует экспериментальный разрыв функции, при этом, вместо 13 можно взять любую другую константу. Ряд Тейлора, в первом приближении, обуславливает </p>
							<div class="meta">
								<span>14 мая 2017</span>
								<span class="icon">16 189</span>
								<span class="icon">81</span>
							</div>
						</div>
						</div>
					</div>
					
				</div>
				
				
			</main>
			<?php include '../footer.php'; ?>
		</div></div>
		<script type="text/javascript">
		(function() { var s = document.createElement("script"); s.type = "text/javascript"; s.async = true; s.src = '//api.usersnap.com/load/2a3946d2-eed6-47f0-b743-1142bfdc00f4.js';
		var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); })();
		</script>
	</body>
</html>
