<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Content-Language" content="ru">
        <meta charset="utf-8">
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
        <title>Educational Portal - HTML Mockup</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="../style.css">

		<script src="/scripts/modernizr-2.8.3.min.js"></script>

	</head>
	<body id="companies-single" class="desktop">
		<div class="site-wrapper"><div class="site">
			<header>
				<div class="menu-overlay" style="opacity:0;visibility:hidden;"></div>
				<?php include '../header.php'; ?>
			</header>

			<main class="content">
				<!-- Start of breadcrumbs -->
				<div class="container"><div class="wrapper">
					<ul class="breadcrumbs">
						<li><a href="../">Главная Портала</a></li>
						<li><a href="#">Компании</a></li>
					</ul>
				</div></div>
				<!-- End of breadcrumbs -->
				<!-- Start of company information -->
				<div class="uni-info container"><div class="wrapper">
					<h2>Компания</h2>
					<div class="col-12">
						<div class="col-5">
							<h4 class="accent icon">85%</h4>
							<img src="/images/noname.jpg" alt="">
						</div>
						<div class="col-7">
							<p>Акронис открывает серию выездных школ для старшеклассников и студентов, интересующихся программированием, робототехникой, анализом данных и биоинформатикой. </p>

							<p>Участников ждут интересные проекты и новые знакомства.помогает участникам: самостоятельно принимать решения, ставить цели и шаг за шагом, достигать их, развить мотивацию к получению новых знаний, расширить кругозор, осознать свои возможности и приобрести уверенность для дальнейшего саморазвития, научиться работать 
							в команде, завести новых друзей и единомышленников.Каждый участник получит возможность реализовать свой проект под руководством экспертов: создать мобильные приложения и web-сервисы, умные устройства и беспилотники, познакомиться с big data, виртуальной реальностью, биоинформатикой, нейротехнологиями и т.дВ программе школы помимо реализации проектов представлены различные мастер-классы, встречи с учеными и успешными предпринимателями, профориентационные игры, экскурсии в компании и встречи 
							возможность реализовать свой проект под руководством экспертов: создать мобильные приложения и web-сервисы, умные устройства и беспилотники, познакомиться с big data, виртуальной реальностью, биоинформатикой, нейротехнологиями и т.дВ программе школы помимо реализации проектов представлены различные мастер-классы, встречи с учеными и успешными предпринимателями, профориентационные игры, экскурсии в компании и встречи </p>
						</div>
						<div class="small-left-side-c contact-info">
							<h4>Москва</h4>
							<h4>+7 (495) 777 77 77</h4>
							<h4>http://www.sfdhdslj.com/</h4>
						</div>
					</div>
				</div></div>
				<!-- End of company information -->
				<!-- Start of analytics & sidebar -->
				<div class="container"><div class="wrapper">
					<h2>Отраслевая аналитика</h2>
					<div class="col-6 analytics">
						<div class="col-12 highlight">
							<h4>Тренды:</h4>
							<a href="/analytics-single/"><h5>Обзор наиболее востребованных ваканский в ИТ-отрасли за Май 2017</h5></a>
							<h5>Контрпример соответствует интеграл по поверхности, что и требовалось доказать. Начало координат усиливает вектор. Интерполяция расточительно концентрирует экспериментальный разрыв функции, при этом, вместо 13 можно взять любую другую константу. Ряд Тейлора, в первом приближении, обуславливает параллельный многочлен.В общем, математический анализ транслирует предел функции.</h5>
							<div class="meta">
								<span>14 мая 2017</span>
								<span class="icon">16 189</span>
								<span class="icon">81</span>
							</div>
						</div>
						<div class="ana-cols">
							<div class="col-6-gutter">
								<h3><a href="#">Зарплаты</a></h3>
								<a href="/analytics-single/">Обзор наиболее востребованных вакансий</a>
								<p>Высшая арифметика по-прежнему востребована. Интеграл по бесконечной области расточительно оправдывает разрыв функции. Полином решительно раскручивает параллельный лист</p>
								<div class="meta">
									<span>14 мая 2017</span>
									<span class="icon">16 189</span>
									<span class="icon">81</span>
								</div>
							</div>
							<div class="col-6-gutter">
								<h3><a href="#">Зарплаты</a></h3>
								<a href="/analytics-single/">Обзор наиболее востребованных вакансий</a>
								<p>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. они просто.</p>
								<div class="meta">
									<span>14 мая 2017</span>
									<span class="icon">16 189</span>
									<span class="icon">81</span>
								</div>
							</div>
						</div>	
						<button><a href="/analytics-all/">ПОКАЗАТЬ БОЛЬШЕ СТАТЕЙ</a></button>
					</div>
					<div class="col-1">&nbsp;</div>
					<div class="col-5 sidebar">
						<h4 class="sidebar-title large">ВУЗы-Партнеры Компании</h4>
						<div class="unis">
							<ul class="remove-bullets">
								<li>
									<div class="img-wrapper">
										<img src="/images/uni-logo-3.png" alt="University logo">
									</div>
									<div class="uni-content">
										<p>Московский Государственный Технический Университет им.Баумана (МГТУ им.Баумана)</p>
										<span class="icon">116</span>
										<span class="icon">95%</span>
										<span class="icon">220</span>
									</div>
								</li>
								<li>
									<div class="img-wrapper">
										<img src="/images/uni-logo-2.png" alt="University logo">
									</div>
									<div class="uni-content">
										<p>Московский Государственный Технический Университет им.Баумана (МГТУ им.Баумана)</p>
										<span class="icon">116</span>
										<span class="icon">95%</span>
										<span class="icon">220</span>
									</div>
								</li>
								<li>
									<div class="img-wrapper">
										<img src="/images/uni-logo-4.png" alt="University logo">
									</div>
									<div class="uni-content">
										<p>Московский Государственный Технический Университет им.Баумана (МГТУ им.Баумана)</p>
										<span class="icon">116</span>
										<span class="icon">95%</span>
										<span class="icon">220</span>
									</div>
								</li>
								<li>
									<div class="img-wrapper">
										<img src="/images/uni-logo-5.png" alt="University logo">
									</div>
									<div class="uni-content">
										<p>Московский Государственный Технический Университет им.Баумана (МГТУ им.Баумана)</p>
										<span class="icon">116</span>
										<span class="icon">95%</span>
										<span class="icon">220</span>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div></div>
				<!-- Start of analytics & sidebar -->
				<div id="development-list" class="container"><div class="wrapper">
					<h2>События <span>(35)</span></h2>
					<div class="col-12">
						<div class="development-item col-3">
							<a href="/events-single/">
							<div class="extra-meta">
								<div class="main-meta">
									<h4>Открытая лекция</h4>
									<h4>20 июля в 19:00</h4>
								</div>

								<p class="quote">Теория систем и системный анализ</p>
								<h4>МГТУ им.Баумана</h4>
							</div>

							<div class="featured-img" style="background-image: url(/images/feat-placeh.jpg);">
								<div class="icon">Онлайн</div>
								<div class="icon">Бесплатно</div>
							</div>
							<div class="meta">
								<p class="quote">Теория систем и системный анализ</p>
								<h4>МГТУ им.Баумана</h4>
							</div>
							</a>
						</div>
						<div class="development-item col-3">
						<a href="/events-single/">
							<div class="extra-meta">
								<div class="main-meta">
									<h4>Открытая лекция</h4>
									<h4>20 июля в 19:00</h4>
								</div>

								<p class="quote">Теория систем и системный анализ</p>
								<h4>МГТУ им.Баумана</h4>
							</div>
							<div class="featured-img" style="background-image: url(/images/feat-placeh.jpg);">
								<div class="icon">Онлайн</div>
								<div class="icon">Бесплатно</div>
							</div>
							<div class="meta">
								<p class="quote">Теория систем и системный анализ</p>
								<h4>МГТУ им.Баумана</h4>
							</div>
							</a>
						</div>
						<div class="development-item col-3">
						<a href="/events-single/">
							<div class="extra-meta">
								<div class="main-meta">
									<h4>Открытая лекция</h4>
									<h4>20 июля в 19:00</h4>
								</div>

								<p class="quote">Теория систем и системный анализ</p>
								<h4>МГТУ им.Баумана</h4>
							</div>
							<div class="featured-img" style="background-image: url(/images/feat-placeh.jpg);">
								<div class="icon">Онлайн</div>
								<div class="icon">Бесплатно</div>
							</div>
							<div class="meta">
								<p class="quote">Теория систем и системный анализ</p>
								<h4>МГТУ им.Баумана</h4>
							</div>
							</a>
						</div>
						<div class="development-item col-3">
						<a href="/events-single/">
							<div class="extra-meta">
								<div class="main-meta">
									<h4>Открытая лекция</h4>
									<h4>20 июля в 19:00</h4>
								</div>

								<p class="quote">Теория систем и системный анализ</p>
								<h4>МГТУ им.Баумана</h4>
							</div>
							<div class="featured-img" style="background-image: url(/images/feat-placeh.jpg);">
								<div class="icon">Онлайн</div>
								<div class="icon">Бесплатно</div>
							</div>
							<div class="meta">
								<p class="quote">Теория систем и системный анализ</p>
								<h4>МГТУ им.Баумана</h4>
							</div>
							</a>
						</div>
					</div>
					<button><a href="/events-all/">ПОКАЗАТЬ БОЛЬШЕ СОБЫТИЙ</a></button>
				</div></div>
				<div class="video-grid container"><div class="wrapper">
					<div class="left-side-c">
						<h2>Онлайн курсы <span>(220)</span></h2>
						<div class="col-12">
							<div class="video-wrapper">
								<a href="/courses-single-applied/">
									<div class="video-container">
										<img src="/images/video-poster.jpg" alt="video poster">
										<div class="top-bar">
											<h6 class="icon category">Веб-разработка </h6>
											<h6 class="price check-circle">Оплачено</h6>
										</div>
										<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
										<div class="bottom-bar">
											<small class="certificate ">Сертификат об окончании</small>
											<h3 class="time-icon"> 120</h3>
										</div>
									</div>
									<span class="video-title"><h5>
										Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
									</h5></span>
									<h6 class="icon">НГУ</h6>
								</a>
							</div>
							<div class="video-wrapper">
								<a href="/courses-single-applied/">
									<div class="video-container">
										<img src="/images/video-poster.jpg" alt="video poster">
										<div class="top-bar">
											<h6 class="icon category">Веб-разработка </h6>
											<h6 class="price ruble">38 000</h6>
										</div>
										<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
										<div class="bottom-bar">
											<small class="certificate hide">Сертификат об окончании</small>
											<h3 class="time-icon"> 120</h3>
										</div>
									</div>
									<span class="video-title"><h5>
										Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
									</h5></span>
									<h6>Лаборатория Касперского</h6>
								</a>
							</div>
							<div class="video-wrapper">
								<a href="/courses-single-applied/">
									<div class="video-container">
										<img src="/images/video-poster.jpg" alt="video poster">
										<div class="top-bar">
											<h6 class="icon category">Веб-разработка </h6>
											<h6 class="price ruble">38 000</h6>
										</div>
										<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
										<div class="bottom-bar">
											<small class="certificate hide">Сертификат об окончании</small>
											<h3 class="time-icon"> 120</h3>
										</div>
									</div>
									<span class="video-title"><h5>
										Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
									</h5></span>
									<img src="/images/kaspersky-logo.png" alt="Kaspersky logo">
								</a>
							</div>
							<div class="video-wrapper">
								<a href="/courses-single-applied/">
									<div class="video-container">
										<img src="/images/video-poster.jpg" alt="video poster">
										<div class="top-bar">
											<h6 class="icon category">Веб-разработка </h6>
											<h6 class="price ruble">38 000</h6>
										</div>
										<i class="play"><small>НАЧАТЬ ОБУЧЕНИЕ</small></i>
										<div class="bottom-bar">
											<small class="certificate hide">Сертификат об окончании</small>
											<h3 class="time-icon"> 120</h3>
										</div>
									</div>
									<span class="video-title"><h5>
										Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
									</h5></span>
									<h6 class="icon">НГУ</h6>
								</a>
							</div>
						</div>
						<button id="add-more-vids"><a href="/courses-all/">ПОКАЗАТЬ БОЛЬШЕ КУРСОВ</a></button>
					</div>
				</div></div>
			</main>
			<?php include '../footer.php'; ?>
			<script type="text/javascript" src="/scripts/jquery.sumoselect.min.js"></script>
		</div></div>
		<script type="text/javascript">
		(function() { var s = document.createElement("script"); s.type = "text/javascript"; s.async = true; s.src = '//api.usersnap.com/load/2a3946d2-eed6-47f0-b743-1142bfdc00f4.js';
		var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); })();
		</script>
		<script type="text/javascript">
		$(document).ready(function(){
			function ribbonResize() {
				var certificate = $('main #courses .video-grid .col-3 .video-container .certificate');
				if (!certificate.hasClass('hide')) {
					var cH = $('.video-grid .video-wrapper .video-container .certificate').height();
					setTimeout(function(){
						$('.cert-style style').html('.video-grid .video-wrapper .video-container .certificate::after {border-width: '+(cH/2+5)+'px}');
					}, 100);
				}
			}
			function init() {
				ribbonResize();
			}
			
			init();

			$(window).resize(function(){
				init();
			});
			$('select.normal').SumoSelect({ csvDispCount: 2, selectAll: true });
			$('select.search').SumoSelect({ csvDispCount: 2, selectAll: true, search: true, searchText:'Enter here.' });
		});
		</script>
	</body>
</html>
