<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Content-Language" content="ru">
        <meta charset="utf-8">
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
        <title>Educational Portal - HTML Mockup</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="../style.css">

		<script src="/scripts/modernizr-2.8.3.min.js"></script>

	</head>
	<body id="single-courses" class="applied desktop">
		<div class="site-wrapper"><div class="site">
			<header>
				<div class="menu-overlay" style="opacity:0;visibility:hidden;"></div>
				<?php include '../header.php'; ?>
			</header>

			<main class="content">
				<!-- Start of breadcrumbs -->
				<div class="container"><div class="wrapper">
					<ul class="breadcrumbs">
						<li><a href="../">Главная Портала</a></li>
						<li><a href="#">Онлайн курсы</a></li>
						<li><a href="#">Веб-разработка</a></li>
						<li><a href="#">JAVA</a></li>
					</ul>
				</div></div>
				<!-- End of breadcrumbs -->
				<!-- Start of Primary & Sidebar -->
				<div class="container building-bg"><div class="wrapper">
					<!-- Start of sidebar -->
					<div id="sidebar">
						<div class="training">
							<button class="sidebar-title">
								<h4>ПРОДОЛЖИТЬ ОБУЧЕНИЕ</h4>
							</button>
							<ul>
								<a href="#"><li>35 000 руб</li></a>
								<a href="#"><li>120 часов видео</li></a>
								<a href="#"><li>5 тестов-опросников</li></a>
								<a href="#"><li>3 практических задания</li></a>
								<a href="#"><li>Зачетный экзамен</li></a>
								<a href="#"><li>Сертификат ВУЗа</li></a>
								<a href="#"><li>Сертификат Портала</li></a>
							</ul>
						</div>
						<div class="course-provided">
							<div class="sidebar-title large">
								<h4>Курс предоставлен</h4>
							</div>
							<ul>
								<li>
									<div class="img-wrapper">
										<img src="/images/uni-logo.png" alt="University logo">
									</div>
									<div class="uni-content">
										<p>МГТУ им.Баумана</p>
										<span class="icon">95%</span>
									</div>
								</li>
							</ul>
						</div>
						<div class="course-teacher">
							<div class="sidebar-title large">
								<h4>Ведущий курса</h4>
							</div>
							<img src="/images/teacher.jpg" alt="teacher\'s picture">
							<h5>
								Преображенский И.С.
							</h5>
							<p>
								Профессор физической теологии, доцент кафедры прикладной и теоретической физики
							</p>
							<p>
								В данном курсе слушатели изучат, каким образом использовать WebSphere MQ API (MQI) для работы с сообщениями и очередями сообщений. В курсе рассматриваются темы, посвященные разработке программ, оперирующих с атрибутами очередей сообщений, безопасностью IBM WebSphere MQ и управлением каналами. В рамках курса последовательно рассматриваются все API вызовы, в лабораторных работах нужно писать приложения, их использующие.
							</p>
						</div>
					</div>
					<!-- End of sidebar -->
					<!-- Start of primary -->
					<div id="primary">
						<!-- Start of course program -->
						<div id="course-program">
							<h2>Программа курса</h2>
							<ul>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle" /]
										<span>Обзор WebSphere MQ</span>
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>видео</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle" /]
										<h6 class="course-type">Упражнение</h6> Работа с объектами WebSphere
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>задание</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle" /]
										<span>Использование вызовов MQCONN, MQOPEN, MQCLOSE И MQDISC</span>
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>видео</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle" /]
										<span>Использование вызова MQPUT</span>
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>тест</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle" /]
										<h6 class="course-type">Упражнение</h6> Использование вызова MQPUT для создания сообщений
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>задание</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle" /]
										<span>Открытие очередей, MQMD, свойства сообщений</span>
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>видео</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle" /]
										<span>Использование вызовов MQGET и MQPUT1</span>
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>видео</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle-thin" /]
										<h6 class="course-type">Упражнение</h6> Работа с сообщениями
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>тест</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "ajust" /]
										<span>Контроль получения сообщений</span>
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>видео</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle-thin" /]
										<h6 class="course-type">Упражнение</h6> Создание динамических очередей и управления сообщениями типа Request
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>тест</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle-thin" /]
										<h6 class="course-type">Упражнение</h6> Получение и настройка свойств сообщений
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>тест</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle-thin" /]
										<span>Безопасность MQI</span>
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>видео</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle-thin" /]
										<span>Использование MQINQ и MQSET</span>
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>видео</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle-thin" /]
										<h6 class="course-type">Упражнение</h6> Работа с атрибутами очередей: MQINQ и MQSET
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>задание</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle-thin" /]
										<span>Поддержка транзакций и триггеринг</span>
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>видео</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle-thin" /]
										<h6 class="course-type">Упражнение</h6> Транзакции: RESPOND и MQTMCGET
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>тест</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle-thin" /]
										<span>Группировка и сегментация сообщений</span>
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>видео</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle-thin" /]
										<span>Асинхронное получение сообщений</span>
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>видео</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle-thin" /]
										<h6 class="course-type">Упражнение</h6> Асинхронное получение сообщений в WebSphere MQ
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>задание</span>
									</div>
								</li></a>
								<a href="#"><li>
									<div class="course-name">
										[@fa "circle-thin" /]
										<h6 class="course-type">Сертификационный экзамен</h6>
									</div>
									<div class="format">
										[@fa "circle-thin" /]<span>экзамен</span>
									</div>
								</li></a>
							</ul>
							<button id="continue-btn">
								<h4>ПРОДОЛЖИТЬ ОБУЧЕНИЕ</h4>
							</button>
						</div>
						<!-- End of course program -->
						<!-- Start of left side content -->
						<div class="left-side-c">
							<h2>
								Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSphere</h2>
							<p>
								В данном курсе слушатели изучат, каким образом использовать WebSphere MQ API (MQI) для работы с сообщениями и очередями сообщений. В курсе рассматриваются темы, посвященные разработке программ, оперирующих с атрибутами очередей сообщений, безопасностью IBM WebSphere MQ и управлением каналами. В рамках курса последовательно рассматриваются все API вызовы, в лабораторных работах нужно писать приложения, их использующие.
							</p>
						</div>
						<!-- End of left side content -->
						<!-- Start of list of what you get from doing the course -->
						<div class="gained-from-course">
							<h4>После изучения курса вы сможете:</h4>
						</div>
						<ul class="gained-list">
							<li>
								Проектировать и разрабатывать программы, использующие MQI
							</li>
							<li>
								Представлять различия при использовании MQI в зависимости от платформы
							</li>
							<li>
								Определять изменения в проектировании программ, необходимые для совместимости с концепцией сообщений и очередей сообщений
							</li>
							<li>
								Проектировать и разрабатывать программы, использующие расширенные возможности MQI (безопасность, группировка и сегментирование сообщений, списки распределения)
							</li>
							<li>
								Детально описывать MQI вызовы
							</li>
						</ul>
						<!-- End of list of what you get from doing the course -->
					</div>
					<!-- End of primary -->
				</div></div>
				<!-- End of Primary & Sidebar -->
				<div class="container video-grid"><div class="wrapper">
					<h2>Курсы, которые проходят вместе с этим</h2>
					<div class="video-wrapper">
						<div class="video-container">
							<img src="/images/video-poster.jpg" alt="video poster">
							<div class="top-bar">
								<h6 class="icon category">Веб-разработка </h6>
								<h6 class="price check-circle">Оплачено</h6>
							</div>
							<i class="play"><small><a href="/courses-single-applied/">НАЧАТЬ ОБУЧЕНИЕ</a></small></i>
							<div class="bottom-bar">
								<small class="certificate">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
								<h3 class="time-icon"> 120</h3>
							</div>
						</div>
						<a href="/courses-single-applied/" class="video-title"><h5>
							Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
						</h5></a>
						<h6 class="icon">НГУ</h6>
					</div>
					<div class="video-wrapper">
						<div class="video-container">
							<img src="/images/video-poster.jpg" alt="video poster">
							<div class="top-bar">
								<h6 class="icon category">Веб-разработка </h6>
								<h6 class="price check-circle">Оплачено</h6>
							</div>
							<i class="play"><small><a href="/courses-single-applied/">НАЧАТЬ ОБУЧЕНИЕ</a></small></i>
							<div class="bottom-bar">
								<small class="certificate">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
								<h3 class="time-icon"> 120</h3>
							</div>
						</div>
						<a href="/courses-single-applied/" class="video-title"><h5>
							Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
						</h5></a>
						<h6 class="icon">НГУ</h6>
					</div>
					<div class="video-wrapper">
						<div class="video-container">
							<img src="/images/video-poster.jpg" alt="video poster">
							<div class="top-bar">
								<h6 class="icon category">Веб-разработка </h6>
								<h6 class="price check-circle">Оплачено</h6>
							</div>
							<i class="play"><small><a href="/courses-single-applied/">НАЧАТЬ ОБУЧЕНИЕ</a></small></i>
							<div class="bottom-bar">
								<small class="certificate">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
								<h3 class="time-icon"> 120</h3>
							</div>
						</div>
						<a href="/courses-single-applied/" class="video-title"><h5>
							Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
						</h5></a>
						<h6 class="icon">НГУ</h6>
					</div>
					<div class="video-wrapper">
						<div class="video-container">
							<img src="/images/video-poster.jpg" alt="video poster">
							<div class="top-bar">
								<h6 class="icon category">Веб-разработка </h6>
								<h6 class="price check-circle">Оплачено</h6>
							</div>
							<i class="play"><small><a href="/courses-single-applied/">НАЧАТЬ ОБУЧЕНИЕ</a></small></i>
							<div class="bottom-bar">
								<small class="certificate">Сертификат об окончании<svg class="ribbon-shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156 27"><path d="M0 0h156s-7.268 12.87-7.53 13.5c-.2.48 7.53 13.5 7.53 13.5H0V0z"/></svg></small>
								<h3 class="time-icon"> 120</h3>
							</div>
						</div>
						<a href="/courses-single-applied/" class="video-title"><h5>
							Полный специальный курс по разработке высоконагруженных проектов на J2EE с IBM WebSpher
						</h5></a>
						<h6 class="icon">НГУ</h6>
					</div>
				</div></div>
			</main>
			<?php include '../footer.php'; ?>
		</div></div>
		<script type="text/javascript">
		(function() { var s = document.createElement("script"); s.type = "text/javascript"; s.async = true; s.src = '//api.usersnap.com/load/2a3946d2-eed6-47f0-b743-1142bfdc00f4.js';
		var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); })();
		</script>
	</body>
</html>
