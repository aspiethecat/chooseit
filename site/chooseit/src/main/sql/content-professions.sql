--
-- Tables
--

CREATE TABLE content_professions (
	content_id    IDENTIFIER NOT NULL,
	profession_id IDENTIFIER NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT content_professions_pkey
		PRIMARY KEY (content_id, profession_id),

	--
	-- Foreign keys
	--

	CONSTRAINT content_professions_content_id_fkey
		FOREIGN KEY (content_id) REFERENCES content_all (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE,

	CONSTRAINT content_professions_profession_id_fkey
		FOREIGN KEY (profession_id) REFERENCES professions (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
);

--
-- Foreign key indexes
--

CREATE INDEX content_professions_profession_id_fki
	ON content_professions (profession_id);

--
-- Comments
--

COMMENT ON TABLE content_professions IS 'Content professions';

COMMENT ON COLUMN content_professions.content_id    IS 'Content identifier';
COMMENT ON COLUMN content_professions.profession_id IS 'Profession identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE content_professions TO chooseit;
