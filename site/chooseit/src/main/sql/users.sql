--
-- Tables and sequences
--

CREATE SEQUENCE user_id;

CREATE TABLE users (
	id                IDENTIFIER NOT NULL DEFAULT NEXTVAL('user_id'),
	creation_time     TIMEVAL NOT NULL,
	modification_time TIMEVAL NOT NULL,
	provider_id       IDENTIFIER,
	name              TEXT NOT NULL,
	email             CHARACTER VARYING (128) NOT NULL,
	password_digest   DIGEST,
	password_salt     SALT,
	family_name       TEXT NOT NULL,
	other_names       TEXT NOT NULL,
	position          TEXT,
	status            ACCOUNT_STATUS NOT NULL DEFAULT 'NEW',
	reason_id         IDENTIFIER,
	reason_text       TEXT,
	description       TEXT,

	--
	-- Primary key
	--

	CONSTRAINT users_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT users_name_unq
		UNIQUE (name),

	CONSTRAINT users_email_unq
		UNIQUE (email),

	--
	-- Foreign keys
	--

	CONSTRAINT users_provider_id_fkey
		FOREIGN KEY (provider_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE,
	--
	-- Checks
	--
	CONSTRAINT users_modification_time_chk
		CHECK (modification_time >= creation_time)
);

ALTER SEQUENCE user_id OWNED BY users.id;

--
-- Foreign key indexes
--

CREATE INDEX users_provider_id_fki
	ON users (provider_id);

--
-- Comments
--

COMMENT ON TABLE users IS 'Users';
COMMENT ON SEQUENCE user_id IS 'User identifiers';

COMMENT ON COLUMN users.id                IS 'Unique identifier';
COMMENT ON COLUMN users.creation_time     IS 'Creation date and time (UTC)';
COMMENT ON COLUMN users.modification_time IS 'Last modification date and time (UTC)';
COMMENT ON COLUMN users.provider_id       IS 'Provider identifier';
COMMENT ON COLUMN users.name              IS 'Login name';
COMMENT ON COLUMN users.email             IS 'E-mail address';
COMMENT ON COLUMN users.password_digest   IS 'Password digest (SHA-256)';
COMMENT ON COLUMN users.password_salt     IS 'Password salt';
COMMENT ON COLUMN users.family_name       IS 'Family name';
COMMENT ON COLUMN users.other_names       IS 'Other names';
COMMENT ON COLUMN users.position          IS 'Position';
COMMENT ON COLUMN users.status            IS 'Status';
COMMENT ON COLUMN users.reason_id         IS 'Blocking reason identifier';
COMMENT ON COLUMN users.reason_text       IS 'Blocking reason description';
COMMENT ON COLUMN users.description       IS 'Short description';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE user_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE users TO chooseit;

--
-- Triggers
--

CREATE TRIGGER users_before_upsert_trg
	BEFORE INSERT OR UPDATE ON users
	FOR EACH ROW EXECUTE PROCEDURE before_entity_upsert();
