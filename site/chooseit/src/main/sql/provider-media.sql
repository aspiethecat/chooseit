--
-- Tables
--

CREATE TABLE provider_media (
	provider_id IDENTIFIER NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT provider_media_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT provider_media_name_unq
		UNIQUE (provider_id, type_id, name),

	--
	-- Foreign keys
	--

	CONSTRAINT provider_media_type_id_fkey
		FOREIGN KEY (type_id) REFERENCES media_types (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT provider_media_mime_type_id_fkey
		FOREIGN KEY (mime_type_id) REFERENCES mime_types (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT provider_media_provider_id_fkey
		FOREIGN KEY (provider_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
) INHERITS (media);

--
-- Foreign key indexes
--

CREATE INDEX provider_media_type_id_fki
	ON provider_media (type_id);

CREATE INDEX provider_media_mime_type_id_fki
	ON provider_media (mime_type_id);

--
-- Comments
--

COMMENT ON TABLE provider_media IS 'Providers media';

COMMENT ON COLUMN provider_media.id                IS 'Unique identifier';
COMMENT ON COLUMN provider_media.sid               IS 'Unique string identifier (random)';
COMMENT ON COLUMN provider_media.creation_time     IS 'Creation date and time (UTC)';
COMMENT ON COLUMN provider_media.modification_time IS 'Last modification date and time (UTC)';
COMMENT ON COLUMN provider_media.type_id           IS 'Type identifier';
COMMENT ON COLUMN provider_media.mime_type_id      IS 'MIME type identifier';
COMMENT ON COLUMN provider_media.name              IS 'Name';
COMMENT ON COLUMN provider_media.size              IS 'Size (in bytes)';
COMMENT ON COLUMN provider_media.description       IS 'Short description';
COMMENT ON COLUMN provider_media.provider_id       IS 'Provider identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE provider_media TO chooseit;
