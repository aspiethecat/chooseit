--
-- Tables
--

CREATE TABLE content_tags (
	content_id IDENTIFIER NOT NULL,
	tag_id     IDENTIFIER NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT content_tags_pkey
		PRIMARY KEY (content_id, tag_id),

	--
	-- Foreign keys
	--

	CONSTRAINT content_tags_content_id_fkey
		FOREIGN KEY (content_id) REFERENCES content_all (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE,

	CONSTRAINT content_tags_tag_id_fkey
		FOREIGN KEY (tag_id) REFERENCES tags (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
);

--
-- Foreign key indexes
--

CREATE INDEX content_tags_tag_id_fki
	ON content_tags (tag_id);

--
-- Comments
--

COMMENT ON TABLE content_tags IS 'Content tags';

COMMENT ON COLUMN content_tags.content_id IS 'Content identifier';
COMMENT ON COLUMN content_tags.tag_id     IS 'Tag identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE content_tags TO chooseit;
