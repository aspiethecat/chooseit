--
-- Tables and sequences
--

CREATE SEQUENCE auth_session_id;

CREATE TABLE auth_sessions (
	id              IDENTIFIER NOT NULL DEFAULT NEXTVAL('auth_session_id'),
	creation_time   TIMEVAL NOT NULL,
	expiration_time TIMEVAL NOT NULL,
	extended_id     SELECTOR NOT NULL,
	user_id         IDENTIFIER NOT NULL,
	ip_address      INET NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT auth_sessions_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT auth_sessions_extended_id_unq
		UNIQUE (extended_id),

	--
	-- Foreign keys
	--

	CONSTRAINT auth_sessions_user_id_fkey
		FOREIGN KEY (user_id) REFERENCES users (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
);

ALTER SEQUENCE auth_session_id OWNED BY auth_sessions.id;

--
-- Foreign key indexes
--

CREATE INDEX auth_sessions_user_id_fki
	ON auth_sessions (user_id);

--
-- Comments
--

COMMENT ON TABLE auth_sessions IS 'Authentication sessions';
COMMENT ON SEQUENCE auth_session_id IS 'Authentication session identifiers';

COMMENT ON COLUMN auth_sessions.id              IS 'Unique identifier';
COMMENT ON COLUMN auth_sessions.creation_time   IS 'Creation date and time (UTC)';
COMMENT ON COLUMN auth_sessions.expiration_time IS 'Expiration date and time (UTC)';
COMMENT ON COLUMN auth_sessions.extended_id     IS 'Extended identifier';
COMMENT ON COLUMN auth_sessions.user_id         IS 'User identifier';
COMMENT ON COLUMN auth_sessions.ip_address      IS 'IP address';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE auth_session_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE auth_sessions TO chooseit;
