--
-- Tables and sequences
--

CREATE SEQUENCE tag_id;

CREATE TABLE tags (
	id   IDENTIFIER NOT NULL DEFAULT NEXTVAL('tag_id'),
	name TEXT NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT tags_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT tags_name_unq
		UNIQUE (name)
);

ALTER SEQUENCE tag_id OWNED BY tags.id;

--
-- Comments
--

COMMENT ON TABLE tags IS 'Tags';
COMMENT ON SEQUENCE tag_id IS 'Tag identifiers';

COMMENT ON COLUMN tags.id   IS 'Unique identifier';
COMMENT ON COLUMN tags.name IS 'Name';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE tag_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE tags TO chooseit;
