--
-- Tables
--

CREATE TABLE content_media (
	content_id IDENTIFIER NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT content_media_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT content_media_name_unq
		UNIQUE (content_id, type_id, name),

	--
	-- Foreign keys
	--

	CONSTRAINT content_media_type_id_fkey
		FOREIGN KEY (type_id) REFERENCES media_types (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT content_media_mime_type_id_fkey
		FOREIGN KEY (mime_type_id) REFERENCES mime_types (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT content_media_content_id_fkey
		FOREIGN KEY (content_id) REFERENCES content_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
) INHERITS (media);

--
-- Foreign key indexes
--

CREATE INDEX content_media_type_id_fki
	ON content_media (type_id);

CREATE INDEX content_media_mime_type_id_fki
	ON content_media (mime_type_id);

--
-- Comments
--

COMMENT ON TABLE content_media IS 'Content media';

COMMENT ON COLUMN content_media.id                IS 'Unique identifier';
COMMENT ON COLUMN content_media.sid               IS 'Unique string identifier (random)';
COMMENT ON COLUMN content_media.creation_time     IS 'Creation date and time (UTC)';
COMMENT ON COLUMN content_media.modification_time IS 'Last modification date and time (UTC)';
COMMENT ON COLUMN content_media.type_id           IS 'Type identifier';
COMMENT ON COLUMN content_media.mime_type_id      IS 'MIME type identifier';
COMMENT ON COLUMN content_media.name              IS 'Name';
COMMENT ON COLUMN content_media.size              IS 'Size (in bytes)';
COMMENT ON COLUMN content_media.description       IS 'Short description';
COMMENT ON COLUMN content_media.content_id        IS 'Content identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE content_media TO chooseit;
