--
-- Tables and sequences
--

CREATE SEQUENCE language_id;

CREATE TABLE languages (
	id         IDENTIFIER NOT NULL DEFAULT NEXTVAL('language_id'),
	iso_alpha2 CHARACTER (2) NOT NULL,
	iso_alpha3 CHARACTER (3) NOT NULL,
	name       TEXT NOT NULL,
	priority   SMALLINT NOT NULL DEFAULT 0,

	--
	-- Primary key
	--

	CONSTRAINT languages_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT languages_iso_alpha2_unq
		UNIQUE (iso_alpha2),

	CONSTRAINT languages_iso_alpha3_unq
		UNIQUE (iso_alpha3)
);

ALTER SEQUENCE language_id OWNED BY languages.id;

--
-- Comments
--

COMMENT ON TABLE languages IS 'Languages';
COMMENT ON SEQUENCE language_id IS 'Language identifiers';

COMMENT ON COLUMN languages.id         IS 'Unique identifier';
COMMENT ON COLUMN languages.iso_alpha2 IS 'ISO 639 Alpha-2 code';
COMMENT ON COLUMN languages.iso_alpha3 IS 'ISO 639 Alpha-3 code';
COMMENT ON COLUMN languages.name       IS 'Name';
COMMENT ON COLUMN languages.priority   IS 'Display priority';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE language_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE languages TO chooseit;

--
-- Data
--

INSERT INTO languages
	(iso_alpha2, iso_alpha3, name, priority)
VALUES
	('RU', 'RUS', 'Русский', 0),
	('EN', 'ENG', 'Английский', 0),
	('DE', 'DEU', 'Немецкий', 1),
	('JA', 'JPN', 'Японский', 1);
