--
-- Tables
--

CREATE TABLE provider_stats (
	id            IDENTIFIER NOT NULL,
	article_count INTEGER NOT NULL DEFAULT 0,
	event_count   INTEGER NOT NULL DEFAULT 0,
	course_count  INTEGER NOT NULL DEFAULT 0,

	--
	-- Primary key
	--

	CONSTRAINT provider_stats_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT provider_stats_id_fkey
		FOREIGN KEY (id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
);

--
-- Comments
--

COMMENT ON TABLE provider_stats IS 'Provider statistics';

COMMENT ON COLUMN provider_stats.id            IS 'Provider identifier';
COMMENT ON COLUMN provider_stats.article_count IS 'Articles count';
COMMENT ON COLUMN provider_stats.event_count   IS 'Events count';
COMMENT ON COLUMN provider_stats.course_count  IS 'Courses count';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE provider_stats TO chooseit;
