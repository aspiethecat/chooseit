--
-- Tables
--

CREATE TABLE location_types (
	id   IDENTIFIER NOT NULL,
	sid  CODE NOT NULL,
	name TEXT NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT location_types_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT location_types_sid_unq
		UNIQUE (sid),

	CONSTRAINT location_types_name_unq
		UNIQUE (name)
);

--
-- Comments
--

COMMENT ON TABLE location_types IS 'Location types';

COMMENT ON COLUMN location_types.id   IS 'Unique identifier';
COMMENT ON COLUMN location_types.sid  IS 'Unique string identifier';
COMMENT ON COLUMN location_types.name IS 'Name';

--
-- Functions
--

CREATE OR REPLACE FUNCTION location_type_id (CODE)
	RETURNS IDENTIFIER AS $$
		SELECT id FROM location_types WHERE sid = $1
	$$ LANGUAGE SQL STABLE;

--
-- Access permissions
--

GRANT SELECT ON TABLE location_types TO chooseit;
GRANT EXECUTE ON FUNCTION location_type_id (CODE) TO chooseit;

--
-- Data
--

INSERT INTO location_types
	(id, sid, name)
VALUES
	(1, 'COUNTRY',  'Страна'),
	(2, 'STATE',    'Штат'),
	(3, 'REPUBLIC', 'Республика'),
	(4, 'DISTRICT', 'Округ'),
	(5, 'AREA',     'Область'),
	(6, 'REGION',   'Район'),
	(7, 'CITY',     'Город'),
	(8, 'STREET',   'Улица');
