--
-- Tables
--

CREATE TABLE group_permissions (
	group_id      IDENTIFIER NOT NULL,
	permission_id IDENTIFIER NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT group_permissions_pkey
		PRIMARY KEY (group_id, permission_id),

	--
	-- Foreign keys
	--

	CONSTRAINT group_permissions_group_id_fkey
		FOREIGN KEY (group_id) REFERENCES groups (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE,

	CONSTRAINT group_permissions_permission_id_fkey
		FOREIGN KEY (permission_id) REFERENCES permissions (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
);

--
-- Foreign key indexes
--

CREATE INDEX group_permissions_permission_id_fki
	ON group_permissions (permission_id);

--
-- Comments
--

COMMENT ON TABLE group_permissions IS 'Group permissions';

COMMENT ON COLUMN group_permissions.group_id      IS 'Group identifier';
COMMENT ON COLUMN group_permissions.permission_id IS 'Permission identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE group_permissions TO chooseit;
