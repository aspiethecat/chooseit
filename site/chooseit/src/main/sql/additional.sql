--
-- Tables
--

CREATE TABLE additional (
	--
	-- Primary key
	--

	CONSTRAINT additional_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT additional_id_fkey
		FOREIGN KEY (id) REFERENCES content_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT additional_provider_id_fkey
		FOREIGN KEY (provider_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	--
	-- Checks
	--

	CONSTRAINT additional_modification_time_chk
		CHECK (modification_time >= creation_time),

	CONSTRAINT additional_provider_name_chk
		CHECK ((provider_name IS NOT NULL) OR (provider_id IS NOT NULL)),

	CONSTRAINT additional_provider_url_chk
		CHECK ((provider_url IS NOT NULL) OR (provider_id IS NOT NULL))
) INHERITS (content);

--
-- Unique indexes
--

CREATE UNIQUE INDEX additional_name_unq1
	ON additional (provider_name, name)
	WHERE provider_id IS NULL;

CREATE UNIQUE INDEX additional_name_unq2
	ON additional (provider_id, name)
	WHERE provider_id IS NOT NULL;

--
-- Foreign key indexes
--

CREATE INDEX additional_provider_id_fki
	ON additional (provider_id);

--
-- Comments
--

COMMENT ON TABLE additional IS 'Additional education';

COMMENT ON COLUMN additional.id                IS 'Unique identifier';
COMMENT ON COLUMN additional.creation_time     IS 'Creation date and time (UTC)';
COMMENT ON COLUMN additional.modification_time IS 'Last modification date and time (UTC)';
COMMENT ON COLUMN additional.provider_id       IS 'Provider identifier';
COMMENT ON COLUMN additional.provider_name     IS 'Provider name';
COMMENT ON COLUMN additional.provider_url      IS 'Provider URL';
COMMENT ON COLUMN additional.name              IS 'Name';
COMMENT ON COLUMN additional.url               IS 'URL';
COMMENT ON COLUMN additional.status            IS 'Status';
COMMENT ON COLUMN additional.description       IS 'Short description';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE additional TO chooseit;

--
-- Triggers
--

CREATE TRIGGER additional_before_upsert_trg
	BEFORE INSERT OR UPDATE ON additional
	FOR EACH ROW EXECUTE PROCEDURE before_entity_upsert();

CREATE TRIGGER additional_before_insert_trg
	BEFORE INSERT ON additional
	FOR EACH ROW EXECUTE PROCEDURE before_content_insert();

CREATE TRIGGER additional_after_delete_trg
	AFTER DELETE ON additional
	FOR EACH ROW EXECUTE PROCEDURE after_content_delete();
