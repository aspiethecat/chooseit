--
-- Tables
--

CREATE TABLE employers (
	--
	-- Primary key
	--
	CONSTRAINT employers_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT employers_id_fkey
		FOREIGN KEY (id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT employers_parent_id_fkey
		FOREIGN KEY (parent_id) REFERENCES employers (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT employers_location_id_fkey
		FOREIGN KEY (location_id) REFERENCES locations (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	--
	-- Checks
	--

	CONSTRAINT employers_modification_time_chk
		CHECK (modification_time >= creation_time)
) INHERITS (providers);

--
-- Unique indexes
--

CREATE UNIQUE INDEX employers_name_unq1
	ON employers (name)
	WHERE parent_id IS NULL;

CREATE UNIQUE INDEX employers_name_unq2
	ON employers (parent_id, name)
	WHERE parent_id IS NOT NULL;

--
-- Foreign key indexes
--

CREATE INDEX employers_location_id_fki
	ON employers (location_id);

--
-- Comments
--

COMMENT ON TABLE employers IS 'Employers';

COMMENT ON COLUMN employers.id                IS 'Unique identifier';
COMMENT ON COLUMN employers.creation_time     IS 'Creation date and time (UTC)';
COMMENT ON COLUMN employers.modification_time IS 'Last modification date and time (UTC)';
COMMENT ON COLUMN employers.parent_id         IS 'Parent provider identifier';
COMMENT ON COLUMN employers.children          IS 'Children count';
COMMENT ON COLUMN employers.name              IS 'Name';
COMMENT ON COLUMN employers.site_url          IS 'Web-site URL';
COMMENT ON COLUMN employers.video_url         IS 'Video presentation URL';
COMMENT ON COLUMN employers.location_id       IS 'Location identifier';
COMMENT ON COLUMN employers.email             IS 'Primary e-mail email';
COMMENT ON COLUMN employers.phone             IS 'Primary phone number';
COMMENT ON COLUMN employers.phone_ext         IS 'Primary phone extension number';
COMMENT ON COLUMN employers.address           IS 'Street address';
COMMENT ON COLUMN employers.zip_code          IS 'ZIP code';
COMMENT ON COLUMN employers.status            IS 'Status';
COMMENT ON COLUMN employers.description       IS 'Short description';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE employers TO chooseit;

--
-- Triggers
--

CREATE TRIGGER employers_before_upsert_trg1
	BEFORE INSERT OR UPDATE ON employers
	FOR EACH ROW EXECUTE PROCEDURE before_entity_upsert();

CREATE TRIGGER employers_before_upsert_trg2
	BEFORE INSERT OR UPDATE ON employers
	FOR EACH ROW EXECUTE PROCEDURE before_tree_upsert();

CREATE TRIGGER employers_before_insert_trg
	BEFORE INSERT ON employers
	FOR EACH ROW EXECUTE PROCEDURE before_provider_insert();

CREATE TRIGGER employers_after_delete_trg
	AFTER DELETE ON employers
	FOR EACH ROW EXECUTE PROCEDURE after_provider_delete();
