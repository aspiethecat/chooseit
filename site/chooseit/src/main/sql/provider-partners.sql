--
-- Tables
--

CREATE TABLE provider_partners (
	provider_id IDENTIFIER NOT NULL,
	partner_id  IDENTIFIER NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT provider_partners_pkey
		PRIMARY KEY (provider_id, partner_id),

	--
	-- Foreign keys
	--

	CONSTRAINT provider_partners_provider_id_fkey
		FOREIGN KEY (provider_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE,

	CONSTRAINT provider_partners_partner_id_fkey
		FOREIGN KEY (partner_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
);

--
-- Foreign key indexes
--

CREATE INDEX provider_partners_partner_id_fki
	ON provider_partners (partner_id);

--
-- Comments
--

COMMENT ON TABLE provider_partners IS 'Provider partners';

COMMENT ON COLUMN provider_partners.provider_id IS 'Provider identifier';
COMMENT ON COLUMN provider_partners.partner_id  IS 'Partner identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE provider_partners TO chooseit;
