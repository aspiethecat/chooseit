--
-- Tables and sequences
--

CREATE SEQUENCE media_id;

CREATE TABLE media (
	id                IDENTIFIER NOT NULL DEFAULT NEXTVAL('media_id'),
	sid               CODE NOT NULL,
	creation_time     TIMEVAL NOT NULL,
	modification_time TIMEVAL NOT NULL,
	type_id           IDENTIFIER NOT NULL,
	mime_type_id      IDENTIFIER NOT NULL,
	name              TEXT NOT NULL,
	size              BIGINT NOT NULL,
	description       TEXT,

	--
	-- Primary key
	--

	CONSTRAINT media_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT media_type_id_fkey
		FOREIGN KEY (type_id) REFERENCES media_types (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT media_mime_type_id_fkey
		FOREIGN KEY (mime_type_id) REFERENCES mime_types (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
);

ALTER SEQUENCE media_id OWNED BY media.id;

--
-- Foreign key indexes
--

CREATE INDEX media_type_id_fki
	ON media (type_id);

CREATE INDEX media_mime_type_id_fki
	ON media (mime_type_id);

--
-- Comments
--

COMMENT ON TABLE media IS 'Media resources';
COMMENT ON SEQUENCE media_id IS 'Media resource identifiers';

COMMENT ON COLUMN media.id                IS 'Unique identifier';
COMMENT ON COLUMN media.sid               IS 'Unique string identifier (random)';
COMMENT ON COLUMN media.creation_time     IS 'Creation date and time (UTC)';
COMMENT ON COLUMN media.modification_time IS 'Last modification date and time (UTC)';
COMMENT ON COLUMN media.type_id           IS 'Type identifier';
COMMENT ON COLUMN media.mime_type_id      IS 'MIME type identifier';
COMMENT ON COLUMN media.name              IS 'Name';
COMMENT ON COLUMN media.size              IS 'Size (in bytes)';
COMMENT ON COLUMN media.description       IS 'Short description';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE media_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE media TO chooseit;

--
-- Triggers
--

CREATE TRIGGER media_before_upsert_trg
	BEFORE INSERT OR UPDATE ON media
	FOR EACH ROW EXECUTE PROCEDURE prevent_upsert();
