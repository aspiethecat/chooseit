--
-- Tables
--

CREATE TABLE content_users (
	content_id IDENTIFIER NOT NULL,
	user_id    IDENTIFIER NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT content_users_pkey
		PRIMARY KEY (content_id, user_id),

	--
	-- Foreign keys
	--

	CONSTRAINT content_users_content_id_fkey
		FOREIGN KEY (content_id) REFERENCES content_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT content_users_user_id_fkey
		FOREIGN KEY (user_id) REFERENCES users (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
);

--
-- Foreign key indexes
--

CREATE INDEX content_users_user_id_fki
	ON content_users (user_id);

--
-- Comments
--

COMMENT ON TABLE content_users IS 'Content users';

COMMENT ON COLUMN content_users.content_id IS 'Content identifier';
COMMENT ON COLUMN content_users.user_id    IS 'User identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE content_users TO chooseit;
