--
-- Tables and sequences
--

CREATE SEQUENCE university_type_id;

CREATE TABLE university_types (
	id          IDENTIFIER NOT NULL DEFAULT NEXTVAL('university_type_id'),
	name        TEXT NOT NULL,
	description TEXT,

	--
	-- Primary key
	--

	CONSTRAINT university_types_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT university_types_name_unq
		UNIQUE (name)
);

ALTER SEQUENCE university_type_id OWNED BY university_types.id;

--
-- Comments
--

COMMENT ON TABLE university_types IS 'University types';
COMMENT ON SEQUENCE university_type_id IS 'University type identifiers';

COMMENT ON COLUMN university_types.id          IS 'Unique identifier';
COMMENT ON COLUMN university_types.name        IS 'Name';
COMMENT ON COLUMN university_types.description IS 'Short description';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE university_type_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE university_types TO chooseit;

--
-- Data
--

INSERT INTO university_types
	(name)
VALUES
	('Университет'),
	('Центр профессионального образования');
