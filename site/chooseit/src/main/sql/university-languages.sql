--
-- Tables
--

CREATE TABLE university_languages (
	university_id IDENTIFIER NOT NULL,
	language_id   IDENTIFIER NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT university_languages_pkey
		PRIMARY KEY (university_id, language_id),

	--
	-- Foreign keys
	--

	CONSTRAINT university_languages_university_id_fkey
		FOREIGN KEY (university_id) REFERENCES universities (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE,

	CONSTRAINT university_languages_language_id_fkey
		FOREIGN KEY (language_id) REFERENCES languages (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
);

--
-- Foreign key indexes
--

CREATE INDEX university_languages_language_id_fki
	ON university_languages (language_id);

--
-- Comments
--

COMMENT ON TABLE university_languages IS 'University teching languages';

COMMENT ON COLUMN university_languages.university_id IS 'University identifier';
COMMENT ON COLUMN university_languages.language_id   IS 'Language identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE university_languages TO chooseit;
