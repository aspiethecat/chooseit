--
-- Tables
--

CREATE TABLE profession_specializations (
	profession_id     IDENTIFIER NOT NULL,
	specialization_id IDENTIFIER NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT profession_specializations_pkey
		PRIMARY KEY (profession_id, specialization_id),

	--
	-- Foreign keys
	--

	CONSTRAINT profession_specializations_profession_id_fkey
		FOREIGN KEY (profession_id) REFERENCES professions (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE,

	CONSTRAINT profession_specializations_specialization_id_fkey
		FOREIGN KEY (specialization_id) REFERENCES specializations (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
);

--
-- Foreign key indexes
--

CREATE INDEX profession_specializations_specialization_id_fki
	ON profession_specializations (specialization_id);

--
-- Comments
--

COMMENT ON TABLE profession_specializations IS 'Profession specializations';

COMMENT ON COLUMN profession_specializations.profession_id     IS 'Profession identifier';
COMMENT ON COLUMN profession_specializations.specialization_id IS 'Specialization identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE profession_specializations TO chooseit;
