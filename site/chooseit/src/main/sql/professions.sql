--
-- Tables and sequences
--

CREATE SEQUENCE profession_id;

CREATE TABLE professions (
	id          IDENTIFIER NOT NULL DEFAULT NEXTVAL('profession_id'),
	parent_id   IDENTIFIER,
	children    INTEGER NOT NULL,
	name        TEXT NOT NULL,
	description TEXT,

	--
	-- Primary key
	--

	CONSTRAINT professions_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT professions_parent_id_fkey
		FOREIGN KEY (parent_id) REFERENCES professions (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
);

ALTER SEQUENCE profession_id OWNED BY professions.id;

--
-- Unique indexes
--

CREATE UNIQUE INDEX professions_name_unq1
	ON professions (name)
	WHERE parent_id IS NULL;

CREATE UNIQUE INDEX professions_name_unq2
	ON professions (parent_id, name)
	WHERE parent_id IS NOT NULL;

--
-- Comments
--

COMMENT ON TABLE professions IS 'Professions';
COMMENT ON SEQUENCE profession_id IS 'Profession identifiers';

COMMENT ON COLUMN professions.id          IS 'Unique identifier';
COMMENT ON COLUMN professions.parent_id   IS 'Parent profession identifier';
COMMENT ON COLUMN professions.children    IS 'Children count';
COMMENT ON COLUMN professions.name        IS 'Name';
COMMENT ON COLUMN professions.description IS 'Short description';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE profession_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE professions TO chooseit;

--
-- Triggers
--

CREATE TRIGGER professions_before_upsert_trg
	BEFORE INSERT OR UPDATE ON professions
	FOR EACH ROW EXECUTE PROCEDURE before_tree_upsert();
