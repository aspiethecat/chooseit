--
-- Tables
--

CREATE TABLE preliminary (
	start_date  DATE NOT NULL,
	total_hours SMALLINT NOT NULL,
	total_weeks SMALLINT,
	price       MONEY,
	currency_id IDENTIFIER,

	--
	-- Primary key
	--

	CONSTRAINT preliminary_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT preliminary_id_fkey
		FOREIGN KEY (id) REFERENCES content_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT preliminary_provider_id_fkey
		FOREIGN KEY (provider_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT preliminary_currency_id_fkey
		FOREIGN KEY (currency_id) REFERENCES currencies (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	--
	-- Checks
	--
	CONSTRAINT preliminary_modification_time_chk
		CHECK (modification_time >= creation_time),

	CONSTRAINT preliminary_provider_name_chk
		CHECK ((provider_name IS NOT NULL) OR (provider_id IS NOT NULL)),

	CONSTRAINT preliminary_provider_url_chk
		CHECK ((provider_url IS NOT NULL) OR (provider_id IS NOT NULL)),

	CONSTRAINT preliminary_total_hours_chk
		CHECK (total_hours > 0),

	CONSTRAINT preliminary_total_weeks_chk
		CHECK ((total_weeks IS NULL) OR (total_weeks > 0)),

	CONSTRAINT preliminary_currency_id_chk
		CHECK (
			((price IS NULL) AND (currency_id IS NULL)) OR
			((price IS NOT NULL) AND (currency_id IS NOT NULL)))
) INHERITS (content);

--
-- Unique indexes
--

CREATE UNIQUE INDEX preliminary_name_unq1
	ON preliminary (provider_name, name)
	WHERE provider_id IS NULL;

CREATE UNIQUE INDEX preliminary_name_unq2
	ON preliminary (provider_id, name)
	WHERE provider_id IS NOT NULL;

--
-- Foreign key indexes
--

CREATE INDEX preliminary_currency_id_fki
	ON preliminary (currency_id);

--
-- Comments
--

COMMENT ON TABLE preliminary IS 'Preliminary education';

COMMENT ON COLUMN preliminary.id                IS 'Unique identifier';
COMMENT ON COLUMN preliminary.creation_time     IS 'Creation date and time (UTC)';
COMMENT ON COLUMN preliminary.modification_time IS 'Last modification date and time (UTC)';
COMMENT ON COLUMN preliminary.provider_id       IS 'Provider identifier';
COMMENT ON COLUMN preliminary.provider_name     IS 'Provider name';
COMMENT ON COLUMN preliminary.provider_url      IS 'Provider URL';
COMMENT ON COLUMN preliminary.name              IS 'Name';
COMMENT ON COLUMN preliminary.url               IS 'URL';
COMMENT ON COLUMN preliminary.status            IS 'Status';
COMMENT ON COLUMN preliminary.description       IS 'Short description';
COMMENT ON COLUMN preliminary.start_date        IS 'Start date';
COMMENT ON COLUMN preliminary.total_hours       IS 'Duration (academic hours)';
COMMENT ON COLUMN preliminary.total_weeks       IS 'Duration (in weeks)';
COMMENT ON COLUMN preliminary.price             IS 'Price';
COMMENT ON COLUMN preliminary.currency_id       IS 'Currency identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE preliminary TO chooseit;

--
-- Triggers
--

CREATE TRIGGER preliminary_before_upsert_trg
	BEFORE INSERT OR UPDATE ON preliminary
	FOR EACH ROW EXECUTE PROCEDURE before_entity_upsert();

CREATE TRIGGER preliminary_before_insert_trg
	BEFORE INSERT ON preliminary
	FOR EACH ROW EXECUTE PROCEDURE before_content_insert();

CREATE TRIGGER preliminary_after_delete_trg
	AFTER DELETE ON preliminary
	FOR EACH ROW EXECUTE PROCEDURE after_content_delete();
