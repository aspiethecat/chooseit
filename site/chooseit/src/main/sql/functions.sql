--
-- Functions
--

CREATE OR REPLACE FUNCTION prevent_upsert()
	RETURNS TRIGGER
	AS $$
		BEGIN
			RAISE EXCEPTION 'Upsert not allowed';
		END;
	$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION before_entity_upsert()
	RETURNS TRIGGER
	AS $$
		BEGIN
			IF (NEW.creation_time IS NOT NULL) THEN
				IF ((TG_OP = 'INSERT') OR (NEW.creation_time <> OLD.creation_time)) THEN
					RAISE EXCEPTION 'Setting creation time not allowed';
				END IF;
			END IF;
			IF (NEW.modification_time IS NOT NULL) THEN
				IF ((TG_OP = 'INSERT') OR (NEW.creation_time <> OLD.creation_time)) THEN
					RAISE EXCEPTION 'Setting modification time not allowed';
				END IF;
			END IF;

			IF (TG_OP = 'INSERT') THEN
				NEW.creation_time = CURRENT_TIMESTAMP AT TIME ZONE 'UTC';
			END IF;

			NEW.modification_time = CURRENT_TIMESTAMP AT TIME ZONE 'UTC';

			RETURN NEW;
		END;
	$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION before_tree_upsert()
	RETURNS TRIGGER
	AS $$
		BEGIN
			IF (TG_OP = 'INSERT') THEN
				NEW.children := 0;
			END IF;

			IF ((TG_OP = 'UPDATE') AND (OLD.parent_id IS NOT NULL)) THEN
				IF ((NEW.parent_id IS NULL) OR (NEW.parent_id <> OLD.parent_id)) THEN
					EXECUTE
						FORMAT('UPDATE %I SET children = children - 1 WHERE id = $1', TG_TABLE_NAME)
						USING OLD.parent_id;
				END IF;
			END IF;

			IF (NEW.parent_id IS NOT NULL) THEN
				IF ((TG_OP = 'INSERT') OR ((OLD.parent_id IS NULL) OR (OLD.parent_id <> NEW.parent_id))) THEN
					EXECUTE
						FORMAT('UPDATE %I SET children = children + 1 WHERE id = $1', TG_TABLE_NAME)
						USING NEW.parent_id;
				END IF;
			END IF;

			RETURN NEW;
		END;
	$$ LANGUAGE PLPGSQL;

--
-- Access permissions
--

GRANT EXECUTE ON FUNCTION prevent_upsert () TO chooseit;
GRANT EXECUTE ON FUNCTION before_entity_upsert() TO chooseit;
GRANT EXECUTE ON FUNCTION before_tree_upsert() TO chooseit;
