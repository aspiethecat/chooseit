--
-- Tables
--

CREATE TABLE provider_rating (
	provider_id IDENTIFIER NOT NULL,
	rating      DECIMAL(2, 1) NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT provider_rating_pkey
		PRIMARY KEY (provider_id),

	--
	-- Foreign keys
	--

	CONSTRAINT provider_rating_provider_id_fkey
		FOREIGN KEY (provider_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
);

--
-- Comments
--

COMMENT ON TABLE provider_rating IS 'Provider ratings';

COMMENT ON COLUMN provider_rating.provider_id IS 'Provider identifier';
COMMENT ON COLUMN provider_rating.rating      IS 'Rating score';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE provider_rating TO chooseit;
