--
-- Tables and sequences
--

CREATE SEQUENCE event_type_id;

CREATE TABLE event_types (
	id          IDENTIFIER NOT NULL DEFAULT NEXTVAL('event_type_id'),
	name        TEXT NOT NULL,
	description TEXT,

	--
	-- Primary key
	--

	CONSTRAINT event_types_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT event_types_name_unq
		UNIQUE (name)
);

ALTER SEQUENCE event_type_id OWNED BY event_types.id;

--
-- Comments
--

COMMENT ON TABLE event_types IS 'Event types';
COMMENT ON SEQUENCE event_type_id IS 'Event type identifiers';

COMMENT ON COLUMN event_types.id          IS 'Unique identifier';
COMMENT ON COLUMN event_types.name        IS 'Name';
COMMENT ON COLUMN event_types.description IS 'Short description';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE event_type_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE event_types TO chooseit;

--
-- Data
--

INSERT INTO event_types
	(name)
VALUES
	('Лекция'),
	('Вебинар'),
	('Встреча'),
	('Мастер-класс');
