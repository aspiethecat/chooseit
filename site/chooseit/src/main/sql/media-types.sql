--
-- Tables
--

CREATE TABLE media_types (
	id   IDENTIFIER NOT NULL,
	sid  CODE NOT NULL,
	name TEXT NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT media_types_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT media_types_sid_unq
		UNIQUE (sid)
);

--
-- Comments
--

COMMENT ON TABLE media_types IS 'Media types';

COMMENT ON COLUMN media_types.id   IS 'Unique identifier';
COMMENT ON COLUMN media_types.sid  IS 'Unique string identifier';
COMMENT ON COLUMN media_types.name IS 'Name';

--
-- Functions
--

CREATE OR REPLACE FUNCTION media_type_id (CODE)
	RETURNS IDENTIFIER AS $$
		SELECT id FROM media_types WHERE sid = $1
	$$ LANGUAGE SQL STABLE;

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE media_types TO chooseit;
GRANT EXECUTE ON FUNCTION media_type_id (CODE) TO chooseit;

--
-- Data
--

INSERT INTO media_types
	(id, sid, name)
VALUES
	(1, 'AVATAR',   'Изображение профиля'),
	(2, 'DOCUMENT', 'Документ');
