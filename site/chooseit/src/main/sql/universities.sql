--
-- Tables
--

CREATE TABLE universities (
	type_id    IDENTIFIER NOT NULL,
	full_name  TEXT,
	military   BOOLEAN NOT NULL DEFAULT FALSE,
	residental BOOLEAN NOT NULL DEFAULT FALSE,
	grade      SMALLINT NOT NULL DEFAULT 0,

	--
	-- Primary key
	--

	CONSTRAINT universities_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT universities_id_fkey
		FOREIGN KEY (id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT universities_parent_id_fkey
		FOREIGN KEY (parent_id) REFERENCES universities (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT universities_location_id_fkey
		FOREIGN KEY (location_id) REFERENCES locations (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT universities_type_id_fkey
		FOREIGN KEY (type_id) REFERENCES university_types (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	--
	-- Checks
	--

	CONSTRAINT universities_modification_time_chk
		CHECK (modification_time >= creation_time)
) INHERITS (providers);

--
-- Unique indexes
--

CREATE UNIQUE INDEX universities_name_unq1
	ON universities (name)
	WHERE parent_id IS NULL;

CREATE UNIQUE INDEX universities_name_unq2
	ON universities (parent_id, name)
	WHERE parent_id IS NOT NULL;

--
-- Foreign key indexes
--

CREATE INDEX universities_location_id_fki
	ON universities (location_id);

CREATE INDEX universities_type_id_fki
	ON universities (type_id);

--
-- Comments
--

COMMENT ON TABLE universities IS 'Universities';

COMMENT ON COLUMN universities.id                IS 'Unique identifier';
COMMENT ON COLUMN universities.creation_time     IS 'Creation date and time (UTC)';
COMMENT ON COLUMN universities.modification_time IS 'Last modification date and time (UTC)';
COMMENT ON COLUMN universities.parent_id         IS 'Parent provider identifier';
COMMENT ON COLUMN universities.children          IS 'Children count';
COMMENT ON COLUMN universities.name              IS 'Name';
COMMENT ON COLUMN universities.site_url          IS 'Web-site URL';
COMMENT ON COLUMN universities.video_url         IS 'Video presentation URL';
COMMENT ON COLUMN universities.location_id       IS 'Location identifier';
COMMENT ON COLUMN universities.email             IS 'Primary e-mail email';
COMMENT ON COLUMN universities.phone             IS 'Primary phone number';
COMMENT ON COLUMN universities.phone_ext         IS 'Primary phone extension number';
COMMENT ON COLUMN universities.address           IS 'Street address';
COMMENT ON COLUMN universities.zip_code          IS 'ZIP code';
COMMENT ON COLUMN universities.status            IS 'Status';
COMMENT ON COLUMN universities.description       IS 'Short description';
COMMENT ON COLUMN universities.type_id           IS 'Type identifier';
COMMENT ON COLUMN universities.full_name         IS 'Full name';
COMMENT ON COLUMN universities.military          IS 'Military facility existence flag';
COMMENT ON COLUMN universities.residental        IS 'Residental facility existence flag';
COMMENT ON COLUMN universities.grade             IS 'Grade';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE universities TO chooseit;

--
-- Triggers
--

CREATE TRIGGER universities_before_upsert_trg1
	BEFORE INSERT OR UPDATE ON universities
	FOR EACH ROW EXECUTE PROCEDURE before_entity_upsert();

CREATE TRIGGER universities_before_upsert_trg2
	BEFORE INSERT OR UPDATE ON universities
	FOR EACH ROW EXECUTE PROCEDURE before_tree_upsert();

CREATE TRIGGER universities_before_insert_trg
	BEFORE INSERT ON universities
	FOR EACH ROW EXECUTE PROCEDURE before_provider_insert();

CREATE TRIGGER universities_after_delete_trg
	AFTER DELETE ON universities
	FOR EACH ROW EXECUTE PROCEDURE after_provider_delete();
