--
-- Tables
--

CREATE TABLE permissions (
	id          IDENTIFIER NOT NULL,
	group_id    IDENTIFIER NOT NULL,
	sid         CODE NOT NULL,
	name        TEXT NOT NULL,
	level_aware BOOLEAN NOT NULL DEFAULT FALSE,
	level_id    IDENTIFIER NOT NULL,
	description TEXT,

	--
	-- Primary key
	--

	CONSTRAINT permissions_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT permissions_sid_unq
		UNIQUE (sid),

	CONSTRAINT permissions_name_unq
		UNIQUE (group_id, name),

	--
	-- Foreign keys
	--

	CONSTRAINT permissions_group_id_fkey
		FOREIGN KEY (group_id) REFERENCES permission_groups (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT permissions_level_id_fkey
		FOREIGN KEY (level_id) REFERENCES permission_levels (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
);

--
-- Foreign key indexes
--

CREATE INDEX permissions_level_id_fki ON permissions (level_id);

--
-- Comments
--

COMMENT ON TABLE permissions IS 'Permissions';

COMMENT ON COLUMN permissions.id          IS 'Unique identifier';
COMMENT ON COLUMN permissions.group_id    IS 'Permission group identifier';
COMMENT ON COLUMN permissions.sid         IS 'Unique string identifier';
COMMENT ON COLUMN permissions.name        IS 'Name';
COMMENT ON COLUMN permissions.level_aware IS 'Level-aware permission flag';
COMMENT ON COLUMN permissions.level_id    IS 'Default level identifier';
COMMENT ON COLUMN permissions.description IS 'Short description';

--
-- Functions
--

CREATE OR REPLACE FUNCTION permission_id (CODE)
	RETURNS IDENTIFIER AS $$
		SELECT id FROM permissions WHERE sid = $1
	$$ LANGUAGE SQL STABLE;

--
-- Access permissions
--

GRANT SELECT ON TABLE permissions TO chooseit;
GRANT EXECUTE ON FUNCTION permission_id (CODE) TO chooseit;
