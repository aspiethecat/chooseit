CREATE DATABASE chooseit
	TEMPLATE template0
	OWNER chooseit
	LC_CTYPE 'ru_RU.UTF-8'
	LC_COLLATE 'ru_RU.UTF-8'
	ENCODING 'UTF-8';

\c chooseit

\i types.sql
\i functions.sql

\i mime-types.sql
\i media-types.sql
\i media-types-mime.sql
\i media.sql

\i languages.sql
\i currencies.sql
\i location-types.sql
\i locations.sql

\i professions.sql
\i specializations.sql
\i profession-specializations.sql
\i university-types.sql

\i categories.sql
\i tags.sql

\i providers.sql
\i provider-partners.sql
\i provider-phones.sql
\i provider-emails.sql
\i provider-media.sql
\i provider-tags.sql
\i provider-stats.sql
\i provider-rating.sql

\i employers.sql
\i universities.sql
\i university-languages.sql
\i university-specializations.sql
\i persons.sql

\i content.sql
\i content-professions.sql
\i content-media.sql
\i content-tags.sql

\i articles.sql
\i article-categories.sql
\i event-types.sql
\i events.sql
\i courses.sql
\i preliminary.sql
\i additional.sql

\i permission-groups.sql
\i permission-levels.sql
\i permissions.sql

\i users.sql
\i user-permissions.sql
\i groups.sql
\i group-permissions.sql

\i content-users.sql

\i auth-tokens.sql
\i auth-sessions.sql
