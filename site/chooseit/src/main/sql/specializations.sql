--
-- Tables and sequences
--

CREATE SEQUENCE specialization_id;

CREATE TABLE specializations (
	id               IDENTIFIER NOT NULL DEFAULT NEXTVAL('specialization_id'),
	parent_id        IDENTIFIER,
	children         INTEGER NOT NULL,
	name             TEXT NOT NULL,
	external_id      DECIMAL (7, 0),
	intl_area_code   DECIMAL (4, 0),
	intl_course_code DECIMAL (3, 0),
	description      TEXT,

	--
	-- Primary key
	--

	CONSTRAINT specializations_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT specializations_external_id_unq
		UNIQUE (external_id),

	--
	-- Foreign keys
	--

	CONSTRAINT specializations_parent_id_fkey
		FOREIGN KEY (parent_id) REFERENCES specializations (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
);

ALTER SEQUENCE specialization_id OWNED BY specializations.id;

--
-- Unique indexes
--

CREATE UNIQUE INDEX specializations_name_unq1
	ON specializations (name)
	WHERE parent_id IS NULL;

CREATE UNIQUE INDEX specializations_name_unq2
	ON specializations (parent_id, name)
	WHERE parent_id IS NOT NULL;

--
-- Comments
--

COMMENT ON TABLE specializations IS 'Specializations';
COMMENT ON SEQUENCE specialization_id IS 'Specialization identifiers';

COMMENT ON COLUMN specializations.id               IS 'Unique identifier';
COMMENT ON COLUMN specializations.parent_id        IS 'Parent specialization identifier';
COMMENT ON COLUMN specializations.children         IS 'Children count';
COMMENT ON COLUMN specializations.name             IS 'Name';
COMMENT ON COLUMN specializations.external_id      IS 'External identifier';
COMMENT ON COLUMN specializations.intl_area_code   IS 'International education area code';
COMMENT ON COLUMN specializations.intl_course_code IS 'International education course code';
COMMENT ON COLUMN specializations.description      IS 'Short description';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE specialization_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE specializations TO chooseit;

--
-- Triggers
--

CREATE TRIGGER specializations_before_upsert_trg
	BEFORE INSERT OR UPDATE ON specializations
	FOR EACH ROW EXECUTE PROCEDURE before_tree_upsert();
