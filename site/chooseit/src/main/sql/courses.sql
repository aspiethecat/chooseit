--
-- Tables
--

CREATE TABLE courses (
	total_hours       SMALLINT NOT NULL,
	total_weeks       SMALLINT,
	int_certificate   BOOLEAN NOT NULL DEFAULT FALSE,
	ext_certificate   BOOLEAN NOT NULL DEFAULT FALSE,
	price             MONEY,
	currency_id       IDENTIFIER,

	--
	-- Primary key
	--

	CONSTRAINT courses_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT courses_id_fkey
		FOREIGN KEY (id) REFERENCES content_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT courses_provider_id_fkey
		FOREIGN KEY (provider_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT courses_currency_id_fkey
		FOREIGN KEY (currency_id) REFERENCES currencies (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	--
	-- Checks
	--

	CONSTRAINT courses_modification_time_chk
		CHECK (modification_time >= creation_time),

	CONSTRAINT courses_total_hours_chk
		CHECK (total_hours > 0),

	CONSTRAINT courses_total_weeks_chk
		CHECK ((total_weeks IS NULL) OR (total_weeks > 0)),

	CONSTRAINT courses_currency_id_chk
		CHECK (
			((price IS NULL) AND (currency_id IS NULL)) OR
			((price IS NOT NULL) AND (currency_id IS NOT NULL)))
) INHERITS (content);

--
-- Unique indexes
--

CREATE UNIQUE INDEX courses_name_unq1
	ON courses (provider_name, name)
	WHERE provider_id IS NULL;

CREATE UNIQUE INDEX courses_name_unq2
	ON courses (provider_id, name)
	WHERE provider_id IS NOT NULL;

--
-- Foreign key indexes
--

CREATE INDEX courses_currency_id_fki
	ON courses (currency_id);

--
-- Comments
--

COMMENT ON TABLE courses IS 'Courses';

COMMENT ON COLUMN courses.id                IS 'Unique identifier';
COMMENT ON COLUMN courses.creation_time     IS 'Creation date and time (UTC)';
COMMENT ON COLUMN courses.modification_time IS 'Last modification date and time (UTC)';
COMMENT ON COLUMN courses.provider_id       IS 'Provider identifier';
COMMENT ON COLUMN courses.provider_name     IS 'Provider name';
COMMENT ON COLUMN courses.provider_url      IS 'Provider URL';
COMMENT ON COLUMN courses.name              IS 'Name';
COMMENT ON COLUMN courses.url               IS 'URL';
COMMENT ON COLUMN courses.status            IS 'Status';
COMMENT ON COLUMN courses.description       IS 'Short description';
COMMENT ON COLUMN courses.total_hours       IS 'Duration (academic hours)';
COMMENT ON COLUMN courses.total_weeks       IS 'Duration (weeks)';
COMMENT ON COLUMN courses.int_certificate   IS 'Internal certificate flag';
COMMENT ON COLUMN courses.ext_certificate   IS 'External certificate flag';
COMMENT ON COLUMN courses.price             IS 'Price';
COMMENT ON COLUMN courses.currency_id       IS 'Currency identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE courses TO chooseit;

--
-- Triggers
--

CREATE TRIGGER courses_before_upsert_trg
	BEFORE INSERT OR UPDATE ON courses
	FOR EACH ROW EXECUTE PROCEDURE before_entity_upsert();

CREATE TRIGGER courses_before_insert_trg
	BEFORE INSERT ON courses
	FOR EACH ROW EXECUTE PROCEDURE before_content_insert();

CREATE TRIGGER courses_after_delete_trg
	AFTER DELETE ON courses
	FOR EACH ROW EXECUTE PROCEDURE after_content_delete();
