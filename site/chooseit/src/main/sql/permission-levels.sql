--
-- Tables
--

CREATE TABLE permission_levels (
	id   IDENTIFIER NOT NULL,
	sid  CODE NOT NULL,
	name TEXT NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT permission_levels_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT permission_levels_sid_unq
		UNIQUE (sid),

	CONSTRAINT permission_levels_name_unq
		UNIQUE (name)
);

--
-- Comments
--

COMMENT ON TABLE permission_levels IS 'Permission levels';

COMMENT ON COLUMN permission_levels.id   IS 'Unique identifier';
COMMENT ON COLUMN permission_levels.sid  IS 'Unique string identifier';
COMMENT ON COLUMN permission_levels.name IS 'Name';

--
-- Functions
--

CREATE OR REPLACE FUNCTION permission_level_id (CODE)
	RETURNS IDENTIFIER AS $$
		SELECT id FROM permission_levels WHERE sid = $1
	$$ LANGUAGE SQL STABLE;

--
-- Access permissions
--

GRANT SELECT ON TABLE permission_levels TO chooseit;
GRANT EXECUTE ON FUNCTION permission_level_id (CODE) TO chooseit;

--
-- Data
--

INSERT INTO permission_levels
	(id, sid, name)
VALUES
	(1, 'USER',     'Пользователь'),
	(2, 'GROUP',    'Группа'),
	(3, 'PROVIDER', 'Провайдер'),
	(4, 'PARTNER',  'Партнёр'),
	(5, 'ALL',      'Все');
