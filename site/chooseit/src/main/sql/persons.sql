--
-- Tables
--

CREATE TABLE persons (
	position TEXT,

	--
	-- Primary key
	--

	CONSTRAINT persons_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT persons_id_fkey
		FOREIGN KEY (id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT persons_parent_id_fkey
		FOREIGN KEY (parent_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT persons_location_id_fkey
		FOREIGN KEY (location_id) REFERENCES locations (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	--
	-- Checks
	--

	CONSTRAINT persons_modification_time_chk
		CHECK (modification_time >= creation_time),

	CONSTRAINT persons_children_chk
		CHECK (children = 0)
) INHERITS (providers);

--
-- Foreign key indexes
--

CREATE INDEX persons_parent_id_fki
	ON persons (parent_id);

CREATE INDEX persons_location_id_fki
	ON persons (location_id);

--
-- Comments
--

COMMENT ON TABLE persons IS 'Persons';

COMMENT ON COLUMN persons.id                IS 'Unique identifier';
COMMENT ON COLUMN persons.creation_time     IS 'Creation date and time (UTC)';
COMMENT ON COLUMN persons.modification_time IS 'Last modification date and time (UTC)';
COMMENT ON COLUMN persons.parent_id         IS 'Parent provider identifier';
COMMENT ON COLUMN persons.children          IS 'Children count';
COMMENT ON COLUMN persons.name              IS 'Name';
COMMENT ON COLUMN persons.site_url          IS 'Web-site URL';
COMMENT ON COLUMN persons.video_url         IS 'Video presentation URL';
COMMENT ON COLUMN persons.location_id       IS 'Location identifier';
COMMENT ON COLUMN persons.email             IS 'Primary e-mail email';
COMMENT ON COLUMN persons.phone             IS 'Primary phone number';
COMMENT ON COLUMN persons.phone_ext         IS 'Primary phone extension number';
COMMENT ON COLUMN persons.address           IS 'Street address';
COMMENT ON COLUMN persons.zip_code          IS 'ZIP code';
COMMENT ON COLUMN persons.status            IS 'Status';
COMMENT ON COLUMN persons.description       IS 'Short description';
COMMENT ON COLUMN persons.position          IS 'Position name';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE persons TO chooseit;

--
-- Triggers
--

CREATE TRIGGER persons_before_upsert_trg1
	BEFORE INSERT OR UPDATE ON persons
	FOR EACH ROW EXECUTE PROCEDURE before_entity_upsert();

CREATE TRIGGER persons_before_upsert_trg2
	BEFORE INSERT OR UPDATE ON persons
	FOR EACH ROW EXECUTE PROCEDURE before_tree_upsert();

CREATE TRIGGER persons_before_insert_trg
	BEFORE INSERT ON persons
	FOR EACH ROW EXECUTE PROCEDURE before_provider_insert();

CREATE TRIGGER persons_after_delete_trg
	AFTER DELETE ON persons
	FOR EACH ROW EXECUTE PROCEDURE after_provider_delete();
