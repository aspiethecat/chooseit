--
-- Tables and sequences
--

CREATE SEQUENCE provider_email_id;

CREATE TABLE provider_emails (
	id          IDENTIFIER NOT NULL DEFAULT NEXTVAL('provider_email_id'),
	provider_id IDENTIFIER NOT NULL,
	email       TEXT NOT NULL,
	description TEXT,

	--
	-- Primary key
	--

	CONSTRAINT provider_emails_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT provider_emails_email_unq
		UNIQUE (provider_id, email),

	--
	-- Foreign keys
	--

	CONSTRAINT provider_emails_provider_id_fkey
		FOREIGN KEY (provider_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
);

ALTER SEQUENCE provider_email_id OWNED BY provider_emails.id;

--
-- Comments
--

COMMENT ON TABLE provider_emails IS 'Provider emails';
COMMENT ON SEQUENCE provider_email_id IS 'Provider email identifiers';

COMMENT ON COLUMN provider_emails.id          IS 'Unique identifier';
COMMENT ON COLUMN provider_emails.provider_id IS 'Provider identifier';
COMMENT ON COLUMN provider_emails.email       IS 'E-mail address';
COMMENT ON COLUMN provider_emails.description IS 'Short description';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE provider_email_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE provider_emails TO chooseit;
