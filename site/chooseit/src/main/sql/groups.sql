--
-- Tables and sequences
--

CREATE SEQUENCE group_id;

CREATE TABLE groups (
	id                IDENTIFIER NOT NULL DEFAULT NEXTVAL('group_id'),
	creation_time     TIMEVAL NOT NULL,
	modification_time TIMEVAL NOT NULL,
	provider_id       IDENTIFIER,
	name              TEXT NOT NULL,
	description       TEXT,

	--
	-- Primary key
	--

	CONSTRAINT groups_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT groups_provider_id_fkey
		FOREIGN KEY (provider_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE,

	--
	-- Checks
	--

	CONSTRAINT groups_modification_time_chk
		CHECK (modification_time >= creation_time)
);

ALTER SEQUENCE group_id OWNED BY groups.id;

--
-- Unique indexes
--

CREATE UNIQUE INDEX groups_name_unq1
	ON groups (name)
	WHERE provider_id IS NULL;

CREATE UNIQUE INDEX groups_name_unq2
	ON groups (provider_id, name)
	WHERE provider_id IS NOT NULL;

--
-- Comments
--

COMMENT ON TABLE groups IS 'Groups';
COMMENT ON SEQUENCE group_id IS 'Group identifiers';

COMMENT ON COLUMN groups.id                IS 'Unique identifier';
COMMENT ON COLUMN groups.creation_time     IS 'Creation date and time (UTC)';
COMMENT ON COLUMN groups.modification_time IS 'Last modification date and time (UTC)';
COMMENT ON COLUMN groups.provider_id       IS 'Provider identifier';
COMMENT ON COLUMN groups.name              IS 'Name';
COMMENT ON COLUMN groups.description       IS 'Short description';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE group_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE groups TO chooseit;

--
-- Triggers
--

CREATE TRIGGER groups_before_upsert_trg
	BEFORE INSERT OR UPDATE ON groups
	FOR EACH ROW EXECUTE PROCEDURE before_entity_upsert();
