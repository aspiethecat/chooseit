--
-- Tables
--

CREATE TABLE university_specializations (
	university_id       IDENTIFIER NOT NULL,
	specialization_id   IDENTIFIER NOT NULL,
	certificate_type_id IDENTIFIER,
	total_years         DECIMAL(2, 1),
	cut_score           SMALLINT,
	price               MONEY,
	currency_id         IDENTIFIER,

	--
	-- Primary key
	--

	CONSTRAINT university_specializations_pkey
		PRIMARY KEY (university_id, specialization_id),

	--
	-- Foreign keys
	--

	CONSTRAINT university_specializations_university_id_fkey
		FOREIGN KEY (university_id) REFERENCES universities (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE,

	CONSTRAINT university_specializations_specialization_id_fkey
		FOREIGN KEY (specialization_id) REFERENCES specializations (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT university_specializations_currency_id_fkey
		FOREIGN KEY (currency_id) REFERENCES currencies (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	--
	-- Checks
	--

	CONSTRAINT university_specializations_total_years_chk
		CHECK ((total_years IS NULL) OR (total_years > 0)),

	CONSTRAINT university_specializations_currency_id_chk
		CHECK (
			((price IS NULL) AND (currency_id IS NULL)) OR
			((price IS NOT NULL) AND (currency_id IS NOT NULL)))
);

--
-- Foreign key indexes
--

CREATE INDEX university_specializations_specialization_id_fki
	ON university_specializations (specialization_id);

CREATE INDEX university_specializations_currency_id_fki
	ON university_specializations (currency_id);

--
-- Comments
--

COMMENT ON TABLE university_specializations IS 'University specializations';

COMMENT ON COLUMN university_specializations.university_id       IS 'University identifier';
COMMENT ON COLUMN university_specializations.specialization_id   IS 'Specialization identifier';
COMMENT ON COLUMN university_specializations.certificate_type_id IS 'Certificate type identifier';
COMMENT ON COLUMN university_specializations.total_years         IS 'Duration (years)';
COMMENT ON COLUMN university_specializations.cut_score           IS 'Cut score';
COMMENT ON COLUMN university_specializations.price               IS 'Price';
COMMENT ON COLUMN university_specializations.currency_id         IS 'Currency identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE university_specializations TO chooseit;
