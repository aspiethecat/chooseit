--
-- Tables
--

CREATE TABLE article_categories (
	article_id  IDENTIFIER NOT NULL,
	category_id IDENTIFIER NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT article_categories_pkey
		PRIMARY KEY (article_id, category_id),

	--
	-- Foreign keys
	--

	CONSTRAINT article_categories_article_id_fkey
		FOREIGN KEY (article_id) REFERENCES articles (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE,

	CONSTRAINT article_categories_category_id_fkey
		FOREIGN KEY (category_id) REFERENCES categories (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
);

--
-- Foreign key indexes
--

CREATE INDEX article_categories_category_id_fki
	ON article_categories (category_id);

--
-- Comments
--

COMMENT ON TABLE article_categories IS 'Article categories';

COMMENT ON COLUMN article_categories.article_id  IS 'Article identifier';
COMMENT ON COLUMN article_categories.category_id IS 'Category identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE article_categories TO chooseit;
