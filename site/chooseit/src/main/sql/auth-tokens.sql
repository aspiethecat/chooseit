--
-- Tables and sequences
--

CREATE SEQUENCE auth_token_id;

CREATE TABLE auth_tokens (
	id               IDENTIFIER NOT NULL DEFAULT NEXTVAL('auth_token_id'),
	creation_time    TIMEVAL NOT NULL,
	expiration_time  TIMEVAL NOT NULL,
	extended_id      SELECTOR NOT NULL,
	validator_digest DIGEST NOT NULL,
	user_id          IDENTIFIER NOT NULL,
	ip_address       INET NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT auth_tokens_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT auth_tokens_extended_id_unq
		UNIQUE (extended_id),

	--
	-- Foreign keys
	--

	CONSTRAINT auth_tokens_user_id_fkey
		FOREIGN KEY (user_id) REFERENCES users (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
);

ALTER SEQUENCE auth_token_id OWNED BY auth_tokens.id;

--
-- Foreign key indexes
--

CREATE INDEX auth_tokens_user_id_fki
	ON auth_tokens (user_id);

--
-- Comments
--

COMMENT ON TABLE auth_tokens IS 'Authentication tokens';
COMMENT ON SEQUENCE auth_token_id IS 'Authentication token identifiers';

COMMENT ON COLUMN auth_tokens.id               IS 'Unique identifier';
COMMENT ON COLUMN auth_tokens.creation_time    IS 'Creation date and time (UTC)';
COMMENT ON COLUMN auth_tokens.expiration_time  IS 'Expiration date and time (UTC)';
COMMENT ON COLUMN auth_tokens.extended_id      IS 'Extended identifier';
COMMENT ON COLUMN auth_tokens.validator_digest IS 'Token validator digest (SHA-256)';
COMMENT ON COLUMN auth_tokens.user_id          IS 'User identifier';
COMMENT ON COLUMN auth_tokens.ip_address       IS 'IP address';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE auth_token_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE auth_tokens TO chooseit;
