--
-- Tables and sequences
--

CREATE SEQUENCE provider_phone_id;

CREATE TABLE provider_phones (
	id          IDENTIFIER NOT NULL DEFAULT NEXTVAL('provider_phone_id'),
	provider_id IDENTIFIER NOT NULL,
	phone       DECIMAL (15, 0) NOT NULL,
	extension   DECIMAL (4, 0),
	description TEXT,

	--
	-- Primary key
	--

	CONSTRAINT provider_phones_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT provider_phones_phone_unq
		UNIQUE (provider_id, phone),

	--
	-- Foreign keys
	--

	CONSTRAINT provider_phones_provider_id_fkey
		FOREIGN KEY (provider_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
);

ALTER SEQUENCE provider_phone_id OWNED BY provider_phones.id;

--
-- Comments
--

COMMENT ON TABLE provider_phones IS 'Provider phones';
COMMENT ON SEQUENCE provider_phone_id IS 'Provider phone identifiers';

COMMENT ON COLUMN provider_phones.id          IS 'Unique identifier';
COMMENT ON COLUMN provider_phones.provider_id IS 'Provider identifier';
COMMENT ON COLUMN provider_phones.phone       IS 'Phone number';
COMMENT ON COLUMN provider_phones.extension   IS 'Extension number';
COMMENT ON COLUMN provider_phones.description IS 'Short description';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE provider_phone_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE provider_phones TO chooseit;
