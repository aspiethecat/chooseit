--
-- Tables and sequences
--

CREATE SEQUENCE mime_type_id;

CREATE TABLE mime_types (
	id   IDENTIFIER NOT NULL DEFAULT NEXTVAL('mime_type_id'),
	sid  CODE NOT NULL,
	name TEXT NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT mime_types_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT mime_types_sid_unq
		UNIQUE (sid)
);

ALTER SEQUENCE mime_type_id OWNED BY mime_types.id;

--
-- Comments
--

COMMENT ON TABLE mime_types IS 'MIME types';
COMMENT ON SEQUENCE mime_type_id IS 'MIME type identifiers';

COMMENT ON COLUMN mime_types.id   IS 'Unique identifier';
COMMENT ON COLUMN mime_types.sid  IS 'Unique string identifier (MIME)';
COMMENT ON COLUMN mime_types.name IS 'Name';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE mime_type_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE mime_types TO chooseit;

--
-- Data
--

INSERT INTO mime_types
	(sid, name)
VALUES
	('image/jpeg',    'Изображение JPEG'),
	('image/png',     'Изображение PNG'),
	('image/svg+xml', 'Изображение SVG');
