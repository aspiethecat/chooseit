--
-- Tables and sequences
--

CREATE SEQUENCE provider_id;

CREATE TABLE providers_all (
	id IDENTIFIER NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT providers_all_pkey
		PRIMARY KEY (id)
);

CREATE TABLE providers (
	id                IDENTIFIER NOT NULL DEFAULT NEXTVAL('provider_id'),
	creation_time     TIMEVAL NOT NULL,
	modification_time TIMEVAL NOT NULL,
	parent_id         IDENTIFIER,
	children          INTEGER NOT NULL,
	name              TEXT NOT NULL,
	site_url          TEXT,
	video_url         TEXT,
	location_id       IDENTIFIER,
	email             TEXT,
	phone             DECIMAL (15, 0),
	phone_ext         DECIMAL (4, 0),
	address           TEXT,
	zip_code          DECIMAL (6, 0),
	status            ACCOUNT_STATUS NOT NULL DEFAULT 'NEW',
	description       TEXT,

	--
	-- Primary key
	--

	CONSTRAINT providers_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT providers_id_fkey
		FOREIGN KEY (id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT providers_parent_id_fkey
		FOREIGN KEY (parent_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT providers_location_id_fkey
		FOREIGN KEY (location_id) REFERENCES locations (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
) WITH OIDS;

ALTER SEQUENCE provider_id OWNED BY providers.id;

--
-- Foreign key indexes
--

CREATE INDEX providers_parent_id_fki
	ON providers (parent_id);

CREATE INDEX providers_location_id_fki
	ON providers (location_id);

--
-- Comments
--

COMMENT ON TABLE providers_all IS 'Providers (all)';
COMMENT ON TABLE providers IS 'Providers';

COMMENT ON SEQUENCE provider_id IS 'Provider identifiers';

COMMENT ON COLUMN providers.id                IS 'Unique identifier';
COMMENT ON COLUMN providers.creation_time     IS 'Creation date and time (UTC)';
COMMENT ON COLUMN providers.modification_time IS 'Last modification date and time (UTC)';
COMMENT ON COLUMN providers.parent_id         IS 'Parent provider identifier';
COMMENT ON COLUMN providers.children          IS 'Children count';
COMMENT ON COLUMN providers.name              IS 'Name';
COMMENT ON COLUMN providers.site_url          IS 'Web-site URL';
COMMENT ON COLUMN providers.video_url         IS 'Video presentation URL';
COMMENT ON COLUMN providers.location_id       IS 'Location identifier';
COMMENT ON COLUMN providers.email             IS 'Primary e-mail email';
COMMENT ON COLUMN providers.phone             IS 'Primary phone number';
COMMENT ON COLUMN providers.phone_ext         IS 'Primary phone extension number';
COMMENT ON COLUMN providers.address           IS 'Street address';
COMMENT ON COLUMN providers.zip_code          IS 'ZIP code';
COMMENT ON COLUMN providers.status            IS 'Status';
COMMENT ON COLUMN providers.description       IS 'Short description';

--
-- Functions
--

CREATE OR REPLACE FUNCTION before_provider_insert()
	RETURNS TRIGGER AS $$
		BEGIN
			INSERT INTO providers_all (id) VALUES (NEW.id);
			RETURN NEW;
		END;
	$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION after_provider_delete()
	RETURNS TRIGGER AS $$
		BEGIN
			DELETE FROM providers_all WHERE id = OLD.id;
			RETURN OLD;
		END;
	$$ LANGUAGE PLPGSQL;

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE provider_id TO chooseit;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE providers_all TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE providers TO chooseit;

GRANT EXECUTE ON FUNCTION before_provider_insert () TO chooseit;
GRANT EXECUTE ON FUNCTION after_provider_delete () TO chooseit;

--
-- Triggers
--

CREATE TRIGGER providers_before_upsert_trg
	BEFORE INSERT OR UPDATE ON providers
	FOR EACH ROW EXECUTE PROCEDURE prevent_upsert();
