--
-- Tables and sequences
--

CREATE SEQUENCE location_id;

CREATE TABLE locations (
	id          IDENTIFIER NOT NULL DEFAULT NEXTVAL('location_id'),
	parent_id   IDENTIFIER,
	children    INTEGER NOT NULL,
	type_id     IDENTIFIER NOT NULL,
	external_id IDENTIFIER,
	name        TEXT NOT NULL,
	zip_code    DECIMAL (6, 0),

	--
	-- Primary key
	--

	CONSTRAINT locations_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT locations_parent_id_fkey
		FOREIGN KEY (parent_id) REFERENCES locations (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT locations_type_id_fkey
		FOREIGN KEY (type_id) REFERENCES location_types (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
);

ALTER SEQUENCE location_id OWNED BY locations.id;

--
-- Unique indexes
--

CREATE UNIQUE INDEX locations_name_unq1
	ON locations (name)
	WHERE parent_id IS NULL;

CREATE UNIQUE INDEX locations_name_unq2
	ON locations (parent_id, name)
	WHERE parent_id IS NOT NULL;

--
-- Foreign key indexes
--

CREATE INDEX locations_type_id_fki
	ON locations (type_id);

--
-- Comments
--

COMMENT ON TABLE locations IS 'Locations';
COMMENT ON SEQUENCE location_id IS 'Location identifiers';

COMMENT ON COLUMN locations.id          IS 'Unique identifier';
COMMENT ON COLUMN locations.parent_id   IS 'Parent location identifier';
COMMENT ON COLUMN locations.children    IS 'Children count';
COMMENT ON COLUMN locations.type_id     IS 'Type identifier';
COMMENT ON COLUMN locations.external_id IS 'External reference identifier';
COMMENT ON COLUMN locations.name        IS 'Name';
COMMENT ON COLUMN locations.zip_code    IS 'ZIP code';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE location_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE locations TO chooseit;

--
-- Triggers
--

CREATE TRIGGER locations_before_upsert_trg
	BEFORE INSERT OR UPDATE ON locations
	FOR EACH ROW EXECUTE PROCEDURE before_tree_upsert();
