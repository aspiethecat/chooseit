--
-- Tables and sequences
--

CREATE SEQUENCE content_id;

CREATE TABLE content_all (
	id IDENTIFIER NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT content_all_pkey
		PRIMARY KEY (id)
);

CREATE TABLE content (
	id                IDENTIFIER NOT NULL DEFAULT NEXTVAL('content_id'),
	creation_time     TIMEVAL NOT NULL,
	modification_time TIMEVAL NOT NULL,
	provider_id       IDENTIFIER,
	provider_name     TEXT,
	provider_url      TEXT,
	name              TEXT NOT NULL,
	url               TEXT,
	status            CONTENT_STATUS NOT NULL DEFAULT 'NEW',
	description       TEXT,

	--
	-- Primary key
	--

	CONSTRAINT content_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT content_id_fkey
		FOREIGN KEY (id) REFERENCES content_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT content_provider_id_fkey
		FOREIGN KEY (provider_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	--
	-- Checks
	--

	CONSTRAINT content_modification_time_chk
		CHECK (modification_time >= creation_time),

	CONSTRAINT content_provider_name_chk
		CHECK ((provider_name IS NOT NULL) OR (provider_id IS NOT NULL)),

	CONSTRAINT content_provider_url_chk
		CHECK ((provider_url IS NOT NULL) OR (provider_id IS NOT NULL))
) WITH OIDS;

ALTER SEQUENCE content_id OWNED BY content.id;

--
-- Unique indexes
--

CREATE UNIQUE INDEX content_name_unq1
	ON content (provider_name, name)
	WHERE provider_id IS NULL;

CREATE UNIQUE INDEX content_name_unq2
	ON content (provider_id, name)
	WHERE provider_id IS NOT NULL;

--
-- Foreign key indexes
--

CREATE INDEX content_provider_id_fki
	ON content (provider_id);

--
-- Comments
--

COMMENT ON TABLE content_all IS 'Content (all)';
COMMENT ON TABLE content IS 'Content';
COMMENT ON SEQUENCE content_id IS 'Content identifiers';

COMMENT ON COLUMN content.id                IS 'Unique identifier';
COMMENT ON COLUMN content.creation_time     IS 'Creation date and time (UTC)';
COMMENT ON COLUMN content.modification_time IS 'Last modification date and time (UTC)';
COMMENT ON COLUMN content.provider_id       IS 'Provider identifier';
COMMENT ON COLUMN content.provider_name     IS 'Provider name';
COMMENT ON COLUMN content.provider_url      IS 'Provider URL';
COMMENT ON COLUMN content.name              IS 'Name';
COMMENT ON COLUMN content.url               IS 'URL';
COMMENT ON COLUMN content.status            IS 'Status';
COMMENT ON COLUMN content.description       IS 'Short description';

--
-- Functions
--

CREATE OR REPLACE FUNCTION before_content_insert()
	RETURNS TRIGGER AS $$
		BEGIN
			INSERT INTO content_all (id) VALUES (NEW.id);
			RETURN NEW;
		END;
	$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION after_content_delete()
	RETURNS TRIGGER AS $$
		BEGIN
			DELETE FROM content_all WHERE id = OLD.id;
			RETURN OLD;
		END;
	$$ LANGUAGE PLPGSQL;

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE content_id TO chooseit;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE content_all TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE content TO chooseit;

GRANT EXECUTE ON FUNCTION before_content_insert () TO chooseit;
GRANT EXECUTE ON FUNCTION after_content_delete () TO chooseit;

--
-- Triggers
--

CREATE TRIGGER content_before_upsert_trg
	BEFORE INSERT OR UPDATE ON content
	FOR EACH ROW EXECUTE PROCEDURE prevent_upsert();
