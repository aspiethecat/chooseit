--
-- Tables and sequences
--

CREATE SEQUENCE currency_id;

CREATE TABLE currencies (
	id          IDENTIFIER NOT NULL DEFAULT NEXTVAL('currency_id'),
	iso_alpha   CHARACTER (3) NOT NULL,
	iso_numeric DECIMAL (3, 0) NOT NULL,
	name        TEXT NOT NULL,
	priority    SMALLINT NOT NULL DEFAULT 0,
	precision   SMALLINT NOT NULL,
	icon        TEXT,

	--
	-- Primary key
	--

	CONSTRAINT currencies_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT currencies_iso_alpha_unq
		UNIQUE (iso_alpha),

	CONSTRAINT currencies_iso_numeric_unq
		UNIQUE (iso_numeric),

	CONSTRAINT currencies_name_unq
		UNIQUE (name)
);

ALTER SEQUENCE currency_id OWNED BY currencies.id;

--
-- Comments
--

COMMENT ON TABLE currencies IS 'Currencies';
COMMENT ON SEQUENCE currency_id IS 'Currency identifiers';

COMMENT ON COLUMN currencies.id          IS 'Unique identifier';
COMMENT ON COLUMN currencies.iso_alpha   IS 'ISO 4217 text code';
COMMENT ON COLUMN currencies.iso_numeric IS 'ISO 4217 numeric code';
COMMENT ON COLUMN currencies.name        IS 'Name';
COMMENT ON COLUMN currencies.priority    IS 'Display priority';
COMMENT ON COLUMN currencies.precision   IS 'Precision';
COMMENT ON COLUMN currencies.icon        IS 'Icon name';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE currency_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE currencies TO chooseit;

--
-- Data
--

INSERT INTO currencies
	(iso_alpha, iso_numeric, precision, name, priority, icon)
VALUES
	('RUB', 643, 2, 'Российский рубль', 0, 'rub'),
	('EUR', 978, 2, 'Евро', 0, 'eur'),
	('USD', 810, 2, 'Американский доллар', 0, 'usd'),
	('GBP', 826, 2, 'Фунт стерлингов', 1, 'gbp'),
	('JPY', 392, 0, 'Японская йена', 1, 'jpy'),
	('ILS', 376, 2, 'Израильский шекель', 1, 'ils');
