--
-- Table
--

CREATE TABLE media_types_mime (
	media_type_id IDENTIFIER NOT NULL,
	mime_type_id  IDENTIFIER NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT media_types_mime_pkey
		PRIMARY KEY (media_type_id, mime_type_id),

	--
	-- Foreign keys
	--

	CONSTRAINT media_types_mime_media_type_id_fkey
		FOREIGN KEY (media_type_id) REFERENCES media_types (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE,

	CONSTRAINT media_types_mime_mime_type_id_fkey
		FOREIGN KEY (mime_type_id) REFERENCES mime_types (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
);

--
-- Foreign key indexes
--

CREATE INDEX media_types_mime_mime_type_id_fki
	ON media_types_mime (mime_type_id);

--
-- Comments
--

COMMENT ON TABLE media_types_mime IS 'Media types MIME';

COMMENT ON COLUMN media_types_mime.media_type_id IS 'Media type identifier';
COMMENT ON COLUMN media_types_mime.mime_type_id  IS 'MIME type identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE media_types_mime TO chooseit;
