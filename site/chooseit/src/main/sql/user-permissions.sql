--
-- Tables
--

CREATE TABLE user_permissions (
	user_id       IDENTIFIER NOT NULL,
	permission_id IDENTIFIER NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT user_permissions_pkey
		PRIMARY KEY (user_id, permission_id),

	--
	-- Foreign keys
	--

	CONSTRAINT user_permissions_user_id_fkey
		FOREIGN KEY (user_id) REFERENCES users (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE,

	CONSTRAINT user_permissions_permission_id_fkey
		FOREIGN KEY (permission_id) REFERENCES permissions (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
);

--
-- Foreign key indexes
--

CREATE INDEX user_permissions_permission_id_fki
	ON user_permissions (permission_id);

--
-- Comments
--

COMMENT ON TABLE user_permissions IS 'User permissions';

COMMENT ON COLUMN user_permissions.user_id       IS 'User identifier';
COMMENT ON COLUMN user_permissions.permission_id IS 'Permission identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE user_permissions TO chooseit;
