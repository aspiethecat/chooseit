--
-- Tables
--

CREATE TABLE permission_groups (
	id          IDENTIFIER NOT NULL,
	parent_id   IDENTIFIER,
	sid         CODE NOT NULL,
	name        TEXT NOT NULL,
	description TEXT,

	--
	-- Primary key
	--

	CONSTRAINT permission_groups_pkey
		PRIMARY KEY (id),

	--
	-- Unique keys
	--

	CONSTRAINT permission_groups_sid_unq
		UNIQUE (sid)
);

--
-- Unique indexes
--

CREATE UNIQUE INDEX permission_groups_name_unq1
	ON permission_groups (name)
	WHERE parent_id IS NULL;

CREATE UNIQUE INDEX permission_groups_name_unq2
	ON permission_groups (parent_id, name)
	WHERE parent_id IS NOT NULL;

--
-- Comments
--

COMMENT ON TABLE permission_groups IS 'Permission groups';

COMMENT ON COLUMN permission_groups.id          IS 'Unique identifier';
COMMENT ON COLUMN permission_groups.parent_id   IS 'Parent group identifier';
COMMENT ON COLUMN permission_groups.sid         IS 'Unique string identifier';
COMMENT ON COLUMN permission_groups.name        IS 'Name';
COMMENT ON COLUMN permission_groups.description IS 'Short description';

--
-- Functions
--

CREATE OR REPLACE FUNCTION permission_group_id (CODE)
	RETURNS IDENTIFIER AS $$
		SELECT id FROM permission_groups WHERE sid = $1
	$$ LANGUAGE SQL STABLE;

--
-- Access permissions
--

GRANT SELECT ON TABLE permission_groups TO chooseit;
GRANT EXECUTE ON FUNCTION permission_group_id (CODE) TO chooseit;

--
-- Data
--

INSERT INTO permission_groups
	(id, parent_id, sid, name)
VALUES
	( 1, NULL, 'SECURITY',                  'Безопасность'),
	( 2,    1, 'SECURITY_USER',             'Пользователи'),
	( 3,    1, 'SECURITY_GROUP',            'Группы'),
	( 4, NULL, 'DICTIONARY',                'Справочники'),
	( 5,    4, 'DICTIONARY_LANGUAGE',       'Языки'),
	( 6,    4, 'DICTIONARY_LOCATION',       'География'),
	( 7,    4, 'DICTIONARY_CURRENCY',       'Валюты'),
	( 8,    4, 'DICTIONARY_EVENT_TYPE',     'Типы событий'),
	( 9,    4, 'DICTIONARY_SPECIALIZATION', 'Специальности'),
	(10,    4, 'DICTIONARY_PROFESSION',     'Профессии'),
	(11,    4, 'DICTIONARY_CATEGORY',       'Категории'),
	(12, NULL, 'PROVIDER',                  'Провайдеры'),
	(13,   12, 'PROVIDER_UNIVERSITY',       'ВУЗы'),
	(14,   12, 'PROVIDER_EMPLOYER',         'Компании'),
	(15,   12, 'PROVIDER_PERSON',           'Люди'),
	(16, NULL, 'CONTENT',                   'Контент'),
	(17,   16, 'CONTENT_ARTICLE',           'Аналитика'),
	(18,   16, 'CONTENT_EVENT',             'События'),
	(19,   16, 'CONTENT_COURSE',            'Курсы'),
	(20,   16, 'CONTENT_PRELIMINARY',       'Довузовская подготовка'),
	(21,   16, 'CONTENT_ADDITIONAL',        'Дополнительное образование');
