--
-- Tables
--

CREATE TABLE events (
	type_id     IDENTIFIER NOT NULL,
	location_id IDENTIFIER,
	start_date  DATE NOT NULL,
	start_time  TIME NOT NULL,
	end_date    DATE,
	online      BOOLEAN NOT NULL DEFAULT FALSE,
	price       MONEY,
	currency_id IDENTIFIER,

	--
	-- Primary key
	--

	CONSTRAINT events_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT events_id_fkey
		FOREIGN KEY (id) REFERENCES content_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT events_provider_id_fkey
		FOREIGN KEY (provider_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT events_type_id_fkey
		FOREIGN KEY (type_id) REFERENCES event_types (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT events_location_id_fkey
		FOREIGN KEY (location_id) REFERENCES locations (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT events_currency_id_fkey
		FOREIGN KEY (currency_id) REFERENCES currencies (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	--
	-- Checks
	--

	CONSTRAINT events_modification_time_chk
		CHECK (modification_time >= creation_time),

	CONSTRAINT events_end_date_chk
		CHECK (end_date >= start_date),

	CONSTRAINT events_currency_id_chk
		CHECK (
			((price IS NULL) AND (currency_id IS NULL)) OR
			((price IS NOT NULL) AND (currency_id IS NOT NULL)))
) INHERITS (content);

--
-- Unique indexes
--

CREATE UNIQUE INDEX events_name_unq1
	ON events (provider_name, name)
	WHERE provider_id IS NULL;

CREATE UNIQUE INDEX events_name_unq2
	ON events (provider_id, name)
	WHERE provider_id IS NOT NULL;

--
-- Foreign key indexes
--

CREATE INDEX events_type_id_fki
	ON events (type_id);

CREATE INDEX events_location_id_fki
	ON events (location_id);

CREATE INDEX events_currency_id_fki
	ON events (currency_id);

--
-- Comments
--

COMMENT ON TABLE events IS 'Events';

COMMENT ON COLUMN events.id                IS 'Unique identifier';
COMMENT ON COLUMN events.creation_time     IS 'Creation date and time (UTC)';
COMMENT ON COLUMN events.modification_time IS 'Last modification date and time (UTC)';
COMMENT ON COLUMN events.provider_id       IS 'Provider identifier';
COMMENT ON COLUMN events.provider_name     IS 'Provider name';
COMMENT ON COLUMN events.provider_url      IS 'Provider URL';
COMMENT ON COLUMN events.name              IS 'Name';
COMMENT ON COLUMN events.url               IS 'URL';
COMMENT ON COLUMN events.status            IS 'Status';
COMMENT ON COLUMN events.description       IS 'Short description';
COMMENT ON COLUMN events.type_id           IS 'Event type identifier';
COMMENT ON COLUMN events.location_id       IS 'Location identifier';
COMMENT ON COLUMN events.start_date        IS 'Start date';
COMMENT ON COLUMN events.start_time        IS 'Start time';
COMMENT ON COLUMN events.end_date          IS 'End date';
COMMENT ON COLUMN events.online            IS 'Online access flag';
COMMENT ON COLUMN events.price             IS 'Price';
COMMENT ON COLUMN events.currency_id       IS 'Currency identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE events TO chooseit;

--
-- Triggers
--

CREATE TRIGGER events_before_upsert_trg
	BEFORE INSERT OR UPDATE ON events
	FOR EACH ROW EXECUTE PROCEDURE before_entity_upsert();

CREATE TRIGGER events_before_insert_trg
	BEFORE INSERT ON events
	FOR EACH ROW EXECUTE PROCEDURE before_content_insert();

CREATE TRIGGER events_after_delete_trg
	AFTER DELETE ON events
	FOR EACH ROW EXECUTE PROCEDURE after_content_delete();
