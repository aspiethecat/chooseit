--
-- Tables
--

CREATE TABLE provider_tags (
	provider_id IDENTIFIER NOT NULL,
	tag_id      IDENTIFIER NOT NULL,

	--
	-- Primary key
	--

	CONSTRAINT provider_tags_pkey
		PRIMARY KEY (provider_id, tag_id),

	--
	-- Foreign keys
	--

	CONSTRAINT provider_tags_provider_id_fkey
		FOREIGN KEY (provider_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE,

	CONSTRAINT provider_tags_tag_id_fkey
		FOREIGN KEY (tag_id) REFERENCES tags (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
);

--
-- Foreign key indexes
--

CREATE INDEX provider_tags_tag_id_fki
	ON provider_tags (tag_id);

--
-- Comments
--

COMMENT ON TABLE provider_tags IS 'Provider tags';

COMMENT ON COLUMN provider_tags.provider_id IS 'Provider identifier';
COMMENT ON COLUMN provider_tags.tag_id      IS 'Tag identifier';

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE provider_tags TO chooseit;
