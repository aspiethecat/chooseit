--
-- Tables
--

CREATE TABLE articles (
	content TEXT,

	--
	-- Primary key
	--

	CONSTRAINT articles_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT articles_id_fkey
		FOREIGN KEY (id) REFERENCES content_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	CONSTRAINT articles_provider_id_fkey
		FOREIGN KEY (provider_id) REFERENCES providers_all (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,

	--
	-- Checks
	--

	CONSTRAINT articles_modification_time_chk
		CHECK (modification_time >= creation_time),

	CONSTRAINT articles_provider_name_chk
		CHECK ((provider_name IS NOT NULL) OR (provider_id IS NOT NULL)),

	CONSTRAINT articles_provider_url_chk
		CHECK ((provider_url IS NOT NULL) OR (provider_id IS NOT NULL))
) INHERITS (content);

--
-- Unique indexes
--

CREATE UNIQUE INDEX articles_name_unq1
	ON articles (provider_name, name)
	WHERE provider_id IS NULL;

CREATE UNIQUE INDEX articles_name_unq2
	ON articles (provider_id, name)
	WHERE provider_id IS NOT NULL;

--
-- Foreign key indexes
--

CREATE INDEX articles_provider_id_fki
	ON articles (provider_id);

--
-- Comments
--

COMMENT ON TABLE articles IS 'Articles';

COMMENT ON COLUMN articles.id                IS 'Unique identifier';
COMMENT ON COLUMN articles.creation_time     IS 'Creation date and time (UTC)';
COMMENT ON COLUMN articles.modification_time IS 'Last modification date and time (UTC)';
COMMENT ON COLUMN articles.provider_id       IS 'Provider identifier';
COMMENT ON COLUMN articles.provider_name     IS 'Provider name';
COMMENT ON COLUMN articles.provider_url      IS 'Provider URL';
COMMENT ON COLUMN articles.name              IS 'Name';
COMMENT ON COLUMN articles.url               IS 'URL';
COMMENT ON COLUMN articles.status            IS 'Status';
COMMENT ON COLUMN articles.description       IS 'Short description';
COMMENT ON COLUMN articles.content           IS 'Content (HTML)';

--
-- Functions
--

CREATE OR REPLACE FUNCTION after_article_upsert_or_delete ()
	RETURNS TRIGGER AS $$
		DECLARE
			old_state BOOLEAN DEFAULT FALSE;
			new_state BOOLEAN DEFAULT FALSE;
		BEGIN
			IF (TG_OP = 'INSERT') THEN
				IF (NEW.provider_id IS NULL) THEN
					RETURN NEW;
				END IF;

				old_state := FALSE;
			ELSE
				old_state := OLD.status IN ('PUBLISHED', 'ARCHIVED');

				IF (TG_OP = 'UPDATE') THEN
					IF ((OLD.provider_id IS NULL) AND (NEW.provider_id IS NOT NULL)) THEN
						old_state := FALSE;
					END IF;
				END IF;
			END IF;

			IF (TG_OP = 'DELETE') THEN
				IF ((OLD.provider_id IS NULL) OR (NOT old_state)) THEN
					RETURN OLD;
				END IF;

				new_state := FALSE;
			ELSE
				new_state := NEW.status IN ('PUBLISHED', 'ARCHIVED');
			END IF;

			IF (NEW.provider_id IS NULL) THEN
				new_state := FALSE;
			END IF;

			IF (old_state AND (NOT new_state)) THEN
				IF (OLD.provider_id IS NOT NULL) THEN
					UPDATE
						provider_stats
					SET
						article_count = article_count - 1
					WHERE
						provider_id = OLD.provider.id;
				END IF;
			END IF;

			IF ((NOT old_state) AND new_state) THEN
				IF (NEW.provider_id IS NOT NULL) THEN
					INSERT INTO provider_stats
						(id, article_count)
					VALUES
						(NEW.provider_id, 1)
					ON CONFLICT (id) DO UPDATE SET
						article_count = article_count + 1;
				END IF;
			END IF;

			RETURN NEW;
		END;
	$$ LANGUAGE PLPGSQL;

--
-- Access permissions
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE articles TO chooseit;

--
-- Triggers
--

CREATE TRIGGER articles_before_upsert_trg
	BEFORE INSERT OR UPDATE ON articles
	FOR EACH ROW EXECUTE PROCEDURE before_entity_upsert();

CREATE TRIGGER articles_before_insert_trg
	BEFORE INSERT ON articles
	FOR EACH ROW EXECUTE PROCEDURE before_content_insert();

CREATE TRIGGER articles_after_upsert_or_delete_trg
	AFTER INSERT OR UPDATE OR DELETE ON articles
	FOR EACH ROW EXECUTE PROCEDURE after_article_upsert_or_delete();

CREATE TRIGGER articles_after_delete_trg
	AFTER DELETE ON articles
	FOR EACH ROW EXECUTE PROCEDURE after_content_delete();
