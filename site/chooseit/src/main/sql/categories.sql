--
-- Tables and sequences
--

CREATE SEQUENCE category_id;

CREATE TABLE categories (
	id          IDENTIFIER NOT NULL DEFAULT NEXTVAL('category_id'),
	parent_id   IDENTIFIER,
	children    INTEGER NOT NULL,
	name        TEXT NOT NULL,
	description TEXT,

	--
	-- Primary key
	--

	CONSTRAINT categories_pkey
		PRIMARY KEY (id),

	--
	-- Foreign keys
	--

	CONSTRAINT categories_parent_id_fkey
		FOREIGN KEY (parent_id) REFERENCES categories (id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
);

ALTER SEQUENCE category_id OWNED BY categories.id;

--
-- Unique indexes
--

CREATE UNIQUE INDEX categories_name_unq1
	ON categories (name)
	WHERE parent_id IS NULL;

CREATE UNIQUE INDEX
	categories_name_unq2
	ON categories (parent_id, name)
	WHERE parent_id IS NOT NULL;

--
-- Foreign key indexes
--

CREATE INDEX categories_parent_id_fki
	ON categories (parent_id);

--
-- Comments
--

COMMENT ON TABLE categories IS 'Article categories';
COMMENT ON SEQUENCE category_id IS 'Article category identifiers';

COMMENT ON COLUMN categories.id          IS 'Unique identifier';
COMMENT ON COLUMN categories.parent_id   IS 'Parent category identifier';
COMMENT ON COLUMN categories.children    IS 'Children count';
COMMENT ON COLUMN categories.name        IS 'Category name';
COMMENT ON COLUMN categories.description IS 'Short description';

--
-- Access permissions
--

GRANT USAGE ON SEQUENCE category_id TO chooseit;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE categories TO chooseit;

--
-- Triggers
--

CREATE TRIGGER categories_before_upsert_trg
	BEFORE INSERT OR UPDATE ON categories
	FOR EACH ROW EXECUTE PROCEDURE before_tree_upsert();
